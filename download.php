<?php
$filename = $_GET['file'];

// Make sure file is in assets/gallery
if (substr(dirname($_GET['file']), 0, 14) != 'assets/gallery') {
    exit;
} 

header("Pragma: public"); // required
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); // required for certain browsers 
header("Content-Type: image/jpg");
header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filename));
readfile("$filename");
exit(1);
?>