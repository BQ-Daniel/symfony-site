Event.observe(window, 'load', function() {
	
	var i = 0;
	$$('.tab-bar .tab').each(function(item) {
		var tab = item;
		i++;
		
		// Add selected class to first tab
		if (i == 1) {
			tab.addClassName('selected');
		}

		tab.observe('click', function (event) {
			if (!tab.hasClassName('selected')) {
				
				$$('.tab-bar .tab.selected').each(function(item) {
					item.removeClassName('selected');
				});
				tab.addClassName('selected');
			
				var slideId = tab.id.replace(/tab/, 'slide');
				var position = $(slideId).positionedOffset();
				
				$$('.scroll').each(function(item) {
					item.style.overflow = 'hidden';
				});

				var slideElement = $(slideId);
				var scrolls = slideElement.getElementsByClassName('scroll');
				var scroll = scrolls[0];
				
				new Effect.Move($('slide-wrapper'), { x: -position.left, y: 0, mode: 'absolute'});

				setTimeout(function () { 
					scroll.setStyle({'overflowY': 'scroll'});
				}, 1500);
			
			}
		});

	});
});
