function changeRequest(num)
{
	for(i=num;i<=4;i++) {
		$('display' + i).innerHTML = '';
	}
    
    $('step').value = num;

    new Ajax.Request('dropDownDisplay.php', {
      method: 'post',
      evalScripts: true,
      parameters: $('form1').serialize(),
      onSuccess: function(transport) {
        $('loading' + num).style.display = 'none';
        $('display' + num).update(transport.responseText);
      },
      onLoading: function() {
        $('loading' + num).style.display = 'block';
      }
    });
}


function viewProduct()
{
    new Ajax.Request('viewProduct.php', {
      method: 'post',
      evalScripts: true,
      parameters: $('form1').serialize(),
      onSuccess: function(transport) {
        $('loading').style.display = 'none';
        $('product').update(transport.responseText);
      },
      onLoading: function() {
        $('loading').style.display = 'block';
        $('product').update('');
      }
    });
}

function viewProductId(id)
{
	new Ajax.Updater('product', 'viewProduct.php', { method: 'post', parameters: { post: 'id=' + id }, onLoading: function () { $('product').innerHTML = ''; $('loading').show(); }, onSuccess: function() { $('loading').hide(); } } )
}

function submitForm(page, form)
{
	var form = (form == null) ? "form" : form;
	var div = form + 'Result'; 
	new Ajax.Updater(div, page, { parameters: $(form).serialize(true), evalScripts:true} );
}