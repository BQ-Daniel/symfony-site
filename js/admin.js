function removeAllOptions(selectbox)
{
	var i;
	selectbox = $(selectbox);
	for(i=selectbox.options.length-1;i>=0;i--)
	{
		selectbox.remove(i);
	}
}

function AddSelectOption(selectObj, text, value, isSelected) 
{
    if (selectObj != null && selectObj.options != null)
    {
        selectObj.options[selectObj.options.length] = new Option(text, value, false, isSelected);
    }
}

function loadCategories(category)
{
	if (category == 'MODEL')
	{
		div = 'productModel';
		removeAllOptions(div);
		if ($('productMake').value != '') {
			removeAllOptions('productSubModel');
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MAKE: $('productMake').value },
					onSuccess: function(transport) { 
						if (transport.responseText != '') { 
							loadOptions(div, transport.responseText);
						}
					}
				}
			);
		}
	}

	if (category == 'SUB_MODEL')
	{
		div = 'productSubModel';
		removeAllOptions(div);
		if ($('productModel').value != '') {
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MODEL: $('productModel').value },
					onSuccess: function(transport) { 
						if (transport.responseText != '') { 
							loadOptions(div, transport.responseText);
						}
					}
				}
			);
		}
	}
}

function loadCats(category)
{
	if (category == 'MAKE')
	{
		div = 'categoryMake';
		removeAllOptions(div);
		new Ajax.Request('categoryOptions.php?' + Math.floor(Math.random()*50000), { method: 'post', parameters: { submit: 'submit' }, 
				onSuccess: function(transport) { 
					if (transport.responseText != '')
					{ 
						$('MODEL').hide();
						$('SUB_MODEL').hide();
						loadOptions(div, transport.responseText);
					}
				}
			}
		);
	}
	else if (category == 'MODEL')
	{
		if ($('categoryMake').value != '')
		{
			// List products for make
			listProducts('MAKE');

			div = 'categoryModel';
			removeAllOptions(div);
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MAKE: $('categoryMake').value },
					onSuccess: function(transport) { 
						$('deleteMake').show();
						if (transport.responseText != '')
						{ 
							loadOptions(div, transport.responseText);
							$('MODEL').show();
							$('SUB_MODEL').hide();
						} else {
							$('MODEL').hide();
						}
						$('MODEL').show();
					}
				}
			);
		}
	}
	else if (category == 'SUB_MODEL')
	{
		if ($('categoryModel').value != '')
		{
			// List products for model
			listProducts('MODEL');

			div = 'categorySubModel';
			removeAllOptions(div);
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MODEL: $('categoryModel').value },
					onSuccess: function(transport) { 
						$('deleteModel').show();
						if (transport.responseText != '')
						{ 
							loadOptions(div, transport.responseText);
						}
						else {
							$('SUB_MODEL').hide();
						}
						$('SUB_MODEL').show();
					}
				}
			);
		}
	}
}

function loadOptions(div, str)
{
	options = str.split(',');
	for (i=0; i<options.length; i++) {
		option = options[i].split(':');
		value = option[0];
		name = option[1].replace(/^\s+|\s+$/g,"");
		name = name.replace(/,/g" ");
		AddSelectOption($(div), name, value, false);		
	}
}


function addCategory(category)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('makeResult', 'manCategory.php', { method: 'post', parameters: { make: $('newMake').value } } )
		loadCats(category);

	}
	else if (category == 'MODEL')
	{
		new Ajax.Updater('modelResult', 'manCategory.php', { method: 'post', parameters: { model: $('newModel').value, editMake: $('categoryMake').value } } )
		loadCats(category);
	}
	else if (category == 'SUB_MODEL')
	{
		new Ajax.Updater('subModelResult', 'manCategory.php', { method: 'post', parameters: { subModel: $('newSubModel').value, editMake: $('categoryMake').value, editModel: $('categoryModel').value } } )
		loadCats(category);
	}
}

function deleteCategory(category)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('makeResult', 'manCategory.php', { method: 'post', parameters: { deleteMake: $('categoryMake').value } } )
		loadCats(category);
		$('modelResult').innerHTML = '';
		$('subModelResult').innerHTML = '';
	}

	if (category == 'MODEL')
	{
		new Ajax.Updater('modelResult', 'manCategory.php', { method: 'post', parameters: { deleteModel: $('categoryModel').value } } )
		loadCats(category);
		$('subModelResult').innerHTML = '';
		$('SUB_MODEL').hide();
	}
}

function listProducts(category)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('content', 'listProducts.php', { method: 'post', parameters: { make: $('categoryMake').value } } )
	}
	if (category == 'MODEL')
	{
		new Ajax.Updater('content', 'listProducts.php', { method: 'post', parameters: { make: $('categoryMake').value, model: $('categoryModel').value } } )
	}
	if (category == 'SUB_MODEL')
	{
		new Ajax.Updater('content', 'listProducts.php', { method: 'post', parameters: { model: $('categoryModel').value,submodel: $('categorySubModel').value } } )
	}
}

function submitProduct()
{
	new Ajax.Updater('content', 'addProduct.php', { parameters: $('addProduct').serialize(true), asynchronous:true, evalScripts:true} );
}

function viewProduct(id)
{
	new Ajax.Updater('content', 'addProduct.php?PRODUCT_ID=' + id );
}

function submitForm(page, form, div)
{
	var div = (div == null) ? "content" : div;
	var form = (form == null) ? "form" : form;
	new Ajax.Updater(div, page, { parameters: $(form).serialize(true), onLoading:function() { $('loadingContent').show(); $(div).hide() }, onSuccess:function() { $('loadingContent').hide(); $(div).show(); }, asynchronous:true, evalScripts:true} );
}

function showPage(page, div)
{
	var div = (div == null) ? "content" : div;
	new Ajax.Updater(div, page, { evalScripts: true });
}