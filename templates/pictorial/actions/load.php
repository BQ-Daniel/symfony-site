<?php
// Auth user
auth('redirect');
//echo dirname(__FILE__);
require_once (dirname(__FILE__) . '/../conf.php');

$mysqli = new mysqli(DB_CATALOG_HOST, DB_CATALOG_USER, DB_CATALOG_PASS,DB_CATALOG_NAME,
    DB_CATALOG_PORT);

if (isset($_GET['p']) || isset($_POST)) {
    $page = isset($_GET['p']) ? $_GET['p'] : '';
    if ($page == 'fitStyle') {
        $fitType = $_POST['fitType_id'];
        echo "<option selected disabled>Select Style</option>";
        $stmt = $mysqli->query("select * from fitStyle where fitType_id='$fitType'");
        while ($row = $stmt->fetch_assoc()) {
            ?>
            <option value="<?php echo $row['fitStyle_id'] ?>"><?php echo $row['fitStyle_name'] ?></option>
            <?php
        }
    } else if ($page == 'fitCategory') {
        $fitStyle = $_POST['fitStyle_id'];
        echo "<option selected disabled>Select Category</option>";
        $stmt = $mysqli->query("select * from fitCategory where fitStyle_id='$fitStyle'");
        while ($row = $stmt->fetch_assoc()) {
            ?>
            <option value="<?php echo $row['fitCategory_id'] ?>"><?php echo $row['fitCategory_name'] ?></option>
            <?php
        }
    } else if ($page == 'fitType') {
        echo "<option selected disabled>Select Type</option>";
        $stmt = $mysqli->query("select * from fitType");
        while ($row = $stmt->fetch_assoc()) {
            ?>
            <option value="<?php echo $row['fitType_id'] ?>"><?php echo $row['fitType_name'] ?></option>
            <?php
        }
    }
}
?>
