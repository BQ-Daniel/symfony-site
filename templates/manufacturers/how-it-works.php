<?php $this->extend($extendDir.'layout2'); ?>
<?php $this->set('title', 'How it works, Become a Manufacturer') ?>
<?php $this->set('metaDescription', 'How it works, Become a Manufacturer') ?>

<?php $this->start('subnav') ?>
    <?php require_once _DIR_ROOT.'/templates/manufacturers/subnav.php' ?>
<?php $this->stop() ?>

<h2>Become a Brake Hose Manufacturer - how it works</h2>

<script type='text/javascript' src='/assets/js/jwplayer.js'></script>

<div style="margin:0; width:100%;">
<iframe width="100%" height="480" src="https://www.youtube.com/embed/jgzuRGUtvOA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>
