<?php $this->extend($extendDir.'layout'); ?>
<?php $this->set('title', 'BQ1006 short video, Become a Manufacturer') ?>
<?php $this->set('metaDescription', 'BQ1006 short video, Become a Manufacturer') ?>

<?php $this->start('subnav') ?>
    <?php require_once _DIR_ROOT.'/templates/manufacturers/subnav.php' ?>
<?php $this->stop() ?>

<h2>Become a Brake Hose Manufacturer - BQ1006 short video</h2>

<script type='text/javascript' src='/assets/js/jwplayer.js'></script>

<div style="margin:0; width:600px;">
<iframe width="853" height="480" src="https://www.youtube.com/embed/hmJaVwkgy-8?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>