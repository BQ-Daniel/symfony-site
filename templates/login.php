<?php $this->extend($extendDir.'layoutapps'); ?>

<?php if ($user): ?>
<script language="JavaScript">
	// Clear cookies
	eraseCookie('ordered_by');
    eraseCookie('order_no');
	eraseCookie('shipping');
	eraseCookie('shipping_other');
</script>
<meta http-equiv="Refresh" content="0; url=/applications" />

<h2>Login Successful - Please Wait</h2>
You are now being redirected to the BrakeQuips Online Applications.

<?php else: ?>

<h2>Woops</h2>
Your access link to the catalogue may have expired or may not be the most recent email. <a href="../">Click here</a> to try again.

<?php endif; ?>
