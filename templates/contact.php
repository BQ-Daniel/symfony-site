<?php $this->extend($extendDir.'layout2') ?>
<?php $this->set('title', 'Contact BrakeQuip') ?>
<?php $this->set('metaDescription', 'Contact BrakeQuip') ?>
<script src="https://www.google.com/recaptcha/api.js?render=6Lf135MUAAAAAOWcTF7sPDBFUihjgihCfjgBSzu9"></script>
 <script>
 grecaptcha.ready(function() {
     grecaptcha.execute('6Lf135MUAAAAAOWcTF7sPDBFUihjgihCfjgBSzu9', {action: 'homepage'}).then(function(token) {
        ...
     });
 });
 </script>
    <style>
    #map {
    width: 100%;
    height: 250px;
    margin-bottom: 30px;
}</style>

<h2>Contact BrakeQuip</h2>

   <div id="map">
   <script type="application/ld+json">{ 
"@context": "https://schema.org", 
"@type": "Organization", 
"name": "BrakeQuip Australia", 
"hasMap": "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3144.2756616831653!2d145.10862941617253!3d-37.99403017972016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad66d1e120e94dd%3A0xe13433edc147dfe8!2sBrakeQuip%20Australia!5e0!3m2!1sen!2sau!4v1588550566264!5m2!1sen!2sau"
}
</script>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3144.2756616831653!2d145.10862941617253!3d-37.99403017972016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad66d1e120e94dd%3A0xe13433edc147dfe8!2sBrakeQuip%20Australia!5e0!3m2!1sen!2sau!4v1588550566264!5m2!1sen!2sau" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap"></script>

<div id="contact-enquiry-bar">
	    <img src="http://www.brakequip.com.au/assets/images/BQ1005.left.png" class="contact-machine">
	    <span>
	    	<p style="font-weight: bold; text-transform: uppercase;">Interested in becoming a Brake Hose Manufacturer?</p>
	    	<p>Register your interest now.</p>
	    <a href="enquiry" class="btn red">REGISTER</a></span></div>



      <?php
      // if the url field is empty

      // otherwise, let the spammer think that they got their message through
      ?>


      <?
      // If the submit button has been hit, send mail
      if (isset($_POST['submit']))
      {

        if(isset($_POST['url']) && $_POST['url'] == ''){


          // Check required fields arnt empty
          $requiredFields = array( 'name', 'bname', 'phone', 'email', 'enquiry');
          $error = false;
          foreach ($requiredFields as $field)
          {
              if (isset($_POST[$field]) && empty($_POST[$field])) {
                  $error = "Please make sure the required fields are not empty.<br /><br />";
                  break;
              }
              if (strpos($_POST['enquiry'], "http") !== false){
              $error = "<p style=text-align:center;>BrakeQuip thanks you for not spamming us.</p><br /><br />";
              break;
            }
          }


          // If no error, send mail
          if (!$error) {
      			$headers="From: <$_POST[email]>\r\n";
      			$headers.="MIME-Version: 1.0\r\n";
      			$headers.="Content-Type: text/html; charset=ISO-8859-1\r\n";
      			$body="
                      <table border=\"0\" cellpadding=\"6\" cellspacing=\"0\">
                        <tr>
                          <td colspan=\"2\">
                          <i><b>Personal Information</b></i>
                          </td>
                        </tr>
                        <tr>
                          <td style='width: 130px;'>Name:</td>
                          <td>$_POST[name]</td>
                        </tr>
                        <tr>
                          <td>Business Name:</td>
                          <td>$_POST[bname]<br /></td>
                        </tr>
                        <tr>
                          <td>Phone number:</td>
                          <td> $_POST[phone]</td>
                        </tr>
                        <tr>
                          <td>Email:</td>
                          <td> $_POST[email]</td>
                        </tr>
    				            <tr>
                          <td>General Enquiry:</td>
                          <td> $_POST[enquiry]</td>
                        </tr>
                      </table>";

                      mail( 'daniel@brakequip.com.au', 'Contact Form', $body,$headers );

                      $formComplete = true;
          }
        }
        // then send the form to your email
        }

      ?>
      <?php if (isset($formComplete)): ?>
      <h2>Thank-you</h2>Your submission has been received. If required, we will be in contact with you shortly.<br />
      <?php else: ?>
      <?php if (isset($error)): ?>
      <font color=red><?php echo $error ?></font>
      <?php endif; ?>


<div class="row justify-content-around">
  <div class="col-md-5" style="background:#f8f8f8;padding-top: 15px;">
  	<h2>GET IN CONTACT</h2>

      <strong>Address:</strong><br>
      40-42 Mills Road,<br>
      Braeside Vic 3195 <br>
      Australia<br>
      <br>
      <span><strong>Ph:</strong> 03 8586 2500</span><br>
      <br>
      <strong>Hours</strong><br>
      Monday - Friday | 8am - 5pm
   </div>

<div class="col-md-5" style="padding-top: 15px;">
<h2>EMAIL</h2>
<form method="post">
  <div class="form-group">
    <label for="formGroupExampleInput">Name</label>
    <input type="text" name="name" class="form-control" id="formContact" placeholder="" value="<?php echo @$_POST['name'] ?>">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Business Name</label>
    <input type="text" name="bname" class="form-control" id="formContact" placeholder="" value="<?php echo @$_POST['bname'] ?>">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Phone No.</label>
    <input type="text" name="phone" class="form-control" id="formContact" placeholder="" value="<?php echo @$_POST['phone'] ?>">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Email</label>
    <input type="text" name="email" class="form-control" id="formContact" placeholder="" value="<?php echo @$_POST['email'] ?>">
    <p style="display:none;" class="antispam">Leave this empty: <input type="text" name="url" /></p>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Message</label>
    <textarea type="text" name="enquiry" class="form-control" id="formContact" placeholder="" value="<?php echo @$_POST['enquiry'] ?>"></textarea>
  </div>
  <button type="submit" class="btn red" name="submit" value="SEND" />Send</button>
</form>

</div>

 </div>

<?php endif; ?>


<script>
$(function() {
    $('#contact-select').change(function() {
        var option = $(this).find('option:selected');
        $('#dvd').toggle(option.hasClass('dvd'));
        $('#general').toggle(option.hasClass('general'));
    }).change();
});
</script>
