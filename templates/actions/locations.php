<?php

$q = new Doctrine_RawSql();
$q->select('{ua.*}, {u.*}')
    ->from('USER_ADDRESS ua INNER JOIN USERS u ON ua.user_id = u.id INNER JOIN price_code pc ON pc.price_code = u.PRICE_CODE')
    ->addComponent('ua', 'UserAddress ua')
    ->addComponent('u', 'ua.Users u')
    ->where("ua.address_type = 'delivery' AND lat != '0.000000' AND lat IS NOT NULL AND u.deleted = 0 AND pc.search_access = 1 AND u.distributor = 1");

$results = $q->execute(array());
$results = $results->toArray();

$json = array();
foreach ($results as $result)
{
    if (empty($result['address1'])) {
        continue;
    }
    $row = array(
        'id' => $result['id'],
        'name' => stripcslashes($result['name']),
        'address' => stripcslashes($result['address1']),
        'address2' => stripcslashes($result['address2']),
        'city' => stripcslashes($result['suburb']),
        'state' => stripcslashes($result['state']),
        'postal' => stripcslashes($result['postcode']),
        'phone' => $result['phone'],
        'lat' => $result['lat'],
        'lng' => $result['lng'],
        'website' => $result['website']
    );
    $json[] = $row;
}

    print json_encode($json);
exit;

?>