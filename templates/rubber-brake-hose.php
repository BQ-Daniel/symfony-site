<?php $this->extend($extendDir.'layout2'); ?>
<?php $this->set('title', 'Rubber brake hose') ?>
<?php $this->set('metaDescription', 'Rubber brake hoses') ?>

<div class="row">
  <div class="col-md-8">
    <h2>Rubber Brake Hose</h2>

    <p>Old-Fashioned 'rubber' brake hoses traditionally fitted to cars and motorbikes have a relatively short reliable life.</p>

    <p>The estimated life of a typical 'rubber' brake hose is 6 years, then, they can become safety hazards. Imagine brake hoses as arteries in the human body. A person could live a normal life with hard and restricted arteries, but it's when they exert their heart to some stress that symptoms arise…the same applies to brake hoses.</p>

    <p>Brake hoses could appear OK but it's when they are subjected to extreme pressures in an 'emergency stop' is when they need to be in good condition to handle this stress.</p>

    <p>Most braking systems with boosted assisted brakes obtain pressures of approximately 1500psi.</p>
</div>
<div class="col-md-4">
    <img src="/assets/images/rubberHoses.jpg" alt="Rubber Brake Hoses" title="Rubber Brake Hoses" class="img-fluid"/>
</div>
</div>
