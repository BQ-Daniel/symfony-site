<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php echo ($this->get('metaDescription') ? $this->get('metaDescription') . ' ' : '') ?>Brake Hose Manufacturing Systems. Stainless steel and rubber brake hose manufacturing systems for all vehicles." />
        <meta name="keywords" content="<?php echo ($this->get('metaKeywords') ? $this->get('metaKeywords') . ', ' : '') ?>brake hose, brake hose Manufacturing, rubber hoses, braided hoses, brake, brakequip, australia" />
        <title>View Product, BrakeQuip Australia</title>

        <meta name="language" content="English" />
        <meta name="google-site-verification" content="KkT2BTpaOl_J6FYcNyV_OrhiOT_zMb5OWDWO1bYTZig" />

        <script language="javascript" type="text/javascript" src="/assets/js/jquery.js?v<?php echo time() ?>"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/global.js?v<?php echo time() ?>"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/jquery-ui.js?v1"></script>

        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/screen.css?0.3" charset="utf-8" />
        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/blitzer/jquery-ui.css" charset="utf-8" />
<!--        <script src="//load.sumome.com/" data-sumo-site-id="1f87a8df884db6d6a7c2bd0846db6f6b82414d85d16cda67a83daff931d3b854" async="async"></script>-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		<meta name="google-site-verification" content="KkT2BTpaOl_J6FYcNyV_OrhiOT_zMb5OWDWO1bYTZig" />
    </head>

    <body>
        <div id="container">
            <div id="topNav">
                <h1 style="color: #ffffff; font-weight: 900;">
                HOLA
                </h1>
            </div>
            <div id="innerCotainer">
                <div id="mainTop" class="mod">
                    <a href="/"><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia" width="225px"/></a>
                    <?php if (!$this->output('subnav')): ?>
                    <?php endif; ?>
                    <div class="cb"></div>
                </div>

                <div id="content" class="mod">
                    <?php echo $this->get('content') ?>
                    <div class="cb"></div>
                </div>
            </div>
        </div>
    </body>
</html>
