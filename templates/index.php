<?php $this->extend($extendDir . 'layoutHome'); ?>

<div class="container">
  <div class="row">
    <div class="col-md-12" id="findmanu">
      <h1 id="finding">
        Finding replacement Brake Hoses is just a click away
      </h1>
      <h2>
        <a href="/find-manufacturer">Click here to find your local manufacturer</a>
      </h2><br>
      <a href="/find-manufacturer"><img src="/assets/images/4hoseTypes.png" alt="Find replacement Brake Hoses" title="Find replacement Brake Hoses" class="img-fluid" style="-webkit-transform: rotate(-7deg);margin: -50px 0 0;"></a>
    </div>
    <div class="col-md-6 pr-md-5 pl-md-3" id="manufactrerinfo">
      <span style="background-color: #f8f8f8;display: block;max-height: 270px;margin: 20px 0 40px;padding-bottom: 50px;">
        <a href="/assets/images/BQ1005-web.png" target="_blank">
          <img src="/assets/images/BQ1005-web.png" alt="Brake Hose Machine, BrakeQuip Australia" title="Brake Hose Machine, BrakeQuip Australia" class="img-fluid" style="margin: -10px 5px;">
        </a>
      </span>
      <h2>Become a Brake Hose Manufacturer</h2>
      <p style="padding: 0px 0px 40px;">Brakequip's revolutionary Brake Hose Manufacturing System can manufacture and test any hydraulic brake or clutch hose for any type of vehicle, and all to international standards - usually while your customer
        waits!
        <br>
        <br>
        Making your own Brake Hoses (brake lines / braided lines) takes the hassle out of waiting for hoses to be delivered and gives you the opportunity to take control of your business.
      </p>
      <div class="list-group list-group-flush col-md-6">
        <a href="/manufacturers/" class="list-group-item list-group-item-action">More information</a>
        <a href="/manufacturers/faq" class="list-group-item list-group-item-action">FAQ</a>
        <a href="/manufacturers/how-it-works" class="list-group-item list-group-item-action">Video</a>
      </div>
    </div>

    <div class="col-md-6 pl-md-5 pr-md-3" id="partfinder">
      <span style="background-color: #f8f8f8;display: block;">
        <a href="/BQAF05/">
          <img src="../assets/images/BQAF05_cover.png" style="max-height: 273px; margin: 20px auto 40px; padding: 20px 0 10px; display: block;" class="img-fluid">
        </a>
      </span>
      <h2>Part Finder</h2>
      <p>In addition to carrying BrakeQuip branded high quality brake plumbing tools, we proudly offer Australia's largest range of Tube Nuts, Banjo Bolts, Copper Washers and more.
      </p>
      <div class="list-group list-group-flush col-md-6">
        <a href="/BQAF05/" class="list-group-item list-group-item-action">View range</a>
        <a href="/find-manufacturer/" class="list-group-item list-group-item-action">Where to buy</a>
      </div>
    </div>
  </div>
</div>
