<?php $this->extend($extendDir . 'layoutapps'); ?>
<?php require_once ('conf.php') ?>
<?php $render_css = PIC_CATALOG_URL . "assets/pictorial/css/style.css"; ?>
<?php $render_js_jquery = PIC_CATALOG_URL . "assets/pictorial/js/jquery.min.js"; ?>
<?php $render_js_boots = PIC_CATALOG_URL . "assets/pictorial/js/bootstrap.min.js"; ?>
<?php $render_js_prettyPhoto = PIC_CATALOG_URL . "assets/pictorial/js/bootstrap.min.js"; ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800" rel="stylesheet">
<link rel="stylesheet" property="stylesheet" href="<?php echo PIC_CATALOG_URL . "assets/pictorial/css/style.css"; ?>">
<link rel="stylesheet" property="stylesheet" href="<?php echo PIC_CATALOG_URL . 'assets/pictorial/lightbox2/dist/css/lightbox.css'; ?>">

<script src="<?php echo $render_js_jquery; ?>"></script>
<script src="<?php echo $render_js_boots; ?>"></script>
<script src="<?php echo $render_js_prettyPhoto; ?>"></script>
<script src="<?php echo PIC_CATALOG_URL . 'assets/pictorial/lightbox2/dist/js/lightbox.js'; ?>"></script>



<div id="cat" class="container" style="mafitStylein-top:35px">
    <form>
        <div class="form-group">
            <select class="form-control" id="fitType" name="fitType"></select>
        </div>
        <div class="form-group">
            <select class="form-control" id="fitStyle" name="fitStyle"></select>
        </div>
        <!--<div class="loading-style" id="loading-style"><img src="https://www.brakequip.com.au/assets/pictorial/images/Preloader_11.gif" class="fitting-img"></div>-->
        <div class="form-group">
            <select class="form-control" id="fitCategory" name="fitCategory" onchange=""></select>
        </div>
       <!-- <div class="loading-category" id="loading-category"><img src="https://www.brakequip.com.au/assets/pictorial/images/Preloader_11.gif" class="fitting-img"></div>-->
    </form>
    <div class="row" id="show_fitData">
    </div>
    <div class="loading-image" id="loading-image"><img src="https://www.brakequip.com.au/assets/pictorial/images/Preloader_11.gif" class="fitting-img"></div>
</div>

    <script>
    var loaded = 0;
    $('#loading-image').hide();
    $(document).ready(function(){
         $('#fitCategory').change(function(){
              var fitCategory_id = $(this).val();
              $.ajax({
                    type: "POST",
                    url: 'fitCategory_id?p=fitCategory_id',
                    data: {fitCategoryID:fitCategory_id},
                    beforeSend: function() {
                        $("#loading-image").show();
                    },
                    success: function(response){
                           $('#show_fitData').show();
                           $('#show_fitData').html(response);
                           $("#loading-image").hide();
                           loaded ++;
                    },
                    dataType: "html"
             })
        })
    })


    $(document).ready(function(){
        $('#fitStyle').hide();
        $('#fitCategory').hide();
        $.get('data.php?p=fitType',function(fitType){
            $('#fitType').html(fitType);
            $("#loading-image").hide();
            //$('#loading-category').hide();
            $('#show_fitData').hide();
        })
        $('#fitType').change(function(){
            $('#fitStyle').hide();
            var fitType = $(this).val();
            $('#show_fitData').hide();
            $.ajax({
                type: "POST",
                url: 'fitStyle.php?p=fitStyle',
                data: {fitType_id:fitType},
                beforeSend: function() {
                    $("#loading-image").show();
                    $('#fitStyle').hide();
                    $('#fitCategory').hide();
                },
                success: function(response){
                    $("#loading-image").hide();
                    $('#fitStyle').show();
                    $('#fitStyle').html(response);
                },
                dataType: "html"
            })
        })
        $('#fitStyle').change(function(){
            $('#fitCategory').hide();
            $('#show_fitData').hide();
            var fitStyle = $(this).val();
            $.ajax({
                type: "POST",
                url: 'fitCategory.php?p=fitCategory',
                data: {fitStyle_id:fitStyle},
                beforeSend: function() {
                    $("#loading-image").show();
                    $('#fitCategory').hide();
                },
                success: function(response){
                    $("#loading-image").hide();
                    $('#fitCategory').show();
                    $('#fitCategory').html(response);
                },
                dataType: "html"
            })
        })
     })
    </script>
