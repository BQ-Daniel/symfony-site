-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2015 at 06:26 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+10:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `PartSearch`
--
--
-- Table structure for table `fitType`
--

CREATE TABLE IF NOT EXISTS `fitData` (
  `fitData_id` int(11) NOT NULL,
  `fitData_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fitCategory_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB AUTO_INCREMENT=1652 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fitData`
--

INSERT INTO `fitData` (`fitData_id`, `fitData_name`, `fitCategory_id`, `status`) VALUES
(10001, 'HF102', 1002, 1),
(10002, 'HF09', 1002, 1),
(10003, 'HF09W', 1002, 1),
(10004, 'SS09W', 1002, 1),
(10005, 'HF10', 1002, 1),
(10006, 'SS10', 1002, 1),
(10007, 'HF11', 1002, 1),
(10008, 'SS11', 1002, 1);

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for table `fitCategory`
--

CREATE TABLE IF NOT EXISTS `fitCategory` (
  `fitCategory_id` int(11) NOT NULL,
  `fitCategory_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fitStyle_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB AUTO_INCREMENT=6178 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fitCategory`
--

INSERT INTO `fitCategory` (`fitCategory_id`, `fitCategory_name`, `fitStyle_id`, `status`) VALUES
(1001, 'Male Swivel', 101, 1),
(1002, 'Female Swivel', 101, 1),
(1003, 'Centre Joiner', 101, 1),
(1004, 'Lifesaver', 101, 1),
(1005, 'Universal', 101, 1),
(1006, 'Compression', 101, 1),
(1007, 'Clutch', 101, 1),

(1008, 'Straight', 102, 1),
(1009, 'Straight - Side Specific', 102, 1),
(1010, 'Bent', 102, 1),
(1011, 'Bent - Side Specific', 102, 1),
(1012, 'Block Style', 102, 1),
(1013, 'Universal', 102, 1),

(1014, 'Clip Mount', 103, 1),
(1015, 'Bracket Mount', 103, 1),
(1016, 'Floating Centre', 103, 1),

(1017, 'Clip Mount', 104, 1),
(1018, 'Bolt on Mount', 104, 1),
(1019, 'No Mount', 104, 1),
(1020, 'Bracket Mount', 104, 1),
(1021, 'Bulkhead Mount', 104, 1),

(1022, 'Standard Mount', 105, 1),
(1023, 'Clip Mount', 105, 1),
(1024, 'Bulkhead Mount', 105, 1),

(1025, 'Clip Mount', 106, 1),
(1026, 'No Mount', 106, 1),
(1027, 'Spline Mount', 106, 1),
(1028, 'Bracket Mount', 106, 1),
(1029, 'Bracket Mount - No Groove', 106, 1),
(1030, 'Bolt on Mount', 106, 1),
(1031, 'Bulkhead Mount', 106, 1),

(1032, 'Standard Mount', 107, 1),
(1033, 'Bulkhead Mount', 107, 1),
(1034, 'Clip Mount', 107, 1),
(1035, 'Spline Mount', 107, 1),

(2001, 'Male Swivel', 201, 1),
(2002, 'Female Swivel', 201, 1),
(2003, 'Crimp Shell', 201, 1),
(2004, 'Lifesaver', 201, 1),
(2005, 'Universal', 201, 1),
(2006, 'Clutch', 201, 1),

(2007, 'Bent', 202, 1),
(2008, 'Block Style', 202, 1),
(2009, 'Straight', 202, 1),

(2010, 'Clip Mount', 203, 1),
(2011, 'Bracket Mount', 203, 1),
(2012, 'Centre Joiner', 203, 1),

(2013, 'Clip Mount', 204, 1),
(2014, 'No Mount', 204, 1),
(2015, 'Bulkhead Mount', 204, 1),

(2016, 'Bulkhead Mount', 205, 1),
(2017, 'Clip Mount', 205, 1),
(2018, 'Standard Mount', 205, 1),

(2019, 'Bolt on Mount', 206, 1),
(2020, 'Clip Mount', 206, 1),
(2021, 'No Mount', 206, 1),

(2022, 'Bulkhead Mount', 207, 1),
(2023, 'Standard Mount', 207, 1),

(2024, 'Female Stem/Seat', 208, 1),
(2025, 'Tube Stem/Seat', 208, 1),
(2026, 'Tube Stem Tube Nuts', 208, 1),

(3001, 'Male Swivel', 301, 1),
(3002, 'Female', 302, 1),
(3003, 'Compression', 303, 1),
(3004, 'Banjo', 304, 1),
(3005, 'Centre Fitting', 305, 1),
(3006, 'Metric Male', 306, 1),
(3007, 'Braze On Fittings', 307, 1),
(3008, 'Components', 308, 1),
(3009, 'Hose', 309, 1),
(3010, 'Crimping Tools', 310, 1),

(4001, 'Australian', 401, 1),
(4002, 'Specialty Mount', 402, 1),
(4003, 'General', 403, 1),

(5001, 'Centre Rubber Hose', 501, 1),
(5002, 'Centre Braided Hose', 502, 1),

(6001, 'Clips', 601, 1),

(7001, 'Banjo Adaptors', 701, 1),
(7002, 'Banjo Bolts', 701, 1),
(7003, 'Banjo Bolts - Double', 701, 1),
(7004, 'Banjo Heads - Weld on', 701, 1),
(7005, 'Banjo Unions', 701, 1),
(7006, 'Bleed Screws', 701, 1),
(7007, 'Bleed Screw Caps', 701, 1),
(7008, 'Bulkhead Nuts', 701, 1),
(7009, 'Copper Washers', 701, 1),
(7010, 'Fitting Plugs', 701, 1),
(7011, 'Joiners - Female', 701, 1),
(7012, 'Joiners - Male', 701, 1),
(7013, 'Power Steering Adaptors', 701, 1),
(7014, 'Thread Adaptors - Male-Female', 701, 1),
(7015, 'Tube Nuts - Female', 701, 1),
(7016, 'Tube Nuts - Male', 701, 1),
(7017, 'T-Pieces', 701, 1),
(7018, 'Specialty Joiners and T-Pieces', 701, 1),

(7019, 'Rubber Hose - Accessories', 702, 1),
(7020, 'Braided Hose - Accessories', 702, 1),

(7021, 'Tube - Cutters', 703, 1),

(7022, 'Flaring Tools', 704, 1),
(7023, 'Flaring Tool Spares', 704, 1),
(7024, 'Handheld Tools', 704, 1),

(7025, 'Brochures - Poster', 705, 1),

(7026, 'Test Cones - Adaptors', 706, 1),
(7027, 'Manufacturing Accessories', 706, 1),

(7028, 'Machine Maintenance', 707, 1),

(7029, 'BE-04 Basic Spares', 708, 1),
(7030, 'Machine Spares', 708, 1),
(7031, 'BQ350 Previous Model', 708, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fitType`
--

CREATE TABLE IF NOT EXISTS `fitType` (
  `fitType_id` int(11) NOT NULL,
  `fitType_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fitType`
--

INSERT INTO `fitType` (`fitType_id`, `fitType_name`, `status`) VALUES
(1, 'Crimp Fittings', 1),
(2, 'Low Profile', 1),
(3, 'Power Steering', 1),
(4, 'Brackets', 1),
(5, 'Centre Support', 1),
(6, 'Clips', 1),
(7, 'Accessories', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fitStyle`
--

CREATE TABLE IF NOT EXISTS `fitStyle` (
  `fitStyle_id` int(11) NOT NULL,
  `fitStyle_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fitType_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:Blocked, 1:Active'
) ENGINE=InnoDB AUTO_INCREMENT=1652 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fitStyle`
--

INSERT INTO `fitStyle` (`fitStyle_id`, `fitStyle_name`, `fitType_id`, `status`) VALUES
(101, 'Misc', 1, 1),
(102, 'Banjo Fittings', 1, 1),
(103, 'Centre Fittings', 1, 1),
(104, 'Imperial Female', 1, 1),
(105, 'Imperial Male', 1, 1),
(106, 'Metric Female', 1, 1),
(107, 'Metric Male', 1, 1),

(201, 'Misc', 2, 1),
(202, 'Banjos Fittings', 2, 1),
(203, 'Centre Fittings', 2, 1),
(204, 'Imperial Female', 2, 1),
(205, 'Imperial Male', 2, 1),
(206, 'Metric Female', 2, 1),
(207, 'Metric Male', 2, 1),
(208, 'Stems', 2, 1),

(301, 'Male Swivel', 3, 1),
(302, 'Female', 3, 1),
(303, 'Compression', 3, 1),
(304, 'Banjo Fittings', 3, 1),
(305, 'Centre Fittings', 3, 1),
(306, 'Metric Male', 3, 1),
(307, 'Braze On Fittings', 3, 1),
(308, 'Components', 3, 1),
(309, 'Hose', 3, 1),

(401, 'Australian', 4, 1),
(402, 'Specialty Mount', 4, 1),
(403, 'General', 4, 1),

(501, 'Centre Rubber Hose', 5, 1),
(502, 'Centre Braided Hose', 5, 1),

(601, 'Clips', 6, 1),

(701, 'Tube nuts, Banjo bolts & more', 7, 1),
(702, 'Hose Manufacture', 7, 1),
(703, 'Tube & accessories', 7, 1),
(704, 'Tools', 7, 1),
(705, 'Promotional', 7, 1),
(706, 'Crimp Machine', 7, 1),
(707, 'Consumables', 7, 1),
(708, 'Spare Parts', 7, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fitCategory`
--
ALTER TABLE `fitData`
  ADD PRIMARY KEY (`fitData_id`);

--
-- Indexes for table `fitCategory`
--
ALTER TABLE `fitCategory`
  ADD PRIMARY KEY (`fitCategory_id`);

--
-- Indexes for table `fitType`
--
ALTER TABLE `fitType`
  ADD PRIMARY KEY (`fitType_id`);

--
-- Indexes for table `fitStyle`
--
ALTER TABLE `fitStyle`
  ADD PRIMARY KEY (`fitStyle_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fitCategory`
--
ALTER TABLE `fitData`
  MODIFY `fitData_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16178;
--
-- AUTO_INCREMENT for table `fitCategory`
--
ALTER TABLE `fitCategory`
  MODIFY `fitCategory_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6178;
--
-- AUTO_INCREMENT for table `fitType`
--
ALTER TABLE `fitType`
  MODIFY `fitType_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `fitStyle`
--
ALTER TABLE `fitStyle`
  MODIFY `fitStyle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1652;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
