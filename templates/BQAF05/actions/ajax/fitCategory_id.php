<?php
require_once (dirname(__FILE__) . '/../../conf.php');
$mysqli = new mysqli(DB_CATALOG_HOST, DB_CATALOG_USER, DB_CATALOG_PASS,DB_CATALOG_NAME,
    DB_CATALOG_PORT);
$output = '';
$PIC_CATALOG_URL = 'https://www.brakequip.com.au/';
$render = $PIC_CATALOG_URL . "assets/pictorial/images/";
$image = $row['image'];
if ($_POST["fitCategoryID"] != '') {
    $sql = "SELECT * FROM fitData WHERE fitCategory_id = '" . $_POST["fitCategoryID"] . "'";
} else {
    $sql = "SELECT * FROM fitData ORDER BY partNo ";
}
$result = mysqli_query($mysqli, $sql);
$count = 0;
$output .= '';
while ($row = mysqli_fetch_array($result)) {
    $count ++;
    $style = $row['style'];
    $new = $row['status'];

    if ($style == "small") {
        $output .= '<div class="col-md-6">';
        $output .= create_small_content($row, $render, $new);
        $output .= '</div>';
    } else if ($style == "large") {
        $output .= create_large_content($row, $render, $new);
    } else if ($style == "table") {
        $output .= '<div class="col-md-12">';
        $output .= create_table_content($row, $render, $new);
        $output .= '</div>';
    } else if ($style == "page") {
        $output = create_page_content($row, $render, $new);
    } else if ($style == "smallacc") {
        $output = create_smallacc_content($row, $render, $new);
    }
}

echo $output;

function create_table_content($row, $render, $new){
  $output = '<div class="borderbox tableData">
     <div class="row">
       <div class="col-lg-4">
         <h2>' . $row['partNo'] . '</h2>
         <span>' . $row['tableDesc'] . '</span>
         <div class="">
           <div class="">
             <a href="' . $render . $row['image'] . '" data-lightbox="image-1" data-title="' . $row['partNo'] . '">
                <img src="' . $render . $row['image'] . '" class="fitting-img">
             </a>
           </div>
         </div>
       </div>
       <div class="col-lg-8">' . $row['table'] . '</div>
      </div>
      </div>';
   return $output;
}

function create_page_content($row, $render, $new){
   $output = '';
   $output .= '<div class="col-md-12">
        <a href="' . $render . $row['image'] . '" data-lightbox="image-1" data-title="">
            <img src="' . $render . $row['image'] . '" class="page">
        </a>
      </div>';
   return $output;
}

function create_smallacc_content($row, $render, $new){
    $output = '';
    $output .= '<div class="col-md-6">
    <div class="partArea borderbox fitsmall acc">
    <div id="notes">'
                 . $row['note1'] . '<br>'
                 . $row['note2'] . '</div>';
    $output .= ($new == 'n' ? '<img src="'.$render."newsticker.png".'" class="newsticker">' : '') .
     '<div class="thumbnail_container">
        <div class="thumbnail">
        <a href="' . $render . $row['image'] . '" data-lightbox="image-1" data-title="' . $row['partNo'] . '">
            <img src="' . $render . $row['image'] . '" class="fitting-img">
        </a>
        </div></div>
        <div class="row partdetail">
        <div class="col-md-6">
        <span class="fitpart"><h1>' . $row['partNo'] . '</h1></span>
        <span class="description">' . $row['description1-1'] . ' ' . $row['description1-2'] . '</span>
        <span class="description">' . $row['description2-1'] . ' ' . $row['description2-2'] . '</span>
        </div>
        <div class="col-md-6" style="margin-top: 3px;">
        <table>
        <tbody>
        <tr>
        <td class="detail"></td>
        </tr>
        <tr>
        <td class="detail">' . $row['detail1-1'] . '</td>
        </tr>
        <tr>
        <td class="detail">' . $row['detail2-1'] . '</td>
        </tr>
        <tr>
        <td class="detail">' . $row['detail3-1'] . '</td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
        </div>
     </div>';
  return $output;
}

function create_large_content($row, $render, $new){
  $output = '<div class="col-md-12">
      <div class="partArea borderbox fitlarge">
       <div id="notes">'
       .$row['note1']. '<br>'
       .$row['note2']. '</div>';
   $output .= '' .($new == 'n' ? '<img src="'.$render."newsticker.png".'" class="newsticker">' : '').
      '<div class="thumbnail_container">
       <div class="thumbnail">
       <a href="' . $render . $row['image'] . '" data-lightbox="image-1" data-title="' . $row['partNo'] . '">
           <img src="'.$render.$row['image'].'" class="fitting-img">
       </a>
       </div></div>
       <div class="row partdetail">
       <div class="col-sm-7">
       <span class="fitpart"><h1>'.$row['partNo'] .'</h1></span>
       <span class="description">'.$row['description1-1']. ' ' .$row['description1-2'].'</span>
       <span class="description">'.$row['description2-1']. ' ' .$row['description2-2'].'</span>
       </div>
       <div class="col-sm-5" style="margin-top: 3px;">
       <table>
       <tbody>
       <tr>
       <td class="detail">'.$row['detail1-1'] .'</td>
       <td class="detail">'.$row['detail1-2'] .'</td>
       </tr>
       <tr>
       <td class="detail">'.$row['detail2-1'] .'</td>
       <td class="detail">'.$row['detail2-2'] .'</td>
       </tr>
       <tr>
       <td class="detail">'.$row['detail3-1'] .'</td>
       <td class="detail">'.$row['detail3-2'] .'</td>
       </tr>
       <tr>
       <td class="finalrow detail">'.$row['fits'] .'</td>
       <td class="finalrow detail">'.$row['fit'] .'</td>
       </tr>
       </tbody>
       </table>
       </div>
       </div>
       </div>
       </div>
       ';
  return $output;
}

function create_small_content($row, $render, $new){
  $output = '<div class="partArea borderbox fitsmall">
     <div id="notes">'
                  . $row['note1'] . '<br>'
                  . $row['note2'] . '</div>';
  $output .= ($new == 'n' ? '<img src="'.$render."newsticker.png".'" class="newsticker">' : '') .
     '<div class="thumbnail_container">
     <div class="thumbnail">
     <a href="' . $render . $row['image'] . '" data-lightbox="image-1" data-title="' . $row['partNo'] . '">
        <img src="' . $render . $row['image'] . '" class="fitting-img">
     </a>
     </div></div>
     <div class="row partdetail">
     <div class="col-sm-7">
     <span class="fitpart"><h1>' . $row['partNo'] . '</h1></span>
     <span class="description">' . $row['description1-1'] . ' ' . $row['description1-2'] . '</span>
     <span class="description">' . $row['description2-1'] . ' ' . $row['description2-2'] . '</span>
     </div>
     <div class="col-sm-5" style="margin-top: 3px;">
     <table>
     <tbody>
     <tr>
     <td class="detail">' . $row['detail1-1'] . '</td>
     <td class="detail">' . $row['detail1-2'] . '</td>
     </tr>
     <tr>
     <td class="detail">' . $row['detail2-1'] . '</td>
     <td class="detail">' . $row['detail2-2'] . '</td>
     </tr>
     <tr>
     <td class="detail">' . $row['detail3-1'] . '</td>
     <td class="detail">' . $row['detail3-2'] . '</td>
     </tr>
     <tr>
     <td class="finalrow detail">' . $row['fits'] . '</td>
     <td class="finalrow detail">' . $row['fit'] . '</td>
     </tr>
     </tbody>
     </table>
     </div>
     </div>
     </div>
     </div>';
  return $output;
}
?>
