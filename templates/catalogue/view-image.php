<?php
$categories2 = Doctrine_Query::create()->from('WebCategory')->where('parent_web_category_id = ? AND hidden = ?', array($breadcrumbs[count($breadcrumbs) - 2]['id'], false))->orderBy('sort_order ASC')->execute();
$breadcrumbCount = count($breadcrumbs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Product, BrakeQuip Australia</title>

        <meta name="language" content="English" />

        <script language="javascript" type="text/javascript" src="/assets/js/jquery.js?v<?php echo time() ?>"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/global.js?v<?php echo time() ?>"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/jquery-ui.js?v1"></script>

        <link rel="stylesheet" type="text/css" href="/assets/css/style.css" charset="utf-8" />
        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/catalogue.css" charset="utf-8" />
        <style type="text/css">
            body {
                background: #ffffff url('/assets/images/bg.jpg') top repeat-x;
                background-color: #ffffff !important;
                font-family: 'Open Sans', sans-serif;
            }
            @media print{
                @page {
                    size: A4 landscape;
                }
                #footerDiv, #left, #right, #recommendsPrinting {
                    display: none;
                }
                body {
                    float: none !important;
                    width: auto !important;
                    margin:  0 !important;
                    padding: 0 !important;
                    color: #000;
                    font: 100%/150% Georgia, "Times New Roman", Times, serif;
                }
            }

            .noselect {
                -webkit-touch-callout: none;
                /* iOS Safari */
                -webkit-user-select: none;
                /* Chrome/Safari/Opera */
                -khtml-user-select: none;
                /* Konqueror */
                -moz-user-select: none;
                /* Firefox */
                -ms-user-select: none;
                /* Internet Explorer/Edge */
                user-select: none;
                /* Non-prefixed version, currently not supported by any browser */
            }
        </style>
      <!--  <script type="text/javascript" language="Javascript">
            $(document).ready(function () {
                $(document).bind("contextmenu", function (e) {
                    return false;
                });
            });
        </script> -->
    </head>

    <body>
      <div class="navbg">
        <div id="topNav">
          <div class="categoryBreadcrumb">
              <?php // if (count($breadcrumbs) > 0): ?>
                  <ul>
                      <li><a href="/catalogue/" style="font-weight:bold;">NEW SEARCH</a></li>
                      <?php foreach ($breadcrumbs as $breadcrumb): $i++; ?>
                          <li<?php echo ($breadcrumbCount == $i ? ' class="last"' : '') ?>><a href="/catalogue/?categoryId=<?php echo $breadcrumbs[($i - 2)]['id'] ?>"><?php echo $breadcrumb['name'] ?></a></li>
                      <?php endforeach; ?>
                  </ul>
                  <div style="clear: both"></div>
              <?php // endif; ?>
          </div>
                <?php
                $ignorePages = array('applications');
                $ignoreModules = array('catalogue', 'gallery', 'order', 'pictorial');
                ?>
            <?php if (in_array($page, $ignorePages) || in_array($modules, $ignoreModules)): ?>
            <?php if (isset($_SESSION['userId'])): ?>
            <div style="z-index:-10;position:absolute;margin-left:60px;width:820px;text-align:center;line-height:38px;color:#fff;font-size:18px;"><?php echo $_SESSION['name'] ?></div>
            <?php endif; ?>
            <?php endif; ?>
            <h1 style="line-height:12px;font-size: 15px;"><?php if (isset($_SESSION['userId'])): ?>
                <a href="/applications">Return to Applications</a> &nbsp; | &nbsp; <a href="/logout">Logout</a>
                <?php else: ?>
                Brake Hose Manufacturing Systems &amp; replacement brake hoses
                <?php endif; ?>
            </h1>
            <nav class="navbar navbar-expand-sm navbar-light bg-light">
              <?php
              $ignorePages = array('applications');
              $ignoreModules = array('catalogue', 'gallery', 'order', 'pictorial');
              ?>
              <?php if (!in_array($page, $ignorePages) && !in_array($modules, $ignoreModules)): ?>
              <div class="navbar-collapse collapse show" id="navbarNavAltMarkup" style="">
                <div class="navbar-nav" style="font-size: 15px;margin-top: -7px;">
                  <a class="nav-item nav-link" href="/"><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia" class="img-fluid mobLogo"/></a>
                  <a class="nav-item nav-link" href="/">Home</a>
                  <a class="nav-item nav-link" href="/find-manufacturer">Find a Manufacturer</a>
                  <a class="nav-item nav-link" href="/rubber-brake-hose">Rubber</a>
                  <a class="nav-item nav-link" href="/braided-brake-hose">Braided</a>
                <?php endif; ?>
                  <a class="nav-item nav-link" href="/contact">Contact</a>
                </div>
              </div>
            </nav>
<!--                <ul>
                <?php
                $ignorePages = array('applications');
                $ignoreModules = array('catalogue', 'gallery', 'order', 'pictorial');
                ?>
                <?php if (!in_array($page, $ignorePages) && !in_array($modules, $ignoreModules)): ?>
                <li><a href="/" class="home<?php echo (empty($modules) && $page == "index" ? ' selected' : '') ?>"><img src="/assets/images/homeIcon.png" /></a></li>
                <li><a href="/find-manufacturer"<?php echo (empty($modules) && $page == "find-manufacturer" ? ' class="selected"' : '') ?>>find a manufacturer</a></li>
                <li><a href="/rubber-brake-hose"<?php echo (empty($modules) && $page == "rubber-brake-hose" ? ' class="selected"' : '') ?>>rubber</a></li>
                <li><a href="/braided-brake-hose"<?php echo (empty($modules) && $page == "braided-brake-hose" ? ' class="selected"' : '') ?>>braided</a></li>
                <?php endif; ?>
                <li><a href="/contact"<?php echo (empty($modules) && $page == "contact" ? ' class="selected"' : '') ?>>contact</a></li>
            </ul>
          -->
        </div>
      </div>

        <div class="container">
            <div id="left">
                <span style="color: #d9534f; cursor: pointer" onclick="window.close()"> &lt; Back to Catalogue (Close window)</span>
            </div>
            <div id="right">
                <a href="/catalogue/view-image?id=<?php echo $part->id ?>&categoryId=<?php echo $category->id ?>&print" target="_blank" class="print fr" onclick="window.print();return false">Print this page</a>
            </div>
            <div><br/>
                <strong>
                    Logged in as: <?php echo $_SESSION['user']; //$_SESSION['name'];                          ?>
                </strong>
            </div>
            <span id="recommendsPrinting" style="font-size: 70%; float: right">BrakeQuip recommends printing in landscape view</span>

            <?php
            $partData = $part->getData();
            $imgSvg = "productsSVG/{$partData["ill_number"]}.svg";
            if (file_exists($imgSvg)) {
                $dataSvg = file_get_contents($imgSvg);
            } else {
                $dataSvg = "No data";
            }
            ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="300px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top">
                                    <a><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia" width="300px"/></a>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"><br/>
                                    <h1 style="font-size: 25px; font-weight: 400">
                                        Part # <?php echo $partAlpha ?><?php echo $partNumber ?>
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"><br/>
                                    <form action="/catalogue/view-image" method="get">
                                        <select name="categoryId" onchange="this.form.submit();" style="width: 100%">
                                            <?php foreach ($categories2 as $result): ?>
                                                <option value="<?php echo $result->id ?>"<?php echo ($result->id == $categoryId ? ' selected' : '') ?>><?php echo stripslashes($result->name) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"><br/>
                                    <div class="warning" style="width: 93%; font-size: 83%; font-weight: bold">
                                        IMPORTANT: WHEN MAKING STAINLESS STEEL BRAIDED HOSES - ADD 24mm TO CUT LENGTH. IF THE HOSE INCLUDES A CENTRE PIECE, 12mm IS THEN ADDED TO EACH END OF THE HOSE.
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table class="noselect" width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left: 30px;">
                            <tr><td colspan="5">&nbsp;</td></tr>
                            <tr style="background-color: #E9E9E9;">
                                <th scope="col">&nbsp;&nbsp;&nbsp;Fitting A</th>
                                <th scope="col">Fitting B</th>
                                <th scope="col">Fitting C</th>
                                <th scope="col">Fitting D</th>
                                <th scope="col">Fitting E</th>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;<?php echo $partData["a_fitting"] != "" ? $partData["a_fitting"] : "N/A" ?></td>
                                <td><?php echo $partData["b_fitting"] != "" ? $partData["b_fitting"] : "N/A" ?></td>
                                <td><?php echo $partData["c_fitting"] != "" ? $partData["c_fitting"] : "N/A" ?></td>
                                <td><?php echo $partData["d_fitting"] != "" ? $partData["d_fitting"] : "N/A" ?></td>
                                <td><?php echo $partData["e_fitting"] != "" ? $partData["e_fitting"] : "N/A" ?></td>
                            </tr>
                            <tr><td colspan="5">&nbsp;</td></tr>
                            <tr style="background-color: #E9E9E9;">
                                <th colspan="2" scope="col">&nbsp;&nbsp;&nbsp;Dimensions</th>
                                <th colspan="3" scope="col"><?php echo $partData["make_hose_instructions"] != "" ? "How to make hose" : "" ?></th>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;<strong>Cut hose at</strong></td>
                                <td><?php echo $partData["cut_hose_at"] != "" ? $partData["cut_hose_at"] . "mm" : "N/A" ?></td>
                                <td colspan="3" rowspan="4"><?php echo $partData["make_hose_instructions"] != "" ? $partData["make_hose_instructions"] : "" ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;<strong>A-C length</strong></td>
                                <td><?php echo $partData["ac_length"] != "" ? $partData["ac_length"] . "mm" : "N/A" ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;<strong>A-D length</strong></td>
                                <td><?php echo $partData["ad_length"] != "" ? $partData["ad_length"] . "mm" : "N/A" ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;<strong>A-E length</strong></td>
                                <td><?php echo $partData["ae_length"] != "" ? $partData["ae_length"] . "mm" : "N/A" ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 10px; padding-bottom: 100px">
                <?php echo $dataSvg; ?>
            </div>
            <div class="disclaimer" style="border:1px solid #999999; background-color: #ebecf0;">
                <p>All images and information are copyright to Brakequip Australia and may not be used without prior written consent from the company.</p>
                <h4 style="color: #999999;">General Disclaimer</h4>

                <p>    The information provided in the BrakeQuip Australia Electronic Catalogue will assist you to the absolute best of our knowledge.
                    While we aim to keep the content of this catalogue accurate and up to date, we make no warranties or representations regarding the accuracy
                    or completeness of the information. We ask you to contact BrakeQuip Australia if you have any concerns about the information provided.
                    We will make every effort to correct any errors once they are brought to our attention.</p>
            </div>
            <div id="footerDiv" style="padding-bottom: 200px"></div>
        </div>

        <?php if (isset($_GET['print'])): ?>
            <script type="text/javascript">
                window.print();
            </script>
        <?php endif; ?>
    </body>
</html>
