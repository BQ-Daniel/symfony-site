<?php
$this->extend($extendDir . 'layoutapps');

//if ($categoryType && $categories && count($categories) == 1) {
//header("Location: /catalogue/?categoryId=" . $categories[0]->id);
//exit;
//}

if ($part) {
    //echo "<pre>";
    //print_r($part->getData());
    //echo "</pre>";
    $partData = $part->getData();
}
?>
<link rel="stylesheet" type="text/css" media="print" href="/assets/css/catalogueprint.css" charset="utf-8" />


<script type="text/javascript" language="Javascript">
    $(document).ready(function () {
        $(document).bind("contextmenu", function (e) {
            return false;
        });
    });
</script>


<div id="catalogue">
    <?php if ($part): ?>
        <div id="hosename" class="row align-items-center pb-3">
            <div class="col-sm-9">
                <span><?php echo $breadcrumbs[0]['name'] . ' ' . $breadcrumbs[1]['name'] ?> - <?php echo $partAlpha ?><?php echo $partNumber ?></span>
            </div>
            <div id="catalogueSearch" class="col-sm-3 float-right">
                <form action="/catalogue/search" method="post" class="row">
                  <input type="text" name="bq_number" value="" class="col-9 form-control" placeholder="Search BQ Number" /><button type="submit" name="search" value="Search" class="btn btn-danger col-2"><i class="fas fa-search"></i></button>
                </form>
            </div>
            <div class="cb"></div>
        </div>
    <?php endif; ?>

    <div class="categoryBreadcrumb">
        <?php // if (count($breadcrumbs) > 0): ?>
            <ul>
                <li><a href="/catalogue/" style="font-weight:bold;">NEW SEARCH</a></li>
                <?php foreach ($breadcrumbs as $breadcrumb): $i++; ?>
                    <li<?php echo ($breadcrumbCount == $i ? ' class="last"' : '') ?>><a href="/catalogue/?categoryId=<?php echo $breadcrumbs[($i - 2)]['id'] ?>"><?php echo $breadcrumb['name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
            <div style="clear: both"></div>
        <?php // endif; ?>
    </div>

    <?php if ($categoryType): ?>
        <div class="dropdown-container">
          <div class="row">

            <div class="col-4">
                <form action="/catalogue/" method="get">
                    <p>Select <?php echo $categoryType->name ?></p>
                    <select name="categoryId" onchange="$('#loading-categories').show(); this.form.submit();" class="form-control">
                        <option value="">Please select</option>
                        <?php foreach ($categories as $categoryResult): ?>
                            <option value="<?php echo $categoryResult->id ?>"><?php echo stripslashes($categoryResult->name) ?></option>
                        <?php endforeach; ?>
                    </select> <span id="loading-categories" style="display: none;"><br>Getting data...</span>
                    <!--<a href="/catalogue/?categoryId=<?php echo $breadcrumbs[count($breadcrumbs) - 2]['id'] ?>" class="backOneLevel" style="margin:10px 0;">Back one level</a>-->
                </form>
            </div>
            <div class="col-4"></div>
            <div class="col-4">
                <div class="bq-search">
                    <form action="/catalogue/search" method="post" class="row">
                      <p>Quick Search</p>
                        <input type="text" name="bq_number" value="" class="col-9 form-control" placeholder="Search BQ Number" /><button type="submit" name="search" value="Search" class="btn btn-danger col-2"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
          </div>
        </div>
    <?php endif; ?>

    <?php if ($part /* && $countOfRows < $threshold */): ?>
        <div id="part">
            <div class="toolbar mod">
                <a href="javascript:window.print()" class="btn btn-light">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 482.5 482.5" style="height:24px;" xml:space="preserve">

              		<path d="M399.25,98.9h-12.4V71.3c0-39.3-32-71.3-71.3-71.3h-149.7c-39.3,0-71.3,32-71.3,71.3v27.6h-11.3
              			c-39.3,0-71.3,32-71.3,71.3v115c0,39.3,32,71.3,71.3,71.3h11.2v90.4c0,19.6,16,35.6,35.6,35.6h221.1c19.6,0,35.6-16,35.6-35.6
              			v-90.4h12.5c39.3,0,71.3-32,71.3-71.3v-115C470.55,130.9,438.55,98.9,399.25,98.9z M121.45,71.3c0-24.4,19.9-44.3,44.3-44.3h149.6
              			c24.4,0,44.3,19.9,44.3,44.3v27.6h-238.2V71.3z M359.75,447.1c0,4.7-3.9,8.6-8.6,8.6h-221.1c-4.7,0-8.6-3.9-8.6-8.6V298h238.3
              			V447.1z M443.55,285.3c0,24.4-19.9,44.3-44.3,44.3h-12.4V298h17.8c7.5,0,13.5-6,13.5-13.5s-6-13.5-13.5-13.5h-330
              			c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h19.9v31.6h-11.3c-24.4,0-44.3-19.9-44.3-44.3v-115c0-24.4,19.9-44.3,44.3-44.3h316
              			c24.4,0,44.3,19.9,44.3,44.3V285.3z"/>
              		<path d="M154.15,364.4h171.9c7.5,0,13.5-6,13.5-13.5s-6-13.5-13.5-13.5h-171.9c-7.5,0-13.5,6-13.5,13.5S146.75,364.4,154.15,364.4
              			z"/>
              		<path d="M327.15,392.6h-172c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h171.9c7.5,0,13.5-6,13.5-13.5S334.55,392.6,327.15,392.6z"
              			/>
              		<path d="M398.95,151.9h-27.4c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h27.4c7.5,0,13.5-6,13.5-13.5S406.45,151.9,398.95,151.9z"
              			/></svg>
                </a>
              <!--<a href="/catalogue/view-image?id=<?php echo $part->id ?>&categoryId=<?php echo $category->id ?>&print" target="_blank" class="print fr">Print this page</a>-->
                <form id="viewLargerImageLink" action="/catalogue/view-image?id=<?php echo $part->id ?>&categoryId=<?php echo $category->id ?>" method="post" target="_blank">
                    <input type="submit" name="submit" value="Submit" style="display:none" />
                </form>
                <!--<a href="/catalogue/?categoryId=<?php echo $breadcrumbs[count($breadcrumbs) - 2]['id'] ?>" class="backOneLevel">Back one level</a>-->
                <form action="/catalogue/" method="get" style="" class="fl">
                    <select name="categoryId" onchange="this.form.submit();" class="form-control">
                        <?php foreach ($categories2 as $result): ?>
                            <option value="<?php echo $result->id ?>"<?php echo ($result->id == $categoryId ? ' selected' : '') ?>><?php echo stripslashes($result->name) ?></option>
                        <?php endforeach; ?>
                    </select>
                </form>
                <div class="cb"></div>
            </div>


            <?php if ($part->website_display == 1): ?>
                <?php
                $imgSvg = "products/{$partData["ill_number"]}.svg";
                if (file_exists($imgSvg)) {
                    $dataSvg = file_get_contents($imgSvg);
                } else {
                    $dataSvg = "No data";
                }
                ?>

                    <div id="hoseimg">
                      <?php echo $dataSvg; ?>
                            <!-- <a href="/catalogue/view-image?id=<?php echo $part->id ?>&categoryId=<?php echo $category->id ?>" target="_blank">
                                <?php echo $dataSvg; ?>
                            </a> -->
                    </div>
                    <div class="row" style="display:none;">
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-2">Fitting A</div>
                          <div class="col-sm-2">Fitting B</div>
                          <div class="col-sm-2">Fitting C</div>
                          <div class="col-sm-2">Fitting D</div>
                          <div class="col-sm-2">Fitting E</div>
                        </div>
                        <div class="row">
                          <div class="col-sm-2"><?php echo $partData["a_fitting"] != "" ? $partData["a_fitting"] : "N/A" ?></div>
                          <div class="col-sm-2"><?php echo $partData["b_fitting"] != "" ? $partData["b_fitting"] : "N/A" ?></div>
                          <div class="col-sm-2"><?php echo $partData["c_fitting"] != "" ? $partData["c_fitting"] : "N/A" ?></div>
                          <div class="col-sm-2"><?php echo $partData["d_fitting"] != "" ? $partData["d_fitting"] : "N/A" ?></div>
                          <div class="col-sm-2"><?php echo $partData["e_fitting"] != "" ? $partData["e_fitting"] : "N/A" ?></div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="row">
                          <div class="col-6">Cut hose</div>
                          <div class="col-6"><?php echo $partData["cut_hose_at"] != "" ? $partData["cut_hose_at"] . "mm" : "N/A" ?></div>
                          <div class="col-6">A-C length</div>
                          <div class="col-6"><?php echo $partData["ac_length"] != "" ? $partData["ac_length"] . "mm" : "N/A" ?></div>
                          <div class="col-6">A-D length</div>
                          <div class="col-6"><?php echo $partData["ad_length"] != "" ? $partData["ad_length"] . "mm" : "N/A" ?></div>
                          <div class="col-6">A-E length</div>
                          <div class="col-6"><?php echo $partData["ae_length"] != "" ? $partData["ae_length"] . "mm" : "N/A" ?></div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="col">
                        <?php echo $partData["make_hose_instructions"] != "" ? $partData["make_hose_instructions"] : "" ?></div>
                      </div>
                    </div>

                    <table class="noselect" width="100%" border="0" cellspacing="15" cellpadding="15">
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr style="background-color: #E9E9E9;">
                        <th scope="col">&nbsp;&nbsp;&nbsp;Fitting A</th>
                        <th scope="col">Fitting B</th>
                        <th scope="col">Fitting C</th>
                        <th scope="col">Fitting D</th>
                        <th scope="col">Fitting E</th>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<?php echo $partData["a_fitting"] != "" ? $partData["a_fitting"] : "N/A" ?></td>
                        <td><?php echo $partData["b_fitting"] != "" ? $partData["b_fitting"] : "N/A" ?></td>
                        <td><?php echo $partData["c_fitting"] != "" ? $partData["c_fitting"] : "N/A" ?></td>
                        <td><?php echo $partData["d_fitting"] != "" ? $partData["d_fitting"] : "N/A" ?></td>
                        <td><?php echo $partData["e_fitting"] != "" ? $partData["e_fitting"] : "N/A" ?></td>
                    </tr>
                    <tr><td colspan="5">&nbsp;</td></tr>
                    <tr style="background-color: #E9E9E9;">
                        <th colspan="2" scope="col">&nbsp;&nbsp;&nbsp;Dimensions</th>
                        <th colspan="3" scope="col"><?php echo $partData["make_hose_instructions"] != "" ? "How to make hose" : "" ?></th>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<strong>Cut hose at</strong></td>
                        <td><?php echo $partData["cut_hose_at"] != "" ? $partData["cut_hose_at"] . "mm" : "N/A" ?></td>
                        <td colspan="3" rowspan="4"><?php echo $partData["make_hose_instructions"] != "" ? $partData["make_hose_instructions"] : "" ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<strong>A-C length</strong></td>
                        <td><?php echo $partData["ac_length"] != "" ? $partData["ac_length"] . "mm" : "N/A" ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<strong>A-D length</strong></td>
                        <td><?php echo $partData["ad_length"] != "" ? $partData["ad_length"] . "mm" : "N/A" ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;<strong>A-E length</strong></td>
                        <td><?php echo $partData["ae_length"] != "" ? $partData["ae_length"] . "mm" : "N/A" ?></td>
                    </tr>
                </table>

                <div style="height:10px;"></div>

                <div id="printbottomtext" class="mod">
                    <div id="catalogueContact" class="" style="padding-bottom: 25px;">
                        <a id="vehicleRequestLink" href="" class="btn" style=""><button type="button" class="btn btn-danger">Can't find what you're looking for?</button></a>
                        <a id="hoseInaccuracyLink" href="" class="btn" style="" target="_blank"><button type="button" class="btn btn-danger">Report hose inaccuracy</button></a>
                        <a href="/catalogue/understand" class="btn" style="" target="_blank"><button type="button" class="btn btn-danger">Understanding the catalogue</button></a>
                    </div>
                    <div class="alert alert-danger" role="alert">
                        IMPORTANT: WHEN MAKING STAINLESS STEEL BRAIDED HOSES - ADD 24mm TO CUT LENGTH. <br>IF THE HOSE INCLUDES A CENTRE PIECE, 12mm IS THEN ADDED TO EACH END OF THE HOSE.
                    </div>
                    <div id="printfooter" style="display:none;">
                      <span>&copy; BrakeQuip Australia <?php echo date('Y') ?> -  All images and information are copyright to Brakequip Australia and may not be used without prior written consent from the company.
                    </span>
                    </div>
                </div>


                <div id="hoseInaccuracyDialog" style="display:none">
                    <p>You are about to report hose inaccuracy for the following part:</p>
                    <p>
                        <?php if (count($breadcrumbs) > 0): ?>
                            <?php foreach ($breadcrumbs as $breadcrumb): $i++; ?>
                                <?php $categoryString .= $breadcrumb['name'] . ' > '; ?>
                            <?php endforeach; ?>
                            <strong><?php echo $categoryString . ' ' . $part->bq_number ?></strong>
                        <?php endif; ?>
                    </p>

                    <form id="inaccurancyForm" action="/catalogue/report-inaccurancy" method="post">
                        <input type="hidden" name="category" value="<?php echo $categoryString ?>" />
                        <input type="hidden" name="partId" value="<?php echo $part->id ?>" />
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <th>Your Name *</th>
                            </tr>
                            <tr>
                                <td><input id="yourname" type="text" name="name" value="" /></td>
                            </tr>
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <th>Why do you believe this is inaccurate? *</th>
                            </tr>
                            <tr>
                                <td><textarea id="reason" name="reason" style="width:100%;height:100px;"></textarea>
                            </tr>
                        </table>
                        <input id="hoseInaccurancySubmit" type="submit" class="submit" name="submit" value="Submit" />
                    </form>
                </div>

                <script>
                    $(function () {
                        $('#hoseInaccuracyLink').click(function () {
                            $("#hoseInaccuracyDialog").dialog('open');
                            $('object').hide();
                            return false;
                        });
                        $("#hoseInaccuracyDialog").dialog({modal: true, width: 600, autoOpen: false, title: 'Report Hose Inaccuracy',
                            open: function () {
                                $('object').css('visibility', 'hidden');
                            },
                            beforeClose: function () {
                                $('object').css('visibility', 'visible');
                                $('object').show();
                            }
                        });

                        $('#hoseInaccurancySubmit').click(function ()
                        {
                            // Make sure fields arn't blank
                            var blank = false;
                            $('input, textarea', $('#inaccurancyForm')).each(function (key, obj) {
                                if (obj.value == '') {
                                    blank = true;
                                    return;
                                }
                            });

                            if (blank) {
                                alert('All fields are required to be filled in');
                                return false;
                            }
                            $.post('/catalogue/report-inaccurancy', $('#inaccurancyForm').serialize(), function (data) {
                                alert('Thank you for your input');
                                $("#hoseInaccuracyDialog").dialog('close');
                                $('#yourname').val('');
                                $('#reason').val('');
                            });


                            return false;
                        });

                    });
                </script>

            <?php else: ?>
                <p>This hose is currently unavailable due to parts still being in production.</p>
            <?php endif; ?>

        </div>
    <?php endif; ?>
    <?php if ($part && $countOfRows >= $threshold): ?>
        <div id="part">
            <p style="font-weight: bold; font-size: 12px; margin-top:20px; text-align:center">Oops, you have reached the amount of hoses you can view for today. Please contact BrakeQuip if you require a hose specification.</p>
        </div>
    <?php endif; ?>
    <?php if (!$categoryType && !$part): ?>
        <div id="part">
            <p style="font-weight: bold; font-size: 12px">Unable to display part. BrakeQuip have been notified about this error and will attempt to fix the problem as soon as possible. Sorry for any inconvenience this may have caused.<br /><br /> Thank you.</p>
            <a href="" id="vehicleRequestLink" class="btn"><span>Can't find what you're looking for?</span></a>
            <a href="/catalogue/understand" class="btn" style="margin-left: 10px;" target="_blank"><span>Understanding our catalogue</span></a>
        </div>
    <?php endif; ?>

</div>


<div id="vehicleRequestDialog" style="display:none">
    <div id="vehicleRequestResult">
        <strong>Can't find what you're looking for?</strong>
        <p>We're always trying to stay on the ball, so if you come across a vehicle that we don't have listed &amp; you think we should, let us know!</p>
    </div>
    <form id="vehicleRequestForm" method="post" action="/catalogue/vehicle-request">
        <table width="100%" border="0" cellspacing="2" cellpadding="2">
            <tr>
                <td width="180">Name <FONT color="#ff0000">*</FONT></td>
                <td><input class="input" type="TEXT" name="name" size=30 value="<?= $_POST[name] ?>" /></td>
            </tr>
            <tr>
                <td> Make <FONT color="#ff0000">*</FONT></td>
                <td><input name="make" type="text" class="input" value="<?= $_POST[make] ?>" size="30" /></td>
            </tr>
            <tr>
                <td>Model <FONT color="#ff0000">*</FONT></td>
                <td><input class="input" type="text" name="model" size="30" value="<?= $_POST[model] ?>" /></td>
            </tr>
            <tr>
                <td>Sub Model</td>
                <td><input class="input" type="TEXT" name="submodel" size=30 value="<?= $_POST[submodel] ?>" /></td>
            </tr>
            <tr>
                <td>Year(s)</td>
                <td><input class="input" type="TEXT" name="year" size=30 value="<?= $_POST[year] ?>" /></td>
            </tr>
            <tr valign="top">
                <td>Anything else to add...</td>
                <td><textarea name="other" cols="30" class="input"><?= $_POST[other] ?>&nbsp;</textarea></td>
            </tr>
            <tr>
                <td valign="top" align="center" colspan="2"><br />
                    <input id="vehicleRequestSubmit" type="button" class="submit" name="submit" value="Submit" /></td>
            </tr>
        </table>
    </form>
</div>



<script>
    $(function () {
        $('#vehicleRequestLink').click(function () {
            $("#vehicleRequestDialog").dialog('open');
            $('object').hide();
            return false;
        });
        $("#vehicleRequestDialog").dialog({modal: true, width: 600, autoOpen: false, title: 'Vehicle Request',
            open: function () {
                $('object').css('visibility', 'hidden');
            },
            beforeClose: function () {
                $('object').css('visibility', 'visible');
                $('object').show();
            }
        });

        $('#vehicleRequestSubmit').click(function () {

            $.post('/catalogue/vehicle-request', $('#vehicleRequestForm').serialize(), function (data) {
                data = $.parseJSON(data);
                if (data.e != undefined) {
                    alert(data.e);
                    return false;
                }

                if (data.s != undefined) {
                    alert(data.s);
                    $("#vehicleRequestDialog").dialog('close');
                    $('#vehicleRequestForm input[type="text"], #vehicleRequestForm textarea').val('');
                }
                return false;
            });

        });
    });
</script>
