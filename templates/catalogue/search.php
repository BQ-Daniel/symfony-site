<?php $this->extend($extendDir . 'layoutapps'); ?>

<div class="categoryBreadcrumb">
    <?php // if (count($breadcrumbs) > 0): ?>
        <ul>
            <li><a href="/catalogue/" style="font-weight:bold;">BACK TO CATALOGUE</a></li>
            <?php foreach ($breadcrumbs as $breadcrumb): $i++; ?>
                <li<?php echo ($breadcrumbCount == $i ? ' class="last"' : '') ?>><a href="/catalogue/?categoryId=<?php echo $breadcrumbs[($i - 2)]['id'] ?>"><?php echo $breadcrumb['name'] ?></a></li>
            <?php endforeach; ?>
        </ul>
        <div style="clear: both"></div>
    <?php // endif; ?>
</div>

<div class="col pt-2">
  <div class="row">
<div class="col-sm-8">
  <h1 class="text-uppercase"><strong><?php echo $_POST['bq_number'] ?></strong></h1>
  Found <?php echo $count ?> <?php echo ($count > 1 || $count == 0 ? 'results' : 'results') ?></p>
  <?php if ($count > 0): ?>
</div>
    <div class="col-sm-4">
      <div class="bq-search pt-3">
          <form action="/catalogue/search" method="post" class="row">
              <input type="text" name="bq_number" value="" class="col-9 form-control" placeholder="Search BQ Number" /><button type="submit" name="search" value="Search" class="btn btn-danger col-2"><i class="fas fa-search"></i></button>
          </form>
      </div>
    </div>
  </div>


    <div>
        <ol style="padding-left: 15px;line-height: 2;">
        <?php foreach ($results[0]['WebPartToCategory'] as $result): ?>
            <li><a href="/catalogue/?categoryId=<?php echo $result['web_category_id'] ?>"><?php echo WebCategoryTable::getParentCategories($result['web_category_id']) ?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>
<?php else: ?>
<p>No results found.</p>

<?php endif; ?>

</div>
