<?php $this->extend($extendDir.'layoutapps'); ?>
<script>
    // Clear cookies
    eraseCookie('ordered-by');
    eraseCookie('shipping');
    eraseCookie('shipping-other');

    location.href = '/applications';
</script>
