<?php $this->extend($extendDir . 'layoutapps'); ?>
<div id="orderProductsContainer">
<form id="orderForm" action="/order/checkout" method="post">
  <div class="row">
    <div id="orderProducts" class="col-sm-8">
        <div id="orderProductsSlider">
            <?php if ($categories): ?>
            <div id="slider">
                <ul class="sliderNav mod">
                    <?php foreach ($categories as $category): ?>
                    <li><a href="#<?php echo strtoupper($category->cat) ?>"><?php echo strtoupper($category->cat) ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <div class="cb"></div>
            </div>
            <div class="list">
                <div id="listHeader">
                    <div class="head mod row">
                        <div class="col-sm-3">Part #</div>
                        <div class="col-sm-5">Description</div>
                        <div class="col-sm-2">Unit Price</div>
                        <div class="col-sm-2">Quantity</div>
                    </div>
                    <div class="cb"></div>
                </div>
                <div class="scroll">
                    <div class="scrollContainer">
                        <?php foreach ($categories as $category): ?>
                        <div id="<?php echo strtoupper($category->cat) ?>" class="panel<?php echo $category->cat != "BQ" ? ' loadProducts' : '' ?>">
                                <?php if ($category->cat == "BQ"): ?>
                                <?php foreach (OrderProductsTable::getProductsByCategory($category->cat) as $product): ?>
                                <div id="partrow" class="row align-items-center">
                                    <div class="col-sm-3"><?php echo $product->part_number ?></div>
                                    <div class="col-sm-5"><?php echo $product->description ?></div>
                                    <div class="col-sm-2">$<?php echo $product->price ?></div>
                                    <div class="col-sm-2"><input id="qty-<?php echo $product->id ?>" type="text" name="qty" class="form-control form-control-sm qty" data-value="<?php echo $product->part_number ?>" value="<?php echo ($cart->getQty($product->id) ? $cart->getQty($product->id) : '') ?>" autocomplete="off" /></div>
                                </div>
                                <div class="cb"></div>
                                <?php endforeach; ?>
                                <?php else: ?>
                                    <div style="padding:10px;">Loading...</div>
                                <?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <div id="liveOrderContainer" class="col-sm-4">
        <div id="liveOrder">
            <h3>live order</h3>
            <ul class="head mod">
                <li class="col-sm-1"></li>
                <li class="col-sm-5">Part #</li>
                <li class="col-sm-3">Qty</li>
                <li class="col-sm-3">Price</li>
            </ul>
            <div class="cb"></div>
            <div id="liveList">
                <?php foreach ($cart->getItems() as $item): ?>
                <ul id="cart-<?php echo $item['id'] ?>" class="mod" onmouseover="this.style.background = '#f6f6f6'" onmouseout="this.style.background = '#fff'">
                    <li class="col-sm-1"><a href="" class="delete"><img src="/assets/images/delete.gif" title="Delete Item from Cart" /></a></li>
                    <li class="col-sm-5"><?php echo $item['part'] ?></li>
                    <li class="col-sm-3"><?php echo $item['qty'] ?></li>
                    <li class="col-sm-3">$<?php echo $cart->getPrice($item['id']) ?></li>
                </ul>
                <div class="cb"></div>
                <?php endforeach; ?>
            </div>
        </div>

<div id="poinfo">
        <div id="totals">
            <?php $last_price = $cart->getTotalWeight(); ?>
            <?php $last_price_per = $cart->getTotalWeight()*5/100; ?>
            <?php $new_total = $last_price + $last_price_per; ?>
            Weight: <span id="totalWeight"> <?php echo number_format($new_total, 2); ?>kg</span> <br>
            Total: <span id="totalPrice"> $<?php echo number_format($cart->getTotalPrice(), 2) ?></span>
        </div>

        <div id="podetails">
          <input id="orderedBy" type="text" name="ordered_by" value="" placeholder="Ordered by (required)" class="form-control form-control-sm storeCookie" /> <br>
          <input id="orderNo" type="text" name="order_no" value="" placeholder="PO number" class="form-control form-control-sm storeCookie<?php echo $user->order_no_req == 'Y' ? ' required' : '' ?>" /><br>

        </div>

        <div class="row" style="height:100px;">
            <div class="col-sm-7">
                <select id="shipping" name="shipping" class="form-control form-control-sm storeCookie">
                    <option value="">Shipping Method</option>
                    <option>Air Bag</option>
                    <option>Courier</option>
                    <option>Road Express</option>
                    <option>Pickup</option>
                    <option>Other</option>
                </select><br /><br />
                <input type="text" id="shippingOther" name="shipping_other" style="width:108px;" value="Please specify" class="form-control form-control-sm storeCookie" />
                <!--[if lt IE 8]>
                <script>
                    $('#shippingOther').attr('style', 'width:105px');
                </script>
                <![endif]-->
            </div>
            <div class="col-sm-5">
            <input id="checkoutBtn" type="submit" name="checkout" value="Continue" class="btn btn-danger fr" />
          </div>
          </div>
            <div class="cb"></div>
        </div>

    </div>



</form>
</div>

<div id="liveListTemplate" style="display:none">
    <ul id="" onmouseover="this.style.background = '#f6f6f6'" onmouseout="this.style.background = '#fff'">
        <li class="col-sm-3"><a href="" class="delete"><img src="/assets/images/delete.gif" title="Delete Item from Cart" /></a></li>
        <li class="col-sm-5"></li>
        <li class="col-sm-2"></li>
        <li class="col-sm-2"></li>
    </ul>
    <div class="cb"></div>
</div>

<script type="text/javascript">
var currentBoxNumber = 0;
$("#partrow").keypress(function (event) {
  if (event.keyCode == 13) {
      textboxes = $("input.qyt");
      currentBoxNumber = textboxes.index(event.target);
      if (textboxes[currentBoxNumber + 1] != null) {
          nextBox = textboxes[currentBoxNumber + 1];
          nextBox.focus();
          nextBox.select();
          event.preventDefault();
          return false;
      }
  }
});
</script>
