<?php
$category = $_REQUEST['category'];
// Include shopping cart
require_once(_DIR_CLASSES.'ShoppingCart.class.php');

$cart = new ShoppingCart();
?>

<?php foreach (OrderProductsTable::getProductsByCategory($category) as $product): ?>
    <div id="partrow" class="row">
        <div class="col-sm-3"><?php echo $product->part_number ?></div>
        <div class="col-sm-5"><?php echo $product->description ?></div>
        <div class="col-sm-2">$<?php echo $product->price ?></div>
        <div class="col-sm-2"><input id="qty-<?php echo $product->id ?>" type="text" name="qty" class="form-control form-control-sm qty" value="<?php echo ($cart->getQty($product->id) ? $cart->getQty($product->id) : '') ?>" data-value="<?php echo $product->part_number ?>" autocomplete="off" /></div>
        <div class="cb"></div>
    </div>
<?php endforeach; ?>
