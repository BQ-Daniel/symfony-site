<?php
// Include shopping cart
require_once(_DIR_CLASSES.'ShoppingCart.class.php');
$cart = new ShoppingCart();
?>
<?php foreach ($cart->getItems() as $item): ?>
    <ul id="cart-<?php echo $item['id'] ?>" class="mod">
        <li class="col-sm-1"><a href="" class="delete"><img src="/assets/images/delete.gif" title="Delete Item from Cart" /></a></li>
        <li class="col-sm-6"><?php echo $item['part'] ?></li>
        <li class="col-sm-2"><?php echo $item['qty'] ?></li>
        <li class="col-sm-2">$<?php echo $cart->getPrice($item['id']) ?></li>
        <div class="cb"></div>
    </ul>
<?php endforeach; ?>
