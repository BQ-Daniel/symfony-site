<?php $this->extend($extendDir . 'layoutapps'); ?>
<h2>Order Summary</h2>

<form id="submitForm" action="/order/submit" method="post">

<div class="card-deck">
  <div class="card">
  <div class="card-header">
    Invoice to
  </div>
  <div class="card-body">
        <?php if ($invoiceAddress): ?>
            <?php echo $invoiceAddress->name ?><br />
            <?php echo nl2br($invoiceAddress->getAddress()) ?>
        <?php else: ?>
        Unknown, please contact BrakeQuip.
        <?php endif; ?>
        <br /><br />
        Ordered by <?php echo $order['ordered_by'] ?><br>
        Shipping via <?php if ($order['shipping'] != 'Other'): ?>
            <?php echo $order['shipping'] ?>
        <?php else: ?>
            <?php echo $order['shipping_other'] ?>
        <?php endif; ?>

  </div>
</div>

<div class="card">
  <div class="card-header">
    Shipping Address
  </div>
  <div class="card-body">
    <textarea name="deliveryAddress" onchange="updateAddress(this)" rows="4" class="form-control"><?php echo $deliveryAddressString ?></textarea>
  </div>
</div>

<div class="card">
  <div class="card-header">
    Comments
  </div>
  <div class="card-body">
    <textarea name="comments" rows="4" class="form-control"></textarea>
  </div>
</div>
</div>

<div id="summary" class="col py-5">
  <div class="row">
      <div class="col-sm-6">
        <div class="row ordersummary">
          <div class="col-4">Part #</div>
          <div class="col-3">Unit Price</div>
          <div class="col-2">Qty</div>
          <div class="col-3">Sub Total</div>
        </div>
        <?php foreach ($cart->getItems() as $item): ?>
        <div class="row orderdetails">
          <div class="col-4"><?php echo $item['part'] ?></div>
          <div class="col-3">$<?php echo $item['price'] ?></div>
          <div class="col-2"><?php echo $item['qty'] ?></div>
          <div class="col-3">$<?php echo $cart->getPrice($item['id']) ?></div>
        </div>
      <?php endforeach; ?>
      </div>

      <div class="col-sm-3">
        <strong>Weight:</strong> <span><?php echo $cart->getTotalWeight() ?>kg</span><br>
        <strong>Total:</strong> <span>$<?php echo $cart->getTotalPrice() ?></span><br><br>
        <small>All prices are plus GST & freight (if applicable)</small>
      </div>

      <div class="col-sm-3" >
        <input id="submitOrderBtn" type="submit" name="submit" value="Process My Order" class="btn btn-success btn-block" onclick="disabledonbeforeunload=true;"/>
        <input type="button" name="edit" value="Edit Order" class="btn btn-warning btn-block" onclick="location.href = '/order/'; return false;" onclick="disabledConfirm_exit=true;" />
        <input type="button" name="cancel" value="Cancel Order" class="btn btn-danger btn-block" onclick="if (confirm('Are you sure you want to cancel this order?')) { location.href = '/order/cancel'; } return false;" />
      </div>
  </div>
</form>
</div>
<div id="stopCreditDialog" style="display: none">
    <?php include('stopCreditTerms.php'); ?><br><br>
    <p><input type="button" name="agree" value="I Agree" class="btn btn-success" onclick="$('#stopCreditDialog').dialog('close'); $('#submitForm').unbind('submit'); $('#submitOrderBtn').click();" /> <input type="button" name="close" value="Cancel" class="btn btn-danger" onclick="$('#stopCreditDialog').dialog('close'); return false;" /></p>
</div>



<script>
    $(function() {
        $("#stopCreditDialog").dialog({modal: true, width:500, autoOpen: false});

        <?php if ($user->stop_credit == 1): ?>
        $('#submitForm').submit(function () {
            $("#stopCreditDialog").dialog('open');
            return false;
        });
        <?php endif; ?>
    });
</script>
<script>
// window.onbeforeunload = function(){
//   return 'You have not yet processed your order.';
// };
</script>
