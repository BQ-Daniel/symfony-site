<?php $this->extend($extendDir.'layout2'); ?>
<?php $this->set('title', 'Find a Brake Hose Manufacturer') ?>
<?php $this->set('metaDescription', 'Brake Hose Manufacturers in Australia') ?>

<!-- css -->
<link href="/assets/css/locator.css" rel="stylesheet" type="text/css" />


<h2>Find a Manufacturer</h2>

    <div id="store-locator-container">
      <form id="user-location" action="#" method="post">
      <div class="input-group mb-3" id="form-container">
          <input type="text" class="form-control" name="address" id="address" placeholder="Enter your suburb and state" aria-label="Enter your suburb and state" aria-describedby="button-addon2" style="margin: 0;width: auto;">
          <div class="input-group-append">
            <input class="btn btn-sm btn-danger" type="submit" id="button-addon2" value="Search">
          </div>
        </form>
      </div>



<!--      <div id="form-container">
        <form id="user-location" method="post" action="#">
            <input required type="text" name="address" id="address" class="" placeholder="Enter your suburb and state" onfocus="if(this.value == 'Enter your suburb and state') { this.value = ''; }" value="Enter your suburb and state"/>
            <input id="submit" class="submit" type="submit" value="Search" style="padding: 9px 10px;">
        </form>
      </div>
    -->

      <div id="map-container">
        <div id="map"></div>
        <div id="loc-list">
            <ul id="list"></ul>
        </div>
      </div>
    </div>


    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="/assets/js/handlebars-1.0.0.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjyxNfJV-GStdtC23GeDbqHOvzVSVZc1E&libraries=places"></script>
    <script src="/assets/js/jquery.storelocator.min.js"></script>
      <script>

$(function() {
  $('#user-location').on('submit', function(e){

    //Stop the form submission
    e.preventDefault();
    //Get the user input and use it
    var userinput = $('form #address').val();

    if (userinput == "")
      {
        alert("The input box was blank.");
        return false;
      }

  });
});

        $(function() {
          $('#map-container').storeLocator({
              dataType: 'json',
              dataLocation: '/locations',
              infowindowTemplatePath: '/templates/find-manufacturer/infowindow-description.html',
              listTemplatePath: '/templates/find-manufacturer/location-list-description.html',
              lengthUnit: 'km',
              listColor2: 'ffffff',
              storeLimit: 10,
              originMarker: true
          });

//          var input = /** @type {HTMLInputElement} */
//          (document.getElementById('address'));
//          var autocomplete = new google.maps.places.Autocomplete(input);

        });

        function initMap() {
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -33.8688, lng: 151.2195},
            zoom: 13
          });
          var card = document.getElementById('form-container');
          var input = document.getElementById('address');
          var types = document.getElementById('type-selector');
          var strictBounds = document.getElementById('strict-bounds-selector');

          map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

          var autocomplete = new google.maps.places.Autocomplete(input);

          // Bind the map's bounds (viewport) property to the autocomplete object,
          // so that the autocomplete requests use the current map bounds for the
          // bounds option in the request.
          autocomplete.bindTo('bounds', map);

          // Set the data fields to return when the user selects a place.
          autocomplete.setFields(
              ['address_components', 'geometry', 'icon', 'name']);

          var infowindow = new google.maps.InfoWindow();
          var infowindowContent = document.getElementById('infowindow-content');
          infowindow.setContent(infowindowContent);
          var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
          });

          autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              // User entered the name of a Place that was not suggested and
              // pressed the Enter key, or the Place Details request failed.
              window.alert("No details available for input: '" + place.name + "'");
              return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
              address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
              ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
          });

          // Sets a listener on a radio button to change the filter type on Places
          // Autocomplete.
          function setupClickListener(id, types) {
            var radioButton = document.getElementById(id);
            radioButton.addEventListener('click', function() {
              autocomplete.setTypes(types);
            });
          }

          setupClickListener('changetype-all', []);
          setupClickListener('changetype-address', ['address']);
          setupClickListener('changetype-establishment', ['establishment']);
          setupClickListener('changetype-geocode', ['geocode']);

          document.getElementById('use-strict-bounds')
              .addEventListener('click', function() {
                console.log('Checkbox clicked! New state=' + this.checked);
                autocomplete.setOptions({strictBounds: this.checked});
              });
        }

      </script>
