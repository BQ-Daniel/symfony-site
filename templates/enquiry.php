<?php $this->extend($extendDir.'layoutapps') ?>
<?php $this->set('title', 'Contact BrakeQuip') ?>
<?php $this->set('metaDescription', 'Contact BrakeQuip') ?>

<h2>Contact BrakeQuip</h2>

<!-- <div id="contact-choice">
	<h3>Enquiry type</h3>
	<span class="btn default" style="margin:20px 20px 20px 0;">Becoming a brake hose manufacturer</span>
	<a href="general-enquiry" class="btn red">I just have a general enquiry</a>
</div> -->

<?
// If the submit button has been hit, send mail
if (isset($_POST['submit']))
{
    // Check required fields arnt empty
    $requiredFields = array( 'name', 'bname', 'phone', 'email', 'address', 'suburb', 'state', 'pcode', 'usebh', 'suppliers');
    $error = false;
    foreach ($requiredFields as $field)
    {
        if (isset($_POST[$field]) && empty($_POST[$field])) {
            $error = "Please make sure the required fields are not empty.<br /><br />";
            break;
        }
        if (strpos($_POST['enquiry'], "http") !== false){
        $error = "<p style=text-align:center;>BrakeQuip thanks you for not spamming us.</p><br /><br />";
        break;
      }
    }

    // If no error, send mail
    if (!$error) {
			$headers="From: john@brakequip.com.au <$email>\r\n";
			$headers.="MIME-Version: 1.0\r\n";
			$headers.="Content-Type: text/html; charset=ISO-8859-1\r\n";
			$body="
                <font face=\"verdana\" size=\"1\" color=\"#000000\">
                <table border=\"0\" cellpadding=\"6\" cellspacing=\"0\">
                <tr>
                <td colspan=\"2\"><i><b>Personal Information</b></i></td></tr>
                <tr>
                <td>Name:</td><td>$_POST[name]<br /></td></tr>
                <tr>
                <td>Phone number:</td><td> $_POST[phone]<br /></td></tr>
                <tr>
                <td>Email:</td><td> $_POST[email]<br /></td></tr>
                <tr>
                <td>Postal Address:</td><td> $_POST[address], $_POST[suburb]<br />
                $_POST[state], $_POST[pcode]<br /></td></tr>

                <tr>
                <td colspan=\"2\"><i><b>Tell us about your business</b></i></td></tr>
                <tr>
                <td>Business name:</td><td> $_POST[bname]<br /></td></tr>
                <tr>
                <td>Do they use <br />Brake Hoses:</td><td> $_POST[usebh]<br /></td></tr>
                <tr>
                <td>Their suppliers:</td><td> $_POST[suppliers]<br /></td></tr>
				<tr>
				<td colspan=\"2\"><i><b>Company type</b></i></td></tr>
				<tr><td>";
			foreach($_POST as $k => $v) {
				/* If post is a checkbox */
				if(substr($k,0,2) == "c_") {
					if(!empty($v))
						$body.="$v<br />";
								}
							}
				$body.="</td>
				</tr>
                </table></font>";
                mail("john@brakequip.com.au", "Interested in becoming a manufacturer", $body,$headers);
                $formComplete = true;
    }
}

?>
<?php if (isset($formComplete)): ?>
<h2>Thank-you</h2>Your submission has been received. If required, we will be in contact with you shortly.<br />
<?php else: ?>
<?php if (isset($error)): ?>
<font color=red><?php echo $error ?></font>
<?php endif; ?>


<div id="enquiry">
<form method="post">
  <ul style="float: left; width: 50%;">
  <h3>Contact information</h3>
	<li><input class="input" type="TEXT" name="name" size="30" value="" placeholder="Name *"></li>
	<li><input class="input" type="TEXT" name="bname" size="30" value="" placeholder="Business Name *"></li>
	<li><input class="input" type="TEXT" name="phone" size="30" value="" placeholder="Phone # *"></li>
	<li><input class="input" type="TEXT" name="email" size="30" value="" placeholder="E-mail *"></li>
	<li><input class="input" type="TEXT" name="address" size="30" value="" placeholder="Street Address *"></li><li><input class="input" type="TEXT" name="suburb" size="30" value="" placeholder="Suburb *"></li><li><input class="input" type="TEXT" name="state" size="30" value="" placeholder="State *"></li>
	<li><input class="input" type="TEXT" name="pcode" size="30" value="" placeholder="Postcode *"></li>
	</ul>
  <ul style="float: right; width: 50%;">
  <h3>About your business</h3>
  <li><input name="usebh" type="text" class="input" value="" size="30" placeholder="How often do you use brake hoses? *"></li>
  <li><input name="suppliers" type="text" class="input" value="" size="30" placeholder="Current brake hose suppliers *"></li>
  <li><label style="display: block; width: 100%; margin-bottom: 10px;">Type of business (required field)</label>
			<ul id="typeof">
				<li><input type="checkbox" name="c_spareparts" value="Spare Parts"><label>Spare parts</label></li>
				<li><input type="checkbox" name="c_repair" value="Brake repair specialist"><label>Brake repair specialist</label></li>
				<li><input type="checkbox" name="c_hydraulic" value="Hydraulic shop"><label>Hydraulic shop</label></li>
				<li><input type="checkbox" name="c_general" value="General workshop"><label>General workshop</label></li>
				<li><input type="checkbox" name="c_remanufacturer" value="Remanufacturer"><label>Remanufacturer</label></li>
				<li><input type="checkbox" name="c_wholesaler" value="Wholesale distributor"><label>Wholesale distributor</label></li>
				<li><input type="checkbox" name="c_home" value="Home mechanic"><label>Home mechanic</label></li>
				<li><input type="checkbox" name="c_franchise" value="Franchise group"><label>Franchise group</label></li>
				<li><input type="checkbox" name="c_other" value="Other"><label>Other</label></li>
			</ul>
			</li>
</ul>
<span id="submit"><input type="submit" class="btn red" name="submit" value="SEND"></span>
</form>
</div>

<?php endif; ?>
<script>
$(function() {
    $('#contact-select').change(function() {
        var option = $(this).find('option:selected');
        $('#dvd').toggle(option.hasClass('dvd'));
        $('#general').toggle(option.hasClass('general'));
    }).change();
});
</script>
