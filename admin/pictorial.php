<?php
require_once('include/load.php');
?>
<b>Manage Pictorial</b><br />
<a href="javascript:void(0)" onclick="showPage('pictorial.php');">List</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_upload.php');">Upload</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_chapters.php');">Chapters</a>

<div style="height: 10px;"></div>

<?php
// UPDATE FUNCTION
if (isset($_POST['submit']) && count($_POST['page_no']) > 0)
{
	$pageNumbers = $_POST['page_no'];
	asort($pageNumbers);

	$i = 0;
	foreach ($pageNumbers as $id => $value) {
		$i++;
		// Get old value
		$oldNo = $_POST['old_page_no'][$id];
		Database::query("UPDATE PICTORIAL_PAGES SET PAGE_NO = '".$i."' WHERE ID = '".$id."'");
		// Save new images with new page_no
		copy('../pictorial/pages/thumbnail/page-'.$oldNo.'.jpg', '../pictorial/pages/thumbnail/new-page-'.$i.'.jpg');
		copy('../pictorial/pages/large/page-'.$oldNo.'.jpg', '../pictorial/pages/large/new-page-'.$i.'.jpg');
		copy('../pictorial/pages/page-'.$oldNo.'.jpg', '../pictorial/pages/new-page-'.$i.'.jpg');
	}

	// Now remove all old images, and rename the new ones back
	$d = dir("../pictorial/pages/");
	while (false !== ($entry = $d->read())) {
		if ($entry == "." || $entry == "..") {
			continue;
		}
		if (is_dir("../pictorial/pages/".$entry)) {
			continue;
		}
		if (substr($entry, 0, 4) != "new-") {
			// Remove old images
			unlink("../pictorial/pages/".$entry);
			unlink("../pictorial/pages/thumbnail/".$entry);
			unlink("../pictorial/pages/large/".$entry);
			continue;
		}
	}
	$d->close();


	// Now rename all the new images
	$q = Database::query("SELECT PAGE_NO FROM PICTORIAL_PAGES ORDER BY PAGE_NO");
	while ($result = Database::fetch_obj($q))
	{
		// Rename images 
		rename("../pictorial/pages/thumbnail/new-page-".$result->PAGE_NO.".jpg", "../pictorial/pages/thumbnail/page-".$result->PAGE_NO.".jpg");
		rename("../pictorial/pages/large/new-page-".$result->PAGE_NO.".jpg", "../pictorial/pages/large/page-".$result->PAGE_NO.".jpg");
		rename("../pictorial/pages/new-page-".$result->PAGE_NO.".jpg", "../pictorial/pages/page-".$result->PAGE_NO.".jpg");
	}
}

// DELETE FUNCTION
if (isset($_REQUEST['delete']) && !empty($_REQUEST['delete']))
{
	$id = v('delete');

	// Get position
	$q = Database::query("SELECT NAME, PAGE_NO FROM PICTORIAL_PAGES WHERE ID = '".$id."'");
	$result = Database::fetch_obj($q);
	$pageNo = v("page_no");
	$pageName = v("name");

	Database::query("DELETE FROM PICTORIAL_PAGES WHERE ID = '".$id."'");

	// Remove old images
	unlink("../pictorial/pages/page-".$pageNo.".jpg");
	unlink("../pictorial/pages/thumbnail/page-".$pageNo.".jpg");
	unlink("../pictorial/pages/large/page-".$pageNo.".jpg");
	
	// Get highest page no
	$q         = Database::query("SELECT PAGE_NO FROM PICTORIAL_PAGES ORDER BY PAGE_NO DESC LIMIT 1"); 
	$count     = (mysql_num_rows($q) > 0 ? Database::result($q) : 0);
	// Update other page no's down one, as we removed one
	Database::query("UPDATE PICTORIAL_PAGES SET PAGE_NO = (PAGE_NO - 1) WHERE PAGE_NO > $pageNo");

	// Now loop through images from the remove page number and rename
	for ($i=$pageNo; $i<$count; $i++)
	{
		$x = $i + 1;
		// Rename images 
		rename("../pictorial/pages/thumbnail/page-$x.jpg", "../pictorial/pages/thumbnail/page-$i.jpg");
		rename("../pictorial/pages/large/page-$x.jpg", "../pictorial/pages/large/page-$i.jpg");
		rename("../pictorial/pages/page-$x.jpg", "../pictorial/pages/page-$i.jpg");
	}

	// Write new book settings js file to remove image from javscript array
	writeNewBookSettingsJS();

	echo '<p class="okMsg">'.$pageName.' has been removed</p>'."\n";
}
?>

<form action="products.php?page=pictorial" method="post">
<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<tr bgcolor="#e4e4e4">
		<td width="50"><b>ORDER NO</b></td>
		<td width="30"><b>PAGE</b></td>
		<td width="200"><b>NAME</b></td>
		<td><b>OPTION</b></td>
	</tr>
</table>
<div style="height: 400px; overflow-y: scroll;">
	<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<?php $q = Database::query("SELECT * FROM PICTORIAL_PAGES ORDER BY PAGE_NO ASC"); ?> 
		<?php while($result = Database::fetch_obj($q)): ?>
		<tr valign="top">
			<td width="50"><input type="hidden" name="old_page_no[<?php echo v("id") ?>]" value="<?php echo v("page_no") ?>" /><input type="text" name="page_no[<?php echo v("id") ?>]" value="<?php echo v("page_no") ?>" size="3" /></td>
			<td width="30"><a href="../pictorial/pages/large/page-<?php echo v("page_no") ?>.jpg?<?php echo rand(0,9999999) ?>" target="_blank"><img src="../pictorial/pages/thumbnail/page-<?php echo v("page_no") ?>.jpg?<?php echo rand(0,9999999) ?>" border="0" title="Click to enlarge" /></a></td>
			<td width="200"><?php echo v("name") ?></td>
			<td><a href="products.php?page=pictorial_upload&id=<?php echo v("id") ?>">Update image</a>, <a href="products.php?page=pictorial&delete=<?php echo v("id") ?>">Delete</a></td>
		</tr>
		<?php endwhile; ?>
	</table>
</div>
<div style="background: #e4e4e4; padding: 2px; text-align: center;"><input type="submit" name="submit" value="Update" /></div>