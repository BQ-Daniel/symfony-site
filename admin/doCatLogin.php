<?php
require_once('include/load.php');

	// Check if the username / link are correct
	if (!empty($_REQUEST['p']))
	{
		$_REQUEST['p'] = addslashes($_REQUEST['p']);
		$q = Database::query("SELECT USER_ID FROM USER_PASSWORDS WHERE DECODE(PASS, '".ENCRYPT_PASS."') = '".$_REQUEST['p']."'");
        
        $_REQUEST['id'] = (mysql_num_rows($q) > 0 ? Database::result($q) : 0);
		if ($_REQUEST['id'] > 0) {
			
			// Register session
			session_register('userId');
            session_register('allowOrder');
			$_SESSION['userId'] = $_REQUEST['id'];
            // Get username
            $sql = "SELECT USERNAME FROM USERS WHERE ID = '".$_REQUEST['id']."'";
            $q   = Database::query($sql);
            $_SESSION['user'] = @mysql_result($q,0);
                

			// Remove any shopping cart info that could be from a previous browser session
			unset($_SESSION['shopping_cart']);
			
			// Add user session
			Database::query("INSERT INTO USER_SESSIONS SET USER_ID =  '".$_REQUEST['id']."', SESSION = '".session_id()."', LAST_ACTIVE = DATE_ADD(NOW(), INTERVAL 15 HOUR)");

			// Remove password
			Database::query("DELETE FROM USER_PASSWORDS WHERE USER_ID = '".$_REQUEST['id']."'");

            // If postcode isn't empty they are allowed to view oreder section
            $q = Database::query("SELECT COUNT(*) FROM USER_ADDRESS WHERE USER_ID = '".$_REQUEST['id']."' AND ADDRESS_TYPE = 'invoice' AND POSTCODE <> ''");
            $_SESSION['allowOrder'] = ( Database::result($q) > 0 ); 


			// Add stats
			Database::query("UPDATE USERS SET LAST_LOGIN = DATE_ADD(NOW(), INTERVAL 15 HOUR), TOTAL_LOGINS = TOTAL_LOGINS + 1 WHERE ID = '".$_REQUEST['id']."'");
			Database::query("INSERT INTO USER_LOGINS SET USER_ID = '".$_REQUEST['id']."', IP = '".$_SERVER['REMOTE_ADDR']."', LOGIN_TIME = DATE_ADD(NOW(), INTERVAL 15 HOUR)");
            session_register('user_logins_id');
            $_SESSION['user_logins_id'] = mysql_insert_id();
			?>
			<script language="JavaScript">
				// Clear cookies
				eraseCookie('ordered-by');
				eraseCookie('shipping');
				eraseCookie('shipping-other');
			</script>
			<meta http-equiv="Refresh" content="5; url=../apps.php">
			<h2>LOGIN SUCCESSFUL - PLEASE WAIT</h2>
			You are now being redirected to the BrakeQuip Electronic Catalogue.
			<?php
		}
		else {
			?>
			<h2 color="red">You have encounted an error</h2>
			You have not followed the link correctly, or your access link to the catalogue has expired. Please try again.
			<?php
		}
	}
	else {
		?>
		<h2 color="red">You have encounted an error</h2>
		You have not followed the link correctly, please try again.
		<?php
	}
	?>