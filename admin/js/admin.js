

function removeAllOptions(selectbox)
{
	var i;
	selectbox = $(selectbox);
	for(i=selectbox.options.length-1;i>=0;i--)
	{
		selectbox.remove(i);
	}
}

function AddSelectOption(selectObj, text, value, isSelected) 
{
    if (selectObj != null && selectObj.options != null)
    {
        selectObj.options[selectObj.options.length] = new Option(text, value, false, isSelected);
    }
}

function loadCategories(category)
{
	if (category == 'MODEL')
	{
		div = 'productModel';
		removeAllOptions(div);
		if ($('productMake').value != '') {
			removeAllOptions('productSubModel');
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MAKE: $('productMake').value },
					onSuccess: function(transport) { 
						if (transport.responseText != '') { 
							loadOptions(div, transport.responseText);
						}
					}
				}
			);
		}
	}

	if (category == 'SUB_MODEL')
	{
		div = 'productSubModel';
		removeAllOptions(div);
		if ($('productModel').value != '') {
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MODEL: $('productModel').value },
					onSuccess: function(transport) { 
						if (transport.responseText != '') { 
							loadOptions(div, transport.responseText);
						}
					}
				}
			);
		}
	}
}

function loadCats(category)
{
	if (category == 'MAKE')
	{
		div = 'categoryMake';
		removeAllOptions(div);
		new Ajax.Request('categoryOptions.php?' + Math.floor(Math.random()*50000), { method: 'post', parameters: { submit: 'submit' }, 
				onSuccess: function(transport) { 
					if (transport.responseText != '')
					{ 
						$('MODEL').hide();
						$('SUB_MODEL').hide();
						loadOptions(div, transport.responseText);
					}
				}
			}
		);
	}
	else if (category == 'MODEL')
	{
		if ($('categoryMake').value != '')
		{
			// List products for make
			listProducts('MAKE');

			div = 'categoryModel';
			removeAllOptions(div);
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MAKE: $('categoryMake').value },
					onSuccess: function(transport) { 
						$('deleteMake').show();
						if (transport.responseText != '')
						{ 
							loadOptions(div, transport.responseText);
							$('MODEL').show();
							$('SUB_MODEL').hide();
						} else {
							$('MODEL').hide();
						}
						$('MODEL').show();
					}
				}
			);
		}
	}
	else if (category == 'SUB_MODEL')
	{
		if ($('categoryModel').value != '')
		{
			// List products for model
			listProducts('MODEL');

			div = 'categorySubModel';
			removeAllOptions(div);
			new Ajax.Request('categoryOptions.php', { method: 'post', parameters: { MODEL: $('categoryModel').value },
					onSuccess: function(transport) { 
						$('deleteModel').show();
						if (transport.responseText != '')
						{ 
							loadOptions(div, transport.responseText);
						}
						else {
							$('SUB_MODEL').hide();
						}
						$('SUB_MODEL').show();
					}
				}
			);
		}
	}
}

function loadOptions(div, str)
{
	options = str.split(',');

	for (i=0; i<options.length; i++) {
		option = options[i].split(':');
		value = option[0];
		name = option[1].replace(/^\s+|\s+$/g,"");
		AddSelectOption($(div), name, value, false);		
	}
}


function addCategory(category)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('makeResult', 'manCategory.php', { method: 'post', parameters: { make: $('newMake').value } } )
		loadCats(category);

	}
	else if (category == 'MODEL')
	{
		new Ajax.Updater('modelResult', 'manCategory.php', { method: 'post', parameters: { model: $('newModel').value, editMake: $('categoryMake').value } } )
		loadCats(category);
	}
	else if (category == 'SUB_MODEL')
	{
		new Ajax.Updater('subModelResult', 'manCategory.php', { method: 'post', parameters: { subModel: $('newSubModel').value, editMake: $('categoryMake').value, editModel: $('categoryModel').value } } )
		loadCats(category);
	}
}

function deleteCategory(category)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('makeResult', 'manCategory.php', { method: 'post', parameters: { deleteMake: $('categoryMake').value } } )
		loadCats(category);
		$('modelResult').innerHTML = '';
		$('subModelResult').innerHTML = '';
	}

	if (category == 'MODEL')
	{
		new Ajax.Updater('modelResult', 'manCategory.php', { method: 'post', parameters: { deleteModel: $('categoryModel').value } } )
		loadCats(category);
		$('subModelResult').innerHTML = '';
		$('SUB_MODEL').hide();
	}
}

function listProducts(category, sub_model)
{
	if (category == 'MAKE')
	{
		new Ajax.Updater('listProducts', 'displayProducts.php', { method: 'post', parameters: { make: $('productMake').value } } )
	}
	if (category == 'MODEL')
	{
		new Ajax.Updater('listProducts', 'displayProducts.php', { method: 'post', parameters: { make: $('productMake').value, model: $('productModel').value } } )
	}
	if (category == 'SUB_MODEL')
	{
		var sub_model = (sub_model == null) ? $('productSubModel').value : sub_model;
		new Ajax.Updater('listProducts', 'displayProducts.php', { method: 'post', parameters: { model: $('productModel').value,submodel: sub_model } } )
	}
}

function submitProduct()
{
	new Ajax.Updater('content', 'addProduct.php', { parameters: $('addProduct').serialize(true), asynchronous:true, evalScripts:true} );
}

function viewProduct(id)
{
	new Ajax.Updater('content', 'addProduct.php?PRODUCT_ID=' + id );
}

function submitForm(page, form, div)
{
	var div = (div == null) ? "content" : div;
	var form = (form == null) ? "form" : form;
	new Ajax.Updater(div, page, { parameters: $(form).serialize(true), onLoading:function() { $('loadingContent').show(); $(div).hide() }, onSuccess:function() { $('loadingContent').hide(); $(div).show(); }, asynchronous:true, evalScripts:true} );
}

function showPage(page, div)
{
	var div = (div == null) ? "content" : div;
	new Ajax.Updater(div, page, { evalScripts: true });
}

//
// getPageScroll()
// Returns array with x,y page scroll values.
// Core code from - quirksmode.org
//
function getPageScroll(){

	var yScroll;

	if (self.pageYOffset) {
		yScroll = self.pageYOffset;
	} else if (document.documentElement && document.documentElement.scrollTop){	 // Explorer 6 Strict
		yScroll = document.documentElement.scrollTop;
	} else if (document.body) {// all other Explorers
		yScroll = document.body.scrollTop;
	}

	arrayPageScroll = new Array('',yScroll) 
	return arrayPageScroll;
}



//
// getPageSize()
// Returns array with page width, height and window width, height
// Core code from - quirksmode.org
// Edit for Firefox by pHaez
//
function getPageSize(){
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}


	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

function showBox()
{
	var arrayPageSize = getPageSize();
	var arrayPageScroll = getPageScroll();
	
	$('overlay').style.height = ((arrayPageSize[1] - 1) + 'px');
	$('overlay').show();
	// center lightbox and make sure that the top and left values are not negative
	// and the image placed outside the viewport
	var lightboxTop = 100;
	var lightboxLeft = ((arrayPageSize[0] - 550) / 2);

	$('box').style.top = (lightboxTop < 0) ? "0px" : lightboxTop + "px";
	$('box').style.left = (lightboxLeft < 0) ? "0px" : lightboxLeft + "px";
	$('box').show();

	selects = document.getElementsByTagName("select");
    for (i = 0; i != selects.length; i++) {
            selects[i].style.display = "none";
    }

}

function closeBox()
{
	$('box').hide();
	$('overlay').hide();
	$('boxContent').innerHTML = '';
	selects = document.getElementsByTagName("select");
    for (i = 0; i != selects.length; i++) {
            selects[i].style.display = "inline";
    }
}

function displayOrder(orderId)
{
	showBox();
	new Ajax.Updater('boxContent', 'viewOrder.php', { parameters: { orderId: orderId }, onLoading:function() { $('boxLoading').style.display = 'block'; }, onSuccess:function() { $('boxLoading').style.display = 'none'; }, asynchronous:true, evalScripts:true} );
}


function validatePictorialUpload()
{
	var form = document.forms[0];
	if (form.name.value == '' || form.page_no.value == '' || form.image.value == '')
	{
		alert('Please make sure all fields are filled in');
		return false;
	}

	return true;
}