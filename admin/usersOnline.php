<?php require_once('include/load.php') ?>
<p><strong>List Online Users</strong></p>

<?php $q = Database::query("SELECT * FROM USER_SESSIONS us INNER JOIN USERS u ON us.USER_ID = u.ID WHERE NOW() < ADDTIME(LAST_ACTIVE, '00:05:00') GROUP BY us.USER_ID ORDER BY LAST_ACTIVE DESC") ?>
<p>The time now is: <b><?php echo date("G:i:s A") ?></b></p>
<table cellspacing="1" cellpadding="4" border="0" width="100%">
	<tr bgcolor="#e4e4e4">
		<td width="80"><b>Username</b></td>
		<td><b>Last Active</b></td>
	</tr>
	<?php while($result = Database::fetch_obj($q)): ?>
	<tr>
		<td><a href="javascript:void(0)" onclick="showPage('modifyUser.php?id=<?php echo $result->ID ?>')"><?php echo $result->USERNAME ?></a></td>
		<td><?php echo date("G:i:s A", strtotime($result->LAST_ACTIVE)) ?></td>
	</tr>
	<?php endwhile; ?>
</table>