<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'users')); 
?>
			<td id="left">
				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="javascript:void(0)" onclick="showPage('modifyUser.php');">Add user</a>
						<li> <a href="javascript:void(0)" onclick="showPage('listUsers.php');">List users</a>
						<li> <a href="javascript:void(0)" onclick="showPage('reports.php');">Reports</a>
						<li> <a href="javascript:void(0)" onclick="showPage('usersOnline.php');">Who's online</a>
					</ul>
					<p>&nbsp;</p>
				</div>
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p> @ USERS</p>
				</div>
				
				<div id="content">
				<?php if (isset($_REQUEST['page'])): ?>
					<?php require_once($_REQUEST['page'].".php"); ?>
				<?php endif; ?>
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>