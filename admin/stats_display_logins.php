<div style="float: left; margin-right: 12px;">
    <img src="images/stats_logins.jpg" title="Logins" alt="Logins" border="0" />
</div>
<div style="float: left;">
    <?php include_template('stats/filter', array('page' => 'display_logins'));  ?>
</div>

<div style="clear: both; height: 15px;"></div>

<?php
if (!isset($_REQUEST['id']) || empty($_REQUEST['id'])) {
	?>
	<b>Invalid User</b><br />
	<a href="stats.php?page=logins">Go Back to Report</a>

	<?php
	exit;
}

// Total results per page
$perPage = 25;

if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}

// Get username
$q = Database::query("SELECT username FROM USERS WHERE ID = '".(int)$_REQUEST['id']."'");
$_REQUEST['username'] = Database::result($q);

// Get logins for the user
$fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? split('/', $_REQUEST['from']) : array());
$toDate = (substr_count($_REQUEST['to'], '/') == 2 ? split('/', $_REQUEST['to']) : array());

$orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.($_REQUEST['orderBy'] == 'duration' ? 'LOGIN_TIME - LOGIN_END' : $_REQUEST['orderBy']).' '.$_REQUEST['sort'].' ' : ' ');

// Get total results
$cq = Database::query("SELECT COUNT(*) FROM USERS u LEFT JOIN USER_LOGINS ul ON u.id = ul.user_id WHERE ul.user_id = '".$_REQUEST['id']."' AND LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY)");
$totalResults = Database::result($cq);

$q = Database::query("SELECT ul.IP, ul.LOGIN_TIME, ul.LOGIN_END FROM USERS u LEFT JOIN USER_LOGINS ul ON u.id = ul.user_id WHERE ul.user_id = '".$_REQUEST['id']."' AND LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) ".$orderBy."LIMIT $_REQUEST[p], $perPage");

$sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');

$url = "id=".$_REQUEST['id']."&orderBy=".$_REQUEST['orderBy']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
$url2 = "id=".$_REQUEST['id']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
?>
<strong>Login durations for <?php echo $_REQUEST['username'] ?></strong>
<div align="right">
	<form id="form" action="stats.php?page=display_logins" method="post"> 
	<input type="hidden" name="from" value="<?php echo $_REQUEST['from'] ?>" />
	<input type="hidden" name="to" value="<?php echo $_REQUEST['to'] ?>" />
	<input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
	<input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
	<input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>" />
		Page: <select name="p" onChange="this.form.submit();">
		<?php
		$num=0;
		for($i=0;$i<ceil($totalResults/$perPage);$i++)
		{
			echo '<option value="'.($num).'"';
			if($_REQUEST['p'] == $num) {
				echo " selected";
			}
			echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
			$num=$num+$perPage;
		}
		?>
		</select>
	</form>
</div>
<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<tr>
		<td class="tdCell">IP / Host</td>
		<td class="tdCell"><a href="stats.php?page=display_logins&orderBy=duration&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;"> Duration</a></td>
	</tr>
	<?php if ($totalResults > 0): ?>
		<?php while($result = Database::fetch_obj($q)): ?>
	<tr>
		<td><?php echo $result->IP ?></td>
		<td><?php echo getTimeDifference($result->LOGIN_TIME, $result->LOGIN_END) ?></td>
	</tr>
		<?php endwhile; ?>
	<?php else: ?>
	<tr>
		<td colspan="3" align="center">No results</td>
	</tr>
	<?php endif; ?>
</table>
<p><a href="stats.php?page=logins&from=<?php echo $_REQUEST['from'] ?>&to=<?php echo $_REQUEST['to'] ?>&orderBy=username">&lt; Show logins between selected dates</a></p>