<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'import')); 
?>
			<td id="left">
				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="javascript:void(0)" onclick="showPage('importProducts.php');">Import Products</a>
						<li> <a href="javascript:void(0)" onclick="showPage('importUsers.php');">Import Users</a>
					</ul>
					<p>&nbsp;</p>
				</div>
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p> @ IMPORT</p>
				</div>
				
				<div id="content" <?php echo (isset($_REQUEST['page']) ? "onLoad=\"showPage('".$_REQUEST['page'].".php');\"" : '') ?>>
				<?php if (isset($_REQUEST['page'])): ?>
					<?php echo javascript_tag("showPage('".$_REQUEST['page'].".php');") ?>
				<?php endif; ?>
					
					
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>