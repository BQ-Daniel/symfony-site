<?php
// Load
include('include/load.php');
include('include/excelReader.php');

error_reporting(E_ALL);

?>
<html>
	<head>
		<title>Brakequip Administration : Import Users</title>
				<link rel="stylesheet" media="screen" href="style.css">
	</head>

	<body>
<?php
// Make sure file exists
if (!file_exists('tmp/'.$_REQUEST['file']))
{
	echo "There has been an error. Please upload the excel spreadsheet again.";
	exit;
}

$added = 0;
$updated = 0;

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('CP1251');
// What file to read
$data->read('tmp/'.$_REQUEST['file']);


// Put data into an array
$users		= array();
$ignored	= 0;
$added		= 0;
$updated	= 0;

// Start at line 2, as we dont want to add the headers
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
	$username = (isset($data->sheets[0]['cells'][$i][1]) ? trim($data->sheets[0]['cells'][$i][1]) : null);
	$email = (isset($data->sheets[0]['cells'][$i][2]) ? trim($data->sheets[0]['cells'][$i][2]) : null);

	if (!empty($username) && !empty($email) && eregi('@', $email)) {
		$users[$username] = $email;
	}
	else {
		$ignored++;
	}
}

// Add to database
foreach ($users as $username => $email) 
{
	// Check if username exists
	$q = Database::query("SELECT ID FROM USERS WHERE USERNAME = '$username'");
	// add 
	if (Database::count_result($q) == 0) {
		Database::query("INSERT INTO USERS SET USERNAME = '$username', EMAIL = '$email', CREATED_AT = NOW()");
		$added++;
	}
	// update
	else {
		$userId = mysql_insert_id();
		Database::query("UPDATE USERS SET EMAIL = '$email' WHERE USERNAME = '$username'");
		$updated++;
	}
}
unlink('tmp/'.$_REQUEST['file']);

?>
<h1>Imported Users</h1>
<p>Here are the results for importing users.</p>

<table cellspacing="2" cellpadding="0" border="0">
	<tr>
		<td class="tdCell" width="100">Users Added</td>
		<td width="50"><?php echo $added ?></td>
		<td class="tdCell" width="100">Users Updated</td>
		<td width="50"><?php echo $updated ?></td>
		<td class="tdCell" width="100">Users Ignored</td>
		<td width="50"><?php echo $ignored ?></td>
	</tr>
</table>

<p>Go back to the Import users page</p>
<p><input type="button" name="back" value="Go Back" onClick="location.href='import.php?page=importUsers'" /></p>



	</body>
</html>