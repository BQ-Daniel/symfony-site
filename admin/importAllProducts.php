<?php
include('include/load.php');
include('include/excelReader.php');
// Excel Importing
require_once('classes/ExcelProducts.class.php');

set_time_limit(0);
	
function addFileToDatabase($file)
{
	$added = 0;
	$updated = 0;
	// ExcelFile($filename, $encoding);
	$data = new Spreadsheet_Excel_Reader();
	// Set output Encoding.
	$data->setOutputEncoding('CP1251');
	// What file to read
	$data->read($file);

        // Read excel document and get products
        $doc = new ExcelProducts();
        $doc->initialize($data);
        $subModels = $doc->getSubModels();


        // If Make doesn't exist, then add it
        $q = Database::query("SELECT MAKE_ID FROM MAKES WHERE MAKE = '".$doc->getMake()."'");
        if (Database::count_result($q) == 0) {
                // Add to database
                Database::query("INSERT INTO MAKES SET MAKE = '".$doc->getMake()."'");
                // Get the Make ID
                $makeId = mysql_insert_id();
        } else {
                $makeId = Database::result($q);
        }

        // If Model doesn't exist, then add it
        $model   = $doc->getModel();
        $modelId = 0;
        if (!empty($model)) {
                $q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MAKE_ID = '".$makeId."' AND MODEL = '".$doc->getModel()."'");
                if (Database::count_result($q) == 0) {
                        // Add to database
                        Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$doc->getModel()."'");
                        // Get the Make ID
                        $modelId = mysql_insert_id();
                } else {
                        $modelId = Database::result($q);
                }
        }
        else {
            $noMajorModel = true;
        }


        foreach ($subModels as $subModel => $years) {

                $subModel = trim(addslashes($subModel));
                // remove any double spaces
                $subModel = str_replace('  ', ' ', $subModel);

            // If no major model, we need to use sub model as the major model for this set
            if (isset($noMajorModel)) {
                $q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MAKE_ID = '".$makeId."' AND MODEL = '".$subModel."'");
                if (Database::count_result($q) == 0) {
                    // Add to database
                    Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."'");
                    // Get the Model ID
                    $modelId = mysql_insert_id();
                } else {
                    $modelId = Database::result($q);
                }  
            }

                // Check if sub model exists, if not add it
                $q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MODEL = '".$subModel."' AND PARENT_MODEL = '".$modelId."'");
                if (Database::count_result($q) == 0) {
                        Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."', PARENT_MODEL = '".$modelId."'");
                        $subModelId = mysql_insert_id();
                        // If there is no parent model, then make the submodel as the parent
                        if ($modelId == 0) {
                                Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."', PARENT_MODEL = '".$subModelId."'");
                                $subModelId = mysql_insert_id();
                        }
                } else {
                        $subModelId = Database::result($q);
                }

                // Delete all products under this sub model
                Database::query("DELETE FROM PRODUCTS WHERE MODEL_ID = '".$subModelId."'");

                foreach ($years as $year => $products) {

                        $year = trim(addslashes($year));
                        foreach ($products as $product) {
                                $bqNumber = str_replace(' ', '', v(BQ_NO, $product));

                                Database::query("INSERT INTO PRODUCTS SET MODEL_ID = '".$subModelId."', BQ_NUMBER = '".$bqNumber."', PART_NUMBER = '".addslashes(v(PART_NO, $product))."', COMMENT = '".addslashes(v(COMMENTS, $product))."', HOSE_POSITION = '".v(POS, $product)."', YEAR = '".$year."', ILL_NUMBER = '".v(ILL_NO, $product)."', DATE_ADDED = DATE_ADD(NOW(), INTERVAL 15 HOUR)");
                                $added++;
                        }
                }
        }

	return array('added' => $added, 'updated' => $updated, 'ignored' => $doc->getTotalIgnores());
}


define('PATH', dirname(__FILE__) . '/files/Live hose catalogue/');

function load($dir) {
    $mydir = opendir($dir);
    while (false !== ($file = readdir($mydir))) {
        if ($file != "." && $file != "..") {
            chmod($dir . $file, 0777);
            if (is_dir($dir . $file)) {
                chdir('.');
                echo "<b>Entering directory</b>: " . $dir . $file . "<br />\n";
                load($dir . $file . '/');
                echo "<b>Imported directory</b><br /><br />\n\n";
            } else {
                echo "<u>Importing file</u>: " . $file . "";
                $rs = addFileToDatabase($dir . $file);
                echo " - Added: " . $rs['added'] . ", Updated: " . $rs['updated'] . ", Ignored: " . $rs['ignored'] . "<br />\n";
            }
        }
    }
    closedir($mydir);
}

load(PATH);
?>