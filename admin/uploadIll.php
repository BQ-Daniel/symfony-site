<?php
require_once('include/load.php');
$mainDir = '../catalogue/products/';
?>					
<p><strong>Upload Illustration</strong></p>
<?php
					if (isset($_POST['upload'])) {
						    if (empty($_POST['ILL_NUMBER'])) {
								$errorMsg = "Please provide an Illustration Number";
							}
							elseif (!empty($_FILES['photo']['tmp_name']))
							{
								// if image isnt jpeg, give error
								$mime = $_FILES['photo']['type'];

								if ($mime == "image/jpeg" || $mime == "image/pjpeg") {
									$uploadPhoto = true;								
								}
								else {
									$errorMsg = "Photo image needs to be jpeg.";
								}
							}


							if (isset($uploadPhoto)) {
								$imgName = $_POST['ILL_NUMBER'].'.jpg';
								$imgName2 = $_POST['ILL_NUMBER'].'_s.jpg';
								if ($_FILES['photo']['name'] == $imgName) {
									if(move_uploaded_file($_FILES['photo']['tmp_name'], $mainDir.$imgName)) {
										resizeimg(650, 200, $mainDir.$imgName, $mainDir.$imgName2, 0);

										echo "<p class=okMsg>Illustration has been uploaded</p>";
									}	
								}
								else {
									$errorMsg = "The image name you uploaded doesn't match the illustration number you entered.";
								}
							}

							if (isset($errorMsg)) {
								unset($_POST['ILL_NUMBER']);
								echo "<p class=errMsg>$errorMsg</p>";
							} 
						}
						?>
					<form action="products.php?page=uploadIll" method="post" enctype="multipart/form-data">
					<?php if (!isset($_POST['delete']) && v('ILL_NUMBER') && file_exists('../catalogue/products/'.v('ILL_NUMBER').'_s.jpg')): ?>
						<p><a href="../catalogue/products/<?php echo v('ILL_NUMBER').'.jpg' ?>" target="_blank">Click here to view image</a></p>
					<?php endif; ?>

					Illustration Number <input type="text" name="ILL_NUMBER" /><br />	
					<input type="file" name="photo" size="50" /> <input type="submit" name="upload" value="Upload Image" />
					</form>

<form action="products.php?page=uploadIll" method="post">
<p><strong>Delete Illustration</strong></p>
<?php
if (isset($_POST['delete'])) {
	// Make sure image exists
	$imgName = $_POST['ILL_NUMBER'].'.jpg';
	$imgName2 = $_POST['ILL_NUMBER'].'_s.jpg';
	if (file_exists($mainDir.$imgName)) {
		@unlink($mainDir.$imgName);
		@unlink($mainDir.$imgName2);
		echo "<p class='okMsg'><b>Success</b>: Illustration ".$_POST['ILL_NUMBER']." image have been deleted.</p>";
	}
	else {
		echo "<p class='error'><b>Error</b>: Illustration ".$_POST['ILL_NUMBER']." does not exist.</p>";
	}
}
?>
Illustration Number <input type="text" name="ILL_NUMBER" value="" /> <input type="submit" name="delete" value="Delete Image" />
</form>