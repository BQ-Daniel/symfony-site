<?php
// Total results per page
$perPage = 15;
// Setup defaults
if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}
?>


<div style="float: left; margin-right: 12px;">
    <a href="stats.php?page=logins"><img src="images/stats_logins.jpg" title="Logins" alt="Logins" border="0" /></a>
</div>
<div style="float: left;">
    <?php include_template('stats/filter_with_username', array('page' => 'logins'));  ?>
</div>

<div style="clear: both; height: 15px;"></div>

<?php if (isset($_POST['submit']) || !empty($_REQUEST['orderBy']) || !empty($_REQUEST['p'])): ?>
    <?php if (!empty($_REQUEST['username'])): ?>
    <div style="width: 425px; float: right;">
        <?php include_template('stats/display_total_logins', array('perPage' => $perPage));  ?>
    </div>
    <?php else: ?>
        <?php include_template('stats/total_logins_for_each_user', array('perPage' => $perPage));  ?>
    <?php endif; ?>
<?php endif; ?>