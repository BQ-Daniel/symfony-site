<?php
include('include/load.php');

if (isset($_POST['make']) && !isset($_POST['model'])) {
	$count = "SELECT COUNT(*) FROM PRODUCTS P LEFT JOIN MODELS M ON M.MODEL_ID = P.MODEL_ID WHERE MAKE_ID = '".$_POST['make']."'";
	$query = "SELECT p.*, mks.*, pmds.*, mds.MODEL as SUB_MODEL FROM PRODUCTS p
LEFT JOIN MODELS mds ON p.MODEL_ID = mds.MODEL_ID
LEFT JOIN MAKES mks ON mds.MAKE_ID = mks.MAKE_ID
LEFT JOIN MODELS pmds ON mds.PARENT_MODEL =  pmds.MODEL_ID
WHERE mks.MAKE_ID = '".$_POST['make']."'";
}
elseif (isset($_POST['model']) && !isset($_POST['submodel']))
{
	$count = "SELECT COUNT(*) FROM PRODUCTS P LEFT JOIN MODELS M ON M.MODEL_ID = P.MODEL_ID WHERE MAKE_ID = '".$_POST['make']."' AND M.PARENT_MODEL = '".$_POST['model']."'";
	$query = "SELECT p.*, mks.*, pmds.*, mds.MODEL as SUB_MODEL FROM PRODUCTS p
LEFT JOIN MODELS mds ON p.MODEL_ID = mds.MODEL_ID
LEFT JOIN MAKES mks ON mds.MAKE_ID = mks.MAKE_ID
LEFT JOIN MODELS pmds ON mds.PARENT_MODEL =  pmds.MODEL_ID
WHERE mks.MAKE_ID = '".$_POST['make']."' AND pmds.MODEL_ID = '".$_POST['model']."'";
}
elseif (isset($_POST['model']) && isset($_POST['submodel']))
{
	$count = "SELECT COUNT(*) FROM PRODUCTS P WHERE P.MODEL_ID = '".$_POST['submodel']."'";
	$query = "SELECT p.*, mks.*, pmds.*, mds.MODEL as SUB_MODEL FROM PRODUCTS p
LEFT JOIN MODELS mds ON p.MODEL_ID = mds.MODEL_ID
LEFT JOIN MAKES mks ON mds.MAKE_ID = mks.MAKE_ID
LEFT JOIN MODELS pmds ON mds.PARENT_MODEL =  pmds.MODEL_ID
WHERE mds.MODEL_ID = '".$_POST['submodel']."'";
}

// Count results
$q = Database::query($count);
$count = Database::result($q);
?>
	
	<table cellspacing="0" cellpadding="5" border="0" width="100%">
		<tr>
			<td class="tdDark" width="80">BQ NUMBER</td>
			<td class="tdDark" width="80">ILL NUMBER</td>
			<td class="tdDark">YEAR</td>
			<td class="tdDark">POSITION</td>
		</tr>
		<?php if ($count > 0): ?>
		<?php $q = Database::query($query); ?>
		<?php while ($product = Database::fetch_obj($q)): ?>
		<tr valign="top">
			<td><a href="javascript:void(0)" onClick="viewProduct('<?php echo $product->PRODUCT_ID ?>')"><?php echo $product->BQ_NUMBER ?></a></td>
			<td><?php echo $product->ILL_NUMBER ?></td>
			<td><?php echo $product->YEAR ?></td>
			<td><?php echo $product->HOSE_POSITION ?></td>
		</tr>
		<?php endwhile; ?>
		<?php else: ?>
		<tr>
			<td colspan="5" align="center">No results</td>
		</tr>
		<?php endif; ?>
	</table>