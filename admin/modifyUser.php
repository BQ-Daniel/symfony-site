<?php require_once('include/load.php') ?>

<?php
if (isset($_POST['action'])) {
    if ($_POST['action'] == 'save') {
        $distributor = (isset($_POST['distributor']) ? '1' : '0');
        $deleted = (isset($_POST['deleted']) ? '1' : '0');
        $order_access = (isset($_POST['order_access']) ? '1' : '0');

        // Make sure username & email are not blank
        if (!empty($_POST['username']) && ! empty($_POST['email']) && ! empty($_POST['invoice_address1']) && ! empty($_POST['delivery_address1'])) {
            $deliveryId = null;
            $invoiceId = null;
            $base_url = "http://" . GOOGLE_MAPS_HOST . "/maps/geo?output=xml" . "&key=" . GOOGLE_MAPS_KEY;
            
            // Make sure username doesn't already exist
            $q = Database::query("SELECT COUNT(*) FROM USERS WHERE USERNAME = '" . addslashes($_POST['username']) . "' AND EMAIL = '" . addslashes($_POST['email']) . "'");
            if (Database::result($q) == 0 || ! empty($_REQUEST['id'])) {

                // Add/Modify user
                if (isset($_REQUEST['id']) && ! empty($_REQUEST['id'])) {
                    Database::query("UPDATE USERS SET USERNAME = '" . addslashes($_POST['username']) . "', EMAIL = '" . addslashes($_POST['email']) . "', ORDER_NO_REQ = '" . (isset($_POST['order_no_req']) ? 'Y' : 'N') . "', ORDER_ACCESS = '".$order_access."', DISTRIBUTOR = '".$distributor."', DELETED = '".$deleted."' WHERE ID = '" . $_REQUEST['id'] . "'");

                    // Check to see if a record exists for invoice
                    $q = Database::query("SELECT COUNT(*) FROM USER_ADDRESS WHERE USER_ID = '" . $_REQUEST['id'] . "' AND ADDRESS_TYPE = 'invoice'");
                    $count = Database::result($q);
                    if (!empty($_POST['invoice_address1'])) {
                        // Insert Invoice address
                        if ($count > 0) {
                            Database::query("UPDATE USER_ADDRESS SET NAME = '" . addslashes($_POST['invoice_name']) . "', ADDRESS1 = '" . addslashes($_POST['invoice_address1']) . "', ADDRESS2 = '" . addslashes($_POST['invoice_address2']) . "', SUBURB = '" . addslashes($_POST['invoice_suburb']) . "', STATE = '" . addslashes($_POST['invoice_state']) . "', POSTCODE = '" . addslashes($_POST['invoice_postcode']) . "' WHERE USER_ID = '" . $_REQUEST['id'] . "' AND ADDRESS_TYPE = 'invoice'");
                        } else {
                            Database::query("INSERT INTO USER_ADDRESS SET USER_ID = '" . $_REQUEST['id'] . "', ADDRESS_TYPE = 'invoice', NAME = '" . addslashes($_POST['invoice_name']) . "', ADDRESS1 = '" . addslashes($_POST['invoice_address1']) . "', ADDRESS2 = '" . addslashes($_POST['invoice_address2']) . "', SUBURB = '" . addslashes($_POST['invoice_suburb']) . "', STATE = '" . addslashes($_POST['invoice_state']) . "', POSTCODE = '" . addslashes($_POST['invoice_postcode']) . "'");
                        }
                        $invoiceId = mysql_insert_id();
                    }
                    // Check to see if a record exists for delivery
                    $q = Database::query("SELECT COUNT(*) FROM USER_ADDRESS WHERE USER_ID = '" . $_REQUEST['id'] . "' AND ADDRESS_TYPE = 'delivery'");
                    $count = Database::result($q);
                    if (!empty($_POST['delivery_address1'])) {
                        if ($count > 0) {
                            // Insert Delivery address
                            Database::query("UPDATE USER_ADDRESS SET NAME = '" . addslashes($_POST['delivery_name']) . "', ADDRESS1 = '" . addslashes($_POST['delivery_address1']) . "', ADDRESS2 = '" . addslashes($_POST['delivery_address2']) . "', SUBURB = '" . addslashes($_POST['delivery_suburb']) . "', STATE = '" . addslashes($_POST['delivery_state']) . "', POSTCODE = '" . addslashes($_POST['delivery_postcode']) . "' WHERE ADDRESS_TYPE = 'delivery' AND USER_ID = '" . $_REQUEST['id'] . "'");
                        } else {
                            Database::query("INSERT INTO USER_ADDRESS SET USER_ID = '" . $_REQUEST['id'] . "', ADDRESS_TYPE = 'delivery', NAME = '" . addslashes($_POST['delivery_name']) . "', ADDRESS1 = '" . addslashes($_POST['delivery_address1']) . "', ADDRESS2 = '" . addslashes($_POST['delivery_address2']) . "', SUBURB = '" . addslashes($_POST['delivery_suburb']) . "', STATE = '" . addslashes($_POST['delivery_state']) . "', POSTCODE = '" . addslashes($_POST['delivery_postcode']) . "'");
                        }
                        $deliveryId = mysql_insert_id();
                    }

                    $msg = "modified";
                } else {
                    Database::query("REPLACE INTO USERS SET USERNAME = '" . $_POST['username'] . "', EMAIL = '" . $_POST['email'] . "', ORDER_NO_REQ = '" . (isset($_POST['order_no_req']) ? 'Y' : 'N') . "', ORDER_ACCESS = '".$order_access."', DISTRIBUTOR = '".$distributor."', DELETED = '".$deleted."', CREATED_AT = CURDATE()");
                    $msg = "added";
                    $_REQUEST['id'] = mysql_insert_id();
                    if (!empty($_POST['invoice_address1'])) {
                        // Insert Invoice address
                        Database::query("REPLACE INTO USER_ADDRESS SET USER_ID = '" . $_REQUEST['id'] . "', NAME = '" . $_POST['invoice_name'] . "', ADDRESS1 = '" . $_POST['invoice_address1'] . "', ADDRESS2 = '" . $_POST['invoice_address2'] . "', SUBURB = '" . $_POST['invoice_suburb'] . "', STATE = '" . $_POST['invoice_state'] . "', POSTCODE = '" . $_POST['invoice_postcode'] . "', ADDRESS_TYPE = 'invoice'");
                        $invoiceId = mysql_insert_id();
                    }
                    if (!empty($_POST['delivery_address1'])) {
                        // Insert Delivery address
                        Database::query("REPLACE INTO USER_ADDRESS SET USER_ID = '" . $_REQUEST['id'] . "', NAME = '" . $_POST['delivery_name'] . "', ADDRESS1 = '" . $_POST['delivery_address1'] . "', ADDRESS2 = '" . $_POST['delivery_address2'] . "', SUBURB = '" . $_POST['delivery_suburb'] . "', STATE = '" . $_POST['delivery_state'] . "', POSTCODE = '" . $_POST['delivery_postcode'] . "', ADDRESS_TYPE = 'delivery'");
                        $deliveryId = mysql_insert_id();
                    }
                }

                $success = "Successfully $msg user: " . v("username") . " - " . v("email") . "<br />";
                
                // Join delivery address together
                if ($deliveryId > 0)
                {
                    $deliveryAddress = trim($_POST['delivery_address1']);
                    if ($_POST['delivery_address2']) {
                        $deliveryAddress .= ' '.trim($_POST['delivery_address2']);
                    }
                    $deliveryAddress .= ' '.trim($_POST['delivery_suburb'].' '.$_POST['delivery_state'].' '.$_POST['delivery_postcode']);

                    // Update GEO codes
                    $request_url = $base_url . "&q=" . urlencode($deliveryAddress);
                    $xml = simplexml_load_file($request_url) or die("url not loading");
                    $status = $xml->Response->Status->code;
                    if (strcmp($status, "200") == 0) {
                        // Successful geocode
                        $geocode_pending = false;
                        $coordinates = $xml->Response->Placemark->Point->coordinates;
                        $coordinatesSplit = split(",", $coordinates);
                        // Format: Longitude, Latitude, Altitude
                        $lat = $coordinatesSplit[1];
                        $lng = $coordinatesSplit[0];

                        $query = sprintf("UPDATE USER_ADDRESS " .
                                        " SET LAT = '%s', LNG = '%s' " .
                                        " WHERE ID = '%s' LIMIT 1;",
                                        mysql_real_escape_string($lat),
                                        mysql_real_escape_string($lng),
                                        mysql_real_escape_string($deliveryId));
                        $update_result = mysql_query($query);
                    } else if (strcmp($status, "620") == 0) {
                        echo "Note: Try saving again, google was unable to save DEVILVERY GEO codes.<br />";
                    } else {
                        echo "Note: Devlivery Address is invalid, Google is unable to find GEO codes.";
                    }
                }

                // Join invoice address together
                if ($invoiceId > 0) {
                    $invoiceAddress = trim($_POST['invoice_address1']);
                    if ($_POST['invoice_address2']) {
                        $invoiceAddress .= ' ' . trim($_POST['invoice_address2']);
                    }
                    $invoiceAddress .= ' ' . trim($_POST['invoice_suburb'] . ' ' . $_POST['invoice_state'] . ' ' . $_POST['invoice_postcode']);

                    // Update GEO codes
                    $request_url = $base_url . "&q=" . urlencode($deliveryAddress);
                    $xml = simplexml_load_file($request_url) or die("url not loading");
                    $status = $xml->Response->Status->code;
                    if (strcmp($status, "200") == 0) {
                        // Successful geocode
                        $geocode_pending = false;
                        $coordinates = $xml->Response->Placemark->Point->coordinates;
                        $coordinatesSplit = split(",", $coordinates);
                        // Format: Longitude, Latitude, Altitude
                        $lat = $coordinatesSplit[1];
                        $lng = $coordinatesSplit[0];

                        $query = sprintf("UPDATE USER_ADDRESS " .
                                        " SET LAT = '%s', LNG = '%s' " .
                                        " WHERE ID = '%s' LIMIT 1;",
                                        mysql_real_escape_string($lat),
                                        mysql_real_escape_string($lng),
                                        mysql_real_escape_string($deliveryId));
                        $update_result = mysql_query($query);
                    } else if (strcmp($status, "620") == 0) {
                        echo "Note: Try saving again, google was unable to save DEVILVERY GEO codes.<br />";
                    } else {
                        echo "Note: Devlivery Address is invalid, Google is unable to find GEO codes.";
                    }
                }
            } else {
                $error = "That username / email address already exists";
            }
        } else {
            $error = "Username / Email / Address fields are required";
        }
    /*
    } elseif ($_POST['action'] == 'delete') {
        Database::query("DELETE FROM USERS WHERE ID = '" . $_REQUEST['id'] . "'");
        Database::query("DELETE FROM USER_ADDRESS WHERE USER_ID = '" . $_REQUEST['id'] . "'");
        Database::query("DELETE FROM USER_SESSIONS WHERE USER_ID = '" . $_REQUEST['id'] . "'");
        // ADD TO DELETE: ORDER HISTORY
        echo "<p class='success'><b>Success</b>: " . $_POST['username'] . " has been deleted</p>";
        exit;
    }
     */
    }
}

$invoice = array();
$delivery = array();

if (isset($_REQUEST['id'])) {
    $q = Database::query("SELECT * FROM USERS WHERE ID = '" . $_REQUEST['id'] . "'");
    $result = Database::fetch_obj($q);

    // Get Invoice address
    $q = Database::query("SELECT * FROM USER_ADDRESS WHERE USER_ID = '" . $_REQUEST['id'] . "' AND address_type ='invoice'");
    $invoice = Database::fetch_obj($q);
    // Get Delivery address
    $q = Database::query("SELECT * FROM USER_ADDRESS WHERE USER_ID = '" . $_REQUEST['id'] . "' AND address_type ='delivery'");
    $delivery = Database::fetch_obj($q);
}
?>

<?php if (isset($_REQUEST['id']) && ! empty($_REQUEST['id'])): ?>
<p><strong>Modify User</strong></p>
<?php else: ?>
<p><strong>Add User</strong></p>
<?php endif; ?>

<?php echo (isset($success) ? "<p class='success'><b>Success</b>: $success</p>" : null) ?>
<?php echo (isset($error) ? "<p class='error'><b>Error</b>: $error</p>" : null) ?>

<form id="form" action="javascript:void(0);" method="post" onSubmit="submitForm('modifyUser.php');"> 
    <input type="hidden" name="id" value="<?php echo v('id') ?>" />
    <input type="hidden" id="action" name="action" value="" />
    <table cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="tdCell">Username</td>
            <td><input type="text" name="username" value="<?php echo v('username') ?>" /></td>
        </tr>
        <tr>
            <td class="tdCell">Email address</td>
            <td><input type="text" name="email" size="50" value="<?php echo v('email') ?>" /></td>
        </tr>
        <tr>
            <td class="tdCell">Require Order Number</td>
            <td><input type="checkbox" name="order_no_req" value="Y"<?php echo (v('order_no_req') == 'Y' ? ' checked' : '' ) ?> /></td>
        </tr>
        <tr>
            <td class="tdCell">Order Access</td>
            <td><input type="checkbox" name="order_access" value="1"<?php echo (v('order_access') == 1 ? ' checked' : '' ) ?> /></td>
        </tr>
        <tr>
            <td class="tdCell">Distributor</td>
            <td><input type="checkbox" name="distributor" value="1"<?php echo (v('distributor') == 1 ? ' checked' : '' ) ?> /></td>
        </tr>
        <tr>
            <td class="tdCell">Deleted</td>
            <td><input type="checkbox" name="deleted" value="1"<?php echo (v('deleted') == 1 ? ' checked' : '' ) ?> /></td>
        </tr>
<?php if ($result != null): ?>
            <tr>
                <td class="tdCell">Total Logins</td>
                <td><?php echo v('TOTAL_LOGINS') ?></td>
            </tr>
            <tr>
                <td class="tdCell">Last login</td>
                <td>
<?php if ($result->LAST_LOGIN == null): ?>
    			Never
<?php else: ?>
        <?php echo date('l jS \of F Y h:i:s A', strtotime($result->LAST_LOGIN)) ?>
<?php endif; ?>
            </td>
        </tr>
        <tr>
            <td class="tdCell">Created on</td>
            <td><?php echo date('l jS \of F Y', strtotime($result->CREATED_AT)) ?></td>
        </tr>
                <?php endif; ?>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="tdCell" colspan="2"><b>Invoice Address</b></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Name</td>
                            <td><input type="text" name="invoice_name" value="<?php echo v("name", $invoice) ?>" size="50" maxlength="100" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Address1</td>
                            <td><input type="text" name="invoice_address1" value="<?php echo v("address1", $invoice) ?>" size="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Address2</td>
                            <td><input type="text" name="invoice_address2" value="<?php echo v("address2", $invoice) ?>" size="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Suburb</td>
                            <td><input type="text" name="invoice_suburb" value="<?php echo v("suburb", $invoice) ?>" size="30" maxlength="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">State</td>
                            <td><input type="text" name="invoice_state" value="<?php echo v("state", $invoice) ?>" size="4" maxlength="3" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Postcode</td>
                            <td><input type="text" name="invoice_postcode" value="<?php echo v("postcode", $invoice) ?>" size="4" maxlength="4" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="tdCell" colspan="2"><b>Delivery Address</b></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Name</td>
                            <td><input type="text" name="delivery_name" value="<?php echo v("name", $delivery) ?>" size="50" maxlength="100" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Address1</td>
                            <td><input type="text" name="delivery_address1" value="<?php echo v("address1", $delivery) ?>" size="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Address2</td>
                            <td><input type="text" name="delivery_address2" value="<?php echo v("address2", $delivery) ?>" size="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Suburb</td>
                            <td><input type="text" name="delivery_suburb" value="<?php echo v("suburb", $delivery) ?>" size="30" maxlength="50" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">State</td>
                            <td><input type="text" name="delivery_state" value="<?php echo v("state", $delivery) ?>" size="4" maxlength="3" /></td>
                        </tr>
                        <tr>
                            <td class="tdCell">Postcode</td>
                            <td><input type="text" name="delivery_postcode" value="<?php echo v("postcode", $delivery) ?>" size="4" maxlength="4" /></td>
                        </tr>
                    </table>
                    <p>
                        <input type="submit" name="submit" value="Save" onClick="$('action').value ='save'" />
<?php if (!empty($_REQUEST['id'])): ?>
            <!-- <input type="submit" name="submit" value="Delete User" onClick="$('action').value ='delete'" /> -->
<?php endif; ?>
    </p>
</form>		