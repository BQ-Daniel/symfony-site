<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'products')); 
?>
			<td id="left">

				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="stats.php?page=logins">Logins</a>
						<li> <a href="stats.php?page=views">Views</a>
						<li> <a href="stats.php?page=sessions">Sessions</a>
					</ul>
					<br />
				</div>

				<br />
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p>@ STATISTICS</p>
				</div>
				
				<div id="content">
				<?php if (isset($_REQUEST['page']) && file_exists('stats_'.$_REQUEST['page'].".php")): ?>
					<?php require_once('stats_'.$_REQUEST['page'].".php"); ?>
			    <?php else: ?>
				<div style="text-align: center; font-weight: bold; margin-bottom: 20px;">Please select an area</div>
                <div style="text-align: center;">
                    <div style="text-align: center; margin: 0 auto 20px auto; width: 280px;">
                        <div style="float: left; padding-right: 10px;">
                            <a href="stats.php?page=logins"><img src="images/stats_logins.jpg" title="Logins" alt="Logins" border="0" /></a>
                        </div>
                        <div style="float: left; padding-right: 10px;">
                            <a href="stats.php?page=views"><img src="images/stats_views.jpg" title="Views" alt="Views" border="0" /></a>
                        </div>
                        <div style="float: left; padding-right: 10px;">
                            <a href="stats.php?page=sessions"><img src="images/stats_sessions.jpg" title="Sessions" alt="Sessions" border="0" /></a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
				<?php endif; ?>
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>