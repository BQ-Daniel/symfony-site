<?php
// Load
include('include/load.php');

error_reporting(E_ALL);

?>
<html>
	<head>
		<title>Brakequip Administration : Import Users</title>
				<link rel="stylesheet" media="screen" href="style.css">
                <script type="text/javascript" src="../js/prototype.js"></script>

	</head>

	<body style="padding: 10px;">
<?php
// Make sure file exists
if (!file_exists($_FILES['file']['tmp_name']))
{
	echo "There has been an error. Please upload the excel spreadsheet again.";
	exit;
}


$added   = array();
$updated = array();

// Open file
$handle = fopen($_FILES['file']['tmp_name'], "r");

// Ignore first line
fgetcsv($handle, 1000, ",");

// Get first line, as this holds the headers
$data = fgetcsv($handle, 1000, ",");
defineFieldNames($data);
$emailMissing = array();

while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
{
    /* USERNAMES */
    $username = trim(addslashes($data[AC_CODE]));
    $email    = trim(addslashes($data[EMAIL_ADDRESS]));

    if (empty($username) || empty($email)) {
        if (empty($email)) {
            $emailMissing[] = $username;
        }
        else {
            continue;
        }
    }

	// Check if username exists
	$q = Database::query("SELECT ID FROM USERS WHERE USERNAME = '$username'");
	// add 
	if (Database::count_result($q) == 0) {
		Database::query("INSERT INTO USERS SET USERNAME = '$username', EMAIL = '$email', CREATED_AT = NOW(), DISTRIBUTOR = 1");
		$added[] = $username;
        $userId = mysql_insert_id();
	}
	// update
	else {
		$userId = Database::result($q);
		Database::query("UPDATE USERS SET ".(!empty($email) ? "EMAIL = '$email', " : '')." DISTRIBUTOR = 1 WHERE USERNAME = '$username'");
		$updated[] = $username;
	}

    if (empty($userId)) {
        continue;
    }

    
    /* ADDRESSES */

    $states = array('VIC', 'NSW', 'QLD', 'SA', 'NT', 'WA', 'TAS', 'NZ', 'ACT');

    // Setup variables for DELIVERY ADDRESS
    $name     = addslashes(trim($data[NAME]));
    $address1 = addslashes(trim($data[ADDRESS1]));
    $address2 = addslashes(trim($data[ADDRESS2]));
    $suburb   = addslashes(trim($data[SUBURB]));
    $state    = strtoupper(trim(substr($suburb, -3)));
    $postcode = addslashes(trim($data[POSTCODE]));
    $phone = addslashes(trim($data[PHONE_NO1]));

    if (!in_array($state, $states)) {
      $state = null;
    }
    else {
        $suburb = trim(str_replace($state, '', $suburb));
    }

    // Check if delivery address exists
    $q = Database::query("SELECT ID FROM USER_ADDRESS WHERE USER_ID = '$userId' and ADDRESS_TYPE = 'delivery'");

    $sql = "SET USER_ID = '$userId', NAME = '$name', ADDRESS1 = '$address1', ADDRESS2 = '$address2', SUBURB = '$suburb', STATE = '$state', POSTCODE = '$postcode', PHONE = '$phone', ADDRESS_TYPE = 'delivery'";
    // add
    if (Database::count_result($q) == 0) {
        Database::query("INSERT INTO USER_ADDRESS " . $sql);
    }
    // update
    else {
        Database::query("UPDATE USER_ADDRESS " . $sql . " WHERE USER_ID = '" . $userId . "' AND ADDRESS_TYPE = 'delivery'");
    }


    // Setup variables for INVOICE ADDRESS
    $name = addslashes(trim($data[INVOICE_NAME]));
    $address1 = addslashes(trim($data[INVOICE_ADDRESS1]));
    $address2 = addslashes(trim($data[INVOICE_ADDRESS2]));
    $suburb = addslashes(trim($data[INVOICE_SUBURB]));
    $state    = strtoupper(trim(substr($suburb, -3)));
    $postcode = addslashes(trim($data[POSTCODE]));
    $phone = addslashes(trim($data[PHONE_NO1]));


    if (!in_array($state, $states)) {
      $state = null;
    }
    else {
        $suburb = trim(str_replace($state, '', $suburb));
    }


    // Check if delivery address exists
    $q = Database::query("SELECT ID FROM USER_ADDRESS WHERE USER_ID = '$userId' and ADDRESS_TYPE = 'invoice'");

    $sql = "SET USER_ID = '$userId', NAME = '$name', ADDRESS1 = '$address1', ADDRESS2 = '$address2', SUBURB = '$suburb', STATE = '$state', POSTCODE = '$postcode', PHONE = '$phone', ADDRESS_TYPE = 'invoice'";
    // add
    if (Database::count_result($q) == 0) {
        Database::query("INSERT INTO USER_ADDRESS " . $sql);
    }
    // update
    else {
        Database::query("UPDATE USER_ADDRESS " . $sql . " WHERE USER_ID = '" . $userId . "' AND ADDRESS_TYPE = 'invoice'");
    }
}

/** 
 * Defines each field name with the value of the key in the array
 *
 * @param array
 */
function defineFieldNames($data)
{
	foreach ($data as $key => $value)
	{
		// Replace -'s with _
		$value = str_replace('-', '', $value);
        $value = str_replace(' ', '_', $value);
        $value = str_replace('/', '', $value);
		define($value, $key);
	}
}
?>

    <h1>Imported Users</h1>
    <p>Here are the results for importing users.

    <br />Launched google batch script to update addresses. This may take awhile depending on how many addresses there are to update.</div>
    <script type="text/javascript">
        new Ajax.Updater('google-loading', 'batch/runAddressScript.php');
    </script>
    
    <br />
    <table cellspacing="2" cellpadding="0" border="0">
        <tr>
            <td class="tdCell">Users Added (<?php echo count($added) ?>)</td>
        </tr>
        <?php foreach ($added as $user): ?>
        <tr>
            <td><?php echo $user ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="tdCell">Users with email address missing (<?php echo count($emailMissing) ?>)</td>
        </tr>
        <?php foreach ($emailMissing as $user): ?>
        <tr>
            <td><?php echo $user ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td class="tdCell">Users Updated (<?php echo count($updated) ?>)</td>
        </tr>
        <?php foreach ($updated as $user): ?>
        <tr>
            <td><?php echo $user ?></td>
        </tr>
        <?php endforeach; ?>
    </table>

    <p>Go back to the Import users page</p>
    <p><input type="button" name="back" value="Go Back" onClick="location.href='import.php?page=importUsers'" /></p>


    </body>
</html>