<?php require_once('include/load.php') ?>

<p><strong>View Orders</strong></p>

<?php $q = Database::query("SELECT * FROM ORDERS o INNER JOIN USERS u ON u.ID = o.USER_ID") ?>
<p><strong>Filter</strong><hr /></p>

<?php
if (!isset($_REQUEST['from'])) {
    $fromDate = date("d/m/y");
    list($day, $mth, $yr) = explode("/", $fromDate);
    $_REQUEST['from'] = array('dd' => $day, 'mm' => $mth, 'yy' => $yr);
}

if (!empty($_GET['from']) && !is_array($_GET['from'])) 
{
    list($yr, $mth, $day) = explode("-", $_GET['from']);
    $_REQUEST['from'] = array('dd' => $day, 'mm' => $mth, 'yy' => $yr);
}
if (!empty($_GET['to']) && !is_array($_GET['to'])) 
{
    list($yr, $mth, $day) = explode("-", $_GET['to']);
    $_REQUEST['to'] = array('dd' => $day, 'mm' => $mth, 'yy' => $yr);
}

$from = @$_REQUEST['from'];
$to = @$_REQUEST['to'];
$fromDate = null;
$toDate = null;

@$fromYear = (strlen($from['yy']) == 2 ? '20'.$from['yy'] : $from['yy']);
@$toYear = (strlen($to['yy']) == 2 ? '20'.$to['yy'] : $to['yy']);

if (@checkdate($from['mm'], $from['dd'], $fromYear)) {
    $fromDate = date("Y-m-d", strtotime($from['dd']."-".$from['mm']."-".$fromYear));
}
if (@checkdate($to['mm'], $to['dd'], $toYear)) {
    $toDate = date("Y-m-d", strtotime("+1 day", strtotime($to['dd']."-".$to['mm']."-".$toYear)));
}
?>



<form id="form" action="orders.php?page=viewOrders" method="post">
<input id="submit" type="submit" name="submit" value="submit" style="display:none" />
<table cellspacing="0" cellpadding="2" border="0">
	<tr>
		<td width="100"><b>Username</b></td>
		<td><input type="text" name="username" value="<?php echo v('username') ?>" /></td>
		<td width="33">&nbsp;</td>
		<td width="100"><b>Order No</b></td>
		<td><input type="text" name="order_no" value="<?php echo v('order_no') ?>" /></td>
	</tr>
	<tr>
		<td><b>Date</b></td>
		<td colspan="4">
        <div class="dateInput">
            From: <input type="text" name="from[dd]" value="<?php echo @$from['dd'] ?>" size="2" maxlength="2" onClick="if (this.value == 'DD') this.value = '';" tabindex="1" />.<input type="text" name="from[mm]" value="<?php echo @$from['mm'] ?>" size="2" maxlength="2" onClick="if (this.value == 'MM') this.value = '';" tabindex="2" />.<input type="text" name="from[yy]" value="<?php echo @$from['yy'] ?>" size="4" maxlength="4" onClick="if (this.value == 'YY') this.value = '';" tabindex="3" /> &nbsp; 
            To: <input type="text" name="to[dd]" value="<?php echo @$to['dd'] ?>" size="2" maxlength="2" onClick="if (this.value == 'DD') this.value = '';" tabindex="4" />.<input type="text" name="to[mm]" value="<?php echo @$to['mm'] ?>" size="2" maxlength="2" onClick="if (this.value == 'MM') this.value = '';" tabindex="5" />.<input type="text" name="to[yy]" value="<?php echo @$to['yy'] ?>" size="4" maxlength="4" onClick="if (this.value == 'YY') this.value = '';" tabindex="6" /> 
        </div>
        </td>
	</tr>
	<tr>
		<td colspan="5" align="right"><input type="submit" name="submit" value="Search" onClick="$('page').value = 0;" /></td>
	</tr>
</table>
<hr />
<?php
// Results per page
$perPage = 50;

if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}

$query = array();

$sql = "SELECT *, o.ID as ORDER_ID, DATE_FORMAT(o.DATE_ADDED, '%a, %d/%m/%y %h:%i%p') as DATE_ADDED FROM ORDERS o INNER JOIN USERS u ON o.USER_ID = u.ID";
$sql_count = "SELECT COUNT(*) FROM ORDERS o INNER JOIN USERS u ON o.USER_ID = u.ID";
if (v('username') != '') {
	$query[] = "u.USERNAME = '".v('username')."'";
}
if (v('order_no') != '') {
	$query[] = "o.ORDER_NO = '".v('order_no')."'";
}


if (!empty($fromDate)) {
	$query[] = "o.DATE_ADDED >= '$fromDate'";
    if (!empty($toDate)) {
        $query[] = "o.DATE_ADDED <= '$toDate'";
    }
}
if (count($query) > 0)
{
	$sql = $sql.' WHERE '.implode(" AND ", $query);
	$sql_count = $sql_count.' WHERE '.implode(" AND ", $query);
}

$orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.$_REQUEST['orderBy'].' '.$_REQUEST['sort'].' ' : ' ORDER BY o.DATE_ADDED '.$_REQUEST['sort']);

$q = Database::query($sql_count);
$count = Database::result($q);

$sql = $sql." $orderBy LIMIT $_REQUEST[p], $perPage";

$sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');
$url = "page=viewOrders&".$_REQUEST['orderBy']."&sort=".$sort."&from=".$fromDate.'&to='.$toDate.'&username='.$_REQUEST['username'].'&order_no='.$_REQUEST['order_no'];
?>
<p>&nbsp;</p>
<input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
<input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
<table cellspacing="1" cellpadding="4" border="0" width="100%">
	<tr>
        <td colspan="2">Total: <?php echo $count ?></td>
		<td colspan="2" align="right">
			Page: <select id="page" name="p" onChange="$('submit').click()">
			<?php
			$num=0;
			for($i=0;$i<ceil($count/$perPage);$i++)
			{
				echo '<option value="'.($num).'"';
				if($_REQUEST['p'] == $num) {
					echo " selected";
				}
				echo ">".($i+1)." of ".(ceil($count/$perPage))." pages</option>\n";
				$num=$num+$perPage;
			}
			?>
			</select>

		</td>
	</tr>
	<tr bgcolor="#e4e4e4">
        <td width="80"><a href="orders.php?orderBy=USERNAME&<?php echo $url ?>"><b>USERNAME</b></a></td>
		<td width="80"><a href="orders.php?orderBy=ORDER_NO&<?php echo $url ?>"><b>ORDER NO</b></a></td>
		<td><a href="orders.php?orderBy=ORDER_BY&<?php echo $url ?>"><b>BY</b></a></td>
		<td><a href="orders.php?orderBy=o.DATE_ADDED&<?php echo $url ?>"><b>DATE</b></a></td>
	</tr>
	<?php if ($count > 0): ?>
		<?php $q = Database::query($sql); ?>
		<?php while ($result = Database::fetch_obj($q)): ?>
	<tr>
        <td><?php echo v('USERNAME') ?></td>
		<td><a href="javascript:void(0);" onClick="displayOrder('<?php echo v('ORDER_ID') ?>');"><?php echo v('ORDER_NO') ?></a></td>
		<td><?php echo v('ORDER_BY') ?></td>
		<td><?php echo v("DATE_ADDED") ?></td>
	</tr>
		<?php endwhile; ?>
	<?php endif; ?>
</table>