<?php

/* 
 * include_template()
 * This will include a template from the templates directory and replace any variables passed in the array
*/
function include_template($file, $array = array(), $dir = null)
{
	if ($dir)
	{
		$filename = $dir . $file . '.php';
	}
	else
	{
		$filename = 'templates/'.$file.'.php';
	}

	if (file_exists($filename))
	{
		$contents = file_get_contents($filename);

		foreach ($array as $key => $value) {
			eval("\$".$key." = \"$value\";");
		}
		
		eval('?>'.$contents);
	}
}

function v($value, $obj = null)
{
	global $_POST, $result;
	
	if (is_null($obj)) {
		$obj = $result;
	}

	if (is_array($obj) && isset($obj[$value])) {
		return trim($obj[$value]);
	}
	
	$value2 = strtoupper($value);
	if ($obj && isset($obj->$value2)) {
		return $obj->$value2;
	}
	elseif (isset($_POST[$value])) {
		return $_POST[$value];
	}
	elseif (isset($_REQUEST[$value])) {
		return $_REQUEST[$value];
	}
	else {
		return false;
	}
}

/* RESIZE IMAGE FUNCTION
	- Reize an image to a certain size.
function resizeimg($forcedwidth, $forcedheight, $sourcefile, $destfile, $imgcomp = 100)
{
	$g_imgcomp=100;
	$g_srcfile=$sourcefile;
	$g_dstfile=$destfile;
	$g_fw=$forcedwidth;
	$g_fh=$forcedheight;

	if(file_exists($g_srcfile))
	{
		$g_is=getimagesize($g_srcfile);
		if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh))
		{
			$g_iw=$g_fw;
			$g_ih=($g_fw/$g_is[0])*$g_is[1];
		}
		else
		{
			$g_ih=$g_fh;
			$g_iw=($g_ih/$g_is[1])*$g_is[0]; 
		}

		switch($g_is[2])
		{
			case '1':
				$img_src=imagecreatefromgif($g_srcfile);
			break;
			case '2':
				$img_src=imagecreatefromjpeg($g_srcfile);
			break;
			case '3':
				$img_src=imagecreatefrompng($g_srcfile);
			break;
		}

		$img_dst=imagecreatetruecolor($g_iw,$g_ih);
		imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1]);

		switch($g_is[2])
		{
			case '1':
				imagejpeg($img_dst, $g_dstfile, $g_imgcomp);
			break;
			case '2':
				imagejpeg($img_dst, $g_dstfile, $g_imgcomp);
			break;
			case '3':
				imagepng($img_dst, $g_dstfile, $g_imgcomp);
			break;
		} 
		imagedestroy($img_dst);
		return true;
	}
	else
		return false;
}
*/

function resizeimg( $forcedwidth, $forcedheight, $sourcefile, $destfile, $other = 0 )
{
    $g_fw = $forcedwidth;
    $g_fh = $forcedheight;
    $g_is = getimagesize( $sourcefile );
    if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh))
    {
        $g_iw=$g_fw;
        $g_ih=($g_fw/$g_is[0])*$g_is[1];
    }
    else
    {
        $g_ih=$g_fh;
        $g_iw=($g_ih/$g_is[1])*$g_is[0]; 
    }

    $img_src = imagecreatefromjpeg( $sourcefile );
    $img_dst = imagecreatetruecolor( $g_iw, $g_ih );
    imagecopyresampled( $img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1] );
    if( !imagejpeg( $img_dst, $destfile, 90 ) )
    {
        exit( );
    }
}


function javascript_tag($javascript)
{
	return "<SCRIPT language=\"JavaScript\">$javascript</SCRIPT>";
}

function auth($option = null)
{
	// Update user login stats
	if (isset($_SESSION['userId'])) {
		Database::query("UPDATE USER_LOGINS SET LOGIN_END = NOW() WHERE (NOW() < ADDTIME(LOGIN_END, '10:00:00') OR LOGIN_END IS NULL) AND USER_ID = '".$_SESSION['userId']."' ORDER BY ID DESC LIMIT 1");
        updateSession();
	}

	// Remove sessions
	Database::query("DELETE FROM USER_SESSIONS WHERE ADDTIME(LAST_ACTIVE, '10:00:00') < NOW()");

	// check if session id exists in database
	$q = Database::query("SELECT COUNT(*) FROM USER_SESSIONS WHERE SESSION = '".session_id()."'");
	// if session doesn't exist - redirect
	if (Database::result($q) == 0) {
		if ($option == "redirect") {
			header('Location: ' . WEBSITE_URL . 'index.php?show=sessionTimeout');
			exit;
		} else {
			return false;
		}
	}

	return true;
}

function updateSession()
{
	Database::query("UPDATE USER_SESSIONS SET LAST_ACTIVE = NOW() WHERE USER_ID = '".$_SESSION['userId']."'");
}

/**
 * Function to calculate date or time difference.
 * 
 * Function to calculate date or time difference. Returns an array or
 * false on error.
 *
 * @author       J de Silva                             <giddomains@gmail.com>
 * @copyright    Copyright &copy; 2005, J de Silva
 * @return       array
 */
function getTimeDifference($start, $end)
{
    $uts['start']      =    strtotime( $start );
    $uts['end']        =    strtotime( $end );
    if( $uts['start']!==-1 && $uts['end']!==-1 )
    {
        if( $uts['end'] >= $uts['start'] )
        {
            $diff    =    $uts['end'] - $uts['start'];
            if( $days=intval((floor($diff/86400))) )
                $diff = $diff % 86400;
            if( $hours=intval((floor($diff/3600))) )
                $diff = $diff % 3600;
            if( $minutes=intval((floor($diff/60))) )
                $diff = $diff % 60;
            $diff    =    intval( $diff );      
			
			$str = ($days > 0 ? $days.' day'.($days > 1 ? 's' : '').' ' : '');
			$str .= ($hours > 0 ? $hours.' hr'.($hours > 1 ? 's' : '').' ' : '');
			$str .= ($minutes > 0 ? $minutes.' min'.($minutes > 1 ? 's' : '').' ' : '');

			return (!empty($str) ? $str : 'Under 1 minute');
        }
    }
    return( false );
}

function getSetting($name)
{
	$sql = "SELECT VALUE FROM SETTINGS WHERE UPPER(NAME) = '".strtoupper($name)."'";
	$q   = Database::query($sql);
	$result = Database::fetch_obj($q);
	if (!$result) {
        return null;
    }
	return $result->VALUE;
}

function writeNewBookSettingsJS()
{
	// Update bookSettings.js with images
	$q = Database::query("SELECT * FROM PICTORIAL_PAGES ORDER BY PAGE_NO ASC");
	$js = "flippingBook.pages = [\n";
	while ($result = Database::fetch_obj($q)){
		$js .= "\t".'"pages/page-'.$result->PAGE_NO.'.jpg?'.rand(99,99999999999).'",'."\n";
	}
	$js = substr($js, 0, -2);
	$js .= "\n];\n\n";

	// Update chapters
	$q = Database::query("SELECT * FROM PICTORIAL_CHAPTERS ORDER BY PAGE ASC");
	$js .= "flippingBook.contents = [\n";
	while ($result = Database::fetch_obj($q)) {
		$js .= "\t".'[ "'.$result->NAME.'", '.$result->PAGE.' ],'."\n";
	}
	$js = substr($js, 0, -2);
	$js .= "\n];\n\n";

	// Now add the rest of the javascript file from the template, and rewrite bookSettings.js
	$contents = file_get_contents("../pictorial/js/bookSettings.template.js");
	$js .= $contents;
	
	// Write to file
	$fp = fopen("../pictorial/js/bookSettings.js", 'w');
	fwrite($fp, $js);
	fclose($fp);
}

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
    $file = $path.$filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use diff. tyoes here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    mail($mailto, $subject, "", $header);
}

function galleryAgreed()
{
    if (!session_is_registered('galleryAgree')) {
        session_register('galleryNotAgree');
        header("Location: ../apps.php");
    }
}
?>