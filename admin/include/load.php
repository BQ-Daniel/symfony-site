<?php 
putenv("TZ=Australia/Melbourne");

define('BASE_DIR', '/home/brakeq/public_html');

ini_set('session.gc_maxlifetime', '36000');
ini_set('session.gc_divisor', '1000');

//ini_set("memory_limit","2000M");

function __autoload($class_name) {
    require_once BASE_DIR.'/admin/classes/'.$class_name . '.class.php';
}

// Configuration
include(BASE_DIR.'/admin/include/config.php');

// Functions
include(BASE_DIR.'/admin/include/functions.php'); 

// Make connection to database
$db = new Database();
$db->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$sql = "SET time_zone = 'Australia/Melbourne'";
mysql_query($sql);
?>