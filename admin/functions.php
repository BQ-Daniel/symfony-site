<?
// Delete user 
function deleteUser($user)
{
	global $loginFile;

	if(file_exists($loginFile))
		$userList=file($loginFile);

	foreach($userList as $k => $v) {
		$v=trim($v);
		list($user2,$pass)=split(":",$v);
		if(trim($user) == trim($user2))
			unset($userList[$k]);
	}
	
	// add user to list
	$fp=fopen($loginFile,"w+");
	foreach($userList as $line) {
		$line=trim($line);
		if(!empty($line))
			fputs($fp,$line."\n");
	}
	fclose($fp);

}

// Add user
function addUser($user,$pass)
{
	global $loginFile,$passSalt;

	$user=trim($user);
	$pass=trim($pass);

	// add user to list
	$fp=fopen($loginFile,"a+");
	$pass=crypt($pass,$passSalt);
	fputs($fp,$user.":".$pass."\n");
	fclose($fp);
}

// User exists
function userExists($user)
{
	global $loginFile;

	$file=file($loginFile);
	
	// search for user
	foreach($file as $line) {
		list($u,$p)=split(":",$line);
		if(strcasecmp($user, $u) == 0) {
			$exists=true;
			break;
		}

	}
	// return result
	if(isset($exists))
		return true;
	else
		return false;
}

// Retrieve users
function getUsers()
{
	global $loginFile;
	
	if(file_exists($loginFile))
		$file=file($loginFile);
	
	if(isset($file))
	{
		foreach($file as $v)
		{
			$v=trim($v);
			list($user,$pass)=split(":",$v);
			$array[]=$user;
		}
	}
	if(isset($array) && count($array) > 0)
		return $array;
}

// Make sure they are logged in
function auth()
{
	global $_SESSION;

	if(!isset($_SESSION['user'])) {
		echo "You are not logged in to view this restricted area.";
		exit;
	}
		
}

// Do login
function dologin($user,$pass)
{
	global $passSalt,$loginFile;

	if(file_exists($loginFile))
		$file=file($loginFile);

	if(isset($file) && count($file) > 0)
	{
		foreach($file as $line)
		{
			list($u,$p)=split(":",$line);
			// Find username
			if(strcasecmp($user, $u) == 0) {
				// check password
				$chkPass=crypt($pass,$passSalt);
				if(trim($chkPass) == trim($p)) {
					$ok=true;
					break;
				}
			}
		}
	}

	if(isset($ok))
		return true;
	else
		return false;
}
?>