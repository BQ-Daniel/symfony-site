<?php
#error_reporting(0);


// Define the column numbers and what they hold
define('MAKE',			1);
define('MODEL',			1);
define('SUBMODEL',		1);
define('YEAR',			2);
define('POS',			3);
define('PART_NO',		4);
define('BQ_NO',			5);
define('ILL_NO',		6);
define('COMMENTS',		7);

class ExcelProducts
{
	var $data;
	var $make;
	var $model;
	var $subModels =		array();
	var $totalAdds =		0;
	var $totalIgnores =		0;

	
	function initialize($data)
	{
		$this->setData($data);
		$this->getMake();
		$this->getModel();
	}
	
	function setData($data)
	{
		$this->data = $data;
	}

	function getMake()
	{
		if ($this->make) {
			return $this->make;
		}

		// Make is on line Line 5
		$this->make = trim($this->data->sheets[0]['cells'][5][MAKE]);

		return $this->make;
	}

	function getModel()
	{
		if ($this->model) {
			return $this->model;
		}

		$this->model = null;
		// Model is on line Line 6
		if (isset($this->data->sheets[0]['cells'][6][MODEL])) {
			$this->model = trim($this->data->sheets[0]['cells'][6][MODEL]);
		}
		
		return $this->model;
	}

	function getSubModels()
	{
		$this->generateSubModels();
		return $this->subModels;
	}
	
	function getTotalAdds()
	{
		return $this->totalAdds;
	}

	function getTotalIgnores()
	{
		return $this->totalIgnores;
	}

	function generateSubModels()
	{
		// Start at line 7
		for ($i = 7; $i <= $this->data->sheets[0]['numRows']; $i++) {
			$keySubModel = (isset($this->data->sheets[0]['cells'][$i][SUBMODEL]) ? trim($this->data->sheets[0]['cells'][$i][SUBMODEL]) : null);
			// remove any "'s 
			$keyYear = (isset($this->data->sheets[0]['cells'][$i][YEAR]) ? trim(str_replace('"', '', $this->data->sheets[0]['cells'][$i][YEAR])) : null);
			if (empty($this->make)) {
				break;
			}

			// If year is not empty 
			if (!empty($keyYear)) {
				$prevSubModel = (!empty($keySubModel) ? $keySubModel : $prevSubModel);
				if (empty($keySubModel)) {
					$keySubModel = (isset($this->data->sheets[0]['cells'][$i][SUBMODEL]) ? $this->data->sheets[0]['cells'][$i][SUBMODEL] : $prevSubModel);
				}
				$this->addSubModel($keySubModel, $keyYear, $i);

				// Start another loop to get group the sub model and year together
				// break out of loop if the submodel and year dont match key model / year
				for ($a = $i+1; $a <= $this->data->sheets[0]['numRows']; $a++) {
					$currentSubModel = (isset($this->data->sheets[0]['cells'][$a][SUBMODEL]) ? trim($this->data->sheets[0]['cells'][$a][SUBMODEL]) : null);
					$currentYear = (isset($this->data->sheets[0]['cells'][$a][YEAR]) ? trim(str_replace('"', '', $this->data->sheets[0]['cells'][$a][YEAR])) : null);
					$currentPos = (isset($this->data->sheets[0]['cells'][$a][POS]) ? trim($this->data->sheets[0]['cells'][$a][POS]) : null);
				
					if (($currentSubModel != null) && ($currentYear != null && $currentYear != $keyYear || $keySubModel != $currentSubModel) || ($currentYear != null && $currentPos != null)) {
						$i = $a-1;
						break;
					}

					if ($currentYear == null) {
						$currentYear = $keyYear;
					}
					
					// add sub model entry
					$this->addSubModel($keySubModel, $currentYear, $a);
				}
			}
		}
	}

	function addSubModel($subModel, $year, $lineNumber)
	{
		$value1 = (isset($this->data->sheets[0]['cells'][$lineNumber][POS]) ? trim($this->data->sheets[0]['cells'][$lineNumber][POS]) : '');
		$value2 = (isset($this->data->sheets[0]['cells'][$lineNumber][BQ_NO]) ? trim($this->data->sheets[0]['cells'][$lineNumber][BQ_NO]) : '');
		$value3 = (isset($this->data->sheets[0]['cells'][$lineNumber][ILL_NO]) ? trim($this->data->sheets[0]['cells'][$lineNumber][ILL_NO]) : '');
		$comments = (isset($this->data->sheets[0]['cells'][$lineNumber][COMMENTS]) ? trim($this->data->sheets[0]['cells'][$lineNumber][COMMENTS]) : '');
		if (!empty($subModel) && !empty($value1) && !empty($value2) && !empty($value3)) {
		
			if (ereg('"', $comments)) {
				$prevNumber = $lineNumber - 1;
				$this->data->sheets[0]['cells'][$lineNumber][COMMENTS] = $this->data->sheets[0]['cells'][$prevNumber][COMMENTS];
			}
			$this->subModels[$subModel][$year][] = $this->data->sheets[0]['cells'][$lineNumber];
			$this->totalAdds++;
		}
		else {
			if ((!empty($this->data->sheets[0]['cells'][$lineNumber][SUBMODEL]) && !empty($this->data->sheets[0]['cells'][$lineNumber][YEAR])) && (empty($value2) || empty($value3))) {
				$this->totalIgnores++;
			}
		}
        $this->freeLine($lineNumber);
	}
	
	/*
	 * clear line from memory as we dont need it anymore
	*/
	function freeLine($i)
	{
		unset($this->data->sheets[0]['cells'][$i]);
	}
}
?>