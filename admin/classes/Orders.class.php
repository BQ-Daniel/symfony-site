<?php

class Orders {

    public $id = 0;
    public $userId = 0;
    public $shippingTo = null;
    public $orderBy = null;
    public $orderNo = null;
    public $shipVia = null;
    public $cost = null;
    public $weight = null;
    public $dateAdded = null;
    public $comments = null;
    public $randUsed = false;
    public $lastOrderNo = 0;
    public $orderDetails = array();

    /**
     * Return object of order by passing the order id
     *
     * @return object
     */
    public static function getOrderById($id) {
        $obj = new Orders();

        $sql = "SELECT * FROM ORDERS o WHERE ID = $id";
        $q = Database::query($sql);

        $result = Database::fetch_obj($q);
        if ($result) {
            $tmp = get_object_vars($result);
            foreach ($tmp as $key => $value) {
                $key = str_replace('_', ' ', $key);
                $key = ucwords(strtolower($key));
                $key = str_replace(' ', '', $key);
                $methodName = 'set' . $key;
                call_user_func(array($obj, $methodName), $value);
            }
        }
        return $obj;
    }

    /**
     * Save order object to database
     *
     */
    public function save() {
        $sql = (empty($this->id) ? "INSERT INTO " : "UPDATE ");


        // Get last order number used
        if (!($this->lastOrderNo = getSetting('last_order_id'))) {
            $this->lastOrderNo = "999999";
        }

        // If no date added, use current date
        if (empty($this->dateAdded)) {
            $date_added = "NOW()";
            $this->setDateAdded(date('Y-m-d'));
        } else {
            $date_added = "'" . $this->dateAdded . "'";
        }

        // If no order number, create one
        if (empty($this->orderNo)) {
            $this->setOrderNo($this->getRandOrderNumber());
        }

        $sql .= "ORDERS SET USER_ID = '" . $this->userId . "', SHIPPING_TO = '" . addslashes($this->shippingTo) . "', ORDER_BY = '" . addslashes($this->orderBy) . "',
		ORDER_NO = '" . addslashes($this->orderNo) . "', EASE_ORDER_NO = '".($this->lastOrderNo + 1)."', SHIP_VIA = '" . addslashes($this->shipVia) . "', COST = '" . $this->cost . "', WEIGHT = '" . $this->weight . "', STATUS = 'S', DATE_ADDED = " . $date_added;

        $sql .= ( empty($this->id) ? "" : " WHERE ID = '" . $this->id . "'");
        Database::query($sql);

        // Get order id
        $this->setId(mysql_insert_id());


        // Loop through order details and add to database
        if (count($this->orderDetails) > 0) {
            foreach ($this->orderDetails as $detail) {
                // Set order id
                $detail->setOrderId($this->getId());

                $sql = ($detail->id == 0 ? "INSERT INTO " : "UPDATE ");
                $sql .= "ORDER_DETAILS SET ORDER_ID = '" . $detail->getOrderId() . "', ORDER_PRODUCT_ID = '" . $detail->getOrderProductId() . "', QTY = '" . $detail->getQty() . "',
				PRICE = '" . $detail->getPrice() . "', WEIGHT = '" . $detail->getWeight() . "'";

                $sql .= ( $detail->id == 0 ? "" : " WHERE ID = " . $detail->getId() . "");

                Database::query($sql);

                $detail->setId(mysql_insert_id());

                // Also remove QTY from stock
                $sql = "UPDATE ORDER_PRODUCTS SET STOCK = (STOCK - " . $detail->getQty() . ") WHERE ID = '" . $detail->getOrderProductId() . "'";
                Database::query($sql);
            }
        }

        // update setting for last order id used
        $sql = "UPDATE SETTINGS SET VALUE = '" . ($this->lastOrderNo + 1) . "' WHERE NAME = 'last_order_id'";
        Database::query($sql);

        // Write order to file
        $this->writeToFile();
    }

    /**
     * Add order details to order
     *
     */
    public function addDetail($obj) {
        $this->orderDetails[] = $obj;
    }

    /**
     * Return a random order number
     *
     * @return integer
     */
    public function getRandOrderNumber() {
        // Get last order number used
        if (empty($this->lastOrderNo)) {
            if (!($this->lastOrderNo = getSetting('last_order_id'))) {
                $this->lastOrderNo = "999999";
            }
        }

        $orderNumber = $this->lastOrderNo;
        do {
            $orderNumber++;
            $sql = "SELECT COUNT(*) FROM ORDERS WHERE ORDER_NO = '" . $orderNumber . "'";
            $q = Database::query($sql);

            $count = Database::result($q);
        } while ($count > 0);

        $this->randUsed = true;

        return $orderNumber;
    }

    public function writeToFile() {
        $users = new Users();
        $user = $users->getUser();

        $obj = new ShoppingCart();
        $cart = $obj->getStoredCart();
        $date = $this->getDateAdded();

        $account = $users->getUsername();
        $account = preg_replace("/[^0-9]/", "", $account);
        if (empty($account)) {
            $account = $users->getUsername();
        }

        $contents = "ACCOUNT=" . $account . "\r\n" .
                "ORDER=" . $this->getOrderNo() . "\r\n" .
                "CUSTOMER REF=" . $this->getOrderBy() . "\r\n" .
                "TYPE=K\r\n" .
                "DATE REQ=" . date("d-m-y", strtotime($date)) . "\r\n";


        // Loop through products
        foreach ($cart->getItems() as $item) {
            $price = str_replace(",", "", $item->getPrice());
            $contents .= "PART NO=" . $item->getName() . "\r\n" .
                    "DESCRIPTION=" . $item->getDescription() . "\r\n" .
                    "QUANTITY=" . $item->getQuantity() . "\r\n" .
                    "PRICE=" . $price . "\r\n";
        }
        // If shipping address is different to what is on file, add to COMMENT field
        if ($cart->getShippingAddress() != '' && $cart->getShippingAddress() != $users->addressToString('delivery')) {
            $contents .= "COMMENT=SHIP TO:\r\n";
            $explode = explode("\r\n", $cart->getShippingAddress());
            foreach ($explode as $line) {
                $line = trim($line);
                if (!empty($line)) {
                    $contents .= "COMMENT=" . $line . "\r\n";
                }
            }
        }
        // Add ship via comment
        $contents .= "COMMENT=SHIP VIA: " . strtoupper(str_replace("_", " ", $this->getShipVia())) . "\r\n";
        // Check for comments
        $comments = trim($this->getComments());
        if (!empty($comments)) {
            $contents .= "COMMENT=$comments\r\n";
        }
        $contents .= "COMMENT=This order was placed using the Online Ordering System.\r\n";

        $file = "orders/" . ++$this->lastOrderNo . ORDER_EXTENSION;
        $filename = $this->lastOrderNo . ORDER_EXTENSION;

        // If file exists, add timestamp to it
        if (file_exists($file)) {
            $time = time();
            $file = "orders/" . $this->lastOrderNo . '-' . $time . ORDER_EXTENSION;
            $filename = $this->lastOrderNo . '-' . $time . ORDER_EXTENSION;
        }

        $fp = fopen($file, "w+");
        flock($fp, LOCK_EX);
        fwrite($fp, $contents, strlen($contents));
        flock($fp, LOCK_UN);
        fclose($fp);

        /* If FTP goes down, we can uncomment this so orders are emailed instead
          mail_attachment($filename, "orders/", "catalogue@brakequip.com.au", "noreply@brakequip.com.aui", "BrakeQuip Web Order", "noreply@brakequip.com.au", "BrakeQuip Online Order", "");
          mail_attachment($filename, "orders/", "john@smytheweb.com", "noreply@brakequip.com.aui", "BrakeQuip Web Order", "noreply@brakequip.com.au", "BrakeQuip Online Order", "");
          unlink($file);
         */
    }

    /**
     * Set order id
     *
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Return order id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set user id
     *
     */
    public function setUserId($userId) {
        $this->userId = $userId;
    }

    /**
     * Return user id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set shipping to address
     *
     */
    public function setShippingTo($shippingTo) {
        $this->shippingTo = $shippingTo;
    }

    /**
     * Return shipping to address
     *
     * @return string
     */
    public function getShippingTo() {
        return $this->shippingTo;
    }

    /**
     * Set who the order was requested by
     *
     */
    public function setOrderBy($orderBy) {
        $this->orderBy = $orderBy;
    }

    /**
     * Return who requested the order
     *
     * @return string
     */
    public function getOrderBy() {
        return $this->orderBy;
    }

    /**
     * Set the order number
     *
     */
    public function setOrderNo($orderNo) {
        $this->orderNo = $orderNo;
    }

    /**
     * Return the order number
     *
     * @return string
     */
    public function getOrderNo() {
        return $this->orderNo;
    }

    /**
     * Set how the order will be shipped
     *
     */
    public function setShipVia($shipVia) {
        $this->shipVia = $shipVia;
    }

    /**
     * Return how the order will be shipped
     *
     * @return string
     */
    public function getShipVia() {
        return $this->shipVia;
    }

    /**
     * Set the total cost of the order
     *
     */
    public function setCost($cost) {
        $this->cost = $cost;
    }

    /**
     * Return the total cost of the order
     *
     * @return float
     */
    public function getCost() {
        return $this->cost;
    }

    /**
     * Set the total weight of order
     *
     */
    public function setWeight($weight) {
        $this->weight = $weight;
    }

    /**
     * Return the total weight of order
     *
     * @return float
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * Set date the order was submitted
     *
     */
    public function setDateAdded($dateAdded) {
        $this->dateAdded = $dateAdded;
    }

    /**
     * Return the date the order was submitted
     *
     * @return date
     */
    public function getDateAdded() {
        return $this->dateAdded;
    }

    /**
     * Set comments
     *
     */
    public function setComments($comments) {
        $this->comments = $comments;
    }

    /**
     * Return comments
     *
     * @return string
     */
    public function getComments() {
        return $this->comments;
    }

}

class OrderDetail {

    public $id = 0;
    public $orderId = 0;
    public $orderProductId = 0;
    public $qty = 0;
    public $price = 0;
    public $weight = 0;

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderProductId($orderProductId) {
        $this->orderProductId = $orderProductId;
    }

    public function getOrderProductId() {
        return $this->orderProductId;
    }

    public function setQty($qty) {
        $this->qty = $qty;
    }

    public function getQty() {
        return $this->qty;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function getWeight() {
        return $this->weight;
    }

}

?>
