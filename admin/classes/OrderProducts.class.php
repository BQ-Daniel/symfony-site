<?php

class OrderProducts
{
	public $fileLocation = null;
	public $partNumber	 = null;
	public $description  = null;
	public $price		 = 0;
	public $weight       = 0;
	public $stock        = 0;
	public $cat			 = null;
	public $fp		     = null;

	public function __construct($fileLocation = null)
	{
		if ($fileLocation) {
			$this->setFile($fileLocation);

			$this->open();
		}
	}

	public function open()
	{
		$this->fp = fopen($this->getFile(), "r");
	}

	public function readLine()
	{
		$fget = fgets($this->fp);

		$explode = explode(',', $fget);
		$csv = '';
		foreach ($explode as $line)
		{
			$line = trim($line);
			$csv .= $this->checkCSVField($line).',';
		}

		$data = $this->getCSVValues(substr($csv, 0, -1));

		// Make sure all fields are there
		if (count($data) == 5)
		{
			$obj = new OrderProducts();
			$obj->setPartNumber($data[0]);
			$obj->setDescription($data[1]);
			$obj->setPrice($data[2]);
			$obj->setWeight($data[3]);
			$obj->setStock($data[4]);

			return $obj;
		}

		return false;
	}

   public function checkCSVField(&$item)
   {
	   $item = str_replace('""', '"', $item);
	   return $item;
    }

function getCSVValues($string, $separator=",")
{
    $elements = explode($separator, $string);
    for ($i = 0; $i < count($elements); $i++) {
        $nquotes = substr_count($elements[$i], '"');
        if ($nquotes %2 == 1) {
            for ($j = $i+1; $j < count($elements); $j++) {
                if (substr_count($elements[$j], '"') %2 == 1) { // Look for an odd-number of quotes
                    // Put the quoted string's pieces back together again
                    array_splice($elements, $i, $j-$i+1,
                        implode($separator, array_slice($elements, $i, $j-$i+1)));
                    break;
                }
            }
        }
        if ($nquotes > 0) {
            // Remove first and last quotes, then merge pairs of quotes
            $qstr =& $elements[$i];
            $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
            $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
            $qstr = str_replace('""', '"', $qstr);
        }
    }
    return $elements;
}

	public function close()
	{
		fclose($this->fp);
	}

	public function import()
	{
		// Select all order parts from database
		$sql = "SELECT * FROM ORDER_PRODUCTS WHERE ACTIVE = 1";
		$q = Database::query($sql);

		// Store all file contents in array
		while ($fget = fgets($this->fp)) {
			$explode = explode(',', $fget);
			$csv = null;
			foreach ($explode as $line)
			{
				$line = trim($line);
				$csv .= $this->checkCSVField($line).',';
			}
			$data = $this->getCSVValues(substr($csv, 0, -1));
			$data[0] = trim($data[0]);
			$file[$data[0]] = true;
		}
		$this->close();

        // If products, dont continue on
        if (count($file) == 0) {
            return false;
        }

		while ($row = Database::fetch_obj($q))
		{
			$row->PART_NUMBER = trim($row->PART_NUMBER);
			if (!isset($file[$row->PART_NUMBER])) {
				Database::query("UPDATE ORDER_PRODUCTS SET ACTIVE = 0 WHERE PART_NUMBER = '".$row->PART_NUMBER."'");
				echo "DELETING ".$row->PART_NUMBER."<br />";
			}
		}
		unset($file);
		unset($data);


		$this->open();
		// Ignore the first 2 lines
		$data = fgetcsv($this->fp, 1000, ",");
		$data = fgetcsv($this->fp, 1000, ",");

		while ($obj = $this->readLine())
		{
			$partNumber = trim($obj->getPartNumber());
			// Check if part number exists
			$q = Database::query("SELECT ID FROM ORDER_PRODUCTS WHERE PART_NUMBER = '".$partNumber."'");
			$result = Database::fetch_obj($q);

            preg_match("/[A-Z]*/", $partNumber, $matches);
            $sortAplha = $matches[0];

            $partNumber2 = $partNumber;
            if (eregi("-", $partNumber)) {
                $pos = strpos($partNumber, '-');
                $partNumber2 = substr($partNumber, 0, $pos);
            }

            preg_match_all("{(\d+)}", $partNumber2, $matches2);


            $sortKey = implode("", $matches2[0]);

			// Product exists, update
			if ($result)
			{
               echo "Update: ".$obj->getPartNumber().", Cat: ".$obj->getCat().", Aplha: $sortAplha, Key: $sortKey<br />\n";
				Database::query("UPDATE ORDER_PRODUCTS SET DESCRIPTION = '".addslashes($obj->getDescription())."', PRICE = '".$obj->getPrice()."', WEIGHT = '".$obj->getWeight()."',
													   STOCK = '".$obj->getStock()."', CAT = '".$obj->getCat()."', SORT_ALPHA = '".$sortAplha."', SORT_KEY = '".$sortKey."', ACTIVE = 1 WHERE ID = '".$result->ID."'");
			}
			// Insert new product
			else
			{
                echo "Insert: ".$obj->getPartNumber().", Cat: ".$obj->getCat()."<br />\n";
				Database::query("INSERT INTO ORDER_PRODUCTS SET PART_NUMBER = '".addslashes($partNumber)."', DESCRIPTION = '".addslashes($obj->getDescription())."', PRICE = '".$obj->getPrice()."', WEIGHT = '".$obj->getWeight()."', STOCK = '".$obj->getStock()."', CAT = '".$obj->getCat()."', SORT_ALPHA = '".$sortAplha."', SORT_KEY = '".$sortKey."', ACTIVE = 1");
			}
		}

		// Close file pointer
		$this->close();
	}

	public function setFile($fileLocation)
	{
		$this->fileLocation = $fileLocation;
	}

	public function getFile()
	{
		return $this->fileLocation;
	}

	public function setPartNumber($partNumber)
	{
		$this->partNumber = $partNumber;
	}

	public function getPartNumber()
	{
		return trim($this->partNumber);
	}

	public function setDescription($description)
	{
		$this->description = $description;
	}

	public function getDescription()
	{
		return trim($this->description);
	}

	public function setPrice($price)
	{
		$this->price = $price;
	}

	public function getPrice()
	{
		return number_format($this->price, 2);
	}

	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	public function getWeight()
	{
		return $this->weight;
	}

	public function setStock($stock)
	{
		$this->stock = $stock;
	}

	public function getStock()
	{
		return (int)$this->stock;
	}

	public function setCat($cat)
	{
		// If cat matches
		$this->cat = $cat;
	}

	public function getCat()
	{
		preg_match("/[A-Z]*/", $this->getPartNumber(), $matches);
       // print_r($matches);
		$cat = $matches[0];

		if (empty($cat)) {
			$cat = 'BQ';
		}
        else {

            switch ($cat)
            {
                case 'BQB':
                case 'BQJAWS':
                case 'BQJAW':
                case 'BQRHC':
                case 'PV':
                case 'T':
                case 'W':
                case 'BQGAUGE':
                case 'BQE':
                case 'BQV':
                    $cat = 'BQ';
                break;
                case (substr($cat, 0, 2) == 'SS'):
                    $cat = 'SS';
                break;
            }
        }

		return $cat;
	}
}
?>
