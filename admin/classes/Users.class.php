<?php

class Users
{
	public $id				= 0;
	public $user            = null;
	public $deliveryAddress = null;
	public $invoiceAddress  = null;
	
	/** 
     * Return the current logged in user
	 *
	 * @return object An object of the row containing the user info
	*/
	public function getUser()
	{
		if ($this->user) {
			return $this->user;
		}

		$this->id = $_SESSION['userId'];

		$q = Database::query("SELECT * FROM USERS WHERE ID = '".$this->id."'");
		$result = Database::fetch_obj($q);

		if ($result) {
			$this->user = $result;
			return $this->user;
		} else {
			return false;
		}
	}

	/** 
     * Return the users invoice address
	 *
	 * @return object
	*/
	public function getInvoiceAddress()
	{
		if ($this->invoiceAddress) {
			return $this->invoiceAddress;
		}

		$q = Database::query("SELECT * FROM USER_ADDRESS WHERE USER_ID = '".$_SESSION['userId']."' AND ADDRESS_TYPE = 'invoice'");
		$result = Database::fetch_obj($q);

		if ($result) {
			$this->invoiceAddress = $result;
			return $this->invoiceAddress;
		} else {
			return false;
		}
	}

	/** 
     * Return the users delivery address
	 *
	 * @return object
	*/
	public function getDeliveryAddress()
	{
		if ($this->deliveryAddress) {
			return $this->deliveryAddress;
		}

		$q = Database::query("SELECT * FROM USER_ADDRESS WHERE USER_ID = '".$_SESSION['userId']."' AND ADDRESS_TYPE = 'delivery'");
		$result = Database::fetch_obj($q);

		if ($result) {
			$this->deliveryAddress = $result;
			return $this->deliveryAddress;
		} else {
			return false;
		}
	}

	public function addressToString($type)
	{
		if ($type == 'invoice') {
			$tmp = $this->invoiceAddress;
		}
		else {
			$tmp = $this->deliveryAddress;
		}
		
		if (!is_object($tmp)) {
			return null;
		}

		$str = $tmp->NAME."\n".$tmp->ADDRESS1."\n";
		if (!empty($tmp->ADDRESS2)) {
			$str .= $tmp->ADDRESS2."\n";
		}
		$str .= $tmp->SUBURB." ".$tmp->STATE." ".$tmp->POSTCODE;

		return $str;
	}

	/** 
     * Return the user id
	 *
	 * @return integer
	*/
	public function getUserId()
	{
		return $this->id;
	}

	/** 
     * Return the users username
	 *
	 * @return string
	*/
	public function getUsername()
	{
		return $this->user->USERNAME;
	}

	/** 
     * Return the users email address
	 *
	 * @return string
	*/
	public function getEmail()
	{
		return $this->user->EMAIL;
	}

	/** 
     * Return the users last login date
	 *
	 * @return date
	*/
	public function getLastLogin()
	{
		return $this->user->LAST_LOGIN;
	}

	/** 
     * Return when the user was created at
	 *
	 * @return date
	*/
	public function getCreatedAt()
	{
		return $this->user->CREATED_AT;
	}

	/** 
     * Return how many times the user has logged in
	 *
	 * @return integer
	*/
	public function getTotalLogins()
	{
		return $this->user->TOTAL_LOGINS;
	}

	/** 
     * Return if the user requires an order number or not
	 *
	 * @return boolean
	*/
	public function getOrderNoRequired()
	{
		return ($this->user->ORDER_NO_REQ == 'Y' ? true : false);
	}
}
?>