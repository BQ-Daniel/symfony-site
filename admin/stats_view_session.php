<?php
// Total results per page
$perPage = 15;
// Setup defaults
if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}

// Get username
$sql = "SELECT u.USERNAME, DATE_FORMAT(ul.LOGIN_TIME, '%a, %d/%m/%y %h:%i:%s %p') as LOGIN_TIME, DATE_FORMAT(ul.LOGIN_END, '%a, %d/%m/%y %h:%i:%s %p') as LOGIN_END FROM USER_LOGINS ul
        INNER JOIN USERS u ON (ul.USER_ID = u.ID)
        WHERE ul.ID = '".addslashes($_REQUEST['id'])."' LIMIT 1";
$q   = Database::query($sql);
$result = Database::fetch_obj($q);
$username  = $result->USERNAME;
?>
<div style="float: left; margin-right: 12px;">
    <a href="stats.php?page=sessions"><img src="images/stats_sessions.jpg" title="Sessions" alt="Sessions" border="0" /></a>
</div>
<div style="float: left;">
<b>Username</b>: <?php echo $username ?><br />
<b>Session Start</b>: <?php echo $result->LOGIN_TIME ?><br />
<b>Session End</b>: <?php echo $result->LOGIN_END ?><br />
</div>

<div style="clear: both;">
    <?php
    $sql = "SELECT mk.MAKE, m.MODEL, sm.MODEL as SUB_MODEL, DATE_FORMAT(uv.VIEW_DATE, '%h:%i:%s %p') as VIEW_DATE FROM USER_VIEWS uv
            INNER JOIN PRODUCTS p ON (p.PRODUCT_ID = uv.PRODUCT_ID)
            INNER JOIN MODELS sm ON (p.MODEL_ID = sm.MODEL_ID)
            INNER JOIN MODELS m ON (sm.PARENT_MODEL = m.MODEL_ID)
            INNER JOIN MAKES mk ON (mk.MAKE_ID = m.MAKE_ID)
            WHERE uv.USER_LOGINS_ID = '".addslashes($_REQUEST['id'])."'
            AND uv.PRODUCT_ID > 0
            GROUP BY sm.MODEL_ID
            ";
    $q   = Database::query($sql);
    $totalResults = Database::count_result($q);
    
    $orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.$_REQUEST['orderBy'].' '.$_REQUEST['sort'].' ' : ' ORDER BY VIEW_DATE ASC');
    $q   = Database::query($sql." $orderBy LIMIT $_REQUEST[p], $perPage");

    $sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');

    $url = "id=".$_REQUEST['id']."&orderBy=".$_REQUEST['orderBy']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
    $url2 = "id=".$_REQUEST['id']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
    ?>

    <div align="right" style="padding-right: 23px;">
        <form id="form" action="stats.php?page=view_session" method="post"> 
        <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>" />
        <input type="hidden" name="from" value="<?php echo $_REQUEST['from'] ?>" />
        <input type="hidden" name="to" value="<?php echo $_REQUEST['to'] ?>" />
        <input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
        <input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
            Page: <select name="p" onChange="this.form.submit();">
            <?php
            $num=0;
            for($i=0;$i<ceil($totalResults/$perPage);$i++)
            {
                echo '<option value="'.($num).'"';
                if($_REQUEST['p'] == $num) {
                    echo " selected";
                }
                echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
                $num=$num+$perPage;
            }
            ?>
            </select>
        </form>
    </div>
    <table cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="tdCell"><a href="stats.php?page=view_session&orderBy=make&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Make</a></td>
            <td class="tdCell"><a href="stats.php?page=view_session&orderBy=model&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Model</a></td>
            <td class="tdCell"><a href="stats.php?page=view_session&orderBy=sub_model&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Sub Model</a></td>
            <td class="tdCell" style="width: 218px;"><a href="stats.php?page=view_session&orderBy=view_date&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Viewed at</a></td>
        </tr>
        <?php if ($totalResults > 0): ?>
            <?php while($result = Database::fetch_obj($q)): ?>
        <tr>
            <td><?php echo $result->MAKE ?></td>
            <td><?php echo $result->MODEL ?></td>
            <td><?php echo $result->SUB_MODEL ?></td>
            <td><?php echo $result->VIEW_DATE ?></td>
        </tr>
            <?php endwhile; ?>
        <?php else: ?>
        <tr>
            <td colspan="3">No results</td>
        </tr>
        <?php endif; ?>
    </table>
</div>