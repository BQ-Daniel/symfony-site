<?php
// Total results per page
$perPage = 15;
// Setup defaults
if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}

$numResults = 50000;
?>


<div style="float: left; margin-right: 12px;">
    <a href="stats.php?page=views"><img src="images/stats_views.jpg" title="Views" alt="Views" border="0" /></a>
</div>
<div style="float: left;">
    <?php include_template('stats/filter_with_make', array('page' => 'views'));  ?>
</div>

<div style="clear: both; height: 15px;"></div>

<?php

if (isset($_POST['submit']) || !empty($_REQUEST['orderBy']))
{
    $_REQUEST['from'] = (isset($_REQUEST['from']) ? $_REQUEST['from'] : date('d/m/Y'));
    $_REQUEST['to'] = (isset($_REQUEST['to']) ? $_REQUEST['to'] : date('d/m/Y', strtotime("+1 day")));


    // Check dates, make sure they are valid
    $fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? split('/', $_REQUEST['from']) : array());
    $toDate = (substr_count($_REQUEST['to'], '/') == 2 ? split('/', $_REQUEST['to']) : array());

    if (count($fromDate) == 3 && count($toDate) == 3) {
        
        $usernameSql = '';
        if (!empty($_REQUEST['username'])) {
            $usernameSql = " AND u.username = '".$_REQUEST['username']."'";
        }
        
        // If specific make is selected
        if (!empty($_POST['make'])) 
        {
            // Get make name
            $q = Database::query("SELECT MAKE FROM MAKES WHERE MAKE_ID = '".$_POST['make']."'");
            $makeName = Database::result($q);

            $q = Database::query("SELECT COUNT(*) as total, m.MODEL FROM USER_VIEWS uv
            INNER JOIN USER_LOGINS ul ON uv.USER_LOGINS_ID = ul.ID
            INNER JOIN USERS u ON ul.USER_ID = u.ID
            INNER JOIN MODELS m ON (m.MODEL_ID = uv.MODEL_ID AND m.PARENT_MODEL = 0)
            WHERE m.MAKE_ID = '".$_POST['make']."' AND uv.MODEL_ID > 0 AND LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND
            LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY)
            ".(!empty($usernameSql) ? $usernameSql : " AND u.username NOT IN ('".implode("', '", $excludeFromStats)."')")."
            GROUP BY uv.MODEL_ID ORDER BY total DESC LIMIT ".$numResults);
        }
        // else display top makes / models / parts
        else 
        {
            $sql = "SELECT COUNT(uv.SUB_MODEL_ID) as total, mk.MAKE, m.MODEL, m2.MODEL as SUB_MODEL, uv.YEAR FROM USER_VIEWS uv
            INNER JOIN USER_LOGINS ul ON uv.USER_LOGINS_ID = ul.ID
            INNER JOIN USERS u ON ul.USER_ID = u.ID
            INNER JOIN MAKES mk ON (mk.MAKE_ID = uv.MAKE_ID)
            INNER JOIN MODELS m ON (m.MODEL_ID = uv.MODEL_ID)
            INNER JOIN MODELS m2 ON (m2.MODEL_ID = uv.SUB_MODEL_ID)
            WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND
            LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) AND uv.PRODUCT_ID = 0
            ".(!empty($usernameSql) ? $usernameSql : " AND u.username NOT IN ('".implode("', '", $excludeFromStats)."')")."
            GROUP BY (uv.SUB_MODEL_ID)
            ORDER BY total DESC LIMIT ".$numResults;
            $q = Database::query($sql);
        }
    }

    $sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');
?>


    <?php if (empty($_POST['make'])): ?>
    <div style="margin-bottom: 10px;"><strong>Top 10 viewed Makes / Models</strong><?php echo (!empty($_REQUEST['username']) ? '<br />for '.$_REQUEST['username'] : '' ) ?></div>
    <table cellspacing="2" cellpadding="2" border="0" width="500">
        <tr>
            <td class="tdCell" style="width: 20px;">#</td>
            <td class="tdCell" style="width: 80px;">Makes</td>
            <td class="tdCell">Model</td>
            <td class="tdCell">Sub Model</td>
            <td class="tdCell">Year</td>
            <td class="tdCell" style="width: 20px;">Total</td>
        </tr>
        <?php $i=1; while ($result = Database::fetch_obj($q)): ?>
        <tr valign="top">
            <td><?php echo $i++ ?>.</td>
            <td><?php echo $result->MAKE ?></td>
            <td><?php echo $result->MODEL ?></td>
            <td><?php echo $result->SUB_MODEL ?></td>
            <td><?php echo $result->YEAR ?></td>
            <td><?php echo $result->total ?></td>
        </tr>
        <?php endwhile; ?>
    </table>
    <?php else: ?>
    <div style="margin-bottom: 10px;"><strong><?php echo $makeName ?>: Top <?php echo $numResults ?> Models</strong><?php echo (!empty($_REQUEST['username']) ? '<br />for '.$_REQUEST['username'] : '' ) ?></div>
    <table cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="tdCell" style="width: 20px;">#</td>
            <td class="tdCell" style="width: 438px;">Model</td>
        </tr>
        <?php $i = 0; while ($result = Database::fetch_obj($q)): $i++; ?>
        <tr>
            <td><?php echo $i ?>.</td>
            <td><?php echo $result->MODEL ?> (<?php echo $result->total ?>)</td>
        </tr>
        <?php endwhile; ?>
    </table>
    <?php endif; ?>
<?php
}
?>