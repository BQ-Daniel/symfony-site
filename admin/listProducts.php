<?php
require_once('include/load.php');
?>
	<table cellspacing="2" cellpadding="2" border="0">
		<tr>
			<td class="tdCell">Make</td>
			<td class="tdCell">Model</td>
			<td class="tdCell">Sub Model</td>
		</tr>
		<tr>
			<td>
				<?php $q = Database::query('SELECT * FROM MAKES'); $makeId = v('MAKE_ID'); ?>
				<select id="productMake" name="MAKE_ID" style="width:100%;" onChange="loadCategories('MODEL'); listProducts('MAKE');">
				<?php if (!empty($makeId)): ?>
					<option value="">Please select</option>
					<?php $q = Database::query('SELECT * FROM MAKES ORDER BY MAKE ASC'); ?>
					<?php $rss = Database::query_to_array($q); ?>
					<?php foreach ($rss as $rs): ?>
					<option value="<?php echo $rs->MAKE_ID ?>"<?php echo ($rs->MAKE_ID == v('MAKE_ID') ? ' selected' : null) ?>><?php echo $rs->MAKE ?></option>
					<?php endforeach; ?>
				<?php else: ?>
					<?php include('categoryOptions.php'); ?>
				<?php endif; ?>
				</select>
			</td>
			<td>
				<select id="productModel" name="MODEL_ID" style="width:100%;" onChange="loadCategories('SUB_MODEL'); listProducts('MODEL');">
				<?php if (v('MAKE_ID')): ?>
					<?php $q = Database::query("SELECT * FROM MODELS WHERE MAKE_ID = '".v('MAKE_ID')."' AND PARENT_MODEL = 0"); ?>
					<?php $results = Database::query_to_array($q); ?>
					<option value="">Please select</option>
					<?php foreach ($results as $rs): ?>
					<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('MODEL_ID') ? ' selected' : '') ?>><?php echo $rs->MODEL ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
				</select>
				</td>
			<td><select id="productSubModel" name="SUB_MODEL_ID" style="width:100%;" onChange="listProducts('SUB_MODEL');">
				<?php 
				$q = null;
				if (v('MODEL_ID')):
					$q = Database::query("SELECT * FROM MODELS WHERE PARENT_MODEL = '".v('MODEL_ID')."'");
				endif;
				if ($q) {
				$results = Database::query_to_array($q); ?>
				<?php if (count($results) > 0): ?>
					<option value="">Please select</option>
					<?php foreach ($results as $rs): ?>
					<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('SUB_MODEL_ID') ? ' selected' : '') ?>><?php echo $rs->MODEL ?></option>
					<?php endforeach; ?>
				<?php endif; } ?>
			</select></td>
		</tr>
	</table>

	<br />

	<div id="listProducts">
		<?php if (isset($_POST['SUB_MODEL_ID'])): ?>
		<script type="text/javascript">
			listProducts('SUB_MODEL');
		</script>
		<?php endif; ?>
	</div>