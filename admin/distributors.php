<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'distributors')); 
?>
			<td id="left">

				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="distributors_locations.php">Locations searched</a>
						<li> <a href="distributors_clicked.php">Distributors clicked</a>
					</ul>
					<br />
				</div>

				<br />
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p>@ DISTRIBUTORS</p>
				</div>
				
				<div id="content">

				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>