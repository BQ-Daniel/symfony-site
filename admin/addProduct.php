<?php	
					// Load
					require_once('include/load.php');
					
					if (isset($_POST['delete']))
					{
						// First get ILL Number to delete
						Database::query("DELETE FROM PRODUCTS WHERE PRODUCT_ID = '".$_POST['PRODUCT_ID']."'");
						$_POST = array();
						$_REQUEST = array();

					}
					elseif (isset($_POST['add'])) {
						$errorMsg = false;

						// Make sure all required fields are filled in
						$requiredFields = array('MAKE_ID', 'MODEL_ID', 'SUB_MODEL_ID', 'BQ_NUMBER', 'YEAR', 'HOSE_POSITION', 'ILL_NUMBER');
						
						foreach($requiredFields as $field) {
							if (empty($_POST[$field])) {
								$errorMsg = "Please make sure required fields are filled in.";
								break;
							}
						}
						

						// If image is being uploaded
						if (!$errorMsg) {
							if (!empty($_FILES['photo']['tmp_name']))
							{
								// if image isnt jpeg, give error
								$mime = $_FILES['photo']['type'];

								if ($mime == "image/jpeg" || $mime == "image/pjpeg") {
									$uploadPhoto = true;								
								}
								else {
									$errorMsg = "Photo image needs to be jpeg.";
								}
							}
						}
						

						// If no errors - add to database
						if (!$errorMsg) {
							if (!empty($_POST['PRODUCT_ID'])) {
								$addToQuery = "PRODUCT_ID = '".$_POST['PRODUCT_ID']."', ";
							}
							else {
								$addToQuery = null;
							}

							Database::query("REPLACE INTO PRODUCTS SET ".$addToQuery." MODEL_ID = '".$_POST['SUB_MODEL_ID']."', BQ_NUMBER = '".$_POST['BQ_NUMBER']."', HOSE_POSITION = '".$_POST['HOSE_POSITION']."', ILL_NUMBER = '".$_POST['ILL_NUMBER']."', CUT_HOSE = '".$_POST['CUT_HOSE']."', AC = '".$_POST['AC']."', AD = '".$_POST['AD']."', AE = '".$_POST['AE']."', COMMENT = '".$_POST['COMMENT']."', DESCRIPTION = '".$_POST['DESCRIPTION']."', YEAR = '".addslashes($_POST['YEAR'])."', DATE_ADDED=CURDATE()");

							$_POST['PRODUCT_ID'] = mysql_insert_id();

							if (isset($uploadPhoto)) {
								$mainDir = '../catalogue/products/';
								$imgName = $_POST['ILL_NUMBER'].'.jpg';
								$imgName2 = $_POST['ILL_NUMBER'].'_s.jpg';
								if(move_uploaded_file($_FILES['photo']['tmp_name'], $mainDir.$imgName)) {
									resizeimg(650, 200, $mainDir.$imgName, $mainDir.$imgName2, 0);
								}	
							}

							echo "<p class=okMsg>Product has been updated</p>";
						}
					}
					
					if (isset($_GET['PRODUCT_ID']) && $_GET['PRODUCT_ID']) {
						$q = Database::query('SELECT p.*, mks.*, pmds.*, p.MODEL_ID as SUB_MODEL_ID FROM PRODUCTS p
						LEFT JOIN MODELS mds ON p.MODEL_ID = mds.MODEL_ID
						LEFT JOIN MAKES mks ON mds.MAKE_ID = mks.MAKE_ID
						LEFT JOIN MODELS pmds ON mds.PARENT_MODEL =  pmds.MODEL_ID
						WHERE PRODUCT_ID = '.v('PRODUCT_ID'));
						$result = Database::fetch_obj($q);
					}
					
					if (isset($errorMsg)) {
						echo "<p class=errMsg>".$errorMsg."</p>";
					}
					
					?>
					<div id="listProducts">
					<a href="javascript:void(0);" onClick="listProducts('SUB_MODEL', '<?php echo v('MAKE_ID') ?>', '<?php echo v('MODEL_ID') ?>', '<?php echo v('SUB_MODEL_ID') ?>')">&lt;&lt; Back to List Products</a>
					<form id="addProduct" action="products.php?page=addProduct" method="post" enctype="multipart/form-data">
					<input type="hidden" name="PRODUCT_ID" value="<?php echo v('PRODUCT_ID') ?>" />
					<table cellspacing="2" cellpadding="2" border="0">
						<tr>
							<td class="tdCell">Make</td>
							<td class="tdCell">Model</td>
							<td class="tdCell">Sub Model</td>
						</tr>
						<tr>
							<td>
								<?php $q = Database::query('SELECT * FROM MAKES'); $makeId = v('MAKE_ID'); ?>
								<select id="productMake" name="MAKE_ID" style="width:100%;" onChange="loadCategories('MODEL');">
								<?php if (!empty($makeId)): ?>
									<option value="">Please select</option>
									<?php $q = Database::query('SELECT * FROM MAKES ORDER BY MAKE ASC'); ?>
									<?php $rss = Database::query_to_array($q); ?>
									<?php foreach ($rss as $rs): ?>
									<option value="<?php echo $rs->MAKE_ID ?>"<?php echo ($rs->MAKE_ID == v('MAKE_ID') ? ' selected' : null) ?>><?php echo $rs->MAKE ?></option>
									<?php endforeach; ?>
								<?php else: ?>
									<?php include('categoryOptions.php'); ?>
								<?php endif; ?>
								</select>
							</td>
							<td>
								<select id="productModel" name="MODEL_ID" style="width:100%;" onChange="loadCategories('SUB_MODEL');">
								<?php if (v('MAKE_ID')): ?>
									<?php $q = Database::query("SELECT * FROM MODELS WHERE MAKE_ID = '".v('MAKE_ID')."' AND PARENT_MODEL = 0"); ?>
									<?php $results = Database::query_to_array($q); ?>
									<option value="">Please select</option>
									<?php foreach ($results as $rs): ?>
									<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('MODEL_ID') ? ' selected' : '') ?>><?php echo $rs->MODEL ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
								</select>
								</td>
							<td><select id="productSubModel" name="SUB_MODEL_ID" style="width:100%;">
								<?php 
								$q = null;
								if (v('SUB_MODEL_ID')): 
									$q = Database::query("SELECT * FROM MODELS WHERE MODEL_ID = '".v('SUB_MODEL_ID')."'"); 
								elseif (v('MODEL_ID')):
									$q = Database::query("SELECT * FROM MODELS WHERE PARENT_MODEL = '".v('MODEL_ID')."'");
								endif;
								$results = Database::query_to_array($q); ?>
								<?php if (count($results) > 0): ?>
									<option value="">Please select</option>
									<?php foreach ($results as $rs): ?>
									<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('SUB_MODEL_ID') ? ' selected' : '') ?>><?php echo $rs->MODEL ?></option>
									<?php endforeach; ?>
								<?php endif; ?>
							</select></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>
					<?php if (v('ILL_NUMBER') && file_exists('../catalogue/products/'.v('ILL_NUMBER').'_s.jpg')): ?>
						<p align="center"><a href="../catalogue/products/<?php echo v('ILL_NUMBER').'.jpg' ?>" target="_blank">Click here to view image</a></p>
					<?php endif; ?>
					<table cellspacing="2" cellpadding="2" border="0">
						<tr>
							<td class="tdCell">Photo</td>
							<td><input type="file" name="photo" size="50" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="tdCell">Brakequip No *</td>
							<td><input type="text" name="BQ_NUMBER" value="<?php echo v('BQ_NUMBER') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Hose Position *</td>
							<td><input type="text" name="HOSE_POSITION" value="<?php echo v('HOSE_POSITION') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Illustration Number *</td>
							<td><input type="text" name="ILL_NUMBER" value="<?php echo v('ILL_NUMBER') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Cut Hose at *</td>
							<td><input type="text" name="CUT_HOSE" size="3" maxlength="3" value="<?php echo v('CUT_HOSE') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">A-C</td>
							<td><input type="text" name="AC" size="3" maxlength="3" value="<?php echo v('AC') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">A-D</td>
							<td><input type="text" name="AD" size="3" maxlength="3" value="<?php echo v('AD') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">A-E</td>
							<td><input type="text" name="AE" size="3" maxlength="3" value="<?php echo v('AE') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Year *</td>
							<td><input type="text" name="YEAR" maxlength="50" value="<?php echo v('YEAR') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Comment</td>
							<td><input type="text" name="COMMENT" size="60" value="<?php echo v('COMMENT') ?>" /></td>
						</tr>
						<tr>
							<td class="tdCell">Description</td>
							<td><textarea name="DESCRIPTION" cols="60" rows="8"><?php echo v('DESCRIPTION') ?></textarea></td>
						</tr>
						<tr>
							<td colspan="2" align="right">
							<?php if (v('PRODUCT_ID')): ?>
								<input type="submit" name="add" value="Modify Product" class="btn" /> 
								<input type="submit" name="delete" value="Delete Product" class="btn" /> 
							<?php else: ?>
								<input type="submit" name="add" value="Add Product" class="btn" />
							<?php endif; ?>

							</td>
						</tr>
					</table>

					</form>
					</div>