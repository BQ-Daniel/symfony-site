<?php
include('../include/load.php');
include('../include/excelReader.php');
error_reporting(0);

// Excel Importing
require_once('../classes/ExcelProducts.class.php');

set_time_limit(0);
	
function addFileToDatabase($file)
{
	$added = 0;
	$updated = 0;
	// ExcelFile($filename, $encoding);
	$data = new Spreadsheet_Excel_Reader();
	// Set output Encoding.
	$data->setOutputEncoding('CP1251');
	// What file to read
	$data->read($file);

        // Read excel document and get products
        $doc = new ExcelProducts();
        $doc->initialize($data);
        $subModels = $doc->getSubModels();

        foreach ($subModels as $subModel => $years) {
            
                // Get sub model id
               $q = mysql_query("SELECT MODEL_ID FROM MODELS WHERE UPPER(MODEL) = '".strtoupper($subModel)."'");
               $result = mysql_fetch_array($q);
               $modelId = $result['MODEL_ID'];
               
               if ($modelId < 0) {
                   echo "SKIPPED MODEL";
                   continue;
               }

                foreach ($years as $year => $products) {

                        $year = trim(addslashes($year));
                        foreach ($products as $product) {
                                $bqNumber = str_replace(' ', '', v(BQ_NO, $product));
                                echo $bqNumber.': '.v(POS, $product).", ".addslashes(v(PART_NO, $product)).' '.$year.'<br />';
                                Database::query("UPDATE PRODUCTS SET HOSE_POSITION = '".v(POS, $product)."' WHERE YEAR = '".$year."' AND BQ_NUMBER = '".$bqNumber."' AND PART_NUMBER = '".addslashes(v(PART_NO, $product))."' AND MODEL_ID = '".$modelId."'");
                                
                                $added++;
                        }
                }
        }

	return array('added' => $added);
}


define('PATH', dirname(__FILE__) . '/files/');

function load($dir) {
    $mydir = opendir($dir);
    while (false !== ($file = readdir($mydir))) {
        if ($file != "." && $file != "..") {
            chmod($dir . $file, 0777);
            if (is_dir($dir . $file)) {
                chdir('.');
                echo "<b>Entering directory</b>: " . $dir . $file . "<br />\n";
                load($dir . $file . '/');
                echo "<b>Imported directory</b><br /><br />\n\n";
            } else {
                echo "<u>Importing file</u>: " . $file . "";
                $rs = addFileToDatabase($dir . $file);
                echo " - Updated: " . $rs['added']."<br />\n";
            }
        }
    }
    closedir($mydir);
}

load(PATH);
?>