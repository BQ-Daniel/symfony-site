<?php
require_once('include/load.php');
sleep(1);

// Select order
$sql = "SELECT *, o.ID as ORDER_ID, DATE_FORMAT(o.DATE_ADDED, '%d/%m/%y %h:%i%p') as DATE_ADDED FROM ORDERS o INNER JOIN USERS u ON o.USER_ID = u.ID WHERE o.ID = '".$_POST['orderId']."'";
$q   = Database::query($sql);
$result = Database::fetch_obj($q);

// If no order, close window
if (!$result):
?>
<script language="javascript" type="text/javascript" defer>
<!--
closeBox();
// -->
</script>
<?php endif; ?>
<div align="center"><font style="font-size: 14px; font-weight: bold;">Order Status: <?php echo (v("STATUS") == "S" ? "<font color=red>SUBMITTED" : "<font color=green>PROCESSED") ?></font></font> </div><br /> <br />
<table cellspacing="2" cellpadding="2" border="0" class="view-order">
	<tr>
		<td class="td">Username</td>
		<td><?php echo v("USERNAME") ?></td>
		<td width="50"></td>
		<td class="td">Date Submitted</td>
		<td><?php echo v("DATE_ADDED") ?></td>
	</tr>
	<tr>
		<td class="td">Order Number</td>
		<td><?php echo v("ORDER_NO") ?></td>
		<td></td>
		<td class="td">Order By</td>
		<td><?php echo v("ORDER_BY") ?></td>
	</tr>
	<tr valign="top">
		<td class="td">Ship Via</td>
		<td><?php echo ucfirst(str_replace('_', ' ', v("SHIP_VIA"))) ?></td>
		<td></td>
		<td class="td">Shipping to</td>
		<td><?php echo nl2br(v("SHIPPING_TO")) ?></td>
	</tr>
	<tr valign="top">
		<td class="td">Total Weight</td>
		<td><?php echo v("WEIGHT") ?>kgs</td>
		<td></td>
		<td class="td">Total Cost</td>
		<td>$<?php echo number_format(v("COST"), 2) ?></td>
	</tr>
</table>
<br /><br />
<?php 
// Select order details
$sql = "SELECT * FROM ORDER_DETAILS od INNER JOIN ORDER_PRODUCTS op ON od.ORDER_PRODUCT_ID = op.ID WHERE od.ORDER_ID = '".$_POST['orderId']."'";
$q   = Database::query($sql);
?>
<div class="orders">
	<table cellspacing="2" cellpadding="2" border="0" class="view-order" width="100%">
		<tr>
			<td class="td">Part Number</td>
			<td class="td">Weight</td>
			<td class="td">Qty</td>
			<td class="td">Unit Price</td>
		</tr>
		<?php while ($result = Database::fetch_obj($q)): ?>
		<tr valign="top">
			<td><?php echo v("PART_NUMBER") ?></td>
			<td><?php echo v("WEIGHT") ?></td>
			<td><?php echo v("QTY") ?></td>
			<td>$<?php echo number_format(v("PRICE"), 2) ?></td>
		</tr>
		<?php endwhile; ?>
	</table>
</div>