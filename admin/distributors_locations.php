<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'distributors'));

$_REQUEST['from'] = (isset($_REQUEST['from']) ? $_REQUEST['from'] : date('d/m/Y'));
$_REQUEST['to'] = (isset($_REQUEST['to']) ? $_REQUEST['to'] : date('d/m/Y', strtotime("+1 day")));
?>
			<td id="left">

				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="distributors_locations.php">Locations searched</a>
						<li> <a href="distributors_clicked.php">Distributors clicked</a>
					</ul>
					<br />
				</div>

				<br />
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p>@ DISTRIBUTORS > Locations searched</p>
				</div>
				
				<div id="content">
                                    <form action="<?php echo $_SERVER['PHP_SELF'] ?> " method="post">
                                    Date <input type="text" name="from" value="<?php echo $_REQUEST['from'] ?>" /> to <input type="text" name="to" value="<?php echo $_REQUEST['to'] ?>" />
                                    <input type="submit" name="submit" value="Go" />
                                    
<?php
    if (isset($_REQUEST['submit']))
    {
        // Check dates, make sure they are valid
        $fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? explode('/', $_REQUEST['from']) : array());
        $toDate = (substr_count($_REQUEST['to'], '/') == 2 ? explode('/', $_REQUEST['to']) : array());

        if (count($fromDate) == 3 && count($toDate) == 3) {
            $sql = "SELECT * FROM distributor_search ds WHERE ds.created_at >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND ".
                    "ds.created_at <= DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) ORDER BY ds.created_at ASC";
            $q = mysql_query($sql) or die(mysql_error());
            $totalResults = mysql_num_rows($q);
            ?>
            <p><b>Total Results:</b> <?php echo $totalResults ?></p>
            
            <?php if ($totalResults > 0): ?>
            <table cellspacing="2" cellpadding="2" border="0" width="100%">
                <tr>
                    <td class="tdCell">Location query</td>
                    <td class="tdCell" style="width: 40px">Radius</td>
                    <td class="tdCell" style="width: 60px">No. results</td>
                    <td class="tdCell" style="width: 60px">Date</td>
                </tr>
            <?php while ($row = mysql_fetch_array($q)): ?>
                <tr valign="top">
                    <td><?php echo stripslashes($row['location']) ?></td>
                    <td><?php echo $row['radius'] ?>km</td>
                    <td><?php echo $row['total_results'] ?></td>
                    <td><?php echo date('d/m/y ', strtotime($row['created_at'])) ?></td>
                </tr>
            <?php endwhile; ?>
            </table>    
            <?php endif; ?>
                                    
            <?php            
        }
    }
?>
                                    </form>
                                    
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>