    <?php
        $_REQUEST['from'] = (isset($_REQUEST['from']) ? $_REQUEST['from'] : date('d/m/Y'));
        $_REQUEST['to'] = (isset($_REQUEST['to']) ? $_REQUEST['to'] : date('d/m/Y', strtotime("+1 day")));


        // Check dates, make sure they are valid
        $fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? split('/', $_REQUEST['from']) : array());
        $toDate = (substr_count($_REQUEST['to'], '/') == 2 ? split('/', $_REQUEST['to']) : array());

        if (count($fromDate) == 3 && count($toDate) == 3) {
            
            $usernameSql = (!empty($_REQUEST['username']) ? " AND UPPER(u.USERNAME) = '".addslashes(strtoupper($_REQUEST['username']))."'" : '');
            $sql = "SELECT *, ul.ID, COUNT(uv.USER_LOGINS_ID) parts_viewed FROM USER_LOGINS ul ".
                    "INNER JOIN USERS u ON ul.USER_ID = u.ID ".
                    "LEFT JOIN USER_VIEWS uv ON (ul.ID = uv.USER_LOGINS_ID AND uv.PRODUCT_ID > 0) ".
                    "WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND ".
                    "LOGIN_TIME <= DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) ".
                    "$usernameSql GROUP BY ul.ID";

            // Count
            $q = Database::query($sql);
            $totalResults = Database::count_result($q);
            
            if ($totalResults > 0) {
                $orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.$_REQUEST['orderBy'].' '.$_REQUEST['sort'].' ' : ' ');
                $sql = $sql." $orderBy LIMIT $_REQUEST[p], $perPage";
                

                $q = Database::query($sql);

                $sql = "SELECT FLOOR(AVG(UNIX_TIMESTAMP(LOGIN_END) - UNIX_TIMESTAMP(LOGIN_TIME))) as avg FROM USER_LOGINS ul ".
                    "INNER JOIN USERS u ON u.ID = ul.USER_ID ".
                    "WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND ".
                    "LOGIN_TIME <= DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) $usernameSql ";
                $query = Database::query($sql);
                $result = Database::fetch_obj($query);
            }
        } else {
            $totalResults  = 0;
        }

        $sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');

        $url = "orderBy=".$_REQUEST['orderBy']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
        $url2 = "sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];

    ?>
    <?php if ($totalResults > 0): ?>
    <div style="float: left;">Average Session Length: <?php echo ($result->avg / 60 > 0 ? floor($result->avg / 60).' mins' : floor($avg).' secs' ) ?> </div>
    <?php endif; ?>
    <div align="right" style="padding-right: 23px;">
        <form id="form" action="stats.php?page=sessions" method="post"> 
        <input type="submit" id="submit-search" name="submit" value="Submit" style="display: none"/>
        <input type="hidden" name="from" value="<?php echo $_REQUEST['from'] ?>" />
        <input type="hidden" name="to" value="<?php echo $_REQUEST['to'] ?>" />
        <input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
        <input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
            Page: <select name="p" onChange="document.getElementById('submit-search').click();">
            <?php
            $num=0;
            for($i=0;$i<ceil($totalResults/$perPage);$i++)
            {
                echo '<option value="'.($num).'"';
                if($_REQUEST['p'] == $num) {
                    echo " selected";
                }
                echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
                $num=$num+$perPage;
            }
            ?>
            </select>
        </form>
    </div>
    <table cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="tdCell"><a href="stats.php?page=sessions&orderBy=username&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Username</a></td>
            <td class="tdCell" style="width: 218px;"><a href="stats.php?page=sessions&orderBy=login_time&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Session Start</a></td>
            <td class="tdCell" style="width: 90px;"><a href="stats.php?page=sessions&orderBy=parts_viewed&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Parts Viewed</a></td>
        </tr>
        <?php if ($totalResults > 0): ?>
            <?php while($result = Database::fetch_obj($q)): ?>
        <tr>
            <td><a href="stats.php?page=view_session&id=<?php echo $result->ID ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php echo $_REQUEST['to'] ?>"><?php echo $result->USERNAME ?></a></td>
            <td><?php echo date('D, d/m/y g:ia ', strtotime($result->LOGIN_TIME)) ?></td>
            <td><?php echo $result->parts_viewed ?></td>
        </tr>
            <?php endwhile; ?>
        <?php else: ?>
        <tr>
            <td colspan="3">No results</td>
        </tr>
        <?php endif; ?>
    </table>