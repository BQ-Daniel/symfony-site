    <?php $_REQUEST['username'] = (isset($_REQUEST['username']) ? $_REQUEST['username'] : '') ?>
    <?php $_REQUEST['id']       = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '') ?>
    
    <form id="filter" action="stats.php?page=<?php echo $page ?>" method="post" style="display: inline">
    <input id="filter-submit" type="submit" name="submit" value="Submit" style="display: none;" />
    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>" />
    <div class="filter" style="height: 111px">
        <div style="margin: 1px 1px;">
            <span>Filter</span>
            <div class="bg">
                <div style="height:70px">
                    <table cellspacing="2" cellpadding="0" border="0">
                        <tr>
                            <td width="100"><b>Username</b></td>
                            <td><input type="text" name="username" value="<?php echo $_REQUEST['username'] ?>" /></td>
                            <td colspan="2" align="right"><img src="images/btn_filter_go.gif" style="cursor: pointer;" onClick="$('filter-submit').click();" /></td>
                        </tr>
                        <tr>
                            <td><b>Make</b></td>
                            <td>
                                <?php 
                                $q = Database::query("SELECT * FROM MAKES ORDER BY MAKE ASC");
                                ?>
                                <select name="make">
                                    <option value=""></option>
                                    <?php while ($result = Database::fetch_obj($q)): ?>
                                    <option value="<?php echo $result->MAKE_ID ?>"<?php echo (@$_POST['make'] == $result->MAKE_ID ? " selected" : "") ?>><?php echo $result->MAKE ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td width="100"><b>Date</b></td>
                            <td>From: <br /><input type="text" name="from" value="<?php echo (!isset($_REQUEST['from']) ? date('d/m/Y') : $_REQUEST['from']) ?>" /></td>
                            <td>To: <br /><input type="text" name="to" value="<?php echo (!isset($_REQUEST['to']) ? date('d/m/Y') : $_REQUEST['to']) ?>" /></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </form>