    <?php
        $_REQUEST['from'] = (isset($_REQUEST['from']) ? $_REQUEST['from'] : date('d/m/Y'));
        $_REQUEST['to'] = (isset($_REQUEST['to']) ? $_REQUEST['to'] : date('d/m/Y', strtotime("+1 day")));


        // Check dates, make sure they are valid
        $fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? split('/', $_REQUEST['from']) : array());
        $toDate = (substr_count($_REQUEST['to'], '/') == 2 ? split('/', $_REQUEST['to']) : array());

        if (count($fromDate) == 3 && count($toDate) == 3) {
            // Count
            $q = Database::query("SELECT * FROM USER_LOGINS ul LEFT JOIN USERS u ON ul.USER_ID = u.ID WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) AND u.USERNAME != '' GROUP BY ul.USER_ID");
            $totalResults = Database::count_result($q);
            
            if ($totalResults > 0) {
                $orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.$_REQUEST['orderBy'].' '.$_REQUEST['sort'].' ' : ' ');
                $q = Database::query("SELECT *, COUNT(ul.USER_ID) total_logins FROM USER_LOGINS ul LEFT JOIN USERS u ON ul.USER_ID = u.ID WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <= DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) AND u.USERNAME != '' GROUP BY ul.USER_ID".$orderBy."LIMIT $_REQUEST[p], $perPage");
            }
        } else {
            $totalResults  = 0;
        }

        $sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');

        $url = "orderBy=".$_REQUEST['orderBy']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
        $url2 = "sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];


    ?>
    <div align="right" style="padding-right: 23px;">
        <form id="form" action="stats.php?page=logins" method="post"> 
        <input type="hidden" name="from" value="<?php echo $_REQUEST['from'] ?>" />
        <input type="hidden" name="to" value="<?php echo $_REQUEST['to'] ?>" />
        <input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
        <input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
            Page: <select name="p" onChange="this.form.submit();">
            <?php
            $num=0;
            for($i=0;$i<ceil($totalResults/$perPage);$i++)
            {
                echo '<option value="'.($num).'"';
                if($_REQUEST['p'] == $num) {
                    echo " selected";
                }
                echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
                $num=$num+$perPage;
            }
            ?>
            </select>
        </form>
    </div>
    <table cellspacing="2" cellpadding="2" border="0">
        <tr>
            <td class="tdCell"><a href="stats.php?page=logins&orderBy=username&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Username</a></td>
            <td class="tdCell" style="width: 50px;"><a href="stats.php?page=logins&orderBy=total_logins&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Logins</a></td>
            <td class="tdCell" style="width: 247px;"><a href="stats.php?page=logins&orderBy=last_login&<?php echo $url2 ?>" style="color:#000; text-decoration: underline;">Last Login</a></td>
        </tr>
        <?php if ($totalResults > 0): ?>
            <?php while($result = Database::fetch_obj($q)): ?>
        <tr>
            <td><a href="stats.php?page=display_logins&id=<?php echo $result->ID ?>&from=<?php echo $_REQUEST['from'] ?>&to=<?php echo $_REQUEST['to'] ?>"><?php echo $result->USERNAME ?></a></td>
            <td><?php echo $result->total_logins ?></td>
            <td>
                <?php if ($result->LAST_LOGIN == null): ?>
                Never
                <?php else: ?>
                    <?php echo date('l jS \of F Y', strtotime($result->LAST_LOGIN)) ?>
                <?php endif; ?>
            </td>
        </tr>
            <?php endwhile; ?>
        <?php else: ?>
        <tr>
            <td colspan="3">No results</td>
        </tr>
        <?php endif; ?>
    </table>