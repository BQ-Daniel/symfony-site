<html>
	<head>
		<title>BrakeQuip - Administration</title>	
		<script type="text/javascript" src="../js/prototype.js"></script>
		<script type="text/javascript" src="../js/scriptaculous.js?load=effects"></script>
		<script type="text/javascript" src="../js/lightbox.js"></script>
		<SCRIPT type="text/javascript" SRC="js/admin.js"></SCRIPT>

		<link rel="stylesheet" media="screen" href="style.css">

		<meta name="description" content="Stainless steel and rubber brake hose manufacturing systems for all vehicles.">
		<meta name="keywords" content="Brake Hose, Brake Hose Manufacturing, Replacement Brake Hoses, New Brake Hoses, Aftermarket Brake hoses, stainless steel braided brake hoses, rubber brake hoses, braided hoses, performance brake hoses, performance hoses">
		</head>



<body leftmargin="0" topmargin="0" bgcolor="#ffffff">
	<div id="overlay" style="display:none"></div>
	<center><table width="779" cellspacing="0" cellpadding="0" border="0" style="border: 1px solid #4A4A4A;">
		<tr>
			<td id="header" colspan="2">
				<img src="images/brakequip.gif" align="left" />
				<?php include_template('nav', array('page' => $page)) ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr valign="top">