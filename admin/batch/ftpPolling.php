<?php


function uploadWebFile($conn_id, $dir, $entry)
{
    if (!ftp_put($conn_id, $entry, $dir . $entry, FTP_BINARY)) {
        mail('daniel@brakequip.com.au', 'Failed to upload ' . $entry, '',  'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        // mail('john@smytheweb.com', 'Failed to upload ' . $entry, '',  'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        return false;
    }

    $size = ftp_size($conn_id, $entry);
    $size2 = ftp_size($conn_id, "ARCHIVE/".$entry);

    $fp = fopen('data.txt', 'a+');
    fwrite($fp, 'Uploaded '.$entry.", Size: ".$size."\r\n");
    fclose($fp);

    // Check if file exists on server
    if ($size == -1 && $size2 == -1) {
        mail('daniel@brakequip.com.au', 'Check that ' . $entry.' was uploaded successfully. '.$size, '', 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        // mail('john@smytheweb.com', 'Check that ' . $entry.' was uploaded successfully. ('.$size.') ('.$size2.')', '', 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
    }

    // Archive
    copy($dir . $entry, $dir . 'archive/' . $entry);
    // Remove file once completed
    unlink($dir . $entry);

    return true;
}

set_time_limit(0);

require_once('../include/load.php');

// Connect to firewall
$conn_id = ftp_connect(FTP_HOST);

// Open a session to an external ftp site
$login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

// Check open
if ((!$conn_id) || (!$login_result)) {
    echo "Connection failed";
    die;
}

// turn on passive mode transfers
ftp_pasv($conn_id, true);

// Upload any order files to the FTP server
$dir = $ordersDir = "../../order/orders/";
$d = dir($dir);
$orderFiles = array();

while (false !== ($entry = $d->read()))
{
    if ($entry != "." && $entry != "..") {
        // Upload order file
        if (substr($entry, -4) == ORDER_EXTENSION)
        {
            if (!uploadWebFile($conn_id, $ordersDir, $entry)) {
                continue;
            }
        }
    }
}
$d->close();

sleep(6);

// Get a list of orders that have been submitted, we want to check if they have been processed or not
$orders = array();
$q = Database::query("SELECT ORDER_NO, EASE_ORDER_NO FROM ORDERS WHERE STATUS = 'S'");
$notProcessed = array();
while ($result = Database::fetch_obj($q))
{
    // If file is still waiting to be processed, then ignore
    if (ftp_size($conn_id, $result->EASE_ORDER_NO . ".WEB") != '-1') {
        continue;
    }

    // Check if file exists, returns -1 if it doesn't
    if (ftp_size($conn_id, "ARCHIVE/" . $result->EASE_ORDER_NO . ".WEB") != '-1') {
        // Update order to be processed
        Database::query("UPDATE ORDERS SET STATUS = 'P' WHERE ORDER_NO = '" . $result->ORDER_NO . "'");
    }
    // Couldn't find archive version, lets record this
    else {
       $notProcessed[$result->EASE_ORDER_NO . ".WEB"] = true;
    }
}

// Poll directory for PARTS file and Processed order numbers
$contents = ftp_nlist($conn_id, ".");
foreach ($contents as $fileName)
{
    $file = substr($fileName, 2);
    // If STATUS stop credit file,
    if (strtoupper($file) == "STATUS.TAB")
    {
        // Move to server
        ftp_get($conn_id, "../tmp/" . $file, $file, FTP_BINARY);
        // Read file
        if (($handle = fopen("../tmp/" . $file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $account = trim($data[0]);
                $stopCredit = (trim($data[1]) == 'Y' ? 1 : 0);

                // Update user table
                mysql_query("UPDATE USERS SET STOP_CREDIT = '" . $stopCredit . "', UPDATED_AT = NOW() WHERE USERNAME = '" . $account . "'");
            }
            fclose($handle);
        }
        // Remove file from server once we are finished
        unlink("../tmp/" . $file);
        continue;
    }


    // If web file, record
    if (substr($file, -4) == ORDER_EXTENSION) {
        // If it's in the not processed yet array,
        // then removed it as EASE hasn't processed it yet
        if (isset($notProcessed[$file])) {
            unset($notProcessed[$file]);
        }
        continue;
    }

    // If not parts file, ignore
    if (strtoupper($file) != strtoupper(PARTS_FILE)) {
        continue;
    }

    $filename = md5(time() + rand(1, 9999999999999));
    @ftp_get($conn_id, "../tmp/" . $filename, $file, FTP_BINARY);

    if (!file_exists("../tmp/" . $filename)) {
        continue;
    }

    $products = new OrderProducts("../tmp/" . $filename);
    $products->import();
    // Remove from server
    unlink("../tmp/" . $filename);
    // Remove from FTP server
    //ftp_delete($conn_id, $file);
}

unset($orders);

// If there are orders in the $notProcessed array
if (count($notProcessed) > 0)
{
    sleep(30);
    // Check archive folder one more time
    foreach ($notProcessed as $file => $value) {
        if (ftp_size($conn_id, "ARCHIVE/" . $file) != '-1') {
            unset($notProcessed[$file]);
        }
    }

    // Still exist, then resubmit
    if (count($notProcessed) > 0)
    {
        mail("daniel@brakequip.com.au", "Order".$notProcessed > 1 ? 's' : ''." not processed [RESUBMITTED]", print_r($notProcessed, true)."\nThey have been resubmitted.", 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        // mail("john@smytheweb.com", "Order".$notProcessed > 1 ? 's' : ''." not processed [RESUBMITTED]", print_r($notProcessed, true)."\nThey have been resubmitted.", 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        foreach ($notProcessed as $file => $value) {
            copy(realpath($ordersDir.'archive/'.$file), $ordersDir.$file);
            uploadWebFile($conn_id, $ordersDir, $file);
        }
    }

}

// Close FTP Connection
ftp_close($conn_id);
?>
