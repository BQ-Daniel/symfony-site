<?php

set_time_limit(0);

error_reporting(E_ALL);

require_once('../include/load.php');

// Connect to firewall
$conn_id = ftp_connect(FTP_HOST);

// Open a session to an external ftp site
$login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

// Check open
if ((!$conn_id) || (!$login_result)) {
    echo "Connection failed";
    die;
}

// turn on passive mode transfers
ftp_pasv($conn_id, true);

// Poll directory for PARTS file and Processed order numbers
$contents = ftp_nlist($conn_id, "");
foreach ($contents as $file) {

    // If not parts file, ignore
    if (strtoupper($file) != strtoupper('DEBTORS.CSV') && strtoupper($file) != strtoupper('DEBTORS2.CSV')) {
        continue;
    }

    $filename = md5(time() + rand(1, 9999999999999));
    @ftp_get($conn_id, "../tmp/" . $filename, $file, FTP_BINARY);

    if (!file_exists("../tmp/" . $filename)) {
        continue;
    }

    // Open file
    $handle = fopen("../tmp/" . $filename, "r");

    // Ignore first line
    fgetcsv($handle, 1000, ",");

    // Get first line, as this holds the headers
    $data = fgetcsv($handle, 1000, ",");
    defineFieldNames($data);

    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
    {
        /* USERNAMES */
        $username = trim(addslashes($data[AC_CODE]));
        $email = trim(addslashes($data[EMAIL_ADDRESS]));

        if (empty($username) || empty($email)) {
            if (empty($email)) {
                $emailMissing[] = $username;
            } else {
                continue;
            }
        }

        // Check if username exists
        $q = Database::query("SELECT ID FROM USERS WHERE USERNAME = '$username'");
        // add 
        if (Database::count_result($q) == 0) {
            Database::query("INSERT INTO USERS SET USERNAME = '$username', EMAIL = '$email', CREATED_AT = NOW(), DISTRIBUTOR = 1");
            $added[] = $username;
            $userId = mysql_insert_id();
        }
        // update
        else {
            $userId = Database::result($q);
            Database::query("UPDATE USERS SET email = '" . $email . "' WHERE USERNAME = '$username'");
            $updated[] = $username;
        }

        if (empty($userId)) {
            continue;
        }


        /* ADDRESSES */

        $states = array('VIC', 'NSW', 'QLD', 'SA', 'NT', 'WA', 'TAS', 'NZ', 'ACT');

        // Setup variables for DELIVERY ADDRESS
        $name = addslashes(trim($data[NAME]));
        $address1 = addslashes(trim($data[ADDRESS1]));
        $address2 = addslashes(trim($data[ADDRESS2]));
        $suburb = addslashes(trim($data[SUBURB]));
        $state = strtoupper(trim(substr($suburb, -3)));
        $postcode = addslashes(trim($data[POSTCODE]));
        $phone = addslashes(trim($data[PHONE_NO1]));

        if (!in_array($state, $states)) {
            $state = null;
        } else {
            $suburb = trim(str_replace($state, '', $suburb));
        }

        // Check if delivery address exists
        $q = Database::query("SELECT ADDRESS1, ADDRESS2, SUBURB, STATE, POSTCODE, PHONE FROM USER_ADDRESS WHERE USER_ID = '$userId' and ADDRESS_TYPE = 'delivery'");

        $sql = "SET USER_ID = '$userId', NAME = '$name', ADDRESS1 = '$address1', ADDRESS2 = '$address2', SUBURB = '$suburb', STATE = '$state', POSTCODE = '$postcode', PHONE = '$phone', UPDATED_AT = NOW(), ADDRESS_TYPE = 'delivery'";
        // add
        if (Database::count_result($q) == 0) {
            Database::query("INSERT INTO USER_ADDRESS " . $sql);
        }
        // update
        else {
            $result = mysql_fetch_assoc($q);
            $update = false;
            foreach ($result as $key => $value) {
                $key = strtolower($key);
                if ($$key != $value) {
                    $update = true;
                }
            }
            if ($update) {
                Database::query("UPDATE USER_ADDRESS " . $sql . " WHERE USER_ID = '" . $userId . "' AND ADDRESS_TYPE = 'delivery'");
            }
        }


        // Setup variables for INVOICE ADDRESS
        $name = addslashes(trim($data[INVOICE_NAME]));
        $address1 = addslashes(trim($data[INVOICE_ADDRESS1]));
        $address2 = addslashes(trim($data[INVOICE_ADDRESS2]));
        $suburb = addslashes(trim($data[INVOICE_SUBURB]));
        $state = strtoupper(trim(substr($suburb, -3)));
        $postcode = addslashes(trim($data[POSTCODE]));
        $phone = addslashes(trim($data[PHONE_NO1]));


        if (!in_array($state, $states)) {
            $state = null;
        } else {
            $suburb = trim(str_replace($state, '', $suburb));
        }


        // Check if delivery address exists
        $q = Database::query("SELECT ADDRESS1, ADDRESS2, SUBURB, STATE, POSTCODE, PHONE FROM USER_ADDRESS WHERE USER_ID = '$userId' and ADDRESS_TYPE = 'invoice'");

        $sql = "SET USER_ID = '$userId', NAME = '$name', ADDRESS1 = '$address1', ADDRESS2 = '$address2', SUBURB = '$suburb', STATE = '$state', POSTCODE = '$postcode', PHONE = '$phone', UPDATED_AT = NOW(), ADDRESS_TYPE = 'invoice'";
        // add
        if (Database::count_result($q) == 0) {
            Database::query("INSERT INTO USER_ADDRESS " . $sql);
        }
        // update
        else {
            $result = mysql_fetch_assoc($q);
            $update = false;
            foreach ($result as $key => $value) {
                $key = strtolower($key);
                if ($$key != $value) {
                    $update = true;
                }
            }
            if ($update) {
                Database::query("UPDATE USER_ADDRESS " . $sql . " WHERE USER_ID = '" . $userId . "' AND ADDRESS_TYPE = 'invoice'");
            }
        }
    }

    // Remove from server
    unlink("../tmp/" . $filename);
}

// Run geocode updates
include('addressGeocodes.php');

// Close FTP Connection
ftp_close($conn_id);

/**
 * Defines each field name with the value of the key in the array
 *
 * @param array
 */
function defineFieldNames($data) {
    foreach ($data as $key => $value) {
        // Replace -'s with _
        $value = str_replace('-', '', $value);
        $value = str_replace(' ', '_', $value);
        $value = str_replace('/', '', $value);
        define($value, $key);
    }
}

?>