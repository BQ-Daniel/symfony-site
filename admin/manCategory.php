<?php
/*
 * Manage Categories
 * This file will add / delete makes, models and sub models
*/

header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// Load
include('include/load.php');

$success = null;
$error   = null;


/*
 * MAKES
 *
*/

// Add Make
if (isset($_POST['make']) && !empty($_POST['make']))
{
	// Make sure make doesn't already exist
	$q = Database::query("SELECT COUNT(*) FROM MAKES WHERE MAKE = '".$_POST['make']."'");
	if (Database::result($q) == 0)
	{
		Database::query("INSERT INTO MAKES SET MAKE = '".$_POST['make']."'");
		$success = $_POST['make'].' has been added';
	}
	else { 
		$error = $_POST['make'].' already exists';
	}
}

// Delete Make
if (isset($_POST['deleteMake']) && !empty($_POST['deleteMake']))
{
	// Get name of make to display
	$q = Database::query("SELECT MAKE FROM MAKES WHERE MAKE_ID = '".$_POST['deleteMake']."'");
	$makeName = Database::result($q);

	// Make sure make doesn't contain any products
	$q = Database::query("SELECT COUNT(*) FROM PRODUCTS P
	LEFT JOIN MODELS M ON M.MODEL_ID = P.MODEL_ID
	WHERE MAKE_ID = '".$_POST['deleteMake']."'");

	if (Database::result($q) == 0) {
		// Now we can delete make
		Database::query("DELETE FROM MAKES WHERE MAKE_ID = '".$_POST['deleteMake']."'");

		$success = $makeName.' has been deleted';
	}
	else {
		$error	= $makeName.' still contains products';
	}
}

/*
 * MODELS
 *
*/

// Add Model
if (isset($_POST['model']) && !empty($_POST['model']))
{
	// Make sure model doesn't already exist
	$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".$_POST['model']."'");
	if (Database::result($q) == 0)
	{
		// add model to database
		Database::query("INSERT INTO MODELS SET MODEL = '".$_POST['model']."', MAKE_ID = '".$_POST['editMake']."'");
		$success = $_POST['model'].' has been added';
	}
	else { 
		$error = $_POST['model'].' already exists';
	}
}

// Delete Model
if (isset($_POST['deleteModel']) && !empty($_POST['deleteModel']))
{
	// Get name of model to display
	$q = Database::query("SELECT MODEL FROM MODELS WHERE MODEL_ID = '".$_POST['deleteModel']."'");
	$modelName = Database::result($q);

	// Make sure model doesn't contain any products
	$q = Database::query("SELECT COUNT(*) FROM PRODUCTS P
	LEFT JOIN MODELS M ON P.MODEL_ID = M.MODEL_ID
	LEFT JOIN MODELS M2 ON M.MODEL_ID = M2.MODEL_ID
	WHERE M2.PARENT_MODEL = '".$_POST['deleteModel']."'");

	if (Database::result($q) == 0) {

		// Now we can delete make
		Database::query("DELETE FROM MODELS WHERE MODEL_ID = '".$_POST['deleteModel']."'");
		// Delete sub models under that model
		Database::query("DELETE FROM MODELS WHERE PARENT_MODEL = '".$_POST['deleteModel']."'");

		$success = $modelName.' has been deleted';
	}
	else {
		$error	= $modelName.' still contains products';
	}
}

/*
 * SUB MODELS
 *
*/

// Add Sub Model
if (isset($_POST['subModel']) && !empty($_POST['subModel']))
{
	// Make sure the sub model doesn't already exist
	$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".$_POST['subModel']."' AND PARENT_MODEL = '".$_POST['editModel']."'");
	if (Database::result($q) == 0)
	{
		Database::query("INSERT INTO MODELS SET MAKE_ID = '".$_POST['editMake']."', MODEL = '".$_POST['subModel']."', PARENT_MODEL = '".$_POST['editModel']."'");
		$success = $_POST['subModel'].' has been added';
	}
	else { 
		$error = $_POST['subModel'].' already exists';
	}
}

// Delete Sub Model
if (isset($_POST['deleteSubModel']) && !empty($_POST['deleteSubModel']))
{
	// Get name of make to display
	$q = Database::query("SELECT MODEL FROM MODELS WHERE MODEL_ID = '".$_POST['deleteSubModel']."'");
	$modelName = Database::result($q);

	// Make sure sub model doesn't contain any products
	$q = Database::query("SELECT COUNT(*) FROM PRODUCTS P
	LEFT JOIN MODELS M ON M.PARENT_MODEL = P.MODEL_ID
	WHERE PARENT_MODEL = '".$_POST['deleteMake']."'");

	if (Database::result($q) == 0) {
		// Now we can delete make
		Database::query("DELETE FROM MODELS WHERE MODEL_ID = '".$_POST['deleteSubModel']."'");

		$success = $modelName.' has been deleted';
	}
	else {
		$error	= $modelName.' still contains products';
	}
}


/*
 * RESULTS
*/

if ($success) {
	echo "<font class='smlSuccess'>$success</font>";
}
elseif ($error) {
	echo "<font class='smlError'>$error</font>";
}
else {
	return false;
}