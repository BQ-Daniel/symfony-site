<?php
require_once('../include/load.php');

// Select all the rows in the markers table
$query = "SELECT * FROM USER_ADDRESS ua INNER JOIN USERS u ON u.ID = ua.USER_ID WHERE u.DELETED = 0";
$result = mysql_query($query);
if (!$result) {
  die("Invalid query: " . mysql_error());
}

// Initialize delay in geocode speed
$delay = 1000;
$base_url = "http://" . GOOGLE_MAPS_HOST . "/maps/geo?output=xml" . "&key=" . GOOGLE_MAPS_KEY;

// Iterate through the rows, geocoding each address
while ($row = @mysql_fetch_assoc($result)) {
  $geocode_pending = true;

  while ($geocode_pending) {
    $address = trim($row["ADDRESS1"]);
    if ($row['ADDRESS2']) {
        $address .= ' '.trim($row['ADDRESS2']);
    }
    $address .= ' '.trim($row['SUBURB'].' '.$row['STATE'].' '.$row['POSTCODE']);

    $id = $row["ID"];
    $request_url = $base_url . "&q=" . urlencode($address);
    $xml = simplexml_load_file($request_url) or die("url not loading");

    $status = $xml->Response->Status->code;
    if (strcmp($status, "200") == 0) {
      // Successful geocode
      $geocode_pending = false;
      $coordinates = $xml->Response->Placemark->Point->coordinates;
      $coordinatesSplit = split(",", $coordinates);
      // Format: Longitude, Latitude, Altitude
      $lat = $coordinatesSplit[1];
      $lng = $coordinatesSplit[0];

      $query = sprintf("UPDATE USER_ADDRESS " .
             " SET LAT = '%s', LNG = '%s' " .
             " WHERE ID = '%s' LIMIT 1;",
             mysql_real_escape_string($lat),
             mysql_real_escape_string($lng),
             mysql_real_escape_string($id));
      $update_result = mysql_query($query);
      if (!$update_result) {
        die("Invalid query: " . mysql_error());
      }
    } else if (strcmp($status, "620") == 0) {
      // sent geocodes too fast
      $delay += 100000;
    } else {
      // failure to geocode
      $geocode_pending = false;
      echo "Address " . $address . " failed to geocoded. ";
      echo "Received status " . $status . "<br />";
      die('asdfadsfafsd');
    }
    usleep($delay);
  }
}
?>