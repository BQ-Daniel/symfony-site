<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'orders')); 
?>
			<td id="left">
	<div id="box" style="display:none"><div class="close" onClick="return closeBox();">&nbsp;</div><img src="images/order_details_header.png" /><br /><img src="images/loadingImage.gif" id="boxLoading" /><div id="boxContent"></div></div>
				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="javascript:void(0)" onclick="showPage('viewOrders.php');">View Orders</a>
					</ul>
					<br />
				</div>

				<br />
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p>@ ORDERS</p>
				</div>
				
				<div id="content">
				<?php if (isset($_REQUEST['page'])): ?>
					<?php require_once($_REQUEST['page'].".php"); ?>
				<?php endif; ?>
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>