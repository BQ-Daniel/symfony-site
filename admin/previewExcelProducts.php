<?php
// Load
include('include/load.php');
// Excel Importing
require_once('classes/ExcelProducts.class.php');
include('include/excel_reader2.php');

set_time_limit(0);

error_reporting(E_ALL);

if ($_FILES['file']['type'] != 'application/vnd.ms-excel' && $_FILES['file']['type'] != 'application/octet-stream') {
	?>
	<div align="center">
		<h2>Error</h2>
		The file you tried to upload is not an excel file. Please <a href="import.php?page=importProducts">click here</a> to try again.
	</div>
	<?php
	exit;
}

// move uplaoded file to a temp directory
mt_srand();
$filename = md5(time().rand(1,550000));
if (!move_uploaded_file($_FILES['file']['tmp_name'], 'tmp/'.$filename))
{
	?>
	<div align="center">
		<h2>Error</h2>
		Unable to move uploaded file to a temp directory. Please <a href="import.php?page=importProducts">click here</a> to try again.
	</div>
	<?php
	exit;
}

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader('tmp/'.$filename, false);

// Read excel document and get products
$doc = new ExcelProducts();
$doc->initialize($data);
$subModels = $doc->getSubModels();
?>
<html>
	<head>
		<title>Importing Excel Products</title>
		<link rel="stylesheet" media="screen" href="style.css">
	</head>
	
	<body>
		<table cellspacing="2" cellpadding="2" border="0" width="70%" align="center">
			<tr>
				<td class="tdCell" width="50%"><strong>Make</strong></td>
				<td class="tdCell" width="50%"><strong>Model</strong></td>
			</tr>
			<tr>
				<td><?php echo $doc->getMake() ?></td>
				<td><?php echo $doc->getModel() ?></td>
			</tr>
			<?php foreach ($subModels as $subModel => $years): ?>
			<tr>
				<td class="tdCell" colspan="2"><strong>Sub Model -> <?php echo $subModel ?></strong></td>
			</tr>
				<?php foreach ($years as $year => $products): ?>
			<tr>
				<td bgcolor="#f9f9f9" colspan="2"><?php echo $year ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<table cellspacing="0" cellpadding="2" border="0">
						<tr style="background: #fcfcfc; font-weight:bold;">
							<td width="90">Hose Position</td>
							<td width="140">Part Number</td>
							<td width="90">BQ Number</td>
							<td width="30">ILL</td>
							<td>Gerenal Comments</td>
						</tr>
					<?php foreach ($products as $product): ?>
						<tr style="">
							<td><?php echo v(POS, $product) ?></td>
							<td><?php echo v(PART_NO, $product) ?></td>
							<td><?php echo v(BQ_NO, $product) ?></td>
							<td><?php echo v(ILL_NO, $product) ?></td>
							<td><?php echo v(COMMENTS, $product) ?></td>
						</tr>
					<?php endforeach; ?>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
				<?php endforeach; ?>
			<?php endforeach; ?>
			<tr>
				<td colspan="2">
					<table>
						<tr>
							<td class="tdCell">Addable products</td>
							<td width="100"><?php echo $doc->getTotalAdds() ?></td>
							<td class="tdCell">Total ignores</td>
							<td width="100"><?php echo $doc->getTotalIgnores() ?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<div style="width:70%; margin-left: auto; margin-right: auto;" align="center">
		<?php 
		$make = $doc->getMake();
		$model = $doc->getModel();
		if (empty($make)): ?>
		<p>Unable to find Make and/or Model - please make sure the excel spreadsheet is correct.</p>
		<p><input type="button" name="back" value="Go back" onClick="location.href='import.php?page=importProducts'" /></p>
		<?php elseif ($doc->getTotalAdds() > 0): ?>
			<p>&nbsp;</p>
			If everything looks ok, add to the database. Otherwise if you need to fix something up edit the Excel sheet and upload it again.
			<p><input type="button" name="back" value="Go back" onClick="location.href='import.php?page=importProducts'" /> | <input type="button" name="ok" value="Add to database" onClick="location.href='doImportProducts.php?file=<?php echo $filename ?>'" /></p>
		<?php else: ?>
			<p>Nothing to add....</p>
			<p><input type="button" name="back" value="Go back" onClick="location.href='import.php?page=importProducts'" /></p>
		<?php endif; ?>
		</div>
	</body>
</html>