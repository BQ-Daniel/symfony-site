<?php
require_once('include/load.php');
?>
<b>Manage Pictorial - Chapters</b><br />
<a href="javascript:void(0)" onclick="showPage('pictorial.php');">List</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_upload.php');">Upload</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_chapters.php');">Chapters</a>

<div style="height: 10px;"></div>

<?php
// ADD CHAPTER
if (isset($_POST['add']))
{
	// Add to database
	Database::query("INSERT INTO PICTORIAL_CHAPTERS SET NAME = '".addslashes($_POST['chapter'])."', PAGE = '".$_POST['page_no']."'");
	echo "<p class\"okMsg\">".v("chapter")." has been added.</p>";
}

// MODIFY CHAPTER
if (isset($_POST['mod']) && !empty($_POST['chapter']))
{
	Database::query("UPDATE PICTORIAL_CHAPTERS SET PAGE = '".$_POST['page_no']."' WHERE ID = '".addslashes($_POST['chapter'])."'");
	echo "<p class\"okMsg\">The chapter has been modifed to page ".$_POST['page_no'].".</p>";
}

// DELETE CHAPTER
if (isset($_POST['delete']))
{
	Database::query("DELETE FROM PICTORIAL_CHAPTERS WHERE ID = '".addslashes($_POST['chapter'])."'");
	echo "<p class\"okMsg\">Chapter has been deleted.</p>";
}

// If they have hit submit, changes would of been done by now.
// Write these changes to file
if (!empty($_POST['chapter'])) {
	// Write new book settings js file
	writeNewBookSettingsJS();
}


// Get list of chapters
$q = Database::query("SELECT * FROM PICTORIAL_CHAPTERS ORDER BY PAGE ASC");
$chapters = array();
while ($result = Database::fetch_obj($q)) {
	$chapters[v("ID")] = v("NAME");
}
?>

<script>
	function updatePageNo(form, id)
	{
		<?php $q = Database::query("SELECT * FROM PICTORIAL_CHAPTERS ORDER BY PAGE ASC"); ?>
		var chapters = [];
		<?php while ($result = Database::fetch_obj($q)): ?>
        chapters[<?php echo $result->ID ?>] = "<?php echo $result->PAGE ?>";
		<?php endwhile; ?>
		
		if (id != '') {
			form.page_no.value = chapters[id];
		}
    }

	function validateChapterPageNo(form)
	{
		if (form.chapter.value == '' || (typeof(form.page_no) != "undefined" && form.page_no.value == '')) {
			alert('Please ensure the correct fields are filled in');
			return false;
		}
		if (typeof(form.page_no) != "undefined" && isNaN(parseInt(form.page_no.value)))
		{
			alert("Page needs to be a number");
			return false;
		}
		return true;
	}
</script>
<form action="products.php?page=pictorial_chapters" method="post" onSubmit="return validateChapterPageNo(this);">
<table cellspacing="2" cellpadding="2" border="0">
	<tr>
		<td width="100" bgcolor="#e4e4e4">Add</td>
		<td><input type="text" name="chapter" value="" /></td>
		<td>Page <input type="text" name="page_no" value="" size="3" maxlength="5" /></td>
		<td><input type="submit" name="add" value="Add" /></td>
	</tr>
</table>
</form>

<form action="products.php?page=pictorial_chapters" method="post" onSubmit="return validateChapterPageNo(this);">
<table cellspacing="2" cellpadding="2" border="0">
	<tr>
		<td width="100" bgcolor="#e4e4e4">Modify</td>
		<td>
			<select name="chapter" onChange="updatePageNo(this.form, this.value)">
				<option value="">Please select...</option>
			<?php foreach ($chapters as $id => $chapter): ?>
				<option value="<?php echo $id ?>"<?php echo (isset($_POST['mod']) && $_POST['chapter'] == $id ? " selected" : "") ?>><?php echo $chapter ?></option>
			<?php endforeach; ?>
			</select>
		</td>
		<td>Page <input type="text" name="page_no" value="<?php echo (isset($_POST['mod']) ? $_POST['page_no'] : '') ?>" size="3" maxlength="5" /></td>
		<td><input type="submit" name="mod" value="Modify" /></td>
	</tr>
</table>
</form>

<form action="products.php?page=pictorial_chapters" method="post" onSubmit="return validateChapterPageNo(this);">
<table cellspacing="2" cellpadding="2" border="0">
	<tr>
		<td width="100" bgcolor="#e4e4e4">Delete</td>
		<td>
			<select name="chapter">
				<option value="">Please select...</option>
			<?php foreach ($chapters as $id => $chapter): ?>
				<option value="<?php echo $id ?>"><?php echo $chapter ?></option>
			<?php endforeach; ?>
			</select>
		</td>
		<td></td>
		<td><input type="submit" name="delete" value="Delete"  /></td>
	</tr>
</table>
	</form>