<?php
// Load
include('include/load.php');

// Include header template
include_template('head', array('page' => 'home')); 
?>
			<td id="left">
<!-- 				<div id="panel">
					<div class="title">
						<p>STATISTICS</p>
					</div>

					<p><b>Most Popular Make</b><br />
					<font class="red">Holden</font>
					</p>

					<p><b>Most Popular Model</b><br />
					<font class="red">Commodore</font>

					</p>

					<p><b>Total Users</b>
					<font class="red">10</font></p>

					<p><b>Total Products</b>
					<font class="red">10</font></p>
				</div> -->
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p> @ HOME</p>
				</div>
				
				<div id="content">
					<b>Distributors</b>
					<hr color="#D62C3C" />
					<p>View distributor stats on what locations are being search and what distributors are being clicked.</p>
                                        <p><a href="distributors.php">Click here</a> to get started. </p>

					<br />
                                    
					<b>Products</b>
					<hr color="#D62C3C" />
					<p>In the Products area you will able to manually add your products to the catalog.<br />
					You will also be able to manage your categories, ie: Make, Model etc. </p> <p><a href="products.php">Click here</a> to get started. </p>

					<br />

					<b>Import</b>
					<hr color="#D62C3C" />
					<p>In the Import area you will be able to upload spreadsheet files which will then be added to the database.</p> <p> <a href="import.php">Click here</a> to get started.</p>
					
					<br />

					<b>Users</b>
					<hr color="#D62C3C" />
					<p>In the Users area you will be able to manage users that are able to use the catalog system.</p> <p> <a href="users.php">Click here</a> to get started.</p>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>