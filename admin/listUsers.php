<?php require_once('include/load.php') ?>
<p><strong>List Users</strong></p>


<?php
// Total results per page
$perPage = 25;

if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}

// Get total amount of users
$q = Database::query("SELECT COUNT(*) FROM USERS");
$totalResults = Database::result($q);

if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = "ASC";
}

$q = Database::query("SELECT * FROM USERS WHERE DELETED = 0 ORDER BY USERNAME ".$_REQUEST['orderBy']." LIMIT $_REQUEST[p], $perPage"); 
$count = mysql_num_rows($q) ?>
<form id="form" action="javascript:void(0);" method="post" onSubmit="submitForm('listUsers.php?orderBy=<?php echo $_REQUEST['orderBy'] ?>');"> 
<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<?php if (count($totalResults) > 0): ?>
	<tr>
		<td>Total users: <?php echo $totalResults ?></td>
		<td colspan="2" align="right">
		<form id="form" action="javascript:void(0);" method="post" onSubmit="submitForm('listUsers.php?orderBy=<?php echo (v('orderBy') == 'ASC' ? 'DESC' : 'ASC') ?>');"> 
			Page: <select name="p" onChange="submitForm('listUsers.php?orderBy=<?php echo $_REQUEST['orderBy'] ?>');">
			<?php
			$num=0;
			for($i=0;$i<ceil($totalResults/$perPage);$i++)
			{
				echo '<option value="'.($num).'"';
				if($_REQUEST['p'] == $num) {
					echo " selected";
				}
				echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
				$num=$num+$perPage;
			}
			?>
			</select>
		</form>
		</td>
	</tr>
	<?php endif; ?>
	<tr bgcolor="#e4e4e4">
		<td width="100"><b><a style="color:#000; text-decoration: underline;" href="javascript:void(0);" onclick="showPage('listUsers.php?orderBy=<?php echo (v('orderBy') == 'ASC' ? 'DESC' : 'ASC') ?> ');">Username</b></td>
		<td width="200"><b>Email address</b></td>
		<td><b>Last login</b></td>
	</tr>
	<?php if ($count > 0): ?>
	<?php $bgcolor = null ?>
	<?php while ($result = Database::fetch_obj($q)): ?>
	<?php $bgcolor = ($bgcolor == "#ffffff" ? "#f4f4f4" : "#ffffff") ?>
	<tr bgcolor="<?php echo $bgcolor ?>">
		<td><a href="javascript:void(0)" onclick="showPage('modifyUser.php?id=<?php echo v('ID') ?>')"><?php echo v('USERNAME') ?></td>
		<td><?php echo v('EMAIL') ?></td>
		<td>
			<?php if ($result->LAST_LOGIN == null): ?>
			Never
			<?php else: ?>
				<?php echo date('l jS \of F Y', strtotime($result->LAST_LOGIN)) ?>
			<?php endif; ?>
		</td>
	</tr>
	<?php endwhile; ?>
	<?php else: ?>
	<tr>
		<td colspan="3">No users in the database</td>
	</tr>
	<?php endif; ?>
</table>