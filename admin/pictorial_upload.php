<?php
require_once('include/load.php');

if (isset($_POST['submit']))
{
	// Upload image
	if ($_FILES['image']['type'] == 'image/jpeg' || $_FILES['image']['type'] == 'image/pjpeg') 
	{
		if(move_uploaded_file($_FILES['image']['tmp_name'], '../pictorial/pages/page-'.$_POST['page_no'].".jpg")) {
			// Now resize the images for display
			// Thumbnail
			resizeimg(30, 43, '../pictorial/pages/page-'.$_POST['page_no'].".jpg", '../pictorial/pages/thumbnail/page-'.$_POST['page_no'].".jpg", 0);
			// Large
			resizeimg(944, 1335, '../pictorial/pages/page-'.$_POST['page_no'].".jpg", '../pictorial/pages/large/page-'.$_POST['page_no'].".jpg", 0);
			// Normal
			resizeimg(413, 584, '../pictorial/pages/page-'.$_POST['page_no'].".jpg", '../pictorial/pages/page-'.$_POST['page_no'].".jpg", 0);

			if (empty($_REQUEST['id'])) {
				// Delete any other entries with the same order_no
				Database::query("DELETE FROM PICTORIAL_PAGES WHERE PAGE_NO = '".v('page_no')."'");

				// Insert page into database
				Database::query("INSERT INTO PICTORIAL_PAGES SET NAME = '".v('name')."', PAGE_NO = '".v('page_no')."'");
			}
			else {
				// Remove old images if the page_no has changed
				if (v('old_page_no') != v('page_no')) {
					unlink("../pictorial/pages/page-".v('old_page_no').".jpg");
					unlink("../pictorial/pages/thumbnail/page-".v('old_page_no').".jpg");
					unlink("../pictorial/pages/large/page-".v('old_page_no').".jpg");
				}

				// Insert page into database
				Database::query("UPDATE PICTORIAL_PAGES SET NAME = '".v('name')."', PAGE_NO = '".v('page_no')."' WHERE ID = '".$_REQUEST['id']."'");
			}

			
			// Write new book settings js file to add new image to javascript array
			writeNewBookSettingsJS();
		}
		else {
			$err = "Unable to upload image";
		}
	}
	else {
		$err = "File needs to be JPEG";
	}
}
?>
<b>Manage Pictorial - Upload</b><br />
<a href="javascript:void(0)" onclick="showPage('pictorial.php');">List</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_upload.php');">Upload</a>, <a href="javascript:void(0)" onclick="showPage('pictorial_chapters.php');">Chapters</a>

<div style="height: 10px;"></div>

<?php if (isset($_POST['submit'])): ?>
	<?php if (!isset($err)): ?>
		<p class="okMsg">Page has been saved. Order no: <?php echo $_POST['page_no'] ?></p>
		<?php unset($_REQUEST); unset($_POST);  ?>
	<?php else: ?>
	<p class="errMsg"><?php echo $err ?></p>
	<?php endif; ?>
<?php endif; ?>

<?php 
if (!isset($_POST['page_no'])) {
	// Get next page number order 
	$q         = Database::query("SELECT PAGE_NO FROM PICTORIAL_PAGES ORDER BY PAGE_NO DESC LIMIT 1"); 
	$page_no   = (mysql_num_rows($q) > 0 ? Database::result($q) : 0);

	$_POST['page_no'] = $page_no + 1;
}


if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) 
{
	$q = Database::query("SELECT * FROM PICTORIAL_PAGES WHERE ID = '".$_REQUEST['id']."'"); 
	$result = Database::fetch_obj($q);
}
?>
<form enctype="multipart/form-data" action="products.php?page=pictorial_upload" method="post" onSubmit="return validatePictorialUpload()">
<input type="hidden" name="id" value="<?php echo v("id") ?>" />
<table cellspacing="2" cellpadding="2" border="0">
	<tr>
		<td bgcolor="#e4e4e4" width="100">Name</td>
		<td><input type="text" name="name" value="<?php echo v('name') ?>" maxlength="50" /></td>
	</tr>
	<tr>
		<td bgcolor="#e4e4e4">Order No</td>
		<td><input type="hidden" name="old_page_no" value="<?php echo v('page_no') ?>" /><input type="text" name="page_no" value="<?php echo v('page_no') ?>" maxlength="5" size="3" /></td>
	</tr>
	<tr>
		<td bgcolor="#e4e4e4">Image</td>
		<td><input type="file" name="image" size="50" /></td>
	</tr>
	<tr>
		<td><input type="submit" name="submit" value="Add page" /></td>
	</tr>
</table>
</form>