<?php
// Load
require_once('include/load.php');
?>
<p><strong>Manage Make</strong></p>

<?php

/////////////////////
//////// MAKE //////
/////////////////////


// Add a new Make
if (isset($_POST['addMake'])) {
	if (!empty($_POST['newMake'])) {
		// make sure the Make doesn't already exist
		$q = Database::query("SELECT COUNT(*) FROM MAKES WHERE MAKE = '".addslashes($_POST['newMake'])."'");
		if (Database::result($q) == 0) {
			Database::query("INSERT INTO MAKES SET MAKE = '".addslashes($_POST['newMake'])."'");
			echo "<p class=\"okMsg\">OK New make (".$_POST['newMake'].") has been added</p>";
		}
		else {
			echo "<p class=errMsg>Error: ".$_POST['newMake']." already exists.</p>";
		}
	}
	else {
		echo "<p class=errMsg>Error: You didn't enter a name for the Make.</p>";
	}
}

// Rename make
if (isset($_POST['renameMake'])) {
	if (!empty($_POST['renameMakeTo'])) {
		// make sure new make doesn't match any other one
		$q = Database::query("SELECT COUNT(*) FROM MAKES WHERE MAKE = '".addslashes($_POST['renameMakeTo'])."'");
		if (Database::result($q) == 0) {
			Database::query("UPDATE MAKES SET MAKE = '".addslashes($_POST['renameMakeTo'])."' WHERE MAKE_ID = '".$_POST['MAKE_ID']."'");
			echo "<p class=\"okMsg\">OK Make has been renamed to: ".$_POST['renameMakeTo']."</p>";
		}
		else {
			echo "<p class=errMsg>Error: The make you entered already exists.</p>";
		}
	}
}

// Delete a make
if (isset($_POST['deleteMake'])) {
	// make sure a make is selected
	if (!empty($_POST['MAKE_ID'])) {
		// Delete all products
		Database::query("DELETE PRODUCTS FROM PRODUCTS LEFT JOIN MODELS ON PRODUCTS.MODEL_ID = MODELS.MODEL_ID WHERE MODELS.MAKE_ID = '".$_POST['MAKE_ID']."'");
		// Delete all models
		Database::query("DELETE FROM MODELS WHERE MAKE_ID = '".$_POST['MAKE_ID']."'");
		// Delete make
		Database::query("DELETE FROM MAKES WHERE MAKE_ID = '".$_POST['MAKE_ID']."'");
		echo "<p class=\"okMsg\">OK Make has been deleted, and all models / products under it also.</p>";

		// Unset submit btn so Models area isnt displayed
		unset($_POST['editModels']);

	}
	else {
		echo "<p class=errMsg>Error: You need to select a Make</p>";
	}
}
?>
<form action="products.php?page=manageCats" method="post">
<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<tr>
		<td width="100">New Make</td>
		<td><input type="text" name="newMake" value="" /></td>
		<td><input type="submit" name="addMake" value="Add Make" /></td>
	</tr>
	<tr valign="top">
		<td>Make</td>
		<td>
			<select id="productMake" name="MAKE_ID" onChange="if ($('productMake').value > 0) { $('editMake').show(); $('displayModels').hide(); } else { $('editMake').hide(); $('displayModels').hide(); }">
				<option value="">Please select</option>
				<?php $q = Database::query('SELECT * FROM MAKES ORDER BY MAKE ASC'); ?>
				<?php $rss = Database::query_to_array($q); ?>
				<?php foreach ($rss as $rs): ?>
				<option value="<?php echo $rs->MAKE_ID ?>"<?php echo ($rs->MAKE_ID == v('MAKE_ID') ? ' selected' : null) ?>><?php echo $rs->MAKE ?></option>
				<?php endforeach; ?>
			</select>
		</td>
		<td><input type="submit" name="deleteMake" value="Delete" onClick="return confirm('Deleting this Make will also remove all Models below it and their products.\nAre you sure you wish to continue?')" /></td>
	</tr>
	<tr>
		<td>Rename to</td>
		<td><input type="text" name="renameMakeTo" value="" /></td>
		<td><input type="submit" name="renameMake" value="Rename" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"><div id="editMake" style="<?php echo (!isset($_POST['MAKE_ID']) || !isset($_POST['editModels'])  ? 'display:none; ' : '') ?>background-color: #e3e3e3;" align="center"><input type="submit" name="editModels" value="Edit Models" /></div></td>
	</tr>
</table>

<?php if (isset($_POST['editModels'])): ?>
<div id="displayModels">
	<p><strong>Manage Models</strong></p> 
	<input type="hidden" name="editModels" value="" />

<?php
/////////////////////
//////// MODEL //////
/////////////////////

// Add a new Model
if (isset($_POST['addModel'])) {
	if (!empty($_POST['newModel'])) {
		// make sure the Model doesn't already exist
		$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".addslashes($_POST['newModel'])."' AND PARENT_MODEL = 0");
		if (Database::result($q) == 0) {
			Database::query("INSERT INTO MODELS SET MAKE_ID = '".$_POST['MAKE_ID']."', MODEL = '".addslashes($_POST['newModel'])."', PARENT_MODEL = 0");
			echo "<p class=\"okMsg\">OK New Model (".$_POST['newModel'].") has been added</p>";
		}
		else {
			echo "<p class=errMsg>Error: ".$_POST['newModel']." already exists.</p>";
		}
	}
	else {
		echo "<p class=errMsg>Error: You didn't enter a name for the Model.</p>";
	}
}

// Rename model
if (isset($_POST['renameModel'])) {
	if (!empty($_POST['renameModelTo'])) {
		// make sure new model doesn't match any other one
		$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".addslashes($_POST['renameModelTo'])."' AND PARENT_MODEL = 0");
		if (Database::result($q) == 0) {
			Database::query("UPDATE MODELS SET MODEL = '".addslashes($_POST['renameModelTo'])."' WHERE MODEL_ID = '".$_POST['MODEL_ID']."'");
			echo "<p class=\"okMsg\">OK: Model has been renamed to: ".$_POST['renameModelTo']."</p>";
		}
		else {
			echo "<p class=errMsg>Error: The make you entered already exists.</p>";
		}
	}
}

// Delete a model
if (isset($_POST['deleteModel'])) {
	// make sure a make is selected
	if (!empty($_POST['MODEL_ID'])) {
		// Delete all products
		Database::query("DELETE PRODUCTS FROM PRODUCTS LEFT JOIN MODELS ON PRODUCTS.MODEL_ID = MODELS.MODEL_ID WHERE MODELS.PARENT_MODEL = '".$_POST['MODEL_ID']."'");
		// Delete model 
		// Delete all sub models
		Database::query("DELETE FROM MODELS WHERE MODEL_ID = '".$_POST['MODEL_ID']."' OR PARENT_MODEL = '".$_POST['MODEL_ID']."'");
	}
	else {
		echo "<p class=errMsg>Error: You need to select a Model</p>";
	}
}
?>

	<table cellspacing="2" cellpadding="2" border="0" width="100%">
		<tr>
			<td width="100">New Model</td>
			<td><input type="text" name="newModel" value="" /></td>
			<td><input type="submit" name="addModel" value="Add Model" /></td>
		</tr>
		<tr valign="top">
			<td>Model</td>
			<td>
				<select id="productModel" name="MODEL_ID" onChange="if ($('productModel').value > 0) { $('editSubModel').show(); } else { $('editSubModel').hide(); }">
					<option value="">Please select</option>
					<?php $q = Database::query("SELECT * FROM MODELS WHERE MAKE_ID = '".$_POST['MAKE_ID']."' AND PARENT_MODEL = 0 ORDER BY MODEL ASC"); ?>
					<?php $rss = Database::query_to_array($q); ?>
					<?php foreach ($rss as $rs): ?>
					<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('MODEL_ID') ? ' selected' : null) ?>><?php echo $rs->MODEL ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td><input type="submit" name="deleteModel" value="Delete" onClick="return confirm('Deleting this Model will also remove all Sub Models below it and their products.\nAre you sure you wish to continue?')" /></td>
		</tr>
		<tr>
			<td>Rename to</td>
			<td><input type="text" name="renameModelTo" value="" /></td>
			<td><input type="submit" name="renameModel" value="Rename" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3"><div id="editSubModel" style="<?php echo (!isset($_POST['MODEL_ID']) || !isset($_POST['editSubModels'])  ? 'display:none; ' : '') ?>background-color: #e3e3e3;" align="center"><input type="submit" name="editSubModels" value="Edit Sub Models" /></div></td>
		</tr>
	</table>
</div>
<?php endif; ?>

<?php if (isset($_POST['editSubModels'])): ?>
<div id="displaySubModels">
	<p><strong>Manage Sub Models</strong></p> 
	<input type="hidden" name="editSubModels" value="" />

<?php
/////////////////////
//// SUB MODEL //////
/////////////////////


// Add a new Sub Model
if (isset($_POST['addSubModel'])) {
	if (!empty($_POST['newSubModel'])) {
		// make sure the Sub Model doesn't already exist
		$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".addslashes($_POST['newSubModel'])."' AND PARENT_MODEL = '".$_POST['MODEL_ID']."'");
		if (Database::result($q) == 0) {
			Database::query("INSERT INTO MODELS SET MAKE_ID = '".$_POST['MAKE_ID']."', MODEL = '".addslashes($_POST['newSubModel'])."', PARENT_MODEL = '".$_POST['MODEL_ID']."'");
			echo "<p class=\"okMsg\">OK New Sub Model (".$_POST['newSubModel'].") has been added</p>";
		}
		else {
			echo "<p class=errMsg>Error: ".$_POST['newSubModel']." already exists.</p>";
		}
	}
	else {
		echo "<p class=errMsg>Error: You didn't enter a name for the Sub Model.</p>";
	}
}

// Rename sub model
if (isset($_POST['renameSubModel'])) {
	if (!empty($_POST['renameSubModelTo'])) {
		// make sure new sub model doesn't match any other one
		$q = Database::query("SELECT COUNT(*) FROM MODELS WHERE MODEL = '".addslashes($_POST['renameModelTo'])."' AND PARENT_MODEL = '".$_POST['MODEL_ID']."'");
		if (Database::result($q) == 0) {
			Database::query("UPDATE MODELS SET MODEL = '".addslashes($_POST['renameSubModelTo'])."' WHERE MODEL_ID = '".$_POST['SUBMODEL_ID']."'");
			echo "<p class=\"okMsg\">OK: Sub Model has been renamed to: ".$_POST['renameSubModelTo']."</p>";
		}
		else {
			echo "<p class=errMsg>Error: The make you entered already exists.</p>";
		}
	}
}

// Delete a sub model
if (isset($_POST['deleteSubModel'])) {
	// make sure a make is selected
	if (!empty($_POST['MODEL_ID'])) {
		// Delete all products
		Database::query("DELETE FROM PRODUCTS WHERE MODEL_ID = '".$_POST['SUBMODEL_ID']."'");
		// Delete model
		Database::query("DELETE FROM MODELS WHERE MODEL_ID = '".$_POST['SUBMODEL_ID']."'");

	}
	else {
		echo "<p class=errMsg>Error: You need to select a Sub Model</p>";
	}
}
?>

	<table cellspacing="2" cellpadding="2" border="0" width="100%">
		<tr>
			<td width="100">New Sub Model</td>
			<td><input type="text" name="newSubModel" value="" /></td>
			<td><input type="submit" name="addSubModel" value="Add Sub Model" /></td>
		</tr>
		<tr valign="top">
			<td>Sub Model</td>
			<td>
				<select id="productSubModel" name="SUBMODEL_ID"">
					<option value="">Please select</option>
					<?php $q = Database::query("SELECT * FROM MODELS WHERE MAKE_ID = '".$_POST['MAKE_ID']."' AND PARENT_MODEL = ".$_POST['MODEL_ID']." ORDER BY MODEL ASC"); ?>
					<?php $rss = Database::query_to_array($q); ?>
					<?php foreach ($rss as $rs): ?>
					<option value="<?php echo $rs->MODEL_ID ?>"<?php echo ($rs->MODEL_ID == v('MODEL_ID') ? ' selected' : null) ?>><?php echo $rs->MODEL ?></option>
					<?php endforeach; ?>
				</select>
			</td>
			<td><input type="submit" name="deleteSubModel" value="Delete" onClick="return confirm('Deleting this Model will also remove all products.\nAre you sure you wish to continue?')" /></td>
		</tr>
		<tr>
			<td>Rename to</td>
			<td><input type="text" name="renameSubModelTo" value="" /></td>
			<td><input type="submit" name="renameSubModel" value="Rename" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</div>
<?php endif; ?>

</form>
