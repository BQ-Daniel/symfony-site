<?php
// Load
include('include/load.php');
include('include/excelReader.php');

define('BQ_NO', 1);
define('PART_NO', 2);
define('ILL_NO', 3);
define('CUT_HOSE', 11);
define('AC', 12);
define('AD', 13);
define('AE', 14);
define('DESC', 15);

error_reporting(E_ALL);

if ($_FILES['file']['type'] != 'application/vnd.ms-excel' && $_FILES['file']['type'] != 'application/octet-stream') {
	?>
	<div align="center">
		<h2>Error</h2>
		The file you tried to upload is not an excel file. Please <a href="import.php?page=importUsers">click here</a> to try again.
	</div>
	<?php
	exit;
}

// move uplaoded file to a temp directory
$filename = md5(time().rand(1,550000));
if (!move_uploaded_file($_FILES['file']['tmp_name'], 'tmp/'.$filename))
{
	?>
	<div align="center">
		<h2>Error</h2>
		Unable to move uploaded file to a temp directory. Please <a href="import.php?page=importUsers">click here</a> to try again.
	</div>
	<?php
	exit;
}

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('CP1251');
// What file to read
$data->read('tmp/'.$filename);

// Put data into an array
$products = array();
$updated = 0;
$ignored = 0;

// Start at line 2, as we dont want to add the headers
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
	$ill_no = (isset($data->sheets[0]['cells'][$i][ILL_NO]) ? trim($data->sheets[0]['cells'][$i][ILL_NO]) : null);

	if (!empty($ill_no)) {
		$bq_number = (isset($data->sheets[0]['cells'][$i][BQ_NO]) ? trim($data->sheets[0]['cells'][$i][BQ_NO]) : null);
		$cut_hose = (isset($data->sheets[0]['cells'][$i][CUT_HOSE]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][CUT_HOSE])) : null);
		$ac = (isset($data->sheets[0]['cells'][$i][AC]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][AC])) : null);
		$ad = (isset($data->sheets[0]['cells'][$i][AD]) ? trim($data->sheets[0]['cells'][$i][AD]) : null);
		$ae = (isset($data->sheets[0]['cells'][$i][AE]) ? trim($data->sheets[0]['cells'][$i][AE]) : null);
		$desc = (isset($data->sheets[0]['cells'][$i][DESC]) ? trim($data->sheets[0]['cells'][$i][DESC]) : null); 
		// Add to products array for displaying
		$products[] =		 array('BQ_NO'      => $bq_number,
								   'ILL_NO'     => $ill_no,
								   'AC'			=> $ac,
								   'AD'			=> $ad,
								   'AE'			=> $ae,
								   'CUT_HOSE'   => $cut_hose,
								   'DESC'       => $desc);

		$q = Database::query("SELECT COUNT(*) FROM PRODUCTS WHERE ILL_NUMBER = '".addslashes($ill_no)."' AND BQ_NUMBER = '".$bq_number."'");
		if (Database::result($q) > 0) {
			Database::query("UPDATE PRODUCTS SET CUT_HOSE = '".addslashes($cut_hose)."', AC='".addslashes($ac)."', AD='".addslashes($ad)."', AE='".addslashes($ae)."', DESCRIPTION='".addslashes($desc)."' WHERE ILL_NUMBER = '".addslashes($ill_no)."' AND BQ_NUMBER = '".$bq_number."'");
			$updated++;
		}
	}
	else {
		$ignored++;
	}
}
?>
<html>
	<head>
		<title>Importing Excel Build Matrix</title>
		<link rel="stylesheet" media="screen" href="style.css">
	</head>
	
	<body>
		<table cellspacing="2" cellpadding="2" border="0" width="80%" align="center">
			<tr align="center">
				<td class="tdCell" style="width:20px"><strong>ILL</strong></td>
				<td class="tdCell" style="width:70px"><strong>CUT HOSE</strong></td>
				<td class="tdCell" style="width:35px"><strong>AC</strong></td>
				<td class="tdCell" style="width:35px"><strong>AD</strong></td>
				<td class="tdCell" style="width:35px"><strong>AE</strong></td>
				<td class="tdCell"><strong>DESCRIPTION</strong></td>
			</tr>
			<?php $bgColor = "#ededed" ?>
			<?php foreach ($products as $product): ?>
			<?php $bgColor = ($bgColor == "#ededed" ? "#ffffff" : "#ededed" ) ?>
			<tr bgcolor="<?php echo $bgColor ?>">
				<td align="center"><?php echo $product['ILL_NO'] ?></td>
				<td align="center"><?php echo (!empty($product['CUT_HOSE']) ? v('CUT_HOSE', $product).'<small>mm</small>' : ''); ?></td>
				<td align="center"><?php echo (!empty($product['AC']) ? v('AC', $product).'<small>mm</small>' : ''); ?></td>
				<td align="center"><?php echo (!empty($product['AD']) ? v('AD', $product).'<small>mm</small>' : ''); ?></td>
				<td align="center"><?php echo (!empty($product['AE']) ? v('AE', $product).'<small>mm</small>' : ''); ?></td>
				<td><?php echo (!empty($product['DESC']) ? v('DESC', $product).'<small>mm</small>' : ''); ?></td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="6">
					<p><b>Updated</b>: Total of <b><?php echo $updated ?></b> products were updated.</p>
					<p><b>Ignored</b>: Total of <b><?php echo $ignored ?></b> products were ignored.</p>
				</td>
			</tr>
		</table>

	</body>
</html>