<?php
// Load
include('include/load.php');
include('include/excelReader.php');

error_reporting(E_ALL);

if ($_FILES['file']['type'] != 'application/vnd.ms-excel' && $_FILES['file']['type'] != 'application/octet-stream') {
	?>
	<div align="center">
		<h2>Error</h2>
		The file you tried to upload is not an excel file. Please <a href="import.php?page=importUsers">click here</a> to try again.
	</div>
	<?php
	exit;
}

// move uplaoded file to a temp directory
$filename = md5(time().rand(1,550000));
if (!move_uploaded_file($_FILES['file']['tmp_name'], 'tmp/'.$filename))
{
	?>
	<div align="center">
		<h2>Error</h2>
		Unable to move uploaded file to a temp directory. Please <a href="import.php?page=importUsers">click here</a> to try again.
	</div>
	<?php
	exit;
}

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('CP1251');
// What file to read
$data->read('tmp/'.$filename);

// Put data into an array
$users = array();
$ignored = 0;

// Start at line 2, as we dont want to add the headers
for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
	$username = (isset($data->sheets[0]['cells'][$i][1]) ? trim($data->sheets[0]['cells'][$i][1]) : null);
	$email = (isset($data->sheets[0]['cells'][$i][2]) ? trim($data->sheets[0]['cells'][$i][2]) : null);

	if (!empty($username) && !empty($email) && eregi('@', $email)) {
		$users[$username] = $email;
	}
	else {
		$ignored++;
	}
}
?>
<html>
	<head>
		<title>Importing Excel Users</title>
		<link rel="stylesheet" media="screen" href="style.css">
	</head>
	
	<body>
		<table cellspacing="2" cellpadding="2" border="0" width="70%" align="center">
			<tr>
				<td class="tdCell" width="20%"><strong>Username</strong></td>
				<td class="tdCell" width="80%"><strong>Email</strong></td>
			</tr>
			<?php foreach ($users as $username => $email): ?>
			<tr>
				<td><?php echo $username ?></td>
				<td><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
			<?php if (count($users) > 0): ?>
			If everything looks ok, add to the database. Otherwise if you need to fix something up edit the Excel sheet and upload it again.
			<p><input type="button" name="back" value="Go back" onClick="location.href='import.php?page=importUsers'" /> | <input type="button" name="ok" value="Add to database" onClick="location.href='doImportUsers.php?file=<?php echo $filename ?>'" /></p>

			<?php else: ?>
			<p>Nothing to add...</p>
			<p><input type="button" name="back" value="Go back" onClick="location.href='import.php?page=importUsers'" /></p>
			<?php endif; ?>
				</td>
			</tr>
		</table>
	</body>
</html>