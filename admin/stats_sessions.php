<?php
// Total results per page
$perPage = 15;
// Setup defaults
if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}
?>


<div style="float: left; margin-right: 12px;">
    <a href="stats.php?page=sessions"><img src="images/stats_sessions.jpg" title="Sessions" alt="Sessions" border="0" /></a>
</div>
<div style="float: left;">
    <?php include_template('stats/filter_with_username', array('page' => 'sessions'));  ?>
</div>

<div style="clear: both; height: 15px;"></div>

<?php if (isset($_POST['submit']) || !empty($_REQUEST['orderBy'])): ?>
    <?php include_template('stats/display_sessions', array('perPage' => $perPage));  ?>
<?php endif; ?>