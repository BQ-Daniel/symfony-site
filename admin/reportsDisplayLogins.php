<?php
// Load
require_once('include/load.php');

if (!isset($_REQUEST['id']) || empty($_REQUEST['id'])) {
	?>
	<b>Invalid User</b><br />
	<a href="users.php?page=reportsLogins">Go Back to Report</a>

	<?php
	exit;
}

// Total results per page
$perPage = 25;

if (!isset($_REQUEST['p'])) {
	$_REQUEST['p'] = 0;
}
if (!isset($_REQUEST['sort'])) {
	$_REQUEST['sort'] = null;
}
if (!isset($_REQUEST['orderBy'])) {
	$_REQUEST['orderBy'] = null;
}

// Get logins for the user
$fromDate = (substr_count($_REQUEST['from'], '/') == 2 ? split('/', $_REQUEST['from']) : array());
$toDate = (substr_count($_REQUEST['to'], '/') == 2 ? split('/', $_REQUEST['to']) : array());

$orderBy = ($_REQUEST['orderBy'] != null ? ' ORDER BY '.($_REQUEST['orderBy'] == 'duration' ? 'LOGIN_TIME - LOGIN_END' : $_REQUEST['orderBy']).' '.$_REQUEST['sort'].' ' : ' ');

$cq = Database::query("SELECT COUNT(*) FROM USERS u LEFT JOIN USER_LOGINS ul ON u.id = ul.user_id WHERE ul.user_id = '".$_REQUEST['id']."' AND LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY)");
$totalResults = Database::result($cq);

$q = Database::query("SELECT * FROM USERS u LEFT JOIN USER_LOGINS ul ON u.id = ul.user_id WHERE ul.user_id = '".$_REQUEST['id']."' AND LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND LOGIN_TIME <=  DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) ".$orderBy."LIMIT $_REQUEST[p], $perPage");

if ($totalResults > 0) {
	$q2 = Database::query("SELECT username FROM USERS WHERE ID = '".$_REQUEST['id']."'");
	$username = Database::result($q2);
}

$sort = ($_REQUEST['sort'] == 'ASC' ? 'DESC' : 'ASC');

$url = "id=".$_REQUEST['id']."&orderBy=".$_REQUEST['orderBy']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
$url2 = "id=".$_REQUEST['id']."&sort=".$sort."&from=".$_REQUEST['from'].'&to='.$_REQUEST['to'];
?>
<p><font size="2"><strong><?php echo $username ?></strong>
<form action="users.php?page=reportsDisplayLogins" method="post">
<input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>" />
From: <input type="text" id="from" name="from" value="<?php echo (!isset($_REQUEST['from']) ? date('d/m/Y') : $_REQUEST['from']) ?>" /> &nbsp;
To: <input type="text" id="to" name="to" value="<?php echo (!isset($_REQUEST['to']) ? date('d/m/Y') : $_REQUEST['to']) ?>" /> <input type="submit" name="submit" value="Go!" />
</form>
<br /><br />
<div align="right">
	<form id="form" action="javascript:void(0);" method="post" onSubmit="submitForm('reports.php?page=reportsDisplayLogins');"> 
	<input type="hidden" name="from" value="<?php echo $_REQUEST['from'] ?>" />
	<input type="hidden" name="to" value="<?php echo $_REQUEST['to'] ?>" />
	<input type="hidden" name="orderBy" value="<?php echo $_REQUEST['orderBy'] ?>" />
	<input type="hidden" name="sort" value="<?php echo $_REQUEST['sort'] ?>" />
	<input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>" />
		Page: <select name="p" onChange="submitForm('reports.php?page=reportsDisplayLogins');">
		<?php
		$num=0;
		for($i=0;$i<ceil($totalResults/$perPage);$i++)
		{
			echo '<option value="'.($num).'"';
			if($_REQUEST['p'] == $num) {
				echo " selected";
			}
			echo ">".($i+1)." of ".(ceil($totalResults/$perPage))." pages</option>\n";
			$num=$num+$perPage;
		}
		?>
		</select>
	</form>
</div>
<table cellspacing="2" cellpadding="2" border="0" width="100%">
	<tr>
		<td class="tdCell">IP</td>
		<td class="tdCell"><a href="javascript:void(0);" style="color:#000; text-decoration: underline;" onClick="showPage('reportsDisplayLogins.php?orderBy=duration&<?php echo $url2 ?>')">Duration</a></td>
		<td class="tdCell"><a href="javascript:void(0);" style="color:#000; text-decoration: underline;" onClick="showPage('reportsDisplayLogins.php?orderBy=login_time&<?php echo $url2 ?>')">Login Time</a></td>
	</tr>
	<?php if ($totalResults > 0): ?>
		<?php while($result = Database::fetch_obj($q)): ?>
	<tr valign="top">
		<td><?php echo gethostbyaddr($result->IP) ?></td>
		<td><?php echo getTimeDifference($result->LOGIN_TIME, $result->LOGIN_END) ?></td>
		<td><?php echo date("d/M/y H:i:s", strtotime($result->LOGIN_TIME)) ?></td>
	</tr>
		<?php endwhile; ?>
	<?php else: ?>
	<tr>
		<td colspan="3" align="center">No results</td>
	</tr>
	<?php endif; ?>
</table>
<p><a href="users.php?page=reportsLogins&from=<?php echo $_REQUEST['from'] ?>&to=<?php echo $_REQUEST['to'] ?>">Go Back to Report</a></p>