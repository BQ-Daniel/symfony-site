<?php
// Load
include('include/load.php');
// Include header template
include_template('head', array('page' => 'products')); 
?>
			<td id="left">

				<div id="panel">
					<div class="title">
						<p>SUB NAVIGATION</p>
					</div>
					
					<ul>
						<li> <a href="javascript:void(0)" onclick="showPage('listProducts.php');">List Products</a>
						<li> <a href="javascript:void(0)" onclick="showPage('manageCats.php');">Manage Categories</a>
						<li> <a href="javascript:void(0)" onclick="showPage('uploadIll.php');">Manage Illustrations</a>
						<li> <a href="javascript:void(0)" onclick="showPage('pictorial.php');">Manage Pictorial</a>
                        <li> <a href="stats.php">Stats</a>
					</ul>
					<br />
				</div>

				<br />
			</td>

			<td id="right">
				<div class="title" style="width: 90%">
					<p>@ PRODUCTS</p>
				</div>
				
				<div id="content">
				<?php if (isset($_REQUEST['page'])): ?>
					<?php require_once($_REQUEST['page'].".php"); ?>
				<?php endif; ?>
				</div>
			</td>
<?php
// Include bottom template
include_template('bottom');
?>