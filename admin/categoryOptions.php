<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Pragma: no-cache");

require_once('include/load.php');

// Load if this is an ajax request
if (count($_POST) == 0 || v('MAKE_ID') || $_POST['submodel']) {
	$q = Database::query('SELECT * FROM MAKES ORDER BY MAKE ASC');
	$rss = Database::query_to_array($q);
	if (count($rss) > 0):
	?>
	<option value="">Please select</option>
	<?php
	endif; 

	foreach ($rss as $rs): ?>
	<option value="<?php echo $rs->MAKE_ID ?>"<?php echo (v('MAKE_ID') == $rs->MAKE_ID ? ' selected' : null) ?>><?php echo $rs->MAKE ?></option>
	<?php endforeach; 
}
else {
	$array = array();
	if (isset($_POST['MAKE']) && !empty($_POST['MAKE'])) {

		$q = Database::query("SELECT * FROM MODELS WHERE MAKE_ID='".$_POST['MAKE']."' AND PARENT_MODEL=0");
		$rss = Database::query_to_array($q);

		foreach ($rss as $rs):
			$array[] = $rs->MODEL_ID.': '.str_replace(',','', $rs->MODEL);
		endforeach; 
		
		if (count($array) == 0) {
			return false;
		}

		echo ":Please Select,",implode(",", $array);

	}
	elseif (isset($_POST['MODEL']) && !empty($_POST['MODEL'])) {

		$q = Database::query("SELECT * FROM MODELS WHERE PARENT_MODEL='".$_POST['MODEL']."'");
		$rss = Database::query_to_array($q);

		foreach ($rss as $rs):
			$array[] = $rs->MODEL_ID.': '.str_replace(',','', $rs->MODEL);
		endforeach; 
		
		if (count($array) == 0) {
			return false;
		}

		echo ":Please Select,",implode(",", $array);
	}
	else {
		$q = Database::query('SELECT * FROM MAKES ORDER BY MAKE ASC');
		$rss = Database::query_to_array($q);

		foreach ($rss as $rs):
			$array[] = $rs->MAKE_ID.': '.str_replace(',','', $rs->MAKE);
		endforeach; 
		
		if (count($array) == 0) {
			return false;
		}

		echo ":Please Select,",implode(",", $array);
	}
}
unset($rs);
?>