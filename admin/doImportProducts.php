<?php
// Load
include('include/load.php');
// Excel Importing
require_once('classes/ExcelProducts.class.php');
include('include/excelReader.php');

error_reporting(E_ALL);

?>
<html>
	<head>
		<title>Brakequip Administration : Import Products</title>
				<link rel="stylesheet" media="screen" href="style.css">
	</head>

	<body>
<?php
// Make sure file exists
if (!file_exists('tmp/'.$_REQUEST['file']))
{
	echo "There has been an error. Please upload the excel spreadsheet again.";
	exit;
}

$added = 0;
$updated = 0;

// ExcelFile($filename, $encoding);
$data = new Spreadsheet_Excel_Reader();
// Set output Encoding.
$data->setOutputEncoding('CP1251');
// What file to read
$data->read('tmp/'.$_REQUEST['file']);

// Read excel document and get products
$doc = new ExcelProducts();
$doc->initialize($data);
$subModels = $doc->getSubModels();


// If Make doesn't exist, then add it
$q = Database::query("SELECT MAKE_ID FROM MAKES WHERE MAKE = '".$doc->getMake()."'");
if (Database::count_result($q) == 0) {
	// Add to database
	Database::query("INSERT INTO MAKES SET MAKE = '".$doc->getMake()."'");
	// Get the Make ID
	$makeId = mysql_insert_id();
} else {
	$makeId = Database::result($q);
}

// If Model doesn't exist, then add it
$model   = $doc->getModel();
$modelId = 0;
if (!empty($model)) {
	$q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MAKE_ID = '".$makeId."' AND MODEL = '".$doc->getModel()."'");
	if (Database::count_result($q) == 0) {
		// Add to database
		Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$doc->getModel()."'");
		// Get the Make ID
		$modelId = mysql_insert_id();
	} else {
		$modelId = Database::result($q);
	}
}
else {
    $noMajorModel = true;
}


foreach ($subModels as $subModel => $years) {

	$subModel = trim(addslashes($subModel));
	// remove any double spaces
	$subModel = str_replace('  ', ' ', $subModel);

    // If no major model, we need to use sub model as the major model for this set
    if (isset($noMajorModel)) {
        $q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MAKE_ID = '".$makeId."' AND MODEL = '".$subModel."'");
        if (Database::count_result($q) == 0) {
            // Add to database
            Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."'");
            // Get the Model ID
            $modelId = mysql_insert_id();
        } else {
            $modelId = Database::result($q);
        }  
    }

	// Check if sub model exists, if not add it
	$q = Database::query("SELECT MODEL_ID FROM MODELS WHERE MODEL = '".$subModel."' AND PARENT_MODEL = '".$modelId."'");
	if (Database::count_result($q) == 0) {
		Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."', PARENT_MODEL = '".$modelId."'");
		$subModelId = mysql_insert_id();
		// If there is no parent model, then make the submodel as the parent
		if ($modelId == 0) {
			Database::query("INSERT INTO MODELS SET MAKE_ID = '".$makeId."', MODEL = '".$subModel."', PARENT_MODEL = '".$subModelId."'");
			$subModelId = mysql_insert_id();
		}
	} else {
		$subModelId = Database::result($q);
	}

	// Delete all products under this sub model
	Database::query("DELETE FROM PRODUCTS WHERE MODEL_ID = '".$subModelId."'");
	
	foreach ($years as $year => $products) {

		$year = trim(addslashes($year));
		foreach ($products as $product) {
			$bqNumber = str_replace(' ', '', v(BQ_NO, $product));

			Database::query("INSERT INTO PRODUCTS SET MODEL_ID = '".$subModelId."', BQ_NUMBER = '".$bqNumber."', PART_NUMBER = '".addslashes(v(PART_NO, $product))."', COMMENT = '".addslashes(v(COMMENTS, $product))."', HOSE_POSITION = '".v(POS, $product)."', YEAR = '".$year."', ILL_NUMBER = '".v(ILL_NO, $product)."', DATE_ADDED = DATE_ADD(NOW(), INTERVAL 15 HOUR)");
			$added++;
		}
	}
}
unlink('tmp/'.$_REQUEST['file']);

?>
<h1>Imported Products</h1>
<p>Here are the results for the import of <strong><?php echo $doc->getMake() ?> <?php echo $doc->getModel() ?></strong></p>

<table cellspacing="2" cellpadding="0" border="0">
	<tr>
		<td class="tdCell" width="100">Additions</td>
		<td width="50"><?php echo $added ?></td>
		<td class="tdCell" width="100">Products Ignored</td>
		<td width="50"><?php echo $doc->getTotalIgnores() ?></td>
	</tr>
</table>

<p>Go back to the Import products page</p>
<p><input type="button" name="back" value="Go Back" onClick="location.href='import.php?page=importProducts'" /></p>



	</body>
</html>