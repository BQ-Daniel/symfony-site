<?php
require_once('include/load.php');

try {
    //echo "brakequip";
    $page = $templateName;
    $output = $engine->render($templateName, array('extendDir' => $extendDir, 'modules' => $modules, 'page' => $page, 'isAjaxRequest' => $isAjaxRequest));
}
catch (Exception $e)
{
    /**
     * Add action files
     */
    // If is an ajax request, include actions/ajax
    if ($isAjaxRequest) {
        // Check for a load file to include, this is always included
        if (file_exists('templates/' . $modules . '/actions/ajax/' . $page . '.php')) {
            require_once('templates/' . $modules . '/actions/ajax/' . $page . '.php');
            exit;
        }
    }
    // Check for a load file to include, this is always included
    if (file_exists('templates/' . $modules . '/actions/load.php')) {
        require_once('templates/' . $modules . '/actions/load.php');
    }

    // Check if action exists, if so include it
    if ($page != 'load' && file_exists('templates/' . $modules . '/actions/' . $page . '.php')) {
        require_once('templates/' . $modules . '/actions/' . $page . '.php');
    }

    $templateName = '404';
    $error = $e->getMessage();
    if (substr($error, 0, 12) != "The template")
    {
        $templateName = '505';
        // Write error message to file
        $fp = fopen('505errors.log', 'a+');
        fwrite($fp, "Date: ".date("r").": ".$error."\r\n");
        fwrite($fp, print_R($_SERVER, true)."\r\n");
        fwrite($fp, "=======================================================\r\n");
    }
    $modules = null;
    $loader = new sfTemplateLoaderFilesystem(dirname(__FILE__).'/templates/%name%.php');
    $engine = new sfTemplateEngine($loader);
    $output = $engine->render($templateName, array('modules' => $modules, 'page' => $templateName, 'e' => $e));
}
echo $output;
?>
