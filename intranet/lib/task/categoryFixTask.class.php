<?php

class categoryViewFixTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = '';
        $this->name = 'categoryFix';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [categoryfix|INFO] task does things.
Call it with:

  [php symfony backup|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $db = $databaseManager->getDatabase($options['connection']);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();


        $q = Doctrine_Query::create()->from('Category c')
                ->select('*')
                ->innerJoin('c.ParentCategory pc')
                ->where('c.category_type_id != pc.sibling_category_type_id AND pc.sibling_category_type_id != 0');
        
        // Add order by            
        //$q->addOrderBy($sort . ' ' . $dir);
        
        $results = $q->execute();
        
        foreach ($results as $result)
        {
            //echo get_class($result);
            $arr = $result->toArray();
            $parent = $result->getParentCategory()->toArray();
            
            //print_r($parent);
            
            $result->setCategoryTypeId($parent['sibling_category_type_id']);
            $result->save();
            echo ".";

        }
    }
}
