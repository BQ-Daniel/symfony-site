<?php

class updateBuildMatrixTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master')
                // add your own options here
        ));

        $this->namespace = '';
        $this->name = 'updateBuildMatrix';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [updateBuildMatrix|INFO] task does things.
Call it with:

  [php symfony updateBuildMatrix|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

       define('PATH', sfConfig::get('sf_data_dir') . '/excel/');
        
        $BQ_NO = 1;
        $PART_NO = 2;
        $ILL_NO = 3;
        $PBR = 4;
        $OL = 5;
        $FITTING_A = 6;
        $FITTING_B = 7;
        $FITTING_C = 8;
        $FITTING_D = 9;
        $FITTING_E = 10;
        $CUT_HOSE = 11;
        $AC = 12;
        $AD = 13;
        $AE = 14;
        $DESC = 15;
        $HOSE_STOR = 16;
        
	$data = new Spreadsheet_Excel_Reader();
	// Set output Encoding.
	$data->setOutputEncoding('CP1251');
	// What file to read
	$data->read(sfConfig::get('sf_data_dir').'/Build_Matrix.xls');


// Start at line 2, as we dont want to add the headers
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
        {
            echo ".";
            // If illustration no isn't empty, add it
            $ill_no = (isset($data->sheets[0]['cells'][$i][$ILL_NO]) ? trim($data->sheets[0]['cells'][$i][$ILL_NO]) : null);
            if (!empty($ill_no))
            {
                // Lets add to database
                if (!$illustration = IllustrationTable::getInstance()->findOneByIllustrationNumber($ill_no)) {
                    $illustration = new Illustration();
                    $illustration->setIllustrationNumber($ill_no);
                    $illustration->save();
                }
            }
            
            
            $bq_number = (isset($data->sheets[0]['cells'][$i][$BQ_NO]) ? trim($data->sheets[0]['cells'][$i][$BQ_NO]) : null);
            $part_number = (isset($data->sheets[0]['cells'][$i][$PART_NO]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$PART_NO])) : null);
            $pbr_number = (isset($data->sheets[0]['cells'][$i][$PBR]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$PBR])) : null);
            $ol_number = (isset($data->sheets[0]['cells'][$i][$OL]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$OL])) : null);
            $fitting_a = (isset($data->sheets[0]['cells'][$i][$FITTING_A]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_A])) : null);
            $fitting_b = (isset($data->sheets[0]['cells'][$i][$FITTING_B]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_B])) : null);
            $fitting_c = (isset($data->sheets[0]['cells'][$i][$FITTING_C]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_C])) : null);
            $fitting_d = (isset($data->sheets[0]['cells'][$i][$FITTING_D]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_D])) : null);
            $fitting_e = (isset($data->sheets[0]['cells'][$i][$FITTING_E]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_E])) : null);
            $cut_hose = (isset($data->sheets[0]['cells'][$i][$CUT_HOSE]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$CUT_HOSE])) : null);
            $ac = (isset($data->sheets[0]['cells'][$i][$AC]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$AC])) : null);
            $ad = (isset($data->sheets[0]['cells'][$i][$AD]) ? trim($data->sheets[0]['cells'][$i][$AD]) : null);
            $ae = (isset($data->sheets[0]['cells'][$i][$AE]) ? trim($data->sheets[0]['cells'][$i][$AE]) : null);
            $desc = (isset($data->sheets[0]['cells'][$i][$DESC]) ? trim($data->sheets[0]['cells'][$i][$DESC]) : null);
            $hose_storage = (isset($data->sheets[0]['cells'][$i][$HOSE_STOR]) ? trim($data->sheets[0]['cells'][$i][$HOSE_STOR]) : null);
            
            $websiteDisplay = true;
            if (empty($bq_number) || empty($part_number) || (empty($ac) && empty($ad) && empty($ae) && empty($cut_hose))) {
                $websiteDisplay = false;
            }
            
            // Add to OE Table
            $oeNumber = false;
            if (!empty($part_number))
            {
                if (!$oeNumber = PartTable::getInstance()->findOneByBqNumber($bq_number)) {
                    continue;
                }
                
                
                $oeNumber->setOeNumber($part_number);
                $oeNumber->setBqNumber($bq_number);
                $oeNumber->setPbrNumber($pbr_number);
                $oeNumber->setOl($ol_number);
                $oeNumber->setAFitting($fitting_a);
                $oeNumber->setBFitting($fitting_b);
                $oeNumber->setCFitting($fitting_c);
                $oeNumber->setDFitting($fitting_d);
                $oeNumber->setEFitting($fitting_e);
                $oeNumber->setCutHoseAt($cut_hose);
                $oeNumber->setAcLength($ac);
                $oeNumber->setAdLength($ad);
                $oeNumber->setAeLength($ae);
                $oeNumber->setMakeHoseInstructions($desc);
                $oeNumber->setHoseStorage($hose_storage);
                $oeNumber->setWebsiteDisplay($websiteDisplay);
                $oeNumber->setStaffId(1);
                $oeNumber->save();
            }
            
            if (!$webPart = WebPartTable::getInstance()->findOneByBqNumber($bq_number)) {
                continue;
            }
            
            // If display on website, then add to website database
            //if ($websiteDisplay) {
                $webPart->setOeNumber($part_number);
                $webPart->setBqNumber($bq_number);
                $webPart->setAcLength($ac);
                $webPart->setAdLength($ad);
                $webPart->setAeLength($ae);
                $webPart->setAFitting($fitting_a);
                $webPart->setBFitting($fitting_b);
                $webPart->setCFitting($fitting_c);
                $webPart->setDFitting($fitting_d);
                $webPart->setEFitting($fitting_e);
                $webPart->setCutHoseAt($cut_hose);
                $webPart->setMakeHoseInstructions($desc);
              //  $webPart->setWebsiteDisplay(true);
                $webPart->save();
            //}
        }
    }

}
