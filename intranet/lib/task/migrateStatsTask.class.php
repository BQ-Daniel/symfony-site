<?php

class migrateStatsTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = 'migrate';
        $this->name = 'stats';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [migrate:stats|INFO] task does things.
Call it with:

  [php symfony migrate:stats|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        $connection->setAttribute ( Doctrine_Core::ATTR_AUTO_FREE_QUERY_OBJECTS, true );
        
        $this->db = new MySQLDB();
        
        // Part view stats
        //$this->partViews();

        $this->categoryViews();

    }
    
    public function categoryViews()
    {

        $results = Doctrine_Query::create()->from('USER_VIEWS uv')->
                innerJoin('uv.MAKES')->where('uv.make_id > 0')->orderBy('id ASC')->execute(array(), Doctrine_Core::HYDRATE_ON_DEMAND);
        //$q = mysql_query("SELECT * FROM USER_VIEWS uv 
          //  INNER JOIN MAKES m ON uv.MAKE_ID = m.MAKE_ID
           // WHERE uv.MAKE_ID > 0");
        
        $total = 0;
        $batchSize = 500;
        foreach ($results as $result)
        {
            $total++;
            
            if (($total % $batchSize) == 0) {
                echo $total."\n";
            }
            Doctrine_Query::create()->delete('USER_VIEWS')->where('id = ?', $result->getId())->execute();
            
            $categoryView = new CategoryView();
            $categoryView->setUserLoginsId($result->getUserLoginsId());
            $categoryView->setCreatedAt(date('Y-m-d H:i:s'));
            $categoryView->save();
            
            $categories = array();
            $cv = new CategoryViewGroup();
            $cv->setCategoryViewId($categoryView->getId());
            
            // Get Make category id from new category table
            $make = $result->getMAKES();

            if (!$makeCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($make->getMake()))->fetchOne()) {
                echo "Make doesn't exist: ".$make->getMake()."\n";
                $categoryView->delete();
                continue;
            }
            $makeCategoryId = $makeCategory->getId();
            $categories[] = $make->getMake();
            
            // Add stat
            $cv->setWebCategoryTypeId($makeCategory->getCategoryTypeId());
            $cv->setWebCategoryId($makeCategoryId);
            $cv->setCategoryString(implode(' > ', $categories));
            $cv->setCreatedAt($result->getViewDate());
            $cv->save();
            
            // Get Model category id from new category table
            $model = MODELSTable::getInstance()->find($result->getModelId());
            if (!$model || !$modelCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($model->getModel()))->andWhere('parent_category_id = ?', $makeCategoryId)->fetchOne()) {
                $categoryView->delete();
                $cv->delete();
                unset($categoryView);
                unset($cv);
                continue;
            }
            $modelCategoryId = $modelCategory->getId();
            $categories[] = $model->getModel();

            // Add stat
            $cv2 = new CategoryViewGroup();
            $cv2->setCategoryViewId($categoryView->getId());
            $cv2->setWebCategoryTypeId($modelCategory->getCategoryTypeId());
            $cv2->setWebCategoryId($modelCategoryId);
            $cv2->setCategoryString(implode(' > ', $categories));
            $cv2->setCreatedAt($result->getViewDate());
            $cv2->save();
           
            
            // Get Sub Model category id from new category table
            $subModel = MODELSTable::getInstance()->find($result->getSubModelId());
            if (!$subModel || !$subModelCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($subModel->getModel()))->andWhere('parent_category_id = ?', $modelCategoryId)->fetchOne()) {
                $categoryView->delete();
                $cv->delete();
                $cv2->delete();
                unset($cv);
                unset($cv2);
                unset($categoryView);
                continue;
            }
            $subModelCategoryId = $subModelCategory->getId();
            $categories[] = $subModel->getModel();

            // Add stat
            $cv3 = new CategoryViewGroup();
            $cv3->setCategoryViewId($categoryView->getId());
            $cv3->setWebCategoryTypeId($subModelCategory->getCategoryTypeId());
            $cv3->setWebCategoryId($subModelCategoryId);
            $cv3->setCategoryString(implode(' > ', $categories));
            $cv3->setCreatedAt($result->getViewDate());
            $cv3->save();
            
            // Now look for year by looking at the category name, parent category id will be the sub model
            
            // Get Year category id from new category table
            $yearCategory = Doctrine_Query::create()->from('Category')->where('upper(name) = ?', strtoupper($result->getYear()))->andWhere('parent_category_id = ?', $subModelCategoryId)->fetchOne();
            if (!$yearCategory) {
                $categoryView->delete();
                $cv->delete();
                $cv2->delete();
                $cv3->delete();
                
                unset($categoryView);
                unset($cv);
                unset($cv2);
                unset($cv3);
                continue;
            }
            
            $yearCategoryId = $yearCategory->getId();
            $categories[] = $yearCategory->getName();

            // Add stat
            $cv4 = new CategoryViewGroup();
            $cv4->setCategoryViewId($categoryView->getId());
            $cv4->setWebCategoryTypeId($yearCategory->getCategoryTypeId());
            $cv4->setWebCategoryId($yearCategoryId);
            $cv4->setCategoryString(implode(' > ', $categories));
            $cv4->setCreatedAt($result->getViewDate());
            $cv4->save();
            
            unset($cv4);
        } 
    }
    
    public function partViews()
    {
        // Select all products that still exist, and have
        //$results = Doctrine_Query::create()->from('USER_VIEWS uv')->innerJoin('uv.PRODUCTS p')->where('uv.product_id > 0')->execute();
        
        $q = mysql_query('SELECT * FROM USER_VIEWS uv INNER JOIN PRODUCTS p ON p.PRODUCT_ID = uv.PRODUCT_ID WHERE uv.PRODUCT_ID > 0') or die(mysql_error());
        
        $total = 0;
        $batchSize = 100;
        while ($result = mysql_fetch_array($q))
        {
            if ($result['USER_LOGINS_ID'] == 0) {
                continue;
            }
            $q2 = mysql_query("SELECT * FROM web_part WHERE
                UPPER(bq_number) = '".strtoupper($result['BQ_NUMBER'])."' OR UPPER(oe_number) = '".strtoupper($result['OE_NUMBER'])."'") or die(mysql_error());
            if (mysql_num_rows($q2) == 0) {
                continue;
            } 
            $total++;
            
            $part = mysql_fetch_array($q2);
            
            $queries[] = "INSERT INTO part_view SET web_part_id = '".$part['id']."',
                user_logins_id = '".$result['USER_LOGINS_ID']."', created_at = '".$result['VIEW_DATE']."'";
            
            if (($total % $batchSize) == 0) {
                if (count($queries) > 0) {
                    echo $total."\n";
                    
                    $this->db->transaction($queries);
                    $queries = array();
                    $this->db->begin();
                }
            }
            
        }
        if (count($queries) > 0) {
            $this->db->transaction($queries);
        }
        
        
    }
}
