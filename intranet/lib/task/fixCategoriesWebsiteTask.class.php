<?php

class fixCategoriesWebsiteTask extends sfBaseTask {
    
    public $log;
    
    protected function configure() {

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = 'update';
        $this->name = 'websiteCategories';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF

EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        set_time_limit(0);
        
        // initialize the database connection        
        $databaseManager = new sfDatabaseManager($this->configuration);
        $conn = $this->conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->setAttribute(Doctrine_Core::ATTR_AUTOCOMMIT, false);
        
        try {
            $this->start();
        }
        catch (Exception $e) {
            $this->log('[Error] '.$e->getMessage());
        }
        $this->log('Finished');       
    }
    
    public function start()
    {
        $conn = $this->conn;
        
        $this->log("Starting update process....");
        // Get time of when the website was last updated
        $setting = SettingTable::getInstance()->findOneByName('website_updated');
        
        if (!$setting) {
            $this->log("Unable to get setting from setting table: website_updated");
            return;
        }
        
        $websiteLastUpdated = $setting->getValue();

        /**
         * Categories
         */
        $q = Doctrine_Query::create()->from('Category')->where('updated_at > ?', $websiteLastUpdated);
        $q->limit(1000);
        $categories = $q->execute();

        if (count($categories) > 0) 
        {
            $this->log("Found " . count($categories) . ' '.(count($categories) > 1 ? 'Categories' : 'Category') . " to be updated");
            $i = 0;
            $conn->beginTransaction();
            do {
                
                foreach ($categories as $category)
                {
                    $i++;
                    $new = false;
                    if (!$webCategory = WebCategoryTable::getInstance()->find($category->getId())) {
                        $webCategory = new WebCategory();
                        $new = true;
                    }

                    $values = $category->toArray();
                    $values['parent_web_category_id'] = $values['parent_category_id'];
                    $values['web_category_type_id'] = $values['category_type_id'];
                    $values['sibling_web_category_type_id'] = $values['sibling_category_type_id'];

                    $webCategory->fromArray($values);
                    $webCategory->save();

                    //$this->log('Category '.($new ? 'added' : 'updated').' '.$category->getName());
                    
                    if (($i % 1000) === 0) {
                        $conn->commit();
                        $conn->beginTransaction();
                        $this->log('.');
                        
                        $q->free();
                        unset($q);
                        
                        $q = Doctrine_Query::create()->from('Category')->where('updated_at > ?', $websiteLastUpdated);
                        $q->limit(1000);
                        $q->offset($i);
                        $categories = $q->execute();

                    }
                }
            }
            while ($categories);
            $conn->commit();
        }
    }
    
    public function log($str)
    {
        $str = '['.date("d/m/y H:i:s").'] '.$str."\n";
        echo $str;
    }
}