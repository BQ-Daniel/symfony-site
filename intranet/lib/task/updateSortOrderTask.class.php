<?php

class updateSortOrderTask extends sfBaseTask
{
    public $total = 0;
    public $counter = 0;
    
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
      // add your own options here
    ));

    $this->namespace        = '';
    $this->name             = 'updateSortOrder';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [updateSortOrder|INFO] task does things.
Call it with:

  [php symfony updateSortOrder|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here
    $this->run(0);
  }
  
  
  public function run($parentCategoryId)
  {
      $q = Doctrine_Query::create()->from('Category c')->where('parent_category_id = ?', $parentCategoryId)->orderBy('name ASC');
      $results = $q->execute();
      
      $i = 0;
      foreach ($results as $result) {
          $i++;
          $this->run($result->getId());
          $result->setSortOrder($i);
          Doctrine_Query::create()->update('WebCategory')->where('id = ?', $result->getId())->set('sort_order', $i)->execute();
          $result->save();
          
          $this->total++;
          $this->counter++;
          if ($this->counter == 1000) {
              $this->counter = 0;
              echo date('r').": ".$this->total."\n";
          }
      }
  }
}
