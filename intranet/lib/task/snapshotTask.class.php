<?php

class snapshotTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = '';
        $this->name = 'snapshot';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [snapshot|INFO] task does things.
Call it with:

  [php symfony snapshot|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) 
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $database = $databaseManager->getDatabase($options['connection']);
        $connection = $database->getConnection();

        // Get database name from connection
        $dsn = $database->getParameter('dsn');
        preg_match('/;dbname=(.*)/', $dsn, $dsn);
        $dbName = $dsn[1];
        // Get username
        $username = $database->getParameter('username');
        // Get password
        $password = $database->getParameter('password');
        
        
        // Check for any snapshots that are over 7 days old
        $results = Doctrine_Query::create()->from('Snapshot')->where('ADDDATE(DATE(created_at), INTERVAL 7 DAY) < CURDATE()')->execute();
        if ($results) {
            foreach ($results as $result) {
                @unlink('snapshots/'.$result->file.'.sql.gz');
                @unlink('snapshots/'.$result->file.'.photos.gz');
                
                Doctrine_Query::create()->delete('SnapshotLog')->where('snapshot_id = ?', $result->id)->execute();
                $result->delete();
            }
            
        }

        // Check for any log files
        $snapshotLog = Doctrine_Query::create()->from('SnapshotLog')->where('snapshot_id = 0')->fetchArray();

        // If nothing to snapshot, then delete
        if (count($snapshotLog) == 0) {
            return false;
        }

        $filename = date('Y-m-d-H-m-i');

        // Run command to backup database
        exec('mysqldump -u ' . $username . ' -p' . $password . ' ' . $dbName . ' > snapshots/' . $filename . '.sql');
        // Now gzip
        exec('gzip -c snapshots/' . $filename . '.sql > snapshots/' . $filename . '.sql.gz');

        // Remove old SQL file
        if (file_exists('snapshots/' . $filename . '.sql.gz')) {
            unlink('snapshots/' . $filename . '.sql');
        }

        // Now to backup the illustrations directory if there has been any modifications
        $count = Doctrine_Query::create()->from('SnapshotLog')->where('snapshot_id = 0')->count();
        if ($count > 0) {
            $illDir = sfConfig::get('app_illustration_dir');
            exec('tar -zcvf snapshots/' . $filename . '.photos.gz ' . $illDir);
        }

        // Add snapshot to database
        $snapshot = new Snapshot();
        $snapshot->file = $filename;
        $snapshot->save();


        // Update all snapshot log entries with snapshot id
        $ids = array();
        foreach ($snapshotLog as $log) {
            $ids[] = $log['id'];
        }
        
        if (count($ids) > 0) {
            Doctrine_Query::create()->update('SnapshotLog')->set('snapshot_id', $snapshot->id)->whereIn('id', $ids)->execute();
        }
    }

}
