<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

function v($value, $obj = null)
{
	global $_POST, $result;
	
	if (is_null($obj)) {
		$obj = $result;
	}

	if (is_array($obj) && isset($obj[$value])) {
		return trim($obj[$value]);
	}
	
	$value2 = strtoupper($value);
	if ($obj && isset($obj->$value2)) {
		return $obj->$value2;
	}
	elseif (isset($_POST[$value])) {
		return $_POST[$value];
	}
	elseif (isset($_REQUEST[$value])) {
		return $_REQUEST[$value];
	}
	else {
		return false;
	}
}


class importProductsTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master')
                // add your own options here
        ));

        $this->namespace = 'import';
        $this->name = 'products';
        $this->briefDescription = 'Import data from excel spreadsheets';
        $this->detailedDescription = <<<EOF
The [import:products|INFO] task does things.
Call it with:

  [php symfony import:products|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        
        /**
         * Setup category types
         */
        $types = array('Make', 'Model', 'Sub Model', 'Year', 'Hose Position');
        $previous = null;
        foreach ($types as $type)
        {
            $this->categoryTypes[strtolower($type)] = $this->getCategoryType($type, $previous);
            $previous = strtolower($type);
        }
        
        define('PATH', sfConfig::get('sf_data_dir') . '/excel/');
        $this->load(PATH);
        
        
        $BQ_NO = 1;
        $PART_NO = 2;
        $ILL_NO = 3;
        $PBR = 4;
        $OL = 5;
        $FITTING_A = 6;
        $FITTING_B = 7;
        $FITTING_C = 8;
        $FITTING_D = 9;
        $FITTING_E = 10;
        $CUT_HOSE = 11;
        $AC = 12;
        $AD = 13;
        $AE = 14;
        $DESC = 15;
        
	$data = new Spreadsheet_Excel_Reader();
	// Set output Encoding.
	$data->setOutputEncoding('CP1251');
	// What file to read
	$data->read(sfConfig::get('sf_data_dir').'/Build_Matrix.xls');


// Start at line 2, as we dont want to add the headers
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
        {
            echo ".";
            // If illustration no isn't empty, add it
            $ill_no = (isset($data->sheets[0]['cells'][$i][$ILL_NO]) ? trim($data->sheets[0]['cells'][$i][$ILL_NO]) : null);
            if (!empty($ill_no))
            {
                // Lets add to database
                if (!$illustration = IllustrationTable::getInstance()->findOneByIllustrationNumber($ill_no)) {
                    $illustration = new Illustration();
                    $illustration->setIllustrationNumber($ill_no);
                    $illustration->save();
                }
            }
            
            
            $bq_number = (isset($data->sheets[0]['cells'][$i][$BQ_NO]) ? trim($data->sheets[0]['cells'][$i][$BQ_NO]) : null);
            $part_number = (isset($data->sheets[0]['cells'][$i][$PART_NO]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$PART_NO])) : null);
            $pbr_number = (isset($data->sheets[0]['cells'][$i][$PBR]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$PBR])) : null);
            $ol_number = (isset($data->sheets[0]['cells'][$i][$OL]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$OL])) : null);
            $fitting_a = (isset($data->sheets[0]['cells'][$i][$FITTING_A]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_A])) : null);
            $fitting_b = (isset($data->sheets[0]['cells'][$i][$FITTING_B]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_B])) : null);
            $fitting_c = (isset($data->sheets[0]['cells'][$i][$FITTING_C]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_C])) : null);
            $fitting_d = (isset($data->sheets[0]['cells'][$i][$FITTING_D]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_D])) : null);
            $fitting_e = (isset($data->sheets[0]['cells'][$i][$FITTING_E]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$FITTING_E])) : null);
            $cut_hose = (isset($data->sheets[0]['cells'][$i][$CUT_HOSE]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$CUT_HOSE])) : null);
            $ac = (isset($data->sheets[0]['cells'][$i][$AC]) ? trim(str_replace("?", "", $data->sheets[0]['cells'][$i][$AC])) : null);
            $ad = (isset($data->sheets[0]['cells'][$i][$AD]) ? trim($data->sheets[0]['cells'][$i][$AD]) : null);
            $ae = (isset($data->sheets[0]['cells'][$i][$AE]) ? trim($data->sheets[0]['cells'][$i][$AE]) : null);
            $desc = (isset($data->sheets[0]['cells'][$i][$DESC]) ? trim($data->sheets[0]['cells'][$i][$DESC]) : null);
            
            $websiteDisplay = true;
            if (empty($bq_number) || empty($part_number) || (empty($ac) && empty($ad) && empty($ae) && empty($cut_hose))) {
                $websiteDisplay = false;
            }
            
            // Add to OE Table
            $oeNumber = false;
            if (!empty($part_number))
            {
                if (!$oeNumber = PartTable::getInstance()->findOneByBqNumber($bq_number)) {
                    $oeNumber = new Part();
                }
                if (!$webPart = WebPartTable::getInstance()->findOneByBqNumber($bq_number)) {
                    $webPart = new WebPart();
                }
                // Set illustration
                if ($illustration) {
                    $oeNumber->setIllustrationId($illustration->getId());
                }
                
                
                $oeNumber->setOeNumber($part_number);
                $oeNumber->setBqNumber($bq_number);
                $oeNumber->setPbrNumber($pbr_number);
                $oeNumber->setOl($ol_number);
                $oeNumber->setAFitting($fitting_a);
                $oeNumber->setBFitting($fitting_b);
                $oeNumber->setCFitting($fitting_c);
                $oeNumber->setDFitting($fitting_d);
                $oeNumber->setEFitting($fitting_e);
                $oeNumber->setCutHoseAt($cut_hose);
                $oeNumber->setAcLength($ac);
                $oeNumber->setAdLength($ad);
                $oeNumber->setAeLength($ae);
                $oeNumber->setMakeHoseInstructions($desc);
                $oeNumber->setWebsiteDisplay($websiteDisplay);
                $oeNumber->setStaffId(1);
                $oeNumber->save();
            }
            
            // If display on website, then add to website database
            if ($websiteDisplay) {
                $webPart->setOeNumber($part_number);
                $webPart->setBqNumber($bq_number);
                $webPart->setAcLength($ac);
                $webPart->setAdLength($ad);
                $webPart->setAeLength($ae);
                $webPart->setCutHoseAt($cut_hose);
                $webPart->setMakeHoseInstructions($desc);
                $webPart->setWebsiteDisplay(true);
                $webPart->save();
            }
        }
    }

    public function load($dir) {
        $mydir = opendir($dir);
        while (false !== ($file = readdir($mydir))) {
            if ($file != "." && $file != "..") {
                chmod($dir . $file, 0777);
                if (is_dir($dir . $file)) {
                    chdir('.');
                    echo "Entering directory: " . $dir . $file . "\n";
                    $this->load($dir . $file . '/');
                    echo "Imported directory\n\n";
                } else {
                    echo "<u>Importing file</u>: " . $file . "";
                    $rs = $this->addFileToDatabase($dir . $file);
                    echo " - Added: " . $rs . "\n";
                }
            }
        }
        closedir($mydir);
    }

    public function addFileToDatabase($file)
    {
        $added = 0;
        $updated = 0;
        // ExcelFile($filename, $encoding);
        $data = new Spreadsheet_Excel_Reader();
        // Set output Encoding.
        $data->setOutputEncoding('CP1251');
        // What file to read
        $data->read($file);

        // Read excel document and get products
        $doc = new ExcelProducts();
        $doc->initialize($data);
        $subModels = $doc->getSubModels();
        
        /**
         * MAKE: INSERT MAKE CATEGORY / and category type
         */
        
        // Insert make if it doesn't exist and return id
        $makeId = $this->checkCategory($doc->getMake(), 0, $this->categoryTypes['make'], $this->categoryTypes['model']);
                
        /**
         * MODEL: INSERT MODEL CATEGORY
         */
        // If Model doesn't exist, then add it
        $noMajorModel = false;
        $model = $doc->getModel();
        if (!empty($model)) {
            $modelId = $this->checkCategory($doc->getModel(), $makeId, $this->categoryTypes['model'], $this->categoryTypes['sub model']);
        }
        else {
           $noMajorModel = true; 
        }
        

        /**
         * SUB MODELS
         */
        foreach ($subModels as $subModel => $years) {

            $subModel = trim(addslashes($subModel));
            // remove any double spaces
            $subModel = str_replace('  ', ' ', $subModel);
            
            // ADD ANOTHER CATEGORY
            // 
            // If no major model, we need to use sub model as the major model for this set
            if ($noMajorModel) {
                $modelId = $this->checkCategory($subModel, $makeId, $this->categoryTypes['model'], $this->categoryTypes['sub model']);
            }
            
            // ADD ANOTHER CATEGORY
            // Check if sub model exists, if not add it
            $subModelId = $this->checkCategory($subModel, $modelId, $this->categoryTypes['sub model'], $this->categoryTypes['year']);

            foreach ($years as $year => $products) {
            
                // ADD YEAR CATERGORY
                $year = trim(addslashes($year));
                $yearId = $this->checkCategory($year, $subModelId, $this->categoryTypes['year'], $this->categoryTypes['hose position']);
                
                
                foreach ($products as $product) {
                    $bqNumber = str_replace(' ', '', v(BQ_NO, $product));
                    
                    // ADD CATEGORY
                    $hosePositionId = $this->checkCategory(v(POS, $product), $yearId, $this->categoryTypes['hose position'], 0);
                    
                    // Add illustration
                    if (!$ill = IllustrationTable::getInstance()->findOneByIllustrationNumber(v(ILL_NO, $product))) {
                        $ill = new Illustration();
                        $ill->setIllustrationNumber(v(ILL_NO, $product));
                        $ill->save();
                    }
                    
                    // Insert into oe and bq
                    if (!$oe = PartTable::getInstance()->findOneByOeNumber(v(PART_NO, $product))) {
                        $oe = new Part();
                    }
                    
                    $oe->setIllustration($ill);
                    $oe->setOeNumber(v(PART_NO, $product));
                    $oe->setBqNumber($bqNumber);
                    $oe->setComments(v(COMMENTS, $product));
                    $oe->save();
                    
                    if (!$oe2 = WebPartTable::getInstance()->findOneByOeNumber(v(PART_NO, $product))) {
                        $oe2 = new WebPart();
                    }
                    
                    $oe2->setIllNumber(v(ILL_NO, $product));
                    $oe2->setOeNumber(v(PART_NO, $product));
                    $oe2->setBqNumber($bqNumber);
                    $oe2->setComments(v(COMMENTS, $product));
                    $oe2->save();
                    
                    // Part TO Category
                    if (!$bqToCategory = PartToCategoryTable::getInstance()->findOneByPartIdAndCategoryId($oe->getId(), $yearId)) {
                        $bqToCategory = new PartToCategory();
                        $bqToCategory->setCategoryId($hosePositionId);
                        $bqToCategory->setPartId($oe->getId());
                        $bqToCategory->save();
                        
                        $bqToCategory2 = new WebPartToCategory();
                        $bqToCategory2->setWebCategoryId($hosePositionId);
                        $bqToCategory2->setWebPartId($oe->getId());
                        $bqToCategory2->save();
                    }
                    
                    // Add bq to category
                    $added++;
                }
            }
        }

        return $added;
    }
    
    
    
    public function getCategoryType($name, $parent = 0)
    {
        if (!empty($parent)) {
            $parentId = @$this->categoryTypes[$parent];
        }
        
        if ($categoryType = CategoryTypeTable::getCategoryTypeByName($name, (int) @$parentId)) {
            return $categoryType->getId();
        }
        
        // If one wasnt found, create it
        $categoryType = new CategoryType();
        $categoryType->setName($name);
        if (!empty($parent)) {
            $parentId = $this->categoryTypes[$parent];
            $categoryType->setParentCategoryTypeId($parentId);
        }
        $categoryType->save();
        
        $categoryType2 = new WebCategoryType();
        $categoryType2->setName($name);
        if (!empty($parent)) {
            $categoryType2->setParentWebCategoryTypeId($parentId);
        }
        $categoryType2->save();
        
        return $categoryType->getId();
        
    }
    
    public function checkCategory($categoryName, $parentId = 0, $categoryTypeId = 1, $siblingCategoryTypeId = 0)
    {
        if ($category = CategoryTable::getCategoryByName($categoryName, $parentId, $categoryTypeId)) {
            return $category->getId();
        }
        
        $sortOrder = 1;
        if ($parentId > 0) {
            $parentCategory = CategoryTable::getInstance()->find($parentId);
            $sortOrder = $parentCategory->getSortOrder();
        }
        
        // If cateogry wasnt found
        $category = new Category();
        $category->setParentCategoryId($parentId);
        $category->setCategoryTypeId($categoryTypeId);
        $category->setSiblingCategoryTypeId($siblingCategoryTypeId);
        $category->setName($categoryName);
        $category->setSortOrder($sortOrder);
        $category->save();
        
        // Add to Web Category
        $category2 = new WebCategory();
        $category2->setParentWebCategoryId($parentId);
        $category2->setWebCategoryTypeId($categoryTypeId);
        $category2->setSiblingWebCategoryTypeId($siblingCategoryTypeId);
        $category2->setName($categoryName);
        $category2->setSortOrder($sortOrder);
        $category2->save();
        
        return $category->getId();
    }

}
