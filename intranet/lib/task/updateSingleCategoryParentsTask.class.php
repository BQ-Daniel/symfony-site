<?php

class updateSingleCategoryParentsTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
            new sfCommandOption('id', null, sfCommandOption::PARAMETER_REQUIRED, 'ID of category')
                // add your own options here
        ));

        $this->namespace = 'bq';
        $this->name = 'updateSingleCategoryParents';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [updateCategoryParents|INFO] task does things.
Call it with:

  [php symfony bq:updateSingleCategoryParents|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        //$conn = $databaseManager->getDatabase($options['connection'])->getConnection();
        //$conn->setAttribute(Doctrine_Core::ATTR_AUTOCOMMIT, false);
        //$conn->g
                $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
                $conn->setAttribute(Doctrine_Core::ATTR_AUTOCOMMIT, false);
        
        $id = $options['id'];
        $conn->beginTransaction();
        
        echo "ID: ".$id;
        
        $category = CategoryTable::getInstance()->find($id);
        if (!$category) {
            exit;
        }
        
        // Get parents
        $parents = CategoryTable::getParentCategories($category->getId());
        $category->setParents(json_encode($parents));
        $category->save();
        
        try {

        
        // Get all siblings
        $subCategories = CategoryTable::getSiblingCategories($category->getId());
        // Loop through categories and 
        foreach ($subCategories as $subCategory)
        {
            if ($subCategory->getParents())
            {
                $parents = json_decode($subCategory['parents'], true);
                foreach ($parents as $key => $parent) {
                    if ($parent['id'] == $id) {
                        $parents[$key]['name'] = $category->getName();
                        break;
                    }
                }
            }
            else {
                $parents = CategoryTable::getParentCategories($subCategory->getId());                
            }
            
            $json = json_encode($parents);
            
            $subCategory->setParents($json);
            $subCategory->save();
            
            // Update category 
            /*
            Doctrine_Core::getTable('Category')
                    ->createQuery('c')
                    ->update()
                    ->set('parents', "'".$json."'")
                    ->where('id', $subCategory['id'])
                    ->execute();
             * 
             */
        }
        $conn->commit();
        } catch(Doctrine_Exception $e) {
            //$conn->rollback();
            echo $e->getMessage();
        }
    }

}
