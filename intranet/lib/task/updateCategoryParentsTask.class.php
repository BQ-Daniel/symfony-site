<?php

class updateCategoryParentsTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master')
                // add your own options here
        ));

        $this->namespace = 'bq';
        $this->name = 'updateCategoryParents';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [updateCategoryParents|INFO] task does things.
Call it with:

  [php symfony bq:updateCategoryParents|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->setAttribute(Doctrine_Core::ATTR_AUTOCOMMIT, false);
        
        $q = Doctrine_Core::getTable('Category')
            ->createQuery('c')
            ->select('c.id')
            ->where('c.parent_category_id > 0 AND c.parents IS NULL');
                
        $q->limit(10000);

        $results = $q->execute();
        
        echo "START!\n";
        
        $i = 0;
        
        $conn->beginTransaction();
        
        foreach ($results as $result)
        {
            $i++;
            
            //print_r($result);
            $categories = CategoryTable::getParentCategories($result->getId());
            
            $result->setParents(json_encode($categories));
            $result->save();
           
            if ($i == 100) {
                $i = 0;
                $conn->commit();
                $conn->beginTransaction();
                echo ".";
            }
        }
        $conn->commit();
        
    }

}
