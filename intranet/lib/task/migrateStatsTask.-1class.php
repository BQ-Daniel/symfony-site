<?php

class migrateStatsTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = 'migrate';
        $this->name = 'stats';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [migrate:stats|INFO] task does things.
Call it with:

  [php symfony migrate:stats|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
        
        // Part view stats
        $this->partViews();
        
        $this->categoryViews();

    }
    
    public function categoryViews()
    {
        $results = Doctrine_Query::create()->from('USER_VIEWS uv')->
                innerJoin('uv.MAKES')->where('uv.make_id > 0')->execute();
        foreach ($results as $result)
        {
            $categoryView = new CategoryView();
            $categoryView->setUserLoginsId($result->getUserLoginsId());
            $categoryView->setCreatedAt(date('Y-m-d H:i:s'));
            $categoryView->save();
            
            $categories = array();
            $cv = new CategoryViewGroup();
            $cv->setCategoryViewId($categoryView->getId());
            
            // Get Make category id from new category table
            $make = $result->getMAKES();
            if (!$makeCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($make->getMake()))->fetchOne()) {
                $categoryView->delete();
                continue;
            }
            $makeCategoryId = $makeCategory->getId();
            $categories[] = $make->getMake();
            
            // Add stat
            $cv->setWebCategoryTypeId($makeCategory->getCategoryTypeId());
            $cv->setWebCategoryId($makeCategoryId);
            $cv->setCategoryString(implode(' > ', $categories));
            $cv->setCreatedAt($result->getViewDate());
            $cv->save();
            
            // Get Model category id from new category table
            $model = MODELSTable::getInstance()->find($result->getModelId());
            if (!$model || !$modelCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($model->getModel()))->andWhere('parent_category_id = ?', $makeCategoryId)->fetchOne()) {
                $categoryView->delete();
                $cv->delete();
                continue;
            }
            $modelCategoryId = $modelCategory->getId();
            $categories[] = $model->getModel();

            // Add stat
            $cv2 = new CategoryViewGroup();
            $cv2->setCategoryViewId($categoryView->getId());
            $cv2->setWebCategoryTypeId($modelCategory->getCategoryTypeId());
            $cv2->setWebCategoryId($modelCategoryId);
            $cv2->setCategoryString(implode(' > ', $categories));
            $cv2->setCreatedAt($result->getViewDate());
            $cv2->save();
           
            
            // Get Sub Model category id from new category table
            $subModel = MODELSTable::getInstance()->find($result->getSubModelId());
            if (!$subModel || !$subModelCategory = Doctrine_Query::create()->from('Category')->where('UPPER(name) = ?', strtoupper($subModel->getModel()))->andWhere('parent_category_id = ?', $modelCategoryId)->fetchOne()) {
                $categoryView->delete();
                $cv->delete();
                $cv2->delete();
                continue;
            }
            $subModelCategoryId = $subModelCategory->getId();
            $categories[] = $subModel->getModel();

            // Add stat
            $cv3 = new CategoryViewGroup();
            $cv3->setCategoryViewId($categoryView->getId());
            $cv3->setWebCategoryTypeId($subModelCategory->getCategoryTypeId());
            $cv3->setWebCategoryId($subModelCategoryId);
            $cv3->setCategoryString(implode(' > ', $categories));
            $cv3->setCreatedAt($result->getViewDate());
            $cv3->save();
            
            // Now look for year by looking at the category name, parent category id will be the sub model
            
            // Get Year category id from new category table
            $yearCategory = Doctrine_Query::create()->from('Category')->where('upper(name) = ?', strtoupper($result->getYear()))->andWhere('parent_category_id = ?', $subModelCategoryId)->fetchOne();
            if (!$yearCategory) {
                $categoryView->delete();
                $cv->delete();
                $cv2->delete();
                $cv3->delete();
                continue;
            }
            $yearCategoryId = $yearCategory->getId();
            $categories[] = $yearCategory->getName();

            // Add stat
            $cv4 = new CategoryViewGroup();
            $cv4->setCategoryViewId($categoryView->getId());
            $cv4->setWebCategoryTypeId($yearCategory->getCategoryTypeId());
            $cv4->setWebCategoryId($yearCategoryId);
            $cv4->setCategoryString(implode(' > ', $categories));
            $cv4->setCreatedAt($result->getViewDate());
            $cv4->save();
        } 
    }
    
    public function partViews()
    {
        // Select all products that still exist, and have
        $results = Doctrine_Query::create()->from('USER_VIEWS uv')->innerJoin('uv.PRODUCTS p')->where('uv.product_id > 0')->execute();
        foreach ($results as $result) {
            $product = $result->getPRODUCTS();
            // Try and find the Part in the new system
            $part = Doctrine_Query::create()->from('Part')->
                            where('UPPER(bq_number) = ?', strtoupper($product->getBqNumber()))->
                            where('UPPER(oe_number) = ?', strtoupper($product->getPartNumber()))->fetchOne();
            if (!$part) {
                continue;
            }

            // Add part view
            $partView = new PartView();
            $partView->setWebPartId($part->getId());
            $partView->setUserLoginsId($result->getUserLoginsId());
            $partView->setCreatedAt($result->getViewDate());
            $partView->save();
        }
    }
}
