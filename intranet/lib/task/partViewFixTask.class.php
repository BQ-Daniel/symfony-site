<?php

class partViewFixTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = '';
        $this->name = 'partviewfix';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [partviewfix|INFO] task does things.
Call it with:

  [php symfony backup|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $db = $databaseManager->getDatabase($options['connection']);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();


        $q = Doctrine_Query::create()->from('PartView pv')
                ->select('*')
                ->leftJoin('pv.WebPart wp')
                ->where('wp.id IS NULL AND pv.id > 60000')
                ->orderBy('pv.user_logins_id', "DESC")
                ->limit(10000);
        
        // Add order by            
        //$q->addOrderBy($sort . ' ' . $dir);
        
        $results = $q->execute();
        
        foreach ($results as $result)
        {
            //echo get_class($result);
            $arr = $result->toArray();
            

            //$webPart = $result->getWebPart();
            if (!$result->getWebPart()->getId()) {
                $arr = $result->toArray();
                $webPartToCategory = Doctrine_Query::create()->
                        from('WebPartToCategory wptc')
                        ->where('wptc.web_category_id = ?', strtoupper($arr['web_part_id']))
                        ->fetchOne(array(), Doctrine_Core::HYDRATE_ARRAY);
                
                // Update part view
                if ($webPartToCategory['web_part_id']) {
                    
                    Doctrine_Query::create()->update('PartView')
                            ->set('web_part_id', '"'.$webPartToCategory['web_part_id'].'"')
                            ->where('id = ?', $result->getId())->execute();

                    echo "Done ".$result->getId()." - User Logins ID: ".$arr['user_logins_id']." - Part ID: ".$webPartToCategory['web_part_id']."\r\n";
                }
                
                //print_r($webPartToCategory);
                //exit;
                
            }
        }
    }
}
