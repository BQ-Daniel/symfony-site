<?php

class backupTask extends sfBaseTask {

    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
                // add your own options here
        ));

        $this->namespace = '';
        $this->name = 'backup';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [backup|INFO] task does things.
Call it with:

  [php symfony backup|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $db = $databaseManager->getDatabase($options['connection']);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();


        $timestamp = date('d-m-y_H-i-s');
        $backupDir = sfConfig::get('sf_root_dir') . '/backups/';
        $backupSqlFile = "" . $timestamp . '.sql';
        $backupDataFile = "" . $timestamp . '.zip';

        // Backup database
        $output[] = date('H:i:s').': Backing up database to ' . $backupSqlFile.' ....';
        exec("mysqldump -u ".$db->getParameter('username') . " -p" . $db->getParameter('password') . " " . $db->getParameter('dbname') . " > " .$backupDir.$backupSqlFile);
        $output[] = date('H:i:s').': Complete';
        $output[] = '';
        
        // Backup files
        $output[] = date('H:i:s').': Backing up files to' . $backupDataFile;
        exec("zip -r " . $backupDir . $backupDataFile . " " . sfConfig::get('sf_root_dir') . "/web/uploads -x \*backup/backup*");
        $output[] = date('H:i:s').': Complete';
        $output[] = '';
        
        $ftpHost = 'ftp.smytheweb.com';
        $ftpUser = 'brakeq@smytheweb.com';
        $ftpPass = 'placeholder';
        $ftpRemoteDir = '/';

        // FTP 
        $ftpError = false;
        try {
            // Connect to firewall
            $conn_id = ftp_connect($ftpHost);
            $output[] = date('H:i:s').": Connecting to $ftpHost....";

            // Open a session to an external ftp site
            $login_result = ftp_login($conn_id, $ftpUser, $ftpPass);
            $output[] = date('H:i:s').": Logging in on to ftp server....";

            // Check open
            if ((!$conn_id) || (!$login_result)) {
                $output[] = date('H:i:s').": Connection failed";
                $ftpError = true;
            }
            else {
                ftp_pasv($conn_id, true);

                // Upload sql
                $output[] = date('H:i:s').": Uploading database file....";
                if (ftp_put($conn_id, $ftpRemoteDir.$backupSqlFile, $backupDir.$backupSqlFile, FTP_ASCII)) {
                    $output[] = date('H:i:s').": Complete";
                }
                else {
                    $output[] = date('H:i:s').": Failed";
                    $ftpError = true;
                }
                
                // Upload zip
                $output[] = date('H:i:s').": Uploading data zip file....";
                if (ftp_put($conn_id, $ftpRemoteDir.$backupDataFile, $backupDir.$backupDataFile, FTP_BINARY)) {
                    $output[] = date('H:i:s').": Complete";
                }
                else {
                    $output[] = date('H:i:s').": Failed";
                    $ftpError = true;
                }
            }
        }
        catch (Exception $e) {
            $ftpError = true;
        }
        
        try {
            // Now list files on server and remove any that are over 10 days old
            $results = Doctrine_Query::create()->from('Backup')->where("DATE_ADD(created_at, INTERVAL 10 DAY) < NOW()")->execute();
            foreach ($results as $result) {
                ftp_delete($conn_id, $ftpRemoteDir . $result->getZip());
                ftp_delete($conn_id, $ftpRemoteDir . $result->getDb());
                unlink($backupDir . $backupSqlFile);
                unlink($backupDir . $backupDataFile);
                $result->delete();
            }
        }
        catch (Exception $e) {
            $output[] = date('H:i:s').": [Error] ".$e->getMessage();
        }

        // Close FTP Connection
        ftp_close($conn_id);

        $bk = new Backup();
        $bk->setZip($backupDataFile);
        $bk->setDb($backupSqlFile);
        $bk->setFtp(($ftpError ? 'Error' : 'Success'));
        $bk->setLog(implode("\n", $output));
        $bk->setCreatedAt(date('Y-m-d h:i:s'));
        $bk->save();
        
        exit;
    }
}
