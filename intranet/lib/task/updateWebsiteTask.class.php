<?php

class updateWebsiteTask extends sfBaseTask {

    public $log;

    protected function configure() {

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'master'),
            new sfCommandOption('staffId', null, sfCommandOption::PARAMETER_OPTIONAL, 'Staff ID of who is requesting the update', null),
                // add your own options here
        ));

        $this->namespace = 'update';
        $this->name = 'website';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [update:website|INFO] task does things.
Call it with:

  [php symfony update:website|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        set_time_limit(0);

        // initialize the database connection        
        $databaseManager = new sfDatabaseManager($this->configuration);
        $conn = $this->conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        //$conn->setAttribute(Doctrine_Core::ATTR_AUTOCOMMIT, false);

        $this->webUpdate = new WebUpdate();
        if ($options['staffId']) {
            $this->webUpdate->setStaffId($options['staffId']);
        }
        $this->webUpdate->save();

        try {
            $this->start();
        } catch (Exception $e) {
            $this->log('[Error] ' . $e->getMessage());
        }
        $this->log('Finished');
        $this->webUpdate->setLog($this->log);
        $this->webUpdate->setFinishedAt(date('Y-m-d H:i:s'));
        $this->webUpdate->save();
    }

    public function start() {
        $conn = $this->conn;

        $this->log("Starting update process....");
        // Get time of when the website was last updated
        $setting = SettingTable::getInstance()->findOneByName('website_updated');

        if (!$setting) {
            $this->log("Unable to get setting from setting table: website_updated");
            return;
        }

        // Get settings
        $websiteLastUpdated = $setting->getValue();
        $ftpHost = 'ftp.brakequip.com.au';
        $ftpUser = 'catalogue@brakequip.com.au';
        $ftpPass = 'placeholder';
        $illDir = dirname(__FILE__) . '/../../web/uploads/illustrations';
        $prodDir = dirname(__FILE__) . '/../../../products';

        // Make FTP connection
        // Connect to FTP
        $ftpConn = ftp_connect($ftpHost);

        // Open a session to an external ftp site
        $loginResult = ftp_login($ftpConn, $ftpUser, $ftpPass);

        // Check open
        if ((!$ftpConn) || (!$loginResult)) {
            $str = "[Error] FTP Connection to website failed: $ftpUser:$ftpPass@$ftpHost";
            $this->log($str);
            $errors[] = $str;
            $this->webUpdate->setFinishedAt(date('Y-m-d H:i:s'));
            $this->webUpdate->save();
            exit;
        } else {
            ftp_pasv($ftpConn, true);
        }

        /**
         * Illustrations
         */
        if ($loginResult) {

            // Delete illustrations
            $results = Doctrine_Query::create()->from('MarkedForDelete d')->where('object_type = ?', 'illustration')->execute();
            if (count($results) > 0) {
                $this->log("Found " . count($results) . " illustration" . (count($results) > 1 ? 's' : '') . " marked to be deleted.");
                foreach ($results as $ill) {
                    $image = $ill->getObjectId();
                    $imageThumb = $ill->getObjectId();

                    if (ftp_size($ftpConn, '/products/' . $image . '.svg') != -1) {
                        $image .= '.svg';
                        $imageThumb .= '_s.svg';
                    } else {
                        $image .= '.jpg';
                        $imageThumb .= '_s.jpg';
                    }

                    if (ftp_size($ftpConn, '/products/' . $image) != -1 && !ftp_delete($ftpConn, '/products/' . $image)) {
                        $errors['ftp'][] = "Failed to delete illustration: " . $image;
                    } else {
                        $this->log('Illustration deleted: ' . $image);
                    }
                    if (ftp_size($ftpConn, '/products/' . $imageThumb) != -1 && !ftp_delete($ftpConn, '/products/' . $imageThumb)) {
                        $errors['ftp'][] = "Failed to delete illustration: " . $imageThumb;
                    } else {
                        $this->log('Illustration deleted: ' . $imageThumb);
                    }
                }
            }

            $illustrations = Doctrine_Query::create()->from('Illustration')->where('updated_at > ?', $websiteLastUpdated)->execute();

            if (count($illustrations) > 0 && $loginResult) {
                $this->log("Found " . count($illustrations) . " illustration" . (count($illustrations) > 1 ? 's' : '') . " to be uploaded");
                foreach ($illustrations as $ill) {
                    if (file_exists($illDir . '/' . $ill->getIllustrationNumber() . '.svg')) {
                        $image = $ill->getIllustrationNumber() . '.svg';
                        $imageThumb = $ill->getIllustrationNumber() . '_s.svg';
                    } else {
                        $image = $ill->getIllustrationNumber() . '.jpg';
                        $imageThumb = $ill->getIllustrationNumber() . '_s.jpg';
                    }

                    if (!file_exists($illDir . '/' . $image)) {
                        $this->log("[Error] Couldn't find image on local server: " . $image);
                        continue;
                    }
                    if (!ftp_put($ftpConn, '/products/' . $image, $illDir . '/' . $image, FTP_BINARY)) {
                        $this->log("[Error] Failed to upload illustration: " . $image);
                    } else {
                        $this->log("Uploaded: " . $image);
                    }
                    if (!ftp_put($ftpConn, '/products/' . $imageThumb, $illDir . '/' . $imageThumb, FTP_BINARY)) {
                        $this->log("[Error] Failed to upload illustration: " . $imageThumb);
                    } else {
                        $this->log("Uploaded: " . $imageThumb);
                    }

                    /*copy($illDir . '/' . $image, $prodDir . '/' . $image);
                    if (file_exists($illDir . '/' . $imageThumb)) {
                        copy($illDir . '/' . $imageThumb, $prodDir . '/' . $imageThumb);
                    }*/
                }
            }
        }

        /**
         * Category Types
         */
        $categoryTypes = Doctrine_Query::create()->from('CategoryType')->where('updated_at > ?', $websiteLastUpdated)->execute();
        if (count($categoryTypes) > 0) {
            $this->log("Found " . count($categoryTypes) . " CategoryType" . (count($categoryTypes) > 1 ? 's' : '') . " to be updated");
            foreach ($categoryTypes as $categoryType) {
                $new = false;
                if (!$webCategoryType = WebCategoryTypeTable::getInstance()->find($categoryType->getId())) {
                    $webCategoryType = new WebCategoryType();
                    $new = true;
                }

                $values = $categoryType->toArray();
                $webCategoryType->fromArray($values);
                $webCategoryType->save();

                $this->log('CategoryType ' . ($new ? 'added' : 'updated') . ': ' . $categoryType->getName());
            }
        }

        /**
         * Parts
         */
        $parts = Doctrine_Query::create()->from('Part p')->leftJoin('p.Illustration i')->where('updated_at > ?', $websiteLastUpdated)->execute();
        if (count($parts) > 0) {
            $this->log("Found " . count($parts) . ' Part' . (count($parts) > 1 ? 's' : '') . " to be updated");
            foreach ($parts as $part) {
                $new = false;
                if (!$webPart = WebPartTable::getInstance()->find($part->getId())) {
                    $webPart = new WebPart();
                    $new = true;
                }

                $values = $part->toArray();
                $values['ill_number'] = null;
                // Get illustration number for webpart table as it doesnt store id's
                if ($values['illustration_id'] > 0) {
                    $values['ill_number'] = $part->getIllustration()->getIllustrationNumber();
                }
                unset($values['illustration_id']);

                $webPart->fromArray($values);
                $webPart->save();

                $this->log('Part ' . ($new ? 'added' : 'updated') . ': ' . $part->getOeNumber());
            }
        }

        // Delete Parts
        $results = Doctrine_Query::create()->from('MarkedForDelete d')->where('object_type = ?', 'part')->execute();

        if (count($results) > 0) {
            $this->log("Found " . count($results) . " Part" . (count($results) > 1 ? 's' : '') . " marked to be deleted");
            $q = Doctrine_Query::create()->delete('WebPart');
            foreach ($results as $result) {
                $q->orWhere('id = ?', $result->getObjectId());
            }
            $q->execute();
            $this->log("Deleted " . count($results) . " Part" . (count($results) > 1 ? 's' : ''));
        }

        /**
         * Categories
         */
        $q = Doctrine_Query::create()->from('Category')->where('updated_at > ?', $websiteLastUpdated);
        $categories = $q->execute();

        if (count($categories) > 0) {
            $this->log("Found " . count($categories) . ' ' . (count($categories) > 1 ? 'Categories' : 'Category') . " to be updated");
            $i = 0;
            foreach ($categories as $category) {
                $i++;
                $new = false;
                if (!$webCategory = WebCategoryTable::getInstance()->find($category->getId())) {
                    $webCategory = new WebCategory();
                    $new = true;
                }

                $values = $category->toArray();
                $values['parent_web_category_id'] = $values['parent_category_id'];
                $values['web_category_type_id'] = $values['category_type_id'];
                $values['sibling_web_category_type_id'] = $values['sibling_category_type_id'];

                $webCategory->fromArray($values);
                $webCategory->save();

                $this->log('Category ' . ($new ? 'added' : 'updated') . ' ' . $category->getName());
            }
        }

        // Delete categories
        $results = Doctrine_Query::create()->from('MarkedForDelete d')->where('object_type = ?', 'category')->execute();

        if (count($results) > 0) {
            $this->log("Found " . count($results) . " " . (count($results) > 1 ? 'categories' : 'category') . " marked to be deleted");

            $q = Doctrine_Query::create()->delete('WebCategory');
            $q2 = Doctrine_Query::create()->delete('WebPartToCategory');
            foreach ($results as $result) {
                $q->orWhere('id = ?', $result->getObjectId());
                $q2->orWhere('web_category_id = ?', $result->getObjectId());
            }
            $q->execute();
            $q2->execute();
            $this->log("Deleted " . count($results) . " " . (count($results) > 1 ? 'categories' : 'category'));
        }

        /**
         * Part To Category 
         */
        $partToCategories = Doctrine_Query::create()->from('PartToCategory')->where('updated_at > ?', $websiteLastUpdated)->execute();

        if (count($partToCategories) > 0) {
            $this->log("Found " . count($partToCategories) . ' part to category link' . (count($partToCategories) > 1 ? 's' : '') . " to be updated");
            foreach ($partToCategories as $partToCategory) {

                if (!$webPartToCategory = WebPartToCategoryTable::getInstance()->find($partToCategory->getId())) {
                    $webPartToCategory = new WebPartToCategory();
                }
                $values = $partToCategory->toArray();
                $values['web_category_id'] = $values['category_id'];
                $values['web_part_id'] = $values['part_id'];
                $webPartToCategory->fromArray($values);
                $webPartToCategory->save();
            }
            $this->log('Linking part and categories completed');
        }
        // Delete links
        $results = Doctrine_Query::create()->from('MarkedForDelete d')->where('object_type = ?', 'part_to_category')->execute();

        if (count($results) > 0) {
            $this->log("Found " . count($results) . " part to category link" . (count($results) > 1 ? 's' : '') . " marked to be deleted");

            $q = Doctrine_Query::create()->delete('WebPartToCategory');
            foreach ($results as $result) {
                $q->orWhere('id = ?', $result->getObjectId());
            }
            $q->execute();
            $this->log("Deleted " . count($results) . " part to category link" . (count($results) > 1 ? 's' : ''));
        }


        // Clear marked for delete table
        Doctrine_Query::create()->delete('MarkedForDelete')->execute();
        // Update last database update
        $setting->setValue(date('Y-m-d H:i:s'));
        $setting->save();
    }

    public
            function log($str) {
        $str = '[' . date("d/m/y H:i:s") . '] ' . $str;
        $this->log = $this->log . "\n" . $str;
        echo $this->log;
        $this->webUpdate->setLog($this->log);
        $this->webUpdate->save();
    }

}
