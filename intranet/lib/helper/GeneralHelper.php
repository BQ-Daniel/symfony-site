<?php

function sortable_link($name, $link, $field, $params = null)
{
    $dir = (isset($_REQUEST['dir']) ? $_REQUEST['dir'] : 'asc');
    $sort = (isset($_REQUEST['sort']) ? $_REQUEST['sort'] : null);
    $page = (int) (isset($_REQUEST['page']) ? $_REQUEST['page'] : 1);

    // Work out dir 
    $dir = ($sort == $field && $dir == 'asc' ? 'desc' : 'asc');
    $class = ($dir == "asc" ? "down" : "up");
    
    echo link_to($name, $link.'?page='.$page.'&sort='.$field.'&dir='.$dir.$params, ($sort == $field ? 'class="'.$class.'"' : ''));
}

function getTimeDifference($start, $end)
{
    $uts['start']      =    strtotime( $start );
    $uts['end']        =    strtotime( $end );
    if( $uts['start']!==-1 && $uts['end']!==-1 )
    {
        if( $uts['end'] >= $uts['start'] )
        {
            $diff    =    $uts['end'] - $uts['start'];
            if( $days=intval((floor($diff/86400))) )
                $diff = $diff % 86400;
            if( $hours=intval((floor($diff/3600))) )
                $diff = $diff % 3600;
            if( $minutes=intval((floor($diff/60))) )
                $diff = $diff % 60;
            $diff    =    intval( $diff );      
			
			$str = ($days > 0 ? $days.' day'.($days > 1 ? 's' : '').' ' : '');
			$str .= ($hours > 0 ? $hours.' hr'.($hours > 1 ? 's' : '').' ' : '');
			$str .= ($minutes > 0 ? $minutes.' min'.($minutes > 1 ? 's' : '').' ' : '');

			return (!empty($str) ? $str : 'Under 1 minute');
        }
    }
    return( false );
}


function resizeimg( $forcedwidth, $forcedheight, $sourcefile, $destfile, $other = 0 )
{
    $g_fw = $forcedwidth;
    $g_fh = $forcedheight;
    $g_is = getimagesize( $sourcefile );
    if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh))
    {
        $g_iw=$g_fw;
        $g_ih=($g_fw/$g_is[0])*$g_is[1];
    }
    else
    {
        $g_ih=$g_fh;
        $g_iw=($g_ih/$g_is[1])*$g_is[0]; 
    }

    $img_src = imagecreatefromjpeg( $sourcefile );
    $img_dst = imagecreatetruecolor( $g_iw, $g_ih );
    imagecopyresampled( $img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1] );
    if( !imagejpeg( $img_dst, $destfile, 50 ) )
    {
        exit( );
    }
}
?>
