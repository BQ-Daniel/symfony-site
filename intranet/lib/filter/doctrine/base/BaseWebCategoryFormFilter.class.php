<?php

/**
 * WebCategory filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWebCategoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'parent_web_category_id'       => new sfWidgetFormFilterInput(),
      'web_category_type_id'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sibling_web_category_type_id' => new sfWidgetFormFilterInput(),
      'name'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sort_order'                   => new sfWidgetFormFilterInput(),
      'created_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'parent_web_category_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'web_category_type_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sibling_web_category_type_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'                         => new sfValidatorPass(array('required' => false)),
      'sort_order'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('web_category_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WebCategory';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'parent_web_category_id'       => 'Number',
      'web_category_type_id'         => 'Number',
      'sibling_web_category_type_id' => 'Number',
      'name'                         => 'Text',
      'sort_order'                   => 'Number',
      'created_at'                   => 'Date',
      'updated_at'                   => 'Date',
    );
  }
}
