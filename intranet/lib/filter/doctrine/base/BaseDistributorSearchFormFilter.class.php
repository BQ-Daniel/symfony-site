<?php

/**
 * DistributorSearch filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseDistributorSearchFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'location'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'radius'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total_results' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'location'      => new sfValidatorPass(array('required' => false)),
      'radius'        => new sfValidatorPass(array('required' => false)),
      'total_results' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('distributor_search_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DistributorSearch';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'location'      => 'Text',
      'radius'        => 'Text',
      'total_results' => 'Number',
      'created_at'    => 'Date',
    );
  }
}
