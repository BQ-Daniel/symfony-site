<?php

/**
 * MarkedForDelete filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMarkedForDeleteFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'object_id'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'object_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'category' => 'category', 'part' => 'part', 'illustration' => 'illustration', 'category_type' => 'category_type', 'part_to_category' => 'part_to_category'))),
      'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'object_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'object_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('category' => 'category', 'part' => 'part', 'illustration' => 'illustration', 'category_type' => 'category_type', 'part_to_category' => 'part_to_category'))),
      'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('marked_for_delete_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MarkedForDelete';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'object_id'   => 'Number',
      'object_type' => 'Enum',
      'created_at'  => 'Date',
      'updated_at'  => 'Date',
    );
  }
}
