<?php

/**
 * USER_VIEWS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUSER_VIEWSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_logins_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'make_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MAKES'), 'add_empty' => true)),
      'model_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MODELS'), 'add_empty' => true)),
      'sub_model_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SUBMODELS'), 'add_empty' => true)),
      'product_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PRODUCTS'), 'add_empty' => true)),
      'year'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'view_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'user_logins_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'make_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MAKES'), 'column' => 'make_id')),
      'model_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MODELS'), 'column' => 'model_id')),
      'sub_model_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SUBMODELS'), 'column' => 'model_id')),
      'product_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PRODUCTS'), 'column' => 'product_id')),
      'year'           => new sfValidatorPass(array('required' => false)),
      'view_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('user_views_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USER_VIEWS';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'user_logins_id' => 'Number',
      'make_id'        => 'ForeignKey',
      'model_id'       => 'ForeignKey',
      'sub_model_id'   => 'ForeignKey',
      'product_id'     => 'ForeignKey',
      'year'           => 'Text',
      'view_date'      => 'Date',
    );
  }
}
