<?php

/**
 * PRODUCTS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePRODUCTSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'model_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bq_number'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'part_number'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ill_number'    => new sfWidgetFormFilterInput(),
      'cut_hose'      => new sfWidgetFormFilterInput(),
      'ac'            => new sfWidgetFormFilterInput(),
      'ad'            => new sfWidgetFormFilterInput(),
      'ae'            => new sfWidgetFormFilterInput(),
      'comment'       => new sfWidgetFormFilterInput(),
      'description'   => new sfWidgetFormFilterInput(),
      'hose_position' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'year'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_added'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'model_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bq_number'     => new sfValidatorPass(array('required' => false)),
      'part_number'   => new sfValidatorPass(array('required' => false)),
      'ill_number'    => new sfValidatorPass(array('required' => false)),
      'cut_hose'      => new sfValidatorPass(array('required' => false)),
      'ac'            => new sfValidatorPass(array('required' => false)),
      'ad'            => new sfValidatorPass(array('required' => false)),
      'ae'            => new sfValidatorPass(array('required' => false)),
      'comment'       => new sfValidatorPass(array('required' => false)),
      'description'   => new sfValidatorPass(array('required' => false)),
      'hose_position' => new sfValidatorPass(array('required' => false)),
      'year'          => new sfValidatorPass(array('required' => false)),
      'date_added'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('products_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PRODUCTS';
  }

  public function getFields()
  {
    return array(
      'product_id'    => 'Number',
      'model_id'      => 'Number',
      'bq_number'     => 'Text',
      'part_number'   => 'Text',
      'ill_number'    => 'Text',
      'cut_hose'      => 'Text',
      'ac'            => 'Text',
      'ad'            => 'Text',
      'ae'            => 'Text',
      'comment'       => 'Text',
      'description'   => 'Text',
      'hose_position' => 'Text',
      'year'          => 'Text',
      'date_added'    => 'Date',
    );
  }
}
