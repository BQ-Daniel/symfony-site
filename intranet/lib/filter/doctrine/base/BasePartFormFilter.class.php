<?php

/**
 * Part filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePartFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'illustration_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Illustration'), 'add_empty' => true)),
      'oe_number'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'bq_number'              => new sfWidgetFormFilterInput(),
      'cut_hose_at'            => new sfWidgetFormFilterInput(),
      'ac_length'              => new sfWidgetFormFilterInput(),
      'ad_length'              => new sfWidgetFormFilterInput(),
      'ae_length'              => new sfWidgetFormFilterInput(),
      'comments'               => new sfWidgetFormFilterInput(),
      'hose_storage'           => new sfWidgetFormFilterInput(),
      'make_hose_instructions' => new sfWidgetFormFilterInput(),
      'a_fitting'              => new sfWidgetFormFilterInput(),
      'b_fitting'              => new sfWidgetFormFilterInput(),
      'c_fitting'              => new sfWidgetFormFilterInput(),
      'd_fitting'              => new sfWidgetFormFilterInput(),
      'e_fitting'              => new sfWidgetFormFilterInput(),
      'pbr_number'             => new sfWidgetFormFilterInput(),
      'ol'                     => new sfWidgetFormFilterInput(),
      'website_display'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'staff_id'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'illustration_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Illustration'), 'column' => 'id')),
      'oe_number'              => new sfValidatorPass(array('required' => false)),
      'bq_number'              => new sfValidatorPass(array('required' => false)),
      'cut_hose_at'            => new sfValidatorPass(array('required' => false)),
      'ac_length'              => new sfValidatorPass(array('required' => false)),
      'ad_length'              => new sfValidatorPass(array('required' => false)),
      'ae_length'              => new sfValidatorPass(array('required' => false)),
      'comments'               => new sfValidatorPass(array('required' => false)),
      'hose_storage'           => new sfValidatorPass(array('required' => false)),
      'make_hose_instructions' => new sfValidatorPass(array('required' => false)),
      'a_fitting'              => new sfValidatorPass(array('required' => false)),
      'b_fitting'              => new sfValidatorPass(array('required' => false)),
      'c_fitting'              => new sfValidatorPass(array('required' => false)),
      'd_fitting'              => new sfValidatorPass(array('required' => false)),
      'e_fitting'              => new sfValidatorPass(array('required' => false)),
      'pbr_number'             => new sfValidatorPass(array('required' => false)),
      'ol'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'website_display'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'staff_id'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('part_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Part';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'illustration_id'        => 'ForeignKey',
      'oe_number'              => 'Text',
      'bq_number'              => 'Text',
      'cut_hose_at'            => 'Text',
      'ac_length'              => 'Text',
      'ad_length'              => 'Text',
      'ae_length'              => 'Text',
      'comments'               => 'Text',
      'hose_storage'           => 'Text',
      'make_hose_instructions' => 'Text',
      'a_fitting'              => 'Text',
      'b_fitting'              => 'Text',
      'c_fitting'              => 'Text',
      'd_fitting'              => 'Text',
      'e_fitting'              => 'Text',
      'pbr_number'             => 'Text',
      'ol'                     => 'Number',
      'website_display'        => 'Boolean',
      'staff_id'               => 'Number',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
    );
  }
}
