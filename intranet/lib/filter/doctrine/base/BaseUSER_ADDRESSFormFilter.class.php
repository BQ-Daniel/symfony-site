<?php

/**
 * USER_ADDRESS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUSER_ADDRESSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address1'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address2'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'suburb'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'state'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'postcode'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone'        => new sfWidgetFormFilterInput(),
      'address_type' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lat'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lng'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'name'         => new sfValidatorPass(array('required' => false)),
      'address1'     => new sfValidatorPass(array('required' => false)),
      'address2'     => new sfValidatorPass(array('required' => false)),
      'suburb'       => new sfValidatorPass(array('required' => false)),
      'state'        => new sfValidatorPass(array('required' => false)),
      'postcode'     => new sfValidatorPass(array('required' => false)),
      'phone'        => new sfValidatorPass(array('required' => false)),
      'address_type' => new sfValidatorPass(array('required' => false)),
      'lat'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'lng'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('user_address_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USER_ADDRESS';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'user_id'      => 'Number',
      'name'         => 'Text',
      'address1'     => 'Text',
      'address2'     => 'Text',
      'suburb'       => 'Text',
      'state'        => 'Text',
      'postcode'     => 'Text',
      'phone'        => 'Text',
      'address_type' => 'Text',
      'lat'          => 'Number',
      'lng'          => 'Number',
    );
  }
}
