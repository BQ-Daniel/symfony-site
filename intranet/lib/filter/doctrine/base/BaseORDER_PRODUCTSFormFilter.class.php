<?php

/**
 * ORDER_PRODUCTS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseORDER_PRODUCTSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'part_number' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description' => new sfWidgetFormFilterInput(),
      'price'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'weight'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stock'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cat'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price_group' => new sfWidgetFormFilterInput(),
      'active'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sort_alpha'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sort_key'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'part_number' => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'price'       => new sfValidatorPass(array('required' => false)),
      'weight'      => new sfValidatorPass(array('required' => false)),
      'stock'       => new sfValidatorPass(array('required' => false)),
      'cat'         => new sfValidatorPass(array('required' => false)),
      'price_group' => new sfValidatorPass(array('required' => false)),
      'active'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sort_alpha'  => new sfValidatorPass(array('required' => false)),
      'sort_key'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('order_products_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDER_PRODUCTS';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'part_number' => 'Text',
      'description' => 'Text',
      'price'       => 'Text',
      'weight'      => 'Text',
      'stock'       => 'Text',
      'cat'         => 'Text',
      'price_group' => 'Text',
      'active'      => 'Number',
      'sort_alpha'  => 'Text',
      'sort_key'    => 'Text',
    );
  }
}
