<?php

/**
 * CategoryViewGroup filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCategoryViewGroupFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'category_view_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CategoryView'), 'add_empty' => true)),
      'web_category_type_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategoryType'), 'add_empty' => true)),
      'web_category_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategory'), 'add_empty' => true)),
      'category_string'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'category_view_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CategoryView'), 'column' => 'id')),
      'web_category_type_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('WebCategoryType'), 'column' => 'id')),
      'web_category_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('WebCategory'), 'column' => 'id')),
      'category_string'      => new sfValidatorPass(array('required' => false)),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('category_view_group_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CategoryViewGroup';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'category_view_id'     => 'ForeignKey',
      'web_category_type_id' => 'ForeignKey',
      'web_category_id'      => 'ForeignKey',
      'category_string'      => 'Text',
      'created_at'           => 'Date',
    );
  }
}
