<?php

/**
 * ORDER_DETAILS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseORDER_DETAILSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ORDERS'), 'add_empty' => true)),
      'order_product_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ORDER_PRODUCTS'), 'add_empty' => true)),
      'qty'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'price'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'weight'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'order_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ORDERS'), 'column' => 'id')),
      'order_product_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ORDER_PRODUCTS'), 'column' => 'id')),
      'qty'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'            => new sfValidatorPass(array('required' => false)),
      'weight'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('order_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDER_DETAILS';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'order_id'         => 'ForeignKey',
      'order_product_id' => 'ForeignKey',
      'qty'              => 'Number',
      'price'            => 'Text',
      'weight'           => 'Text',
    );
  }
}
