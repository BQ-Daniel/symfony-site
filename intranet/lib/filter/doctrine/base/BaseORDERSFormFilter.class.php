<?php

/**
 * ORDERS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseORDERSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('USERS'), 'add_empty' => true)),
      'shipping_to' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_by'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_no'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ship_via'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cost'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'weight'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_added'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('USERS'), 'column' => 'id')),
      'shipping_to' => new sfValidatorPass(array('required' => false)),
      'order_by'    => new sfValidatorPass(array('required' => false)),
      'order_no'    => new sfValidatorPass(array('required' => false)),
      'ship_via'    => new sfValidatorPass(array('required' => false)),
      'cost'        => new sfValidatorPass(array('required' => false)),
      'weight'      => new sfValidatorPass(array('required' => false)),
      'status'      => new sfValidatorPass(array('required' => false)),
      'date_added'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('orders_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDERS';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'user_id'     => 'ForeignKey',
      'shipping_to' => 'Text',
      'order_by'    => 'Text',
      'order_no'    => 'Text',
      'ship_via'    => 'Text',
      'cost'        => 'Text',
      'weight'      => 'Text',
      'status'      => 'Text',
      'date_added'  => 'Date',
    );
  }
}
