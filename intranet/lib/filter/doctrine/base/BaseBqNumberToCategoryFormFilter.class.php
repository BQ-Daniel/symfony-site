<?php

/**
 * BqNumberToCategory filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBqNumberToCategoryFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bq_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BqNumber'), 'add_empty' => true)),
      'category_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Category'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'bq_number_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('BqNumber'), 'column' => 'id')),
      'category_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Category'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('bq_number_to_category_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BqNumberToCategory';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'bq_number_id' => 'ForeignKey',
      'category_id'  => 'ForeignKey',
    );
  }
}
