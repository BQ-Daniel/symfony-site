<?php

/**
 * MODELS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMODELSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'make_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'model'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'parent_model' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'make_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'model'        => new sfValidatorPass(array('required' => false)),
      'parent_model' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('models_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MODELS';
  }

  public function getFields()
  {
    return array(
      'model_id'     => 'Number',
      'make_id'      => 'Number',
      'model'        => 'Text',
      'parent_model' => 'Number',
    );
  }
}
