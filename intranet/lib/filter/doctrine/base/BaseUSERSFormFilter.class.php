<?php

/**
 * USERS filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUSERSFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'username'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'email'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_login'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'total_logins' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_no_req' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'distributor'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'stop_credit'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'deleted'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'username'     => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'last_login'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'total_logins' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'order_no_req' => new sfValidatorPass(array('required' => false)),
      'distributor'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'stop_credit'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'deleted'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('users_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USERS';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'username'     => 'Text',
      'email'        => 'Text',
      'last_login'   => 'Date',
      'created_at'   => 'Date',
      'total_logins' => 'Number',
      'order_no_req' => 'Text',
      'distributor'  => 'Number',
      'stop_credit'  => 'Number',
      'deleted'      => 'Number',
    );
  }
}
