<?php

/**
 * PriceCode filter form base class.
 *
 * @package    brakequip
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePriceCodeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'price_code'       => new sfWidgetFormFilterInput(),
      'description'      => new sfWidgetFormFilterInput(),
      'catalogue_access' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'search_access'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'pg_51'            => new sfWidgetFormFilterInput(),
      'pg_52'            => new sfWidgetFormFilterInput(),
      'pg_53'            => new sfWidgetFormFilterInput(),
      'pg_54'            => new sfWidgetFormFilterInput(),
      'pg_55'            => new sfWidgetFormFilterInput(),
      'pg_56'            => new sfWidgetFormFilterInput(),
      'pg_57'            => new sfWidgetFormFilterInput(),
      'pg_59'            => new sfWidgetFormFilterInput(),
      'pg_61'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'price_code'       => new sfValidatorPass(array('required' => false)),
      'description'      => new sfValidatorPass(array('required' => false)),
      'catalogue_access' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'search_access'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'pg_51'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_52'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_53'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_54'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_55'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_56'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_57'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_59'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pg_61'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('price_code_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PriceCode';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'price_code'       => 'Text',
      'description'      => 'Text',
      'catalogue_access' => 'Boolean',
      'search_access'    => 'Boolean',
      'pg_51'            => 'Number',
      'pg_52'            => 'Number',
      'pg_53'            => 'Number',
      'pg_54'            => 'Number',
      'pg_55'            => 'Number',
      'pg_56'            => 'Number',
      'pg_57'            => 'Number',
      'pg_59'            => 'Number',
      'pg_61'            => 'Number',
    );
  }
}
