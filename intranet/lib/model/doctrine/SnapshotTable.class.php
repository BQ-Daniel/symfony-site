<?php


class SnapshotTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Snapshot');
    }
}