<?php


class StaffTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Staff');
    }
    
    /**
     * Checks if username exists, case insensitive
     * @param string $username
     * @return boolean/object USERS 
     */
    public static function usernameExists($username)
    {
        $q = Doctrine_Query::create()->from('Staff')->where('UPPER(username) = ?', strtoupper($username));
        return $q->fetchOne();
    }
}