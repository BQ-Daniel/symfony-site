<?php


class PartTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Part');
    }
    
    public static function getPartByBqNumber($bqNumber)
    {
        return Doctrine_Query::create()->from('Part')->where('UPPER(bq_number) = ?', strtoupper($bqNumber))->fetchOne();
    }
    
    public static function getPartByOeNumber($oeNumber)
    {
        return Doctrine_Query::create()->from('Part')->where('UPPER(oe_number) = ?', strtoupper($oeNumber))->fetchOne();
    }
    
    public static function delete($id)
    {
        $part = PartTable::getInstance()->find($id);
        $partId = $id;
        $part->delete();
        
        MarkedForDeleteTable::markForDelete($id, 'part');
        SnapshotLogTable::add('Part Deleted: '.$part->bq_number, 'part', $partId);
    }
    
    public static function deleteByCategoryId($categoryId)
    {
        $parts = Doctrine_Query::create()->from('Part p')->innerJoin('p.PartToCategory')->where('category_id = ?', $categoryId)->execute();
        foreach ($parts as $part) {
            Doctrine_Query::create()->delete('Part p')->where('id= ?', $part->getId())->execute();
            MarkedForDeleteTable::markForDelete($part->getId(), 'part');
        }
    }
}