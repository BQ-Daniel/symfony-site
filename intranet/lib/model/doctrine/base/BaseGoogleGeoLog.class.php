<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('GoogleGeoLog', 'website');

/**
 * BaseGoogleGeoLog
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_address_id
 * @property text $address
 * @property text $result
 * @property boolean $success
 * @property USER_ADDRESS $USER_ADDRESS
 * 
 * @method integer      getId()              Returns the current record's "id" value
 * @method integer      getUserAddressId()   Returns the current record's "user_address_id" value
 * @method text         getAddress()         Returns the current record's "address" value
 * @method text         getResult()          Returns the current record's "result" value
 * @method boolean      getSuccess()         Returns the current record's "success" value
 * @method USER_ADDRESS getUSERADDRESS()     Returns the current record's "USER_ADDRESS" value
 * @method GoogleGeoLog setId()              Sets the current record's "id" value
 * @method GoogleGeoLog setUserAddressId()   Sets the current record's "user_address_id" value
 * @method GoogleGeoLog setAddress()         Sets the current record's "address" value
 * @method GoogleGeoLog setResult()          Sets the current record's "result" value
 * @method GoogleGeoLog setSuccess()         Sets the current record's "success" value
 * @method GoogleGeoLog setUSERADDRESS()     Sets the current record's "USER_ADDRESS" value
 * 
 * @package    brakequip
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseGoogleGeoLog extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('google_geo_log');
        $this->hasColumn('id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 8,
             ));
        $this->hasColumn('user_address_id', 'integer', 8, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 8,
             ));
        $this->hasColumn('address', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('result', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('success', 'boolean', null, array(
             'type' => 'boolean',
             'default' => 1,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('USER_ADDRESS', array(
             'local' => 'user_address_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}