<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('Category', 'master');

/**
 * BaseCategory
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $parent_category_id
 * @property integer $category_type_id
 * @property integer $sibling_category_type_id
 * @property string $name
 * @property integer $sort_order
 * @property text $parents
 * @property boolean $hidden
 * @property Category $Category
 * @property CategoryType $CategoryType
 * @property CategoryType $Categories
 * 
 * @method integer      getId()                       Returns the current record's "id" value
 * @method integer      getParentCategoryId()         Returns the current record's "parent_category_id" value
 * @method integer      getCategoryTypeId()           Returns the current record's "category_type_id" value
 * @method integer      getSiblingCategoryTypeId()    Returns the current record's "sibling_category_type_id" value
 * @method string       getName()                     Returns the current record's "name" value
 * @method integer      getSortOrder()                Returns the current record's "sort_order" value
 * @method text         getParents()                  Returns the current record's "parents" value
 * @method boolean      getHidden()                   Returns the current record's "hidden" value
 * @method Category     getCategory()                 Returns the current record's "Category" value
 * @method CategoryType getCategoryType()             Returns the current record's "CategoryType" value
 * @method CategoryType getCategories()               Returns the current record's "Categories" value
 * @method Category     setId()                       Sets the current record's "id" value
 * @method Category     setParentCategoryId()         Sets the current record's "parent_category_id" value
 * @method Category     setCategoryTypeId()           Sets the current record's "category_type_id" value
 * @method Category     setSiblingCategoryTypeId()    Sets the current record's "sibling_category_type_id" value
 * @method Category     setName()                     Sets the current record's "name" value
 * @method Category     setSortOrder()                Sets the current record's "sort_order" value
 * @method Category     setParents()                  Sets the current record's "parents" value
 * @method Category     setHidden()                   Sets the current record's "hidden" value
 * @method Category     setCategory()                 Sets the current record's "Category" value
 * @method Category     setCategoryType()             Sets the current record's "CategoryType" value
 * @method Category     setCategories()               Sets the current record's "Categories" value
 * 
 * @package    brakequip
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCategory extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('category');
        $this->hasColumn('id', 'integer', 11, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => 11,
             ));
        $this->hasColumn('parent_category_id', 'integer', 11, array(
             'type' => 'integer',
             'default' => 0,
             'length' => 11,
             ));
        $this->hasColumn('category_type_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 11,
             ));
        $this->hasColumn('sibling_category_type_id', 'integer', 11, array(
             'type' => 'integer',
             'default' => 0,
             'length' => 11,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('sort_order', 'integer', 3, array(
             'type' => 'integer',
             'default' => 0,
             'length' => 3,
             ));
        $this->hasColumn('parents', 'text', null, array(
             'type' => 'text',
             'notnull' => true,
             ));
        $this->hasColumn('hidden', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));

        $this->option('type', 'INNODB');
        $this->option('collate', 'utf8_unicode_ci');
        $this->option('charset', 'utf8');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Category', array(
             'local' => 'category_type_id',
             'foreign' => 'id'));

        $this->hasOne('CategoryType', array(
             'local' => 'category_type_id',
             'foreign' => 'id'));

        $this->hasOne('CategoryType as Categories', array(
             'local' => 'id',
             'foreign' => 'category_type_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}