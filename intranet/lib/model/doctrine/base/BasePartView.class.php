<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('PartView', 'website');

/**
 * BasePartView
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_logins_id
 * @property integer $category_view_id
 * @property integer $web_part_id
 * @property datetime $created_at
 * @property CategoryView $CategoryView
 * @property WebPart $WebPart
 * @property USER_LOGINS $USER_LOGINS
 * 
 * @method integer      getId()               Returns the current record's "id" value
 * @method integer      getUserLoginsId()     Returns the current record's "user_logins_id" value
 * @method integer      getCategoryViewId()   Returns the current record's "category_view_id" value
 * @method integer      getWebPartId()        Returns the current record's "web_part_id" value
 * @method datetime     getCreatedAt()        Returns the current record's "created_at" value
 * @method CategoryView getCategoryView()     Returns the current record's "CategoryView" value
 * @method WebPart      getWebPart()          Returns the current record's "WebPart" value
 * @method USER_LOGINS  getUSERLOGINS()       Returns the current record's "USER_LOGINS" value
 * @method PartView     setId()               Sets the current record's "id" value
 * @method PartView     setUserLoginsId()     Sets the current record's "user_logins_id" value
 * @method PartView     setCategoryViewId()   Sets the current record's "category_view_id" value
 * @method PartView     setWebPartId()        Sets the current record's "web_part_id" value
 * @method PartView     setCreatedAt()        Sets the current record's "created_at" value
 * @method PartView     setCategoryView()     Sets the current record's "CategoryView" value
 * @method PartView     setWebPart()          Sets the current record's "WebPart" value
 * @method PartView     setUSERLOGINS()       Sets the current record's "USER_LOGINS" value
 * 
 * @package    brakequip
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePartView extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('part_view');
        $this->hasColumn('id', 'integer', 11, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 11,
             ));
        $this->hasColumn('user_logins_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 11,
             ));
        $this->hasColumn('category_view_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 11,
             ));
        $this->hasColumn('web_part_id', 'integer', 11, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => 11,
             ));
        $this->hasColumn('created_at', 'datetime', null, array(
             'type' => 'datetime',
             ));


        $this->index('user_logins_id', array(
             'fields' => 
             array(
              0 => 'user_logins_id',
             ),
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('CategoryView', array(
             'local' => 'category_view_id',
             'foreign' => 'id'));

        $this->hasOne('WebPart', array(
             'local' => 'web_part_id',
             'foreign' => 'id'));

        $this->hasOne('USER_LOGINS', array(
             'local' => 'user_logins_id',
             'foreign' => 'id'));
    }
}