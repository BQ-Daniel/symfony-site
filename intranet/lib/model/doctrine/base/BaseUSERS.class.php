<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('USERS', 'website');

/**
 * BaseUSERS
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property date $last_login
 * @property date $created_at
 * @property integer $total_logins
 * @property string $order_no_req
 * @property integer $distributor
 * @property integer $order_access
 * @property integer $stop_credit
 * @property integer $deleted
 * @property string $price_code
 * @property Doctrine_Collection $USER_SESSIONS
 * @property Doctrine_Collection $USER_LOGINS
 * @property Doctrine_Collection $ORDERS
 * 
 * @method integer             getId()            Returns the current record's "id" value
 * @method string              getUsername()      Returns the current record's "username" value
 * @method string              getEmail()         Returns the current record's "email" value
 * @method date                getLastLogin()     Returns the current record's "last_login" value
 * @method date                getCreatedAt()     Returns the current record's "created_at" value
 * @method integer             getTotalLogins()   Returns the current record's "total_logins" value
 * @method string              getOrderNoReq()    Returns the current record's "order_no_req" value
 * @method integer             getDistributor()   Returns the current record's "distributor" value
 * @method integer             getOrderAccess()   Returns the current record's "order_access" value
 * @method integer             getStopCredit()    Returns the current record's "stop_credit" value
 * @method integer             getDeleted()       Returns the current record's "deleted" value
 * @method string              getPriceCode()     Returns the current record's "price_code" value
 * @method Doctrine_Collection getUSERSESSIONS()  Returns the current record's "USER_SESSIONS" collection
 * @method Doctrine_Collection getUSERLOGINS()    Returns the current record's "USER_LOGINS" collection
 * @method Doctrine_Collection getORDERS()        Returns the current record's "ORDERS" collection
 * @method USERS               setId()            Sets the current record's "id" value
 * @method USERS               setUsername()      Sets the current record's "username" value
 * @method USERS               setEmail()         Sets the current record's "email" value
 * @method USERS               setLastLogin()     Sets the current record's "last_login" value
 * @method USERS               setCreatedAt()     Sets the current record's "created_at" value
 * @method USERS               setTotalLogins()   Sets the current record's "total_logins" value
 * @method USERS               setOrderNoReq()    Sets the current record's "order_no_req" value
 * @method USERS               setDistributor()   Sets the current record's "distributor" value
 * @method USERS               setOrderAccess()   Sets the current record's "order_access" value
 * @method USERS               setStopCredit()    Sets the current record's "stop_credit" value
 * @method USERS               setDeleted()       Sets the current record's "deleted" value
 * @method USERS               setPriceCode()     Sets the current record's "price_code" value
 * @method USERS               setUSERSESSIONS()  Sets the current record's "USER_SESSIONS" collection
 * @method USERS               setUSERLOGINS()    Sets the current record's "USER_LOGINS" collection
 * @method USERS               setORDERS()        Sets the current record's "ORDERS" collection
 * 
 * @package    brakequip
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseUSERS extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('USERS');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('username', 'string', 10, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 10,
             ));
        $this->hasColumn('email', 'string', 100, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 100,
             ));
        $this->hasColumn('last_login', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('created_at', 'date', 25, array(
             'type' => 'date',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => false,
             'autoincrement' => false,
             'length' => 25,
             ));
        $this->hasColumn('total_logins', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'default' => '0',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('order_no_req', 'string', 1, array(
             'type' => 'string',
             'fixed' => 1,
             'unsigned' => false,
             'primary' => false,
             'default' => 'N',
             'notnull' => true,
             'autoincrement' => false,
             'length' => 1,
             ));
        $this->hasColumn('distributor', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('order_access', 'integer', 1, array(
             'type' => 'integer',
             'notnull' => true,
             'default' => 0,
             'length' => 1,
             ));
        $this->hasColumn('stop_credit', 'integer', 1, array(
             'type' => 'integer',
             'notnull' => true,
             'default' => 0,
             'length' => 1,
             ));
        $this->hasColumn('deleted', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('price_code', 'string', 5, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 5,
             ));
	$this->hasColumn('threshold', 'integer', 4, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => 4,
             ));
        $this->hasColumn('website', 'string', null, array(
             'type' => 'string',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => false,
             'notnull' => true,
             'autoincrement' => false,
             'length' => '',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('USER_SESSIONS', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('USER_LOGINS', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('ORDERS', array(
             'local' => 'id',
             'foreign' => 'user_id'));
    }
}
