<?php

/**
 * BaseWebCategoryType
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $parent_web_category_type_id
 * @property string $name
 * @property Doctrine_Collection $CategoryViewGroup
 * 
 * @method integer             getId()                          Returns the current record's "id" value
 * @method integer             getParentWebCategoryTypeId()     Returns the current record's "parent_web_category_type_id" value
 * @method string              getName()                        Returns the current record's "name" value
 * @method Doctrine_Collection getCategoryViewGroup()           Returns the current record's "CategoryViewGroup" collection
 * @method WebCategoryType     setId()                          Sets the current record's "id" value
 * @method WebCategoryType     setParentWebCategoryTypeId()     Sets the current record's "parent_web_category_type_id" value
 * @method WebCategoryType     setName()                        Sets the current record's "name" value
 * @method WebCategoryType     setCategoryViewGroup()           Sets the current record's "CategoryViewGroup" collection
 * 
 * @package    brakequip
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseWebCategoryType extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('web_category_type');
        $this->hasColumn('id', 'integer', 11, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => 11,
             ));
        $this->hasColumn('parent_web_category_type_id', 'integer', 11, array(
             'type' => 'integer',
             'default' => 0,
             'length' => 11,
             ));
        $this->hasColumn('name', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CategoryViewGroup', array(
             'local' => 'id',
             'foreign' => 'web_category_type_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}