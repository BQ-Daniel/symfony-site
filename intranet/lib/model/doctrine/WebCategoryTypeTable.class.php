<?php


class WebCategoryTypeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('WebCategoryType');
    }
    
    public function getCategoryTypeByName($name, $parentId = 0)
    {
        $query = Doctrine_Query::create()->from('WebCategoryType ct')->where('parent_web_category_type_id = ?', $parentId);
        $query->andWhere('UPPER(ct.name) LIKE ?', '%' . strtoupper($name) . '%');
        return $query->fetchOne();
    }
}