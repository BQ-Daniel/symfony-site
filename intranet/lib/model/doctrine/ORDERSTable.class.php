<?php
class ORDERSTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ORDERS');
    }
    
    public static function getOrdersToday()
    {
        return Doctrine_Query::create()->from('ORDERS')->where('DATE(DATE_ADDED) = CURDATE()')->execute();
    }
    
    public static function getCountOrdersToday()
    {
        return Doctrine_Query::create()->from('ORDERS')->where('DATE(DATE_ADDED) = CURDATE()')->count();
    }
    
    /**
     * Get number of orders between 2 dates 
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return int
     */
    public static function getCountOrdersMonthYear($fromDate, $toDate)
    {
        $query = Doctrine_Query::create()->from('ORDERS');
        if ($fromDate) {
            $query->addWhere('DATE(DATE_ADDED) >= ?', $fromDate->format('Y-m-d'));
        }
        if ($toDate) {
            $query->addWhere("DATE(DATE_ADDED) <= ?", $toDate->format('Y-m-d'));
        }
        return $query->count();
    }
    
    /**
     * Get average order cost per customer 
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return float
     */
    public static function getAverageOrderCostPerCustomer($fromDate, $toDate)
    {
        $query = Doctrine_Query::create()->from('ORDERS')
                ->select('AVG(COST) as cost');
        if ($fromDate) {
            $query->addWhere('DATE(DATE_ADDED) >= ?', $fromDate->format('Y-m-d'));
        }
        if ($toDate) {
            $query->addWhere("DATE(DATE_ADDED) <= ?", $toDate->format('Y-m-d'));
        }
        $result = $query->fetchOne(array(), Doctrine_Core::HYDRATE_ARRAY);
        
        if (!$result) {
            return 0;
        }
        return $result['cost'];
    }
    
    /**
     * Get average orders per customer
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return type
     */
    public static function getAverageOrdersPerCustomer($fromDate, $toDate)
    {
        if (!$fromDate && !$toDate) {
            return false;
        }
        
        $query = "SELECT avg(count) as avgCount FROM ("
                . "SELECT COUNT(*) AS count "
                . "FROM ORDERS WHERE ";
        if ($fromDate) {
            $sql[] = "DATE(DATE_ADDED) >= '".$fromDate->format('Y-m-d')."'";
        }
        if ($toDate) {
            $sql[] = "DATE(DATE_ADDED) <= '".$toDate->format('Y-m-d')."'";
        }
        $query .= implode(' AND ',$sql)." GROUP BY USER_ID"
                . ") as counts";
        
        return Doctrine_Manager::getInstance()->getCurrentConnection()->fetchOne($query);

    }
}