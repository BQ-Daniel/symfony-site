<?php

class IllustrationTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Illustration');
    }

    public static function removeFromSystem($id) {
        $ill = IllustrationTable::getInstance()->find($id);

        if (!$ill) {
            return false;
        }

        // Remove images
        $fileName = sfConfig::get('app_illustration_dir') . '/' . $ill->getIllustrationNumber();
        $thumbFileName = sfConfig::get('app_illustration_dir') . '/' . $ill->getIllustrationNumber();
        if (file_exists($fileName . ".svg")) {
            $fileName .= ".svg";
            $thumbFileName .= "_s.svg";
        } else {
            $fileName .= ".jpg";
            $thumbFileName .= "_s.jpg";
        }
        @unlink($fileName);
        @unlink($thumbFileName);

        MarkedForDeleteTable::markForDelete($ill->getIllustrationNumber(), 'illustration');

        Doctrine_Query::create()->update('Part')->set('illustration_id', 0)->where('illustration_id = ?', $id)->execute();

        SnapshotLogTable::add('Deleted Illustration: ' . $ill->illustration_number, 'illustration', $ill->id);

        // Remove from database
        $ill->delete();
    }

    public static function getIllustationByNumber($number) {
        return Doctrine_Query::create()->from('Illustration')->where('UPPER(illustration_number) = ?', strtoupper($number))->fetchOne();
    }

}
