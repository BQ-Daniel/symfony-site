<?php


class CategoryTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Category');
    }
    /**
     * Get list of categories
     * 
     * @param array $parameters
     * @param array $options
     * @return Doctrine_Collection
     */
    public static function getCategories($parameters = array(), $options = array())
    {
        $query = Doctrine_Query::create()->from('Category c');
        $query->leftJoin('c.CategoryType ct');
        
        foreach ($parameters as $field => $value) {
            if (in_array($field, array('name'))) {
                continue;
            }
            $query->addWhere($field.' = ?', array($value));
        }

        // If name field, add LIKE
        if (isset($parameters['name'])) {
            $query->andWhere('UPPER(c.name) LIKE ?', '%' . strtoupper($parameters['name']) . '%');
        }
        
        // Order by
        $query->orderBy('c.sort_order ASC');
        
        return $query->execute();
    }
    
    /**
     * Return an array of breadcrumbs
     * @param int $categoryId
     * @return array 
     */
    public static function getParentCategories($categoryId)
    {
        $categories = array();
        $category = CategoryTable::getInstance()->find($categoryId);
        
        if (!$category) {
            return $categories;
        }
        
        $categories[] = array('id' => $category->getId(), 'name' => $category->getName(), 'type' => $category->getCategoryTypeId(), 'typeName' => $category->getCategoryType()->getName()); 
        $categoryId = $category->getParentCategoryId();
        
        while ($categoryId > 0)
        {
            $category = CategoryTable::getInstance()->find($categoryId);
            if (!$category) {
                break;
            }
            $categoryId = $category->getParentCategoryId();
            if ($categoryId == 0) {
                $categoryId = $category->getId();
                $category = CategoryTable::getInstance()->find($categoryId);
                $categories[] = array('id' => $category->getId(), 'name' => $category->getName(), 'type' => $category->getCategoryTypeId(), 'typeName' => $category->getCategoryType()->getName()); 
                break;
            }
            $categories[] = array('id' => $category->getId(), 'name' => $category->getName(), 'type' => $category->getCategoryTypeId(), 'typeName' => $category->getCategoryType()->getName()); 
        }
        return array_reverse($categories);
    }
    
    /**
     * Delete category and all it's siblings
     * This includes parts
     * 
     * @param int $categoryId 
     */
    public static function delete($categoryId)
    {        
        $category = CategoryTable::getInstance()->find($categoryId);
        if ($category) { $category->delete(); }
        $pResults = Doctrine_Query::create()->from('PartToCategory')->where('category_id = ?', $categoryId)->execute();
        foreach ($pResults as $link) {
            $link->delete();
            MarkedForDeleteTable::markForDelete($link->id, 'part_to_category');
        }
        MarkedForDeleteTable::markForDelete($category->getId(), 'category');
        SnapshotLogTable::add('Deleted Category: '.$category->name, 'category', $category->id, true);
        
        $query = Doctrine_Query::create()->from('Category')->where('parent_category_id = ?', $categoryId);
        $queries[] = $query;
        
        for ($i=0; $i<count($queries);$i++)
        {
            $query = $queries[$i];
            $results = $query->execute();
            foreach ($results as $result) {
                $result->delete();
                MarkedForDeleteTable::markForDelete($result->getId(), 'category');
                SnapshotLogTable::add('Deleted Category: '.$result->name, 'category', $result->id, true);
                $newQuery = Doctrine_Query::create()->from('Category')->where('parent_category_id = ?', $result->getId());
                array_push($queries, $newQuery);
                
                $pResults = Doctrine_Query::create()->from('PartToCategory')->where('category_id = ?', $result->getId())->execute();
                foreach ($pResults as $link) {
                    $link->delete();
                    MarkedForDeleteTable::markForDelete($link->id, 'part_to_category');
                }
                // Delete parts under this category
                //PartTable::deleteByCategoryId($result->getId());
            }
        }
    }
    
    public function getCategoryByName($name, $parentId = null, $categoryTypeId = null)
    {
        $query = Doctrine_Query::create()->from('Category');
        if ($parentId) {
            $query->addWhere('parent_category_id = ?', $parentId);
        }
        if ($categoryTypeId) {
            $query->addWhere('category_type_id = ?', $categoryTypeId);
        }
        $query->andWhere('UPPER(name) LIKE ?', '%' . strtoupper($name) . '%');
        return $query->fetchOne();
    }
    
    public static function getLastSortOrder($parentCategoryId) 
    {
        $result = Doctrine_Query::create()->from('Category')->where('parent_category_id = ?', $parentCategoryId)->
                orderBy('sort_order DESC')->limit(1)->fetchOne();
        if (!$result) {
            return 1;
        }
        return $result->getSortOrder();
    }
}