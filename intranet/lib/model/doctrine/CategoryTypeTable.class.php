<?php


class CategoryTypeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CategoryType');
    }
    
    public static function getCategoryTypes($parameters = array(), $options = array())
    {
        $query = Doctrine_Query::create()->from('CategoryType ct');
        foreach ($parameters as $field => $value) {
            if (in_array($field, array('name'))) {
                continue;
            }
            $query->addWhere($field, $value);
        }
        
        // If name field, add LIKE
        if (isset($parameters['name'])) {
            $query->andWhere('UPPER(ct.name) LIKE ?', '%' . strtoupper($parameters['name']) . '%');
        }
        
        
        return $query->execute();
    }
    
    public function getCategoryTypeByName($name, $parentId = 0)
    {
        $query = Doctrine_Query::create()->from('CategoryType ct')->where('parent_category_type_id = ?', $parentId);
        $query->andWhere('UPPER(ct.name) LIKE ?', '%' . strtoupper($name) . '%');
        return $query->fetchOne();
    }
}