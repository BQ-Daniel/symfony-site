<?php


class SnapshotLogTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SnapshotLog');
    }
    
    public static function add($log, $object, $objectId, $deleted = false)
    {
        $staffId = sfContext::getInstance()->getUser()->getStaffId();
        
        $obj = new SnapshotLog();
        $obj->log = $log;
        $obj->object = $object;
        $obj->object_id = $objectId;
        $obj->deleted = $deleted;
        $obj->staff_id = $staffId;
        $obj->save();
        
        return $obj;
        
    }
}