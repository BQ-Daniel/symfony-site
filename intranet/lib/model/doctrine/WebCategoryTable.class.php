<?php


class WebCategoryTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('WebCategory');
    }
    
    public function getCategoryByName($name, $parentId = null, $categoryTypeId = null)
    {
        $query = Doctrine_Query::create()->from('WebCategory');
        if ($parentId) {
            $query->addWhere('parent_web_category_id = ?', $parentId);
        }
        if ($categoryTypeId) {
            $query->addWhere('web_category_type_id = ?', $categoryTypeId);
        }
        $query->andWhere('UPPER(name) LIKE ?', '%' . strtoupper($name) . '%');
        return $query->fetchOne();
    }
}