<?php


class MarkedForDeleteTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('MarkedForDelete');
    }
    
    public static function markForDelete($id, $type)
    {
        $mark = new MarkedForDelete();
        $mark->setObjectId($id);
        $mark->setObjectType($type);
        $mark->save();
    }
}