<?php


class USER_SESSIONSTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('USER_SESSIONS');
    }
    
    public static function getUsersOnline()
    {
        $q = Doctrine_Query::create()->from('USER_SESSIONS')->where("NOW() < ADDTIME(LAST_ACTIVE, '00:05:00')");
        $q->orderBy('LAST_ACTIVE DESC')->groupBy('USER_ID');
        return $q->execute();
    }
    
    public static function getCountUsersOnline()
    {
        $q = Doctrine_Query::create()->from('USER_SESSIONS us')->innerJoin('us.USERS u')
                        ->where("NOW() < ADDTIME(last_active, '00:05:00')")->groupBy('us.USER_ID');
        return $q->count();
    }
}