<?php


class USERSTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('USERS');
    }
    
    /**
     * Checks if email address exists, case insensitive
     * @param string $email
     * @return boolean/object USERS 
     */
    public static function emailExists($email)
    {
        return Doctrine_Query::create()->from('USERS')->where('UPPER(email) = ?', strtoupper($email))->fetchOne();
    }
    
    /**
     * Checks if username exists, case insensitive
     * @param string $username
     * @return boolean/object USERS 
     */
    public static function usernameExists($username)
    {
        $q = Doctrine_Query::create()->from('USERS')->where('UPPER(username) = ?', strtoupper($username));
        return $q->fetchOne();
    }
}