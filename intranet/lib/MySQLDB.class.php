<?php
class MySQLDB
{
   private $connection;          // The MySQL database connection

   /* Class constructor */
   function MySQLDB(){
      /* Make connection to database */
      $this->connection = mysql_connect('brakequip.com.au', 'brakeq_app', 'placeholder') or die(mysql_error());
      mysql_select_db('brakeq_copy', $this->connection) or die(mysql_error());
	  
   }

   /* Transactions functions */

   function begin(){
      $null = mysql_query("START TRANSACTION", $this->connection);
      return mysql_query("BEGIN", $this->connection);
   }
   
   function query($sql) {
       $result = mysql_query($sql);
       if (!$result) {
           throw new Exception('SQL Error: ['.$sql.']: '.mysql_error());
           return false;
       }
       return $result;
   }

   function commit(){
      return mysql_query("COMMIT", $this->connection);
   }
   
   function rollback(){
      return mysql_query("ROLLBACK", $this->connection);
   }

   function transaction($q_array)
   {
        if (count($q_array) == 0) {
            return false;
        }
        $this->begin();

        foreach ($q_array as $qa) {
            $result = mysql_query($qa, $this->connection);
            if (!$result) {
                throw new Exception('SQL Error: [' . $qa . ']: ' . mysql_error());
                $this->rollback();
                return false;
            }
        }
        $this->commit();
        return true;
    }

}
?>
