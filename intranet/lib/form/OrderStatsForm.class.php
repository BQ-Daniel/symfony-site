<?php

/**
 * Order stats filter
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class OrderStatsForm extends BaseForm {

    public function configure() {
        
        for ($i=date('Y');$i>=(date('Y')-5); $i--) {
            $years[$i] = $i;
        }
        $dateWidget = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%'), array());
        $config = array('config' => '{buttonImage: "/images/config_date.png", buttonImageOnly: true}', 'date_widget' => $dateWidget);
        
        $this->setWidgets(array(
                    'date'     => new sfWidgetFormDate(array('format' => '%month% %year%', 'years' => $years), array()),
                    'from_date'     => new sfWidgetFormJQueryDate($config, $attributes = array()),
                    'to_date'       => new sfWidgetFormJQueryDate($config, $attributes = array())
        ));

        $this->setValidators(array(
                    'date'      => new sfValidatorPass(array(), array()),
                    'from_date'     => new sfValidatorPass(array(), array()),
                    'to_date'       => new sfValidatorPass(array(), array()),
        ));

        $this->widgetSchema->setNameFormat('stats[%s]');
    }
}
