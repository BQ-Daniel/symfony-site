<?php

/**
 * Search form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class SearchForm extends BaseForm {

    public function configure() {

        $this->setWidgets(array(
                    'oe_number'                 => new sfWidgetFormInput(array(), array('maxlength' => 100, 'class' => 'input')),
                    'bq_number'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input')),
                    'pbr_number'                => new sfWidgetFormInput(array(), array('maxlength' => 10, 'class' => 'input small')),
                    'ol'                        => new sfWidgetFormInput(array(), array('maxlength' => 5, 'class' => 'input small')),
                    'cut_hose_at'               => new sfWidgetFormInput(array(), array('maxlength' => 15, 'class' => 'input medium')),
                    'ac_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'ad_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'ae_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'hose_storage'              => new sfWidgetFormInput(array(), array('class' => 'input')),
                    'make_hose_instructions'    => new sfWidgetFormInput(array(), array('class' => 'input')),
                    'a_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'b_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'c_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'd_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'e_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'fittings'                  => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input medium')),
                    'illustration_number'       => new sfWidgetFormInput(array(), array('class' => 'input small')),
                    
        ));

        $this->setValidators(array(
                    'oe_number'                    => new sfValidatorPass(array(), array()),
                    'bq_number'                      => new sfValidatorPass(array(), array()),
                    'pbr_number'                => new sfValidatorPass(array(), array()),
                    'ol'                        => new sfValidatorPass(array(), array()),
                    'cut_hose_at'               => new sfValidatorPass(array(), array()),
                    'ac_length'                 => new sfValidatorPass(array(), array()),
                    'ad_length'                 => new sfValidatorPass(array(), array()),
                    'ae_length'                 => new sfValidatorPass(array(), array()),
                    'hose_storage'              => new sfValidatorPass(array(), array()),
                    'make_hose_instructions'    => new sfValidatorPass(array(), array()),
                    'a_fitting'                 => new sfValidatorPass(array(), array()),
                    'b_fitting'                 => new sfValidatorPass(array(), array()),
                    'c_fitting'                 => new sfValidatorPass(array(), array()),
                    'd_fitting'                 => new sfValidatorPass(array(), array()),
                    'e_fitting'                 => new sfValidatorPass(array(), array()),
                    'fittings'                  => new sfValidatorPass(array(), array()),
                    'illustration_number'       => new sfValidatorPass()
        ));

        $this->widgetSchema->setNameFormat('search[%s]');
    }
}
