<?php

/**
 * Part form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class EditUserForm extends BaseForm {

    public function configure() {
        
        $this->setWidgets(array(
          'username'     => new sfWidgetFormInputText(array(), array('maxlength' => 10)),
          'org_username' => new sfWidgetFormInputHidden(array(), array()), 
          'email'        => new sfWidgetFormInputText(array(), array('maxlength' => 100)),
          'org_email'    => new sfWidgetFormInputHidden(array(), array()), 
          'order_no_req' => new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1')),
          'order_access' => new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1')),
          'stop_credit'  => new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1')),
          'distributor'  => new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1')),
          'deleted'      => new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1')),
		  'threshold'    => new sfWidgetFormInputText(array(), array()),
		  'price_code'    => new sfWidgetFormInputText(array(), array()),
          'org_threshold' => new sfWidgetFormInputHidden(array(), array()),
          'website'       => new sfWidgetFormInputText(array(), array()),
          'org_website'   => new sfWidgetFormInputHidden(array(), array()),
          ));

        $this->setValidators(array(
          'username'     => new sfValidatorString(array('max_length' => 10, 'required' => true)),
          'org_username' => new sfValidatorPass(),
          'email'        => new sfValidatorEmail(array('max_length' => 110, 'required' => true,), array('invalid' => "Invalid email address format")),
          'org_email'    => new sfValidatorPass(),
          'order_no_req' => new sfValidatorInteger(array('required' => false)),
          'order_access' => new sfValidatorInteger(array('required' => false)),
          'stop_credit'  => new sfValidatorInteger(array('required' => false)),
          'distributor'  => new sfValidatorInteger(array('required' => false)),
          'deleted'      => new sfValidatorInteger(array('required' => false)),
          'threshold'    => new sfValidatorInteger(array('required' => true)),
		  'price_code'    => new sfValidatorString(array('required' => true)),
          'org_threshold'=> new sfValidatorPass(),
          'website' => new sfValidatorString(array('required' => false)),
          'org_website' => new sfValidatorString(array('required' => false)),
            ));
        
        
        // add a post validator
        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                //new sfValidatorCallback(array('callback' => array('customFormValidator', 'emailExists'))),
                new sfValidatorCallback(array('callback' => array('customFormValidator', 'usernameExists')))
            )
        ));

        $this->widgetSchema->setNameFormat('users[%s]');
    }
}
