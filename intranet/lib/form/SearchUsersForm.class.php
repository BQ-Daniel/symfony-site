<?php

/**
 * Part form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class SearchUsersForm extends BaseForm {

    public function configure() {
        
        $dateWidget = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%'), array());
        $config = array('config' => '{buttonImage: "/images/config_date.png", buttonImageOnly: true}', 'date_widget' => $dateWidget);
        
        $this->setWidgets(array(
                    'username'      => new sfWidgetFormInput(array(), array('maxlength' => 10, 'class' => 'input')),
                    'email'         => new sfWidgetFormInput(array(), array('maxlength' => 100, 'class' => 'input')),
                    'from_date'     => new sfWidgetFormJQueryDate($config, $attributes = array()),
                    'to_date'       => new sfWidgetFormJQueryDate($config, $attributes = array())
        ));

        $this->setValidators(array(
                    'username'      => new sfValidatorPass(array(), array()),
                    'email'         => new sfValidatorPass(array(), array()),
                    'from_date'     => new sfValidatorPass(array(), array()),
                    'to_date'       => new sfValidatorPass(array(), array()),
        ));

        $this->widgetSchema->setNameFormat('users[%s]');
    }
}
