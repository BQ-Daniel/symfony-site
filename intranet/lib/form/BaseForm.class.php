<?php

/**
 * Base project form.
 * 
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe 
 */
class BaseForm extends sfFormSymfony {

    /**
     * Override construct method
     * Set default messages for required and invalid error messages
     *
     */
    public function __construct($object = null, $options = array(), $CSRFSecret = null) {
        sfValidatorBase::setDefaultMessage('required', 'This field is required');
        sfValidatorBase::setDefaultMessage('invalid', 'This field is invalid');
    
        parent::__construct($object, $options, false);
    }
    

    public function bindInputValuesFromArray($array, $fileArray = array()) {
        $newArray = array();
        foreach ($this->getFormFieldSchema() as $field) {
            if (isset($array[$field->getName()])) {
                $newArray[$field->getName()] = $array[$field->getName()];
            }
        }
        $this->bind($newArray, $fileArray);
    }

}
