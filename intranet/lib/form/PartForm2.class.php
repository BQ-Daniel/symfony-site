<?php

/**
 * Part form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class PartForm2 extends BaseForm {

    public function configure() {

        $this->setWidgets(array(
                    'illustration_id'           => new sfWidgetFormInputHidden(array(), array()),
                    'oe_number'                 => new sfWidgetFormInput(array(), array('maxlength' => 100, 'class' => 'input')),
                    'bq_number'                 => new sfWidgetFormInput(array(), array('maxlength' => 12, 'class' => 'input')),
                    'cut_hose_at'               => new sfWidgetFormInput(array(), array('maxlength' => 15, 'class' => 'input medium')),
                    'pbr_number'                => new sfWidgetFormInput(array(), array('maxlength' => 10, 'class' => 'input small')),
                    'ol'                        => new sfWidgetFormInput(array(), array('maxlength' => 5, 'class' => 'input small')),
                    'ac_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'ad_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'ae_length'                 => new sfWidgetFormInput(array(), array('maxlength' => 3, 'class' => 'input small')),
                    'hose_storage'              => new sfWidgetFormTextarea(array(), array('class' => 'textarea')),
                    'make_hose_instructions'    => new sfWidgetFormTextarea(array(), array('class' => 'textarea')),
                    'a_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 8, 'class' => 'input medium')),
                    'b_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 8, 'class' => 'input medium')),
                    'c_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 8, 'class' => 'input medium')),
                    'd_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 8, 'class' => 'input medium')),
                    'e_fitting'                 => new sfWidgetFormInput(array(), array('maxlength' => 8, 'class' => 'input medium')),
                    'illustration'              => new sfWidgetFormInputFile(array(), array('size' => '35')),
                    'illustration_number'       => new sfWidgetFormInput(array(), array('class' => 'input small')),
                    
        ));

        $this->setValidators(array(
                    'illustration_id'           => new sfValidatorPass(array(), array()),
                    'oe_number'                 => new sfValidatorString(array(), array('required' => true)),
                    'bq_number'                 => new sfValidatorPass(array(), array()),
                    'pbr_number'                => new sfValidatorPass(array(), array()),
                    'ol'                        => new sfValidatorPass(array(), array()),
                    'cut_hose_at'               => new sfValidatorPass(array(), array()),
                    'ac_length'                 => new sfValidatorPass(array(), array()),
                    'ad_length'                 => new sfValidatorPass(array(), array()),
                    'ae_length'                 => new sfValidatorPass(array(), array()),
                    'hose_storage'              => new sfValidatorPass(array(), array()),
                    'make_hose_instructions'    => new sfValidatorPass(array(), array()),
                    'a_fitting'                 => new sfValidatorPass(array(), array()),
                    'b_fitting'                 => new sfValidatorPass(array(), array()),
                    'c_fitting'                 => new sfValidatorPass(array(), array()),
                    'd_fitting'                 => new sfValidatorPass(array(), array()),
                    'e_fitting'                 => new sfValidatorPass(array(), array()),
                    'illustration'              => new sfValidatorFile(array(
                                                    'required'   => false,
                                                    'path'       => sfConfig::get('app_illustration_dir')
                                                  ), array(
                                                    'mime_types' => 'The file must be a supported type.'
                                                  )),
                    'illustration_number'       => new sfValidatorPass()
        ));

        $this->widgetSchema->setNameFormat('part[%s]');
    }
}
