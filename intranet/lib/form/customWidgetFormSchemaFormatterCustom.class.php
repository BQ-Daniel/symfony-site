<?php
class customWidgetFormSchemaFormatterCustom extends sfWidgetFormSchemaFormatter
{
  protected
    $rowFormat       = "<tr valign=\"top\">
  <th abbr=\"%labelName%\">%label%</th>
  <td>%field%%help%%hidden_fields%</td>
</tr>
",
    $helpFormat      = '<br />%help%',
    $errorRowFormat  = "<tr><td colspan=\"2\">
%errors%</td></tr>
",
    $errorListFormatInARow     = "  <ul class=\"error_list\">
%errors%  </ul>
",
    $errorRowFormatInARow      = "    <li>%error%</li>
",
    $namedErrorRowFormatInARow = "    <li>%error%</li>
",
    $decoratorFormat = "<table>
  %content%</table>";
}
?>
