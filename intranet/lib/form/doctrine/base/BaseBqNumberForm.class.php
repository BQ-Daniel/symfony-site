<?php

/**
 * BqNumber form base class.
 *
 * @method BqNumber getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBqNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'oe_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OeNumber'), 'add_empty' => false)),
      'part'         => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'oe_number_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OeNumber'))),
      'part'         => new sfValidatorString(array('max_length' => 12)),
      'created_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('bq_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BqNumber';
  }

}
