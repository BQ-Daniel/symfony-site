<?php

/**
 * CategoryView form base class.
 *
 * @method CategoryView getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCategoryViewForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'user_logins_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('USER_LOGINS'), 'add_empty' => false)),
      'created_at'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_logins_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('USER_LOGINS'))),
      'created_at'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('category_view[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CategoryView';
  }

}
