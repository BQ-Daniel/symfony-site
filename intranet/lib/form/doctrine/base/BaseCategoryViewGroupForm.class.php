<?php

/**
 * CategoryViewGroup form base class.
 *
 * @method CategoryViewGroup getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCategoryViewGroupForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'category_view_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CategoryView'), 'add_empty' => false)),
      'web_category_type_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategoryType'), 'add_empty' => false)),
      'web_category_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategory'), 'add_empty' => false)),
      'category_string'      => new sfWidgetFormTextarea(),
      'created_at'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'category_view_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CategoryView'))),
      'web_category_type_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategoryType'))),
      'web_category_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('WebCategory'))),
      'category_string'      => new sfValidatorString(),
      'created_at'           => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('category_view_group[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CategoryViewGroup';
  }

}
