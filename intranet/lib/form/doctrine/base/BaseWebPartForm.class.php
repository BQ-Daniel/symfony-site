<?php

/**
 * WebPart form base class.
 *
 * @method WebPart getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWebPartForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'bq_number'              => new sfWidgetFormInputText(),
      'oe_number'              => new sfWidgetFormInputText(),
      'ill_number'             => new sfWidgetFormInputText(),
      'cut_hose_at'            => new sfWidgetFormInputText(),
      'ac_length'              => new sfWidgetFormInputText(),
      'ad_length'              => new sfWidgetFormInputText(),
      'ae_length'              => new sfWidgetFormInputText(),
      'comments'               => new sfWidgetFormInputText(),
      'make_hose_instructions' => new sfWidgetFormTextarea(),
      'a_fitting'              => new sfWidgetFormInputText(),
      'b_fitting'              => new sfWidgetFormInputText(),
      'c_fitting'              => new sfWidgetFormInputText(),
      'd_fitting'              => new sfWidgetFormInputText(),
      'e_fitting'              => new sfWidgetFormInputText(),
      'website_display'        => new sfWidgetFormInputCheckbox(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bq_number'              => new sfValidatorString(array('max_length' => 10)),
      'oe_number'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'ill_number'             => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cut_hose_at'            => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'ac_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ad_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ae_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'comments'               => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'make_hose_instructions' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'a_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'b_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'c_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'd_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'e_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'website_display'        => new sfValidatorBoolean(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('web_part[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WebPart';
  }

}
