<?php

/**
 * USER_VIEWS form base class.
 *
 * @method USER_VIEWS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUSER_VIEWSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'user_logins_id' => new sfWidgetFormInputText(),
      'make_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MAKES'), 'add_empty' => false)),
      'model_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MODELS'), 'add_empty' => false)),
      'sub_model_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SUBMODELS'), 'add_empty' => false)),
      'product_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PRODUCTS'), 'add_empty' => false)),
      'year'           => new sfWidgetFormInputText(),
      'view_date'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_logins_id' => new sfValidatorInteger(),
      'make_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MAKES'), 'required' => false)),
      'model_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MODELS'), 'required' => false)),
      'sub_model_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SUBMODELS'), 'required' => false)),
      'product_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PRODUCTS'), 'required' => false)),
      'year'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'view_date'      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_views[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USER_VIEWS';
  }

}
