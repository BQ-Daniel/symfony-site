<?php

/**
 * ORDER_PRODUCTS form base class.
 *
 * @method ORDER_PRODUCTS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseORDER_PRODUCTSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'part_number' => new sfWidgetFormInputText(),
      'description' => new sfWidgetFormTextarea(),
      'price'       => new sfWidgetFormInputText(),
      'weight'      => new sfWidgetFormInputText(),
      'stock'       => new sfWidgetFormInputText(),
      'cat'         => new sfWidgetFormInputText(),
      'price_group' => new sfWidgetFormInputText(),
      'active'      => new sfWidgetFormInputText(),
      'sort_alpha'  => new sfWidgetFormInputText(),
      'sort_key'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'part_number' => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'description' => new sfValidatorString(array('required' => false)),
      'price'       => new sfValidatorString(array('max_length' => 10)),
      'weight'      => new sfValidatorString(array('max_length' => 10)),
      'stock'       => new sfValidatorString(array('max_length' => 12)),
      'cat'         => new sfValidatorString(array('max_length' => 8)),
      'price_group' => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'active'      => new sfValidatorInteger(array('required' => false)),
      'sort_alpha'  => new sfValidatorString(array('max_length' => 5)),
      'sort_key'    => new sfValidatorString(array('max_length' => 5)),
    ));

    $this->widgetSchema->setNameFormat('order_products[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDER_PRODUCTS';
  }

}
