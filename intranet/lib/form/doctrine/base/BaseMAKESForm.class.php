<?php

/**
 * MAKES form base class.
 *
 * @method MAKES getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMAKESForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'make_id' => new sfWidgetFormInputHidden(),
      'make'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'make_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('make_id')), 'empty_value' => $this->getObject()->get('make_id'), 'required' => false)),
      'make'    => new sfValidatorString(array('max_length' => 100)),
    ));

    $this->widgetSchema->setNameFormat('makes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MAKES';
  }

}
