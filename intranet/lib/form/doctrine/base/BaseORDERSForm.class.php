<?php

/**
 * ORDERS form base class.
 *
 * @method ORDERS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseORDERSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'user_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('USERS'), 'add_empty' => false)),
      'shipping_to' => new sfWidgetFormTextarea(),
      'order_by'    => new sfWidgetFormInputText(),
      'order_no'    => new sfWidgetFormInputText(),
      'ship_via'    => new sfWidgetFormInputText(),
      'cost'        => new sfWidgetFormInputText(),
      'weight'      => new sfWidgetFormInputText(),
      'status'      => new sfWidgetFormInputText(),
      'date_added'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('USERS'))),
      'shipping_to' => new sfValidatorString(),
      'order_by'    => new sfValidatorString(array('max_length' => 100)),
      'order_no'    => new sfValidatorString(array('max_length' => 20)),
      'ship_via'    => new sfValidatorString(array('max_length' => 50)),
      'cost'        => new sfValidatorString(array('max_length' => 10)),
      'weight'      => new sfValidatorString(array('max_length' => 10)),
      'status'      => new sfValidatorString(array('max_length' => 1)),
      'date_added'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('orders[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDERS';
  }

}
