<?php

/**
 * WebCategory form base class.
 *
 * @method WebCategory getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWebCategoryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'parent_web_category_id'       => new sfWidgetFormInputText(),
      'web_category_type_id'         => new sfWidgetFormInputText(),
      'sibling_web_category_type_id' => new sfWidgetFormInputText(),
      'name'                         => new sfWidgetFormInputText(),
      'sort_order'                   => new sfWidgetFormInputText(),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'parent_web_category_id'       => new sfValidatorInteger(array('required' => false)),
      'web_category_type_id'         => new sfValidatorInteger(),
      'sibling_web_category_type_id' => new sfValidatorInteger(array('required' => false)),
      'name'                         => new sfValidatorString(array('max_length' => 100)),
      'sort_order'                   => new sfValidatorInteger(array('required' => false)),
      'created_at'                   => new sfValidatorDateTime(),
      'updated_at'                   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('web_category[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WebCategory';
  }

}
