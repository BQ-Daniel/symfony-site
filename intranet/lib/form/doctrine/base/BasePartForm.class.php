<?php

/**
 * Part form base class.
 *
 * @method Part getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePartForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'illustration_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Illustration'), 'add_empty' => true)),
      'oe_number'              => new sfWidgetFormInputText(),
      'bq_number'              => new sfWidgetFormInputText(),
      'cut_hose_at'            => new sfWidgetFormInputText(),
      'ac_length'              => new sfWidgetFormInputText(),
      'ad_length'              => new sfWidgetFormInputText(),
      'ae_length'              => new sfWidgetFormInputText(),
      'comments'               => new sfWidgetFormInputText(),
      'hose_storage'           => new sfWidgetFormTextarea(),
      'make_hose_instructions' => new sfWidgetFormTextarea(),
      'a_fitting'              => new sfWidgetFormInputText(),
      'b_fitting'              => new sfWidgetFormInputText(),
      'c_fitting'              => new sfWidgetFormInputText(),
      'd_fitting'              => new sfWidgetFormInputText(),
      'e_fitting'              => new sfWidgetFormInputText(),
      'pbr_number'             => new sfWidgetFormInputText(),
      'ol'                     => new sfWidgetFormInputText(),
      'website_display'        => new sfWidgetFormInputCheckbox(),
      'staff_id'               => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'illustration_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Illustration'), 'required' => false)),
      'oe_number'              => new sfValidatorString(array('max_length' => 100)),
      'bq_number'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'cut_hose_at'            => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'ac_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ad_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ae_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'comments'               => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'hose_storage'           => new sfValidatorString(array('max_length' => 10000, 'required' => false)),
      'make_hose_instructions' => new sfValidatorString(array('max_length' => 10000, 'required' => false)),
      'a_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'b_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'c_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'd_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'e_fitting'              => new sfValidatorString(array('max_length' => 12, 'required' => false)),
      'pbr_number'             => new sfValidatorPass(array('required' => false)),
      'ol'                     => new sfValidatorInteger(array('required' => false)),
      'website_display'        => new sfValidatorBoolean(array('required' => false)),
      'staff_id'               => new sfValidatorInteger(),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('part[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Part';
  }

}
