<?php

/**
 * USERS form base class.
 *
 * @method USERS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUSERSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'username'     => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'last_login'   => new sfWidgetFormDate(),
      'created_at'   => new sfWidgetFormDate(),
      'total_logins' => new sfWidgetFormInputText(),
      'order_no_req' => new sfWidgetFormInputText(),
      'distributor'  => new sfWidgetFormInputText(),
      'stop_credit'  => new sfWidgetFormInputText(),
      'deleted'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'username'     => new sfValidatorString(array('max_length' => 10)),
      'email'        => new sfValidatorString(array('max_length' => 100)),
      'last_login'   => new sfValidatorDate(array('required' => false)),
      'created_at'   => new sfValidatorDate(array('required' => false)),
      'total_logins' => new sfValidatorInteger(array('required' => false)),
      'order_no_req' => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'distributor'  => new sfValidatorInteger(),
      'stop_credit'  => new sfValidatorInteger(array('required' => false)),
      'deleted'      => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('users[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USERS';
  }

}
