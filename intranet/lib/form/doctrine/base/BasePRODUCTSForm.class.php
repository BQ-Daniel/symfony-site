<?php

/**
 * PRODUCTS form base class.
 *
 * @method PRODUCTS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePRODUCTSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'product_id'    => new sfWidgetFormInputHidden(),
      'model_id'      => new sfWidgetFormInputText(),
      'bq_number'     => new sfWidgetFormInputText(),
      'part_number'   => new sfWidgetFormInputText(),
      'ill_number'    => new sfWidgetFormInputText(),
      'cut_hose'      => new sfWidgetFormInputText(),
      'ac'            => new sfWidgetFormInputText(),
      'ad'            => new sfWidgetFormInputText(),
      'ae'            => new sfWidgetFormInputText(),
      'comment'       => new sfWidgetFormInputText(),
      'description'   => new sfWidgetFormTextarea(),
      'hose_position' => new sfWidgetFormInputText(),
      'year'          => new sfWidgetFormInputText(),
      'date_added'    => new sfWidgetFormDate(),
    ));

    $this->setValidators(array(
      'product_id'    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('product_id')), 'empty_value' => $this->getObject()->get('product_id'), 'required' => false)),
      'model_id'      => new sfValidatorInteger(),
      'bq_number'     => new sfValidatorString(array('max_length' => 10)),
      'part_number'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'ill_number'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cut_hose'      => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'ac'            => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ad'            => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ae'            => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'comment'       => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'description'   => new sfValidatorString(array('required' => false)),
      'hose_position' => new sfValidatorString(array('max_length' => 20)),
      'year'          => new sfValidatorString(array('max_length' => 50)),
      'date_added'    => new sfValidatorDate(),
    ));

    $this->widgetSchema->setNameFormat('products[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PRODUCTS';
  }

}
