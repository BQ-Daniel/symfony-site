<?php

/**
 * MODELS form base class.
 *
 * @method MODELS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMODELSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'model_id'     => new sfWidgetFormInputHidden(),
      'make_id'      => new sfWidgetFormInputText(),
      'model'        => new sfWidgetFormInputText(),
      'parent_model' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'model_id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('model_id')), 'empty_value' => $this->getObject()->get('model_id'), 'required' => false)),
      'make_id'      => new sfValidatorInteger(),
      'model'        => new sfValidatorString(array('max_length' => 100)),
      'parent_model' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('models[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MODELS';
  }

}
