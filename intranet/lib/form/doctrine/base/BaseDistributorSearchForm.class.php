<?php

/**
 * DistributorSearch form base class.
 *
 * @method DistributorSearch getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseDistributorSearchForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'location'      => new sfWidgetFormInputText(),
      'radius'        => new sfWidgetFormInputText(),
      'total_results' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDate(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'location'      => new sfValidatorString(array('max_length' => 100)),
      'radius'        => new sfValidatorString(array('max_length' => 5)),
      'total_results' => new sfValidatorInteger(),
      'created_at'    => new sfValidatorDate(),
    ));

    $this->widgetSchema->setNameFormat('distributor_search[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'DistributorSearch';
  }

}
