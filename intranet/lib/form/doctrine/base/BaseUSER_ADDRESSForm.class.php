<?php

/**
 * USER_ADDRESS form base class.
 *
 * @method USER_ADDRESS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUSER_ADDRESSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'user_id'      => new sfWidgetFormInputText(),
      'name'         => new sfWidgetFormInputText(),
      'address1'     => new sfWidgetFormTextarea(),
      'address2'     => new sfWidgetFormTextarea(),
      'suburb'       => new sfWidgetFormInputText(),
      'state'        => new sfWidgetFormInputText(),
      'postcode'     => new sfWidgetFormInputText(),
      'phone'        => new sfWidgetFormInputText(),
      'address_type' => new sfWidgetFormInputText(),
      'lat'          => new sfWidgetFormInputText(),
      'lng'          => new sfWidgetFormInputText(),
      //'website'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'      => new sfValidatorInteger(),
      'name'         => new sfValidatorString(array('max_length' => 100)),
      'address1'     => new sfValidatorString(),
      'address2'     => new sfValidatorString(),
      'suburb'       => new sfValidatorString(array('max_length' => 50)),
      'state'        => new sfValidatorString(array('max_length' => 3)),
      'postcode'     => new sfValidatorString(array('max_length' => 5)),
      'phone'        => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'address_type' => new sfValidatorString(array('max_length' => 8)),
      'lat'          => new sfValidatorNumber(),
      'lng'          => new sfValidatorNumber(),
      //'website'     => new sfValidatorString()
    ));

    $this->widgetSchema->setNameFormat('user_address[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'USER_ADDRESS';
  }

}
