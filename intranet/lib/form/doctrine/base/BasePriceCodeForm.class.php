<?php

/**
 * PriceCode form base class.
 *
 * @method PriceCode getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePriceCodeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'price_code'       => new sfWidgetFormInputText(),
      'description'      => new sfWidgetFormInputText(),
      'catalogue_access' => new sfWidgetFormInputCheckbox(),
      'search_access'    => new sfWidgetFormInputCheckbox(),
      'pg_51'            => new sfWidgetFormInputText(),
      'pg_52'            => new sfWidgetFormInputText(),
      'pg_53'            => new sfWidgetFormInputText(),
      'pg_54'            => new sfWidgetFormInputText(),
      'pg_55'            => new sfWidgetFormInputText(),
      'pg_56'            => new sfWidgetFormInputText(),
      'pg_57'            => new sfWidgetFormInputText(),
      'pg_59'            => new sfWidgetFormInputText(),
      'pg_61'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'price_code'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'description'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'catalogue_access' => new sfValidatorBoolean(array('required' => false)),
      'search_access'    => new sfValidatorBoolean(array('required' => false)),
      'pg_51'            => new sfValidatorInteger(array('required' => false)),
      'pg_52'            => new sfValidatorInteger(array('required' => false)),
      'pg_53'            => new sfValidatorInteger(array('required' => false)),
      'pg_54'            => new sfValidatorInteger(array('required' => false)),
      'pg_55'            => new sfValidatorInteger(array('required' => false)),
      'pg_56'            => new sfValidatorInteger(array('required' => false)),
      'pg_57'            => new sfValidatorInteger(array('required' => false)),
      'pg_59'            => new sfValidatorInteger(array('required' => false)),
      'pg_61'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('price_code[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PriceCode';
  }

}
