<?php

/**
 * MarkedForDelete form base class.
 *
 * @method MarkedForDelete getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMarkedForDeleteForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'object_id'   => new sfWidgetFormInputText(),
      'object_type' => new sfWidgetFormChoice(array('choices' => array('category' => 'category', 'part' => 'part', 'illustration' => 'illustration', 'category_type' => 'category_type', 'part_to_category' => 'part_to_category'))),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'object_id'   => new sfValidatorInteger(),
      'object_type' => new sfValidatorChoice(array('choices' => array(0 => 'category', 1 => 'part', 2 => 'illustration', 3 => 'category_type', 4 => 'part_to_category'), 'required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('marked_for_delete[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MarkedForDelete';
  }

}
