<?php

/**
 * Product form base class.
 *
 * @method Product getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseProductForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'bq_number'              => new sfWidgetFormInputText(),
      'oe_number'              => new sfWidgetFormInputText(),
      'ill_number'             => new sfWidgetFormInputText(),
      'cut_hose_at'            => new sfWidgetFormInputText(),
      'ac_length'              => new sfWidgetFormInputText(),
      'ad_length'              => new sfWidgetFormInputText(),
      'ae_length'              => new sfWidgetFormInputText(),
      'comment'                => new sfWidgetFormInputText(),
      'make_hose_instructions' => new sfWidgetFormTextarea(),
      'website_display'        => new sfWidgetFormInputCheckbox(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bq_number'              => new sfValidatorString(array('max_length' => 10)),
      'oe_number'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'ill_number'             => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'cut_hose_at'            => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'ac_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ad_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'ae_length'              => new sfValidatorString(array('max_length' => 3, 'required' => false)),
      'comment'                => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'make_hose_instructions' => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'website_display'        => new sfValidatorBoolean(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('product[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Product';
  }

}
