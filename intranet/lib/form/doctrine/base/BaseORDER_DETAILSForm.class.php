<?php

/**
 * ORDER_DETAILS form base class.
 *
 * @method ORDER_DETAILS getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseORDER_DETAILSForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'order_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ORDERS'), 'add_empty' => false)),
      'order_product_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ORDER_PRODUCTS'), 'add_empty' => false)),
      'qty'              => new sfWidgetFormInputText(),
      'price'            => new sfWidgetFormInputText(),
      'weight'           => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'order_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ORDERS'))),
      'order_product_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ORDER_PRODUCTS'))),
      'qty'              => new sfValidatorInteger(),
      'price'            => new sfValidatorString(array('max_length' => 10)),
      'weight'           => new sfValidatorString(array('max_length' => 10)),
    ));

    $this->widgetSchema->setNameFormat('order_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ORDER_DETAILS';
  }

}
