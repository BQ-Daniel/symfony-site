<?php

/**
 * Staff form base class.
 *
 * @method Staff getObject() Returns the current form's model object
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseStaffForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'name'             => new sfWidgetFormInputText(),
      'email'            => new sfWidgetFormInputText(),
      'username'         => new sfWidgetFormInputText(),
      'website_username' => new sfWidgetFormInputText(),
      'password'         => new sfWidgetFormInputText(),
      'level'            => new sfWidgetFormInputText(),
      'last_login'       => new sfWidgetFormDateTime(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'             => new sfValidatorString(array('max_length' => 150)),
      'email'            => new sfValidatorString(array('max_length' => 150)),
      'username'         => new sfValidatorString(array('max_length' => 50)),
      'website_username' => new sfValidatorString(array('max_length' => 50)),
      'password'         => new sfValidatorString(array('max_length' => 40)),
      'level'            => new sfValidatorInteger(),
      'last_login'       => new sfValidatorDateTime(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('staff[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Staff';
  }

}
