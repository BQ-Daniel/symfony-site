<?php

/**
 * USERS form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class USERSForm extends BaseUSERSForm
{
  public function configure()
  {
      $this->widgetSchema['order_no_req'] = new sfWidgetFormInputCheckbox(array('value_attribute_value' => 'Y'));
      $this->widgetSchema['order_no_req']->setDefault('N');
      $this->widgetSchema['distributor'] = new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1'));
      $this->widgetSchema['deleted'] = new sfWidgetFormInputCheckbox(array('value_attribute_value' => '1'));
      
      $this->validatorSchema['distributor'] = new sfValidatorPass(array(), array());
      $this->validatorSchema['deleted'] = new sfValidatorPass(array(), array());
  }
}
