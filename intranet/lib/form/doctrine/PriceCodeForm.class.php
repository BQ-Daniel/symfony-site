<?php

/**
 * PriceCode form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PriceCodeForm extends BasePriceCodeForm
{
    public function configure()
    {
        parent::configure();

        $this->validatorSchema['price_code']  = new sfValidatorString(array('max_length' => 255, 'required' => true));
        $this->validatorSchema['description'] = new sfValidatorString(array('max_length' => 255, 'required' => true));

        $this->validatorSchema['pg_51']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_52']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_53']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_54']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_55']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_56']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_57']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_59']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));
        $this->validatorSchema['pg_61']  = new sfValidatorInteger(array('min' => 0, 'max' => 99, 'required' => false));

        $this->widgetSchema->setNameFormat('priceCodes[' . $this->options['index'] . '][%s]');
    }

}
