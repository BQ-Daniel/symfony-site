<?php

/**
 * Staff form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class StaffForm extends BaseStaffForm {

    public function configure()
    {
        /**
         * Widgets
         */
        $this->widgetSchema['org_username'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword();
        $this->widgetSchema['confirm_password'] = new sfWidgetFormInputPassword();
        $levels = sfConfig::get('app_admin_levels');
        $this->widgetSchema['level'] = new sfWidgetFormSelect(array('choices' => $levels), array());
        
        unset($this->widgetSchema['created_at']);
        unset($this->widgetSchema['updated_at']);
        unset($this->widgetSchema['last_login']);
        
        /**
         * Validators
         */
        unset($this->validatorSchema['created_at']);
        unset($this->validatorSchema['updated_at']);
        unset($this->validatorSchema['last_login']);
        
        $this->validatorSchema['org_username'] = new sfValidatorPass(array(), array());
        $this->validatorSchema['confirm_password'] = new sfValidatorPass(array(), array());

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                new sfValidatorCallback(array('callback' => array('customFormValidator', 'staffUsernameExists'))),
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password', array(), array('invalid' => "Passwords did not match")))
        ));
    }
   

}
