<?php

/**
 * Part form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PartForm extends BasePartForm {

    public function configure() {
        // Delete fields
        $fields = array('updated_at', 'created_at', 'staff_id');
        
        $this->widgetSchema['org_oe_number'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['org_bq_number'] = new sfWidgetFormInputHidden();

        foreach ($fields as $field) {
            unset($this->validatorSchema[$field]);
            unset($this->widgetSchema[$field]);
        }
        
        $this->widgetSchema['illustration_id'] = new sfWidgetFormInputHidden();
        $this->validatorSchema['illustration_id'] = new sfValidatorPass();
        
        $this->widgetSchema['illustration'] = new sfWidgetFormInputFile(array(), array('size' => '35'));
        $this->validatorSchema['illustration'] = new sfValidatorFile(array(
                                                    'required'   => false,
                                                    'path'       => sfConfig::get('app_illustration_dir')
                                                  ), array(
                                                    'mime_types' => 'The file must be a supported type.'
                                                  ));
        
        $this->widgetSchema['illustration_number'] = new sfWidgetFormInput(array(), array('class' => 'input small'));
        $this->validatorSchema['illustration_number'] = new sfValidatorPass();
       
        $this->validatorSchema['org_oe_number'] = new sfValidatorPass(array(), array());
        $this->validatorSchema['org_bq_number'] = new sfValidatorPass(array(), array());

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                new sfValidatorCallback(array('callback' => array('customFormValidator', 'BqNumberExists'))),
                new sfValidatorCallback(array('callback' => array('customFormValidator', 'OeNumberExists')))
            )
        ));
    }

}
