<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dmFormValidator
 *
 * @author John
 */
class customFormValidator {

    /**
     * Check if an email exists
     * 
     * @param sfValidatorCallback $validatorObject
     * @param string $values
     * @throw sfValidatorError
     * @return array $values
     */
    public static function emailExists($validator, $values) {
        $error = null;
        $email = $values['email'];
        $orgEmail = $values['org_email'];

        if (empty($email) || !sfContext::getInstance()->getRequest()->getParameter('save')) {
            return $values;
        }

        // If they don't match, they have changed the email address, or its a new user
        if ($email != $orgEmail) {
            if (USERSTable::emailExists($email)) {
                $error = "Email address already exists";
            }
        }

        if ($error) {
            // Throw error bound to field
            $error = new sfValidatorError($validator, $error);
            throw new sfValidatorErrorSchema($validator, array('email' => $error));
        }
        return $values;
    }
    
    /**
     * Check if an username already exists
     * 
     * @param sfValidatorCallback $validatorObject
     * @param string $values
     * @throw sfValidatorError
     * @return array $values
     */
    public static function usernameExists($validator, $values) {
        $error = null;
        $username = $values['username'];
        $orgUsername = $values['org_username'];

        if (empty($username) || !sfContext::getInstance()->getRequest()->getParameter('save')) {
            return $values;
        }

        // If they don't match, they have changed the email address, or its a new user
        if ($username != $orgUsername) {
            if (USERSTable::usernameExists($username)) {
                $error = "Username already exists";
            }
        }

        if ($error) {
            // Throw error bound to field
            $error = new sfValidatorError($validator, $error);
            throw new sfValidatorErrorSchema($validator, array('username' => $error));
        }
        return $values;
    }

    
    /**
     * Check if a satff username already exists
     * 
     * @param sfValidatorCallback $validatorObject
     * @param string $values
     * @throw sfValidatorError
     * @return array $values
     */
    public static function staffUsernameExists($validator, $values) 
    {
        $error = null;
        $username = $values['username'];
        $orgUsername = $values['org_username'];

        if (empty($username)) {
            return $values;
        }

        // If they don't match, they have changed the email address, or its a new user
        if ($username != $orgUsername) {
            if (StaffTable::usernameExists($username)) {
                $error = "Username already exists";
            }
        }

        if ($error) {
            // Throw error bound to field
            $error = new sfValidatorError($validator, $error);
            throw new sfValidatorErrorSchema($validator, array('username' => $error));
        }
        return $values;
    }
    
    public static function BqNumberExists($validator, $values) 
    {
        $error = null;
        $bqNumber = $values['bq_number'];
        $orgBqNumber = $values['org_bq_number'];

        if (empty($bqNumber)) {
            return $values;
        }
        
        // If they don't match, they have changed the email address, or its a new user
        if ($bqNumber != $orgBqNumber) {
            if (PartTable::getPartByBqNumber($bqNumber)) {
                $error = "BQ Number already exists";
            }
        }

        if ($error) {
            // Throw error bound to field
            $error = new sfValidatorError($validator, $error);
            throw new sfValidatorErrorSchema($validator, array('bq_number' => $error));
        }
        return $values;
    }
    
    public static function OeNumberExists($validator, $values) 
    {
        $error = null;
        $oeNumber = $values['oe_number'];
        $orgOeNumber = $values['org_oe_number'];

        if (empty($oeNumber)) {
            return $values;
        }
        
        // If they don't match, they have changed the email address, or its a new user
        if ($oeNumber != $orgOeNumber) {
            if (PartTable::getPartByOeNumber($oeNumber)) {
                $error = "OE Number already exists";
            }
        }

        if ($error) {
            // Throw error bound to field
            $error = new sfValidatorError($validator, $error);
            throw new sfValidatorErrorSchema($validator, array('oe_number' => $error));
        }
        return $values;
    }
    
    public static function compareFields($validator, $values, $args) 
    {
    
    }
    
}

?>
