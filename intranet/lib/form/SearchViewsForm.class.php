<?php

/**
 * Part form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class SearchViewsForm extends BaseForm {

    public function configure() {
        
        $dateWidget = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%'), array());
        $config = array('config' => '{buttonImage: "/images/config_date.png", buttonImageOnly: true}', 'date_widget' => $dateWidget);
        
        // Get category types
        $choices = array();
        $results = CategoryTypeTable::getCategoryTypes();
        foreach ($results as $result) {
            $choices[$result->getId()] = $result->getName();
        }
        
        
        $this->setWidgets(array(
                    'username'      => new sfWidgetFormInput(array(), array('maxlength' => 10, 'class' => 'input')),
                    'category_type_id' => new sfWidgetFormSelect(array('choices' => $choices)),
                    'from_date'     => new sfWidgetFormJQueryDate($config, $attributes = array()),
                    'to_date'       => new sfWidgetFormJQueryDate($config, $attributes = array())
        ));

        $this->setValidators(array(
                    'username'      => new sfValidatorPass(array(), array()),
                    'category_type_id' => new sfValidatorString(array('required' => true)),
                    'from_date'     => new sfValidatorPass(array(), array()),
                    'to_date'       => new sfValidatorPass(array(), array()),
        ));

        $this->widgetSchema->setNameFormat('stats[%s]');
    }
}
