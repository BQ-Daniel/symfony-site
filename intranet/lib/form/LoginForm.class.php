<?php

/**
 * Login form.
 *
 * @package    brakequip
 * @subpackage form
 * @author     John Smythe
 */
class LoginForm extends BaseForm {

    public function configure() {

        $this->setWidgets(array(
                    'username'              => new sfWidgetFormInput(array(), array('maxlength' => 150)),
                    'password'              => new sfWidgetFormInputPassword(array(), array()),
        ));


        $this->setValidators(array(
                    'username'             => new sfValidatorString(array('required' => true), array('required' => 'Required', 'invalid' => 'Invalid username')),
                    'password'             => new sfValidatorString(array('required' => true), array('required' => 'Required'))
        ));

        $this->widgetSchema->setNameFormat('login[%s]');
    }
}
