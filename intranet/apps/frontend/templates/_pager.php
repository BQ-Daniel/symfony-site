
<?php
if (!isset($urlFor)) {
    $urlFor = url_for($sf_params->get('module').'/'.$sf_params->get('action'));
}
if (isset($params)) {
    $params = '&'.$params;
}
else {
    $params = null;
}
?>
<table cellspacing="0" cellpadding="0" width="100%">
    <tr valign="middle">
        <td>Total Results: <?php echo $pager->getNbResults() ?></td>
        <td>
            <div class="pager">
        <?php if ($pager->haveToPaginate()): ?>
                <ul>
                    <li class="first"><?php echo link_to('First', $urlFor.'?page='.$pager->getFirstPage().$params) ?></li>
                    <?php foreach ($pager->getLinks() as $page): ?>
                    <li class="no"><?php echo ($page == $pager->getPage()) ? '<span>'.$page.'</span>' : link_to($page, $urlFor.'?page='.$page.$params) ?></li>
                    <?php endforeach ?>
                    <li><?php echo link_to('Last', $urlFor.'?page='.$pager->getLastPage().$params) ?></li>
                </ul>
                <div class="cb"></div>
        <?php endif; ?>    
            </div>
        </td>
    </tr>
</table>