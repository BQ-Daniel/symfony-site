<?php if ($sf_params->get('success')): ?>
<div class="success"><?php echo $sf_params->get('success') ?></div>
<?php endif; ?>

<?php if ($sf_user->getFlash('success')): ?>
<div class="success"><?php echo $sf_user->getFlash('success') ?></div>
<?php endif; ?>

<?php if ($sf_params->get('error')): ?>
<div class="error"><?php echo $sf_params->get('error') ?></div>
<?php endif; ?>

<?php if ($sf_user->getFlash('error')): ?>
<div class="error"><?php echo $sf_user->getFlash('error') ?></div>
<?php endif; ?>
