<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>web</title>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php //include_title() ?>
        <link rel="shortcut icon" href="/favicon.ico" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>
    </head>

    <body> 
        <div id="launch-website-login-as" title="Enter username you would like to login as">
            Account of who you would like to login under<br /><br />
            <input type="text" name="login_as" value="<?php echo $sf_user->getWebsiteUsername() ?>" class="input" /><br /><br />
        </div>
        
        <!-- Header -->
        <div id="header">
            <div id="top-navigation">
                <ul>
                    <li><a href="<?php echo url_for('home/index') ?>"><span>Home</span></a></li>
                    <li><a href="<?php echo url_for('part/edit') ?>" class="no-link"><span>Parts</span></a>
                        <ul>
                            <li><a href="<?php echo url_for('part/edit') ?>"><span>Add</span></a></li>
                            <li><a href="<?php echo url_for('category/index') ?>"><span>Categories</span></a></li>
                            <li><a href="<?php echo url_for('illustration/index') ?>"><span>Illustrations</span></a></li>
                            <li><a href="<?php echo url_for('search/index') ?>"><span>Search</span></a></li>
                        </ul>
                    </li>
                    <li><a href="#" class="no-link"><span>Website</span></a>
                        <ul>
                            <li><a href="launch-online-applications"><span>Launch Online Applications</span></a></li>
                            <li><a href="<?php echo url_for('website/update') ?>"><span>Updates</span></a></li>
                            <li><a href="<?php echo url_for('website/orders') ?>"><span>Orders</span></a></li>
                            <li><a href="<?php echo url_for('website/whosOnline') ?>"><span>Who's Online</span></a></li>
                            <li><a href="<?php echo url_for('users/index') ?>"><span>Users</span></a></li>
                            <li><a href="<?php echo url_for('stats/index') ?>"><span>Statistics</span></a></li>
                            <li><a href="<?php echo url_for('website/googlegeo') ?>"><span>Google Geo</span></a></li>
                            <li><a href="<?php echo url_for('website/catalogueExport') ?>"><span>Catalogue Export</span></a></li>
                            <li><a href="<?php echo url_for('website/priceCode') ?>"><span>Price Codes</span></a></li>
                        </ul>
                    </li>
                    <?php if ($sf_user->getLevel() == 1): ?>
		    <!--<li><a href="<?php echo url_for('admin/index') ?>"><span>Administration</span></a></li>--->
                    <li><a href="#" class="no-link"><span>Settings</span></a>
                        <ul>
                            <li><a href="<?php echo url_for('admin/index') ?>"><span>Admin accounts</span></a></li>
                            <li><a href="<?php echo url_for('admin/general') ?>"><span>General</span></a></li>
                            <li><a href="<?php echo url_for('backup/index') ?>"><span>Backup</span></a></li>
			    <li><a href="<?php echo url_for('backup/snapshot') ?>"><span>Restore points</span></a></li>
                            
                        </ul>
                    </li>
                    <?php endif; ?>
                    <!--<li><a href="<?php echo url_for('backup/index') ?>"><span>Backup</span></a>
                        <ul>
                            <li><a href="<?php echo url_for('backup/snapshot') ?>"><span>Snapshots</span></a></li>
                        </ul>
                    </li>--->
                    <li><a href="#"><span>Reports</span></a>
                        <ul>
                            <li><a href="<?php echo url_for('report/notOnWebsite') ?>"><span>Not On Website</span></a></li>
                        </ul>
                    </li>
                </ul>
                <?php include_component('home', 'updates') ?>
            </div>
            <div id="toolbar">
                <ul>
                    <li><a href="<?php echo url_for('part/edit') ?>"><?php echo image_tag('toolbar_addpart.jpg') ?></a></li>
                    <li><a href="<?php echo url_for('search/index') ?>"><?php echo image_tag('toolbar_search.jpg') ?></a></li>
                    <li><a href="<?php echo url_for('website/orders') ?>"><?php echo image_tag('toolbar_orders.jpg') ?></a></li>
                </ul>
                <div class="quick-search">
                    <form action="<?php echo url_for('search/quick') ?>" method="post">
                    <input type="text" name="quick_search_keyword" value="" class="input search-input" /><input type="submit" name="quick_search" value="Search" />
                    </form>
                </div>
               
            </div>
            <div class="title">
                <h1><?php include_slot('title') ?></h1>
            </div>
        </div>
        <!-- //Header -->

        <!-- Content -->
        <div id="content-container">
            <div id="content">
                <?php echo $sf_content ?>
            </div>
        </div>
        <!-- //Content -->

    </body>
</html>

<script>

    $('#content-container').height($(window).height() - $('#header').height());
</script>
