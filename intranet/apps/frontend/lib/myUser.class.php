<?php

class myUser extends sfBasicSecurityUser
{
    
    public function getStaffId()
    {
        return $this->getAttribute('staffId');
    }
    
    public function getUsername()
    {
        return $this->getAttribute('username');
    }
    
    public function getWebsiteUsername()
    {
        return $this->getAttribute('websiteUsername');
    }
    
    public function getLevel()
    {
        return $this->getAttribute('level', 0);
    }
    
    public function setParameter($name, $value)
    {
        $this->setAttribute($name, $value);
    }
    
    public function getParameter($name, $value = null)
    {
        return $this->getAttribute($name, $value);
    }
}
