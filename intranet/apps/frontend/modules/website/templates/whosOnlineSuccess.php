<?php slot('title', 'Website') ?>

<h2>Who's Online</h2>

    <p>The time now is: <b><?php echo date("G:i:s A") ?></b></p>
    <div style="width: 500px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
<div class="list-table">
    
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th width="150"><?php echo sortable_link('Username', url_for('website/whosOnline'), 'username') ?></th>
            <th width="100"><?php echo sortable_link('Last Active', url_for('website/whosOnline'), 'last_active') ?></th>
        </tr>
        <?php if ($totalResults > 0): ?>
            <?php foreach ($pager->getResults() as $result): ?>
        <tr>
            <td><a href=""><?php echo $result->getUSERS()->getUsername() ?></a></td>
            <td><?php echo date("G:i:s A", strtotime($result->getLastActive())) ?></td>
        </tr>
            <?php endforeach; ?>
        <?php else: ?>
        <tr>
            <td colspan="2">No one is currently online on the website</td>
        </tr>
        <?php endif; ?>
    </table>
</div>
        <div style="width: 500px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>