
<div style="width: 600px;">
<div class="fl">
    <h2>Order Details </h2>
    <h3>Order Number: <?php echo $order->getOrderNo() ?></h3>
</div>
<?php if ($search): ?>
<div class="fr">
    <a href="back-to-results" onclick="backToResults(); return false;" class="button"><span>Back to Results</span></a>
</div>
<?php endif; ?>
</div>

<div class="cb" style="height: 20px;"></div>

<div class="info-table" style="width: 600px">
    <table cellspacing="0" cellpadding="0" border="0" width="600">
        <tr>
            <th width="100">Username</th>
            <td><?php echo $order->getUSERS()->getUsername() ?></td>
            <th>Order Number</th>
            <td><?php echo $order->getOrderNo() ?></td>
        </tr>
        <tr>
            <th>Date Submitted</th>
            <td><?php echo date('D jS M y, g:ia', strtotime($order->getDateAdded())) ?></td>
            <th>Ordered By</th>
            <td><?php echo $order->getOrderBy() ?></td>
        </tr>
        <tr>
            <th>Ship Via</th>
            <td><?php echo ucfirst(str_replace('_', ' ', $order->getShipVia())) ?></td>
            <th>Status</th>
            <td><?php echo $order->getStatus() == "S" ? '<font color="red"><b>Submitted</b></font>' : '<font color="green"><b>Processed</b></font>' ?></td>
        </tr>
        <tr>
            <th colspan="4">Shipping To</th>
        </tr>
        <tr>
            <td colspan="4"><?php echo nl2br($order->getShippingTo()) ?></td>
        </tr>
    </table>
</div>

<div style="height: 20px;"></div>

    <div style="width: 600px; margin-top: 20px">
    <h2 class="fr" style="font-size: 16px;">Total Price: $<?php echo number_format($order->getCost(), 2); ?></h2>
    <br class="cb" />
    <h2 class="fr" style="font-size: 16px;">Total Weight: <?php echo $order->getWeight() ?>kg</h2>
    </div>
<div class="cb"></div>

<div class="list-table">
    <table cellspacing="0" cellpadding="0" border="0" width="600">
        <tr>
            <th width="200">Part Number</th>
            <th>Weight</th>
            <th>Qty</th>
            <th>Unit Price</th>
            <th>Total</th>
        </tr>
        <?php foreach ($order->getORDER_DETAILS() as $detail): ?>
        <?php $product = $detail->getORDER_PRODUCTS()  ?>
        <tr>
            <td><?php echo $product->getPartNumber() ?></td>
            <td><?php echo $detail->getWeight() ?></td>
            <td><?php echo $detail->getQty() ?></td>
            <td>$<?php echo $detail->getPrice() ?></td>
            <td>$<?php echo number_format($detail->getQty() * $detail->getPrice(), 2) ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
    <div style="width: 600px; margin-top: 20px">
    <h2 class="fr" style="font-size: 16px;">Total Price: $<?php echo number_format($order->getCost(), 2); ?></h2>
    <br class="cb" />
    <h2 class="fr" style="font-size: 16px;">Total Weight: <?php echo $order->getWeight() ?>kg</h2>
    </div>
