<?php slot('title', 'Website') ?>

<h2>Updates</h2>

<p>Website was last updated: <b><?php echo $lastUpdated ?></b></p>

<div style="width: 600px;"><a href="/website/doUpdate" class="button fr"><span>Update Website</span></a></div>

<div class="cb"></div>

<div style="width: 600px;">
    <?php include_partial('global/pager', array('pager' => $pager)); ?>
</div>
<div class="list-table">
    <table cellspacing="0" cellpadding="0" border="0" width="600">
        <tr>
            <th width="120"><?php echo sortable_link('Started At', url_for('website/update'), 'created_at') ?></th>
            <th width="120"><?php echo sortable_link('Finished At', url_for('website/update'), 'finished_at') ?></th>
            <th width="150"><?php echo sortable_link('Requested By', url_for('website/update'), 'staff_id') ?></th>
            <th width="100">Action</th>
        </tr>
        <?php if ($totalResults > 0): ?>
        <?php foreach ($pager->getResults() as $result): ?>
                <tr>
                    <td><?php echo date("d/m/y h:i:s A", strtotime($result->getCreatedAt())) ?></td>
                    <td><?php echo ($result->getFinishedAt() != '' ? date("d/m/y h:i:s A", strtotime($result->getFinishedAt())) : 'Not finished yet') ?></td>
                    <td><?php echo ($result->getStaff()->getName() != '' ? $result->getStaff()->getName() : 'System') ?></td>
                    <td><a href="<?php echo url_for('website/viewUpdateLog?id='.$result->getId()) ?>" class="button fr"><span>View Log</span></a></td>
                </tr>
        <?php endforeach; ?>
        <?php else: ?>
                <tr>
                    <td colspan="4">No updates have been made</td>
                </tr>
        <?php endif; ?>
            </table>
        </div>
        <div style="width: 600px;">
    <?php include_partial('global/pager', array('pager' => $pager)); ?>
</div>

