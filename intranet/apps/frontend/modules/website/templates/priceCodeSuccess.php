<style>
    .price-code-row,
    .price-code-button-line {
        clear: left;
        width: 70%;
        border-bottom: 1px #D1D1D1 solid;
        margin-bottom: 30px;
    }

    .price-code-button-line {
        border: none;
    }

    .price-code-button-line button,
    .price-code-remove {
        background-color: #3FA9F5;
        color: #FFFFFF;
        line-height: 30px;
        font-size: 20px;
        padding: 10px 10px;
        border-radius: .50rem;
        border: 2px #0071bc solid;
    }

    .price-code-remove {
        background-color: #F53A2D;
        border-color: #C73226;
        margin-top: 27px;
    }

    .price-code-row label {
        display: inline-block;
        width: 100%;
        font-size: 18px;
    }

    .price-code-row input {
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .50rem;
        height: 50px;
        line-height: 18px;
        font-size: 20px;
        padding: 10px 10px;
        margin: 10px 0 0;
    }

    .price-code-code input,
    .price-code-group input {
        width: 60px;
    }

    .price-code-first,
    .price-code-second {
        clear: left;
        width: 100%;
        margin-bottom: 20px;
    }

    .price-code-code,
    .price-code-group {
        float: left;
        width: 10%;
    }

    .price-code-description {
        float: left;
        width: 30%
    }

    .price-code-catalogue,
    .price-code-search {
        float:left;
        width: 20%;
        text-align: center;
    }

    .clear {
        clear: left;
    }


</style>

<div style="display: none" id="price-code-prototype">
    <div class="price-code-row" data-role="price-code-box">
        <div class="price-code-first">
            <div class="price-code-code">
                <label for="priceCodes___name___price_code">Price code</label>
                <input type="text" name="priceCodes[__name__][price_code]" id="priceCodes___name___price_code">
            </div>

            <div class="price-code-description">
                <label for="priceCodes___name___description">Description</label>
                <input type="text" name="priceCodes[__name__][description]" id="priceCodes___name___description">
            </div>

            <div class="price-code-catalogue">
                <label for="priceCodes___name___catalogue_access">Order access</label>
                <input type="checkbox" name="priceCodes[__name__][catalogue_access]" checked="checked" id="priceCodes___name___catalogue_access">
            </div>

            <div class="price-code-search">
                <label for="priceCodes___name___search_access">Search access</label>
                <input type="checkbox" name="priceCodes[__name__][search_access]" checked="checked" id="priceCodes___name___search_access">
            </div>

            <div class="clear"></div>
        </div>

        <div class="price-code-second">
            <div class="price-code-group">
                <label for="priceCodes___name___pg_51">Pg 51</label>
                <input type="text" name="priceCodes[__name__][pg_51]" id="priceCodes___name___pg_51">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_52">Pg 52</label>
                <input type="text" name="priceCodes[__name__][pg_52]" id="priceCodes___name___pg_52">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_53">Pg 53</label>
                <input type="text" name="priceCodes[__name__][pg_53]" id="priceCodes___name___pg_53">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_54">Pg 54</label>
                <input type="text" name="priceCodes[__name__][pg_54]" id="priceCodes___name___pg_54">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_55">Pg 55</label>
                <input type="text" name="priceCodes[__name__][pg_55]" id="priceCodes___name___pg_55">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_56">Pg 56</label>
                <input type="text" name="priceCodes[__name__][pg_56]" id="priceCodes___name___pg_56">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_57">Pg 57</label>
                <input type="text" name="priceCodes[__name__][pg_57]" id="priceCodes___name___pg_57">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_59">Pg 59</label>
                <input type="text" name="priceCodes[__name__][pg_59]" id="priceCodes___name___pg_59">
            </div>
            <div class="price-code-group">
                <label for="priceCodes___name___pg_61">Pg 61</label>
                <input type="text" name="priceCodes[__name__][pg_61]" id="priceCodes___name___pg_61">
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <input type="hidden" name="priceCodes[__name__][id]" id="priceCodes___name___id">
    </div>
</div>

<form method="POST">
    <div id="price-code-holder">
        <?php foreach ($forms as $priceCodeForm) { ?>
            <div class="price-code-row" data-role="price-code-box">
                <div class="price-code-first">
                    <div class="price-code-code">
                        <?php echo $priceCodeForm['price_code']->renderLabel() ?>
                        <?php echo $priceCodeForm['price_code']->renderError() ?>
                        <?php echo $priceCodeForm['price_code']->render() ?>
                    </div>

                    <div class="price-code-description">
                        <?php echo $priceCodeForm['description']->renderLabel() ?>
                        <?php echo $priceCodeForm['description']->renderError() ?>
                        <?php echo $priceCodeForm['description']->render() ?>
                    </div>

                    <div class="price-code-catalogue">
                      <label for="priceCodes___name___catalogue_access">Order access</label>
                        <?php echo $priceCodeForm['catalogue_access']->render() ?>
                    </div>

                    <div class="price-code-search">
                        <?php echo $priceCodeForm['search_access']->renderLabel() ?>
                        <?php echo $priceCodeForm['search_access']->render() ?>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="price-code-second">
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_51']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_51']->renderError() ?>
                        <?php echo $priceCodeForm['pg_51']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_52']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_52']->renderError() ?>
                        <?php echo $priceCodeForm['pg_52']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_53']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_53']->renderError() ?>
                        <?php echo $priceCodeForm['pg_53']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_54']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_54']->renderError() ?>
                        <?php echo $priceCodeForm['pg_54']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_55']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_55']->renderError() ?>
                        <?php echo $priceCodeForm['pg_55']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_56']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_56']->renderError() ?>
                        <?php echo $priceCodeForm['pg_56']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_57']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_57']->renderError() ?>
                        <?php echo $priceCodeForm['pg_57']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_59']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_59']->renderError() ?>
                        <?php echo $priceCodeForm['pg_59']->render() ?>
                    </div>
                    <div class="price-code-group">
                        <?php echo $priceCodeForm['pg_61']->renderLabel() ?>
                        <?php echo $priceCodeForm['pg_61']->renderError() ?>
                        <?php echo $priceCodeForm['pg_61']->render() ?>
                    </div>

                    <div>
                        <button data-role="price-code-remove" class="price-code-remove" type="submit">Delete</button>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="clear"></div>

                <?php echo $priceCodeForm->renderHiddenFields() ?>
            </div>
        <?php } ?>
    </div>

    <div class="price-code-button-line">
        <button id="price-code-add-new" type="submit">Add new</button>
    </div>

    <div class="price-code-button-line">
        <button type="submit">Save</button>
    </div>

    <input type="hidden" value="<?php echo $index ?>" name="index" id="price-code-index">
</form>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var $priceCodeHolder = jQuery('#price-code-holder');
        var $index           = jQuery('#price-code-index');
        var index            = <?php echo $index ?>;

        jQuery("#price-code-add-new").click(function (e) {
            e.preventDefault();

            var prototype = $('#price-code-prototype');

            var newForm = prototype;
            newForm = newForm.html().replace(/__name__/g, index);

            // increase the index with one for the next item
            index = index + 1;

            // Display the form in the page in an li, before the "Add a tag" link li
            $priceCodeHolder.append(newForm);

            $index.val(index);
        });

        jQuery('[data-role=price-code-remove]').live('click', function (e) {
            e.preventDefault();

            $(this).closest('[data-role=price-code-box]').remove();
        });
    });
</script>
