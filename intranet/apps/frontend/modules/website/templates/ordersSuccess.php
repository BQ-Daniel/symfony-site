<?php slot('title', 'Website') ?>

<h2>Orders</h2>

<div id="search-form" class="filter" style="width: 700px;">
    <form action="<?php echo url_for('website/orders') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="700">
            <tr>
                <th width="70">Username</th>
                <td><?php echo $form['username']->render() ?></td>
                <th width="80" style="padding-left: 20px">Order Number</th>
                <td><?php echo $form['order_no']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th width="70">From Date</th>
                <td width="180"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="70" style="padding-left: 20px">To Date</th>
                <td width="180"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('Order Number', url_for('website/orders'), 'order_no') ?></th>
                <th width="100"><?php echo sortable_link('Username', url_for('website/orders'), 'username') ?></th>
                <th><?php echo sortable_link('Order By', url_for('website/orders'), 'order_by') ?></th>
                <th><?php echo sortable_link('Date', url_for('website/orders'), 'date_added') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $order): ?>
            <tr>
                <td><a href="view-order" onclick="viewOrderDetails('<?php echo $order->getId() ?>'); return false;"><?php echo $order->getOrderNo() ?></a></td>
                <td><a href="<?php echo url_for('users/edit?id='.$order->getUSERS()->getId()) ?>"><?php echo $order->getUSERS()->getUsername() ?></a></td>
                <td><?php echo $order->getOrderBy() ?></td>
                <td><?php echo date('D jS M y, g:ia', strtotime($order->getDateAdded())) ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
</div>
<div id="order-details">

</div>

<?php endif; ?>