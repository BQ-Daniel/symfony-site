<?php slot('title', 'Catalogue Export') ?>

<a href="?download=true">Generate & Download CSV</a>

<?php
/*
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <th>OE Number</th>
                <th width="100">BQ Number</th>
                <th>ILL Number</th>
                <th>Categories</th>
            </tr>
            
            <?php foreach ($sf_data->getRaw('results') as $result): ?>
            <tr>
                <td><?php echo $result['Part']['oe_number'] ?></td>
                <td><?php echo $result['Part']['bq_number'] ?></td>
                <td><?php echo $result['Part']['Illustration']['illustration_number'] ?></td>
                <td><?php 
                $data = json_decode($result['Category']['parents'], true);
                $categories = array();
                foreach ($data as $k => $v) {
                    $categories[] = $v['name'];
                }
                echo implode(' > ', $categories);
                ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
 * 
 */
?>