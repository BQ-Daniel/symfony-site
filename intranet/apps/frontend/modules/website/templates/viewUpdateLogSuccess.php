<?php slot('title', 'Website') ?>

<h2>View Log</h2>

<p><a href="javascript:history.go(-1);" class="button"><span>Go back</span></a></p>

<div class="cb" style="height: 10px;"></div>

Started At: <b><?php echo date("d/m/y h:i:s A", strtotime($webUpdate->getCreatedAt())) ?></b><br />
Finished At: <b><?php echo date("d/m/y h:i:s A", strtotime($webUpdate->getFinishedAt())) ?></b><br />
Requested By: <b><?php echo $webUpdate->getStaff()->getName() ?></b><br />

<div class="cb" style="height: 10px;"></div>

<div style="width: 800px; height: 500px; overflow: scroll; padding: 5px; border: 1px solid #ccc; line-height: 16px">
    <?php echo nl2br($webUpdate->getLog()) ?>
</div>



