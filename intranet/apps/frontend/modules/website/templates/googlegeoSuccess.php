<?php slot('title', 'Website') ?>

<h2>Google Geo Log</h2>

<div id="search-form" class="filter" style="width: 700px;">
    <form action="<?php echo url_for('website/googlegeo') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="700">
            <tr>
                <th width="70">Username</th>
                <td><?php echo $form['username']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th width="70">From Date</th>
                <td width="180"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="70" style="padding-left: 20px">To Date</th>
                <td width="180"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="850">
            <tr>
                <th width="100"><?php echo sortable_link('Username', url_for('website/googlegeo'), 'username') ?></th>
                <th><?php echo sortable_link('Success', url_for('website/googlegeo'), 'success') ?></th>
                <th>Address</th>
                <th>Status</th>
                <th><?php echo sortable_link('Date', url_for('website/googlegeo'), 'g.created_at') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('users/edit?id='.$result->USER_ADDRESS->USERS->id) ?>"><?php echo $result->USER_ADDRESS->USERS->username ?></a></td>
                <td><?php echo $result->success ? '<strong style="color:green">YES</strong>' : '<strong style="color:red;">NO</strong>' ?></td>
                <?php 
                    $address = $result->USER_ADDRESS->address1.' '.$result->USER_ADDRESS->address2;
                    $address .= ucwords($result->USER_ADDRESS->suburb.' '.$result->USER_ADDRESS->state.' '.$result->USER_ADDRESS->postcode);
                ?>
                <td><?php echo $result->address ? $result->address : $address ?></td>
                <td>
                    <?php 

                    echo $result->getJson()->status;
           
                    
                    ?>
                </td>
                <td><?php echo date('D jS M y', strtotime($result->created_at)) ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
</div>
<div id="order-details">

</div>

<?php endif; ?>