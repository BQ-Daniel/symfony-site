<?php

/**
 * View update log
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class viewUpdateLogAction extends sfAction {

    public function execute($request) 
    {
        $id = false;
        if ($request->getParameter('id', false)) {
            $id = $request->getParameter('id');
        }
        
        $q = Doctrine_Query::create()->from('WebUpdate w')->leftJoin('w.Staff s')->where('id = ?', $id);
        if (!$id || !$this->webUpdate = $q->fetchOne()) {
            $this->redirect('website/update');
            exit;
        }
        
        
    }

}