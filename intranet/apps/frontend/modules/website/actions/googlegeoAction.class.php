<?php

/**
 * Google geo
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class googlegeoAction extends sfAction {
    
    public function execute($request)
    {
        $this->form = $form = new SearchOrdersForm();
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'g.created_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        if (empty($sort)) {
            $sort = 'g.created_at';
            $dir = 'desc';
        }
        
        $parameters = $request->getParameter($this->form->getName());

        if ($request->getParameter('page', null) || $request->getParameter('sort', null) || $request->getParameter('return', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }

            $request->setParameter('submit', true);
            if ($request->getParameter('return', null)) {
                $keys = array('sort', 'page', 'dir');
                foreach ($keys as $key) {
                    $$key = $parameters[$key];
                    unset($parameters[$key]);
                }
            }
        }
        $this->form->bind($parameters);
        
        if ($request->getParameter('submit') && $this->form->isValid() || $request->getParameter('page'))
        {   
            $values = $this->form->getValues();
            $q = Doctrine_Query::create()->from('GoogleGeoLog g')
                    ->innerJoin('g.USER_ADDRESS ua')
                    ->innerJoin('ua.USERS u');
            
            // Where clause
            $fromDate = $values['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $q->addWhere('g.created_at >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $toDate = $values['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $q->addWhere("g.created_at <= DATE_ADD('".$toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']."', INTERVAL 1 DAY)");
            }
            
            if (!empty($values['username'])) {
                $q->addWhere('u.username = ?', $values['username']);
            }
            
            // Add order by            
            $q->addOrderBy($sort.' '.$dir);

            $this->pager = new sfDoctrinePager('GoogleGeoLog', sfConfig::get('app_results_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($page);
            $this->pager->init();
            
            // Set in session to store on next page
            $parameters['page'] = $page;
            $parameters['sort'] = $sort;
            $parameters['dir'] = $dir;
            $this->sort = $sort;
            $this->dir = $dir;
            $this->getUser()->setParameter('search_parameters', serialize($parameters));
            
            $this->totalResults = $this->pager->getNbResults();
            
            if ($this->totalResults == 0) {
                $this->getUser()->setParameter('error', 'No results found');
            }
            
        }
            
    }
}