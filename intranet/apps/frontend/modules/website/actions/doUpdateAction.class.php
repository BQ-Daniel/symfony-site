<?php

/**
 * do website update
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class doUpdateAction extends sfAction {

    public function execute($request) 
    {
        
        echo "<h2>DO NOT CLOSE WINDOW UNTIL FINISHED</h2>";
        
// Remember the current dir and change it to sf root
$currentDir = getcwd();
chdir(sfConfig::get('sf_root_dir'));

// Instantiate the task and run it
$task = new updateWebsiteTask($this->dispatcher, new sfFormatter());
$rc = $task->run(array(), array('staffId' => $this->getUser()->getStaffId()));
exit;
        
        $phpPath = exec("which php");
        exec('php ../symfony update:website --staffId='.$this->getUser()->getStaffId().' > log.txt &');
        return sfView::NONE;
    }

}