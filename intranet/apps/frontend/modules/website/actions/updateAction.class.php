<?php

/**
 * launch website
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class updateAction extends sfAction {

    public function execute($request) 
    {
        $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'created_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction



        $q = Doctrine_Query::create()->from('WebUpdate w')->leftJoin('w.Staff s');

        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);

        $this->pager = new sfDoctrinePager('WebUpdate', sfConfig::get('app_results_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($page);
        $this->pager->init();

        $this->totalResults = $this->pager->getNbResults();
        
        $this->lastUpdated = 'Never';
        if ($setting = SettingTable::getInstance()->findOneByName('website_updated')) {
            $this->lastUpdated = date('d/m/y h:i:s A', strtotime($setting->getValue()));
        }
    }

}