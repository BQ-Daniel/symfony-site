<?php

/**
 * launch website
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class launchWebsiteAction extends sfAction {

    public function execute($request) 
    {
        // Check if user exists
        if (!$user = USERSTable::usernameExists($request->getParameter('username'))) {
            return $this->printJSON('error', "That username doesn't exist");
        }
        
        // Insert password into database
	srand(time()*999999999*time());
	$newPassword = md5(time().rand(0,999999999));
        
        try {
            Doctrine_Query::create()->from('USER_PASSWORDS')->where('user_id = ?', $user->getId())->delete()->execute();
            
            
            $qm = Doctrine_Manager::getInstance()->getCurrentConnection();
            $qm->exec("INSERT INTO USER_PASSWORDS SET USER_ID = '".$user->getId()."', PASS = ENCODE('".$newPassword."', '".sfConfig::get('app_website_encrypt_pass')."'), DATE_ADDED = NOW()");
        }
        catch (Exception $e) {
            return $this->printJSON('error', "Unable to log you in at this time");
        }
        
        return $this->printJSON('success', $newPassword); 
    }
    
    private function printJSON($messageType, $msg)
    {
        print '{"'.$messageType.'": "'.$msg.'"}';
        return sfView::NONE;
    }

}