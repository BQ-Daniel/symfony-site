<?php

/**
 * orders
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class ordersAction extends sfAction {
    
    public function execute($request)
    {
        $this->form = $form = new SearchOrdersForm();
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'date_added'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        $parameters = $request->getParameter($this->form->getName());
        if ($request->getParameter('page', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }
        }
        $this->form->bind($parameters);
        
        if ($request->getParameter('submit') && $this->form->isValid() || $request->getParameter('page'))
        {   
            $values = $this->form->getValues();
            $q = Doctrine_Query::create()->from('ORDERS o')->leftJoin('o.USERS u');
           
            // Where clause
            $fromDate = $values['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $q->addWhere('date_added >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $toDate = $values['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $q->addWhere("date_added <= DATE_ADD('".$toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']."', INTERVAL 1 DAY)");
            }
            
            if (!empty($values['username'])) {
                $q->addWhere('username = ?', $values['username']);
            }
            if (!empty($values['order_no'])) {
                $q->addWhere('order_no = ?', $values['order_no']);
            }
            
            // Add order by            
            $q->addOrderBy($sort.' '.$dir);

            $this->pager = new sfDoctrinePager('ORDERS', sfConfig::get('app_results_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($page);
            $this->pager->init();
            
            // Set in session to store on next page
            $this->getUser()->setParameter('search_parameters', serialize($parameters), false);
            
            $this->totalResults = $this->pager->getNbResults();
            
            if ($this->totalResults == 0) {
                $this->getUser()->setParameter('error', 'No results found');
            }
            
        }
            
    }
}