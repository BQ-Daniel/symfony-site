<?php

/**
 * whos online
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class whosOnlineAction extends sfAction {

    public function execute($request) 
    {
        $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'last_active'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction



        $q = Doctrine_Query::create()->from('USER_SESSIONS us')->innerJoin('us.USERS u')
                        ->where("NOW() < ADDTIME(last_active, '00:05:00')")->groupBy('us.USER_ID');

        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);

        $this->pager = new sfDoctrinePager('USER_SESSIONS', sfConfig::get('app_results_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($page);
        $this->pager->init();

        $this->totalResults = $this->pager->getNbResults();
    }

}