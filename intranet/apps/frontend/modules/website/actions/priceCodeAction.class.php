<?php

class priceCodeAction extends sfActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function execute($request)
    {
        $forms = array();

        $priceCodes = PriceCodeTable::getInstance()->findAll();
        $dbCount    = count($priceCodes);

        foreach ($priceCodes as $i => $priceCode) {
            $forms[$i] = new PriceCodeForm($priceCode, array(
                'index' => $i
            ));
        }

        $index = (int) $request->getParameter('index', $dbCount);

        if ($request->isMethod('post')) {

            $priceCodes = $request->getParameter('priceCodes');
            $isValid    = true;

            for ($i = 0; $i < $index; $i++) {
                if (isset($priceCodes[$i])) {
                    if ($i >= $dbCount) {
                        $form = new PriceCodeForm(null, array(
                            'index' => $i
                        ));
                    } else {
                        $form = $forms[$i];
                    }

                    $form->bind($priceCodes[$i]);

                    if (!$form->isValid()) {
                        $isValid = false;
                    }

                    $forms[$i] = $form;
                }
            }

            if ($isValid) {
                for ($i = 0; $i < $index; $i++) {
                    /** @var PriceCodeForm $form */
                    $form = $forms[$i];

                    if (isset($priceCodes[$i])) {
                        $form->save();
                    } else {
                        /** @var PriceCode */
                        $priceCode = $form->getObject();
                        $priceCode->delete();
                    }
                }

                $this->redirect('@default?module=website&action=priceCode');
            }
        }

        $this->forms = $forms;
        $this->index = $index;
    }
}
