<?php

/**
 * do website update
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class catalogueExportAction extends sfAction {

    public function execute($request) 
    {   
        $q = Doctrine_Query::create()
                ->select('ptc.*, p.*, c.*, i.*')
                ->from('Part p')
                ->innerJoin('p.PartToCategory ptc')
                ->innerJoin('ptc.Category c')
                ->innerJoin('p.Illustration i');
               
          

        if (isset($_GET['download'])) {
            ini_set('memory_limit', '1024M');
	    // disable caching
            $now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");
	    header('Content-type: application/csv');
	    header("Content-Disposition: attachment;filename=catalogue".date("Y-m-d").".csv");
	    header("Content-Transfer-Encoding: binary");
            $output = fopen("php://output",'w') or die("Can't open php://output");
            
            $data = array(
                'OE Number',
                'BQ Number',
                'ILL Number',  
            );
            fputcsv($output, $data);
            
            // disable caching
            /*$now = gmdate("D, d M Y H:i:s");
            header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
            header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
            header("Last-Modified: {$now} GMT");

            // force download  
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");

            // disposition / encoding on response body
            header("Content-Disposition: attachment;filename=catalogue".date("Y-m-d").".csv");
            header("Content-Transfer-Encoding: binary");*/
            
            //$q->limit(1000);
            
            $results = $q->fetchArray();
            //print_r($results);die;
            
                foreach ($results as $result) {
                    $i++;
                    
                    if (count($result['PartToCategory']) > 0)
                    {
                        foreach ($result['PartToCategory'] as $ptc) 
                        { unset($data);
                            // get categories
                            $data = json_decode($ptc['Category']['parents'], true);
                            $categories = array();
                            foreach ($data as $k => $v) {
                                $categories[] = $v['name'];
                            }

                            $data = array(
                                $result['oe_number'],
                                $result['bq_number'],
                                $result['Illustration']['illustration_number'] ,
                            );
                            $data = array_merge($data, $categories);
                            fputcsv($output, $data);
                        }
                    }
                    else {
                        $data = array(
                            $result['oe_number'],
                            $result['bq_number'],
                            $result['Illustration']['illustration_number'],
                            'Not assigned to any categories',
                        );
                        fputcsv($output, $data);
                    }
                    

                    if (($i % 1000) === 0) {
                        $q->offset($i);
                        $results = $q->fetchArray();
                        break;
                    }
                }  
            
           
            exit;
        }

        //$this->results = $q->fetchArray();
        
        
        return sfView::SUCCESS;
    }

}