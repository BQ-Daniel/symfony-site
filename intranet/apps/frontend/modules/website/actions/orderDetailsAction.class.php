<?php

/**
 * order details (AJAX REQEUST)
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class orderDetailsAction extends sfAction {
    
    public function execute($request)
    {
        $this->search = true;
        $id = $request->getParameter('id');
        $this->order = Doctrine_Query::create()->from('ORDERS o')->leftJoin('o.USERS u')
                ->innerJoin('o.ORDER_DETAILS od')->innerJoin('od.ORDER_PRODUCTS op')
                ->where('id = ?', $id)->fetchOne();
        
        return $this->renderPartial('website/orderDetails');
    }
}