<?php

/**
 * edit user
 * 
 * @package    brakequip
 * @subpackage users
 * @author     John Smythe
 */
class editAction extends sfAction {
    
    public function execute($request)
    {   
        // If user id exists, we are editing user
        if ($this->staffId = $request->getParameter('id')) {
            $this->staff = StaffTable::getInstance()->find($this->staffId);
        }
       
        
        $this->form = new StaffForm();
        
        // Set defaults
        if ($this->staff) {
            $this->form = new StaffForm($this->staff);
            $this->form->getWidget('org_username')->setAttribute('value', $this->staff->getUsername());
            
            // Unset fields we don't need
            unset($this->form['password']);
        }
        
        // If saving staff details
        if ($request->getParameter('save'))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            
            if ($this->form->isValid()) 
            {

                $values = $this->form->getValues();
                if (!empty($values['password'])) {
                    $values['password'] = sha1($values['password']);
                }
                $values['updated_at'] = date('Y-m-d h:i:s');

                $staff = new Staff();
                if ($this->staff) {
                    $staff = $this->staff;
                }
                else {
                    $values['created_at'] = date('Y-m-d h:i:s');
                }
                $staff->fromArray($values);
                $staff->save();
                
                $this->getUser()->setAttribute('username', $staff->getUsername());
                $this->getUser()->setAttribute('websiteUsername', $staff->getWebsiteUsername());
                
                $this->staff = $staff;
                
                $request->setParameter('success', 'Staff successfully saved');

            }
        }
        
        // If changing password
        if ($request->getParameter('change_password'))
        {
            $newPassword = $request->getParameter('new_password');
            $confirmPassword = $request->getParameter('confirm_password');
            
            // Make sure fields arnt blank
            if (empty($newPassword) && empty($confirmPassword)) {
                $request->setParameter('changeError', 'Both fields are required');
                return sfView::SUCCESS;
            }
            
            // Make sure passwords match
            if ($newPassword != $confirmPassword) {
                $request->setParameter('changeError', 'Passwords do not match');
                return sfView::SUCCESS;
            }
            
            // All is well save password
            $this->staff->setPassword(sha1($newPassword));
            $this->staff->save();
            $request->setParameter('changeSuccess', 'Successfully changed password');
        }
        
        // If they want to delete staff
        if ($request->getParameter('delete'))
        {
            // Make sure they arn't deleting themself
            if ($this->staff->getId() == $this->getUser()->getAttribute('staffId')) {
                $request->setParameter('error', 'You cannot delete yourself');
                return sfView::SUCCESS;
            }
            $this->staff->delete();
            
            $this->getUser()->setFlash('success', 'Successfully deleted staff member');
            $this->redirect('/admin/index');
        }
        
    }
}