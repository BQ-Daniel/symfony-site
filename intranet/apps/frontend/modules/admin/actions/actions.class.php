<?php

/**
 * admin actions.
 *
 * @package    brakequip
 * @subpackage admin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class adminActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $sort = $request->getParameter('sort', 'name'); // default sort field
        $dir = $request->getParameter('dir', 'asc'); // default direction
        
        $this->levels = sfConfig::get('app_admin_levels');
        
        $q = Doctrine_Query::create()->from('Staff');
        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);
        
        $this->pager = new sfDoctrinePager('Staff', sfConfig::get('app_results_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();

        $this->totalResults = $this->pager->getNbResults();

    }

    #new function added for restrictaion
      public function executeGeneral(sfWebRequest $request)
    {
            return sfView::SUCCESS;
        //$sort = $request->getParameter('sort', 'name'); // default sort field
       // $dir = $request->getParameter('dir', 'asc'); // default direction
        
        //$this->levels = sfConfig::get('app_admin_levels');
        
        //$q = Doctrine_Query::create()->from('Staff');
        //
        // Add order by            
       
        //$q->addOrderBy($sort . ' ' . $dir);
        
        //$this->pager = new sfDoctrinePager('Staff', sfConfig::get('app_results_per_page'));
        //$this->pager->setQuery($q);
        //$this->pager->setPage($request->getParameter('page', 1));
        //$this->pager->init();

        //$this->totalResults = $this->pager->getNbResults();
          

    }
    
}
