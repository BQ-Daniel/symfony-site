<?php

/**
 * edit restriction
 * 
 * @package    brakequip
 * @subpackage restriction
 * @author     Kuldeep
 */
class generalAction extends sfAction {

    public function execute($request) {
        
        //saving Restriction threshold details
        if ($request->getParameter('save')) {

            $email = $request->getParameter('email');
            $output = array();
            # make an array of emails.
            $emailArray = explode(',',$email);
            # remove extra spaces.
            $emailArrayFinal = array_map('trim',$emailArray);
            # put emails array in final array.
            $output['emails'] = $emailArrayFinal;
            # put threshold in json output.
            $threshold = $request->getParameter('threshold');
            $output['threshold'] = $threshold;
            # create json output.
            $outputfinal = json_encode($output);
            # update in the database.
            $values = array('NAME' => 'restriction_setting', 'VALUE' => $outputfinal);
            Doctrine_Query::create()->update('SETTINGS')->set($values)->where('ID = ?', 4)->execute();

            $this->msg = 'Settings changed successfully.';
            
        } elseif ($request->getParameter('import_debtors')) {
            exec('/usr/local/bin/php /home/brakeq/public_html/include/batch/usersUpdate.php 2>&1');

            $this->exportMsg = 'Debtors script successfully executed';
        } elseif ($request->getParameter('import_parts')) {
            exec('/usr/local/bin/php /home/brakeq/public_html/include/batch/ftpPolling.php 2>&1');

            $this->exportMsg = 'Parts script successfully executed';
        } 
        
        #get saved data from database.
        $restriction = Doctrine_Query::create()->from('SETTINGS')->where('ID = ?', 4)->fetchOne();
        $data = $restriction->getVALUE();
        # get decoded json data.
        $dataUse = json_decode($data,true);
        # make an emails string.
        $this->emails = implode(', ',$dataUse['emails']);
        # set threshold.
        $this->threshold = $dataUse['threshold'];
            
    }

}
