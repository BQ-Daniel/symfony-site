<?php slot('title', 'Administration') ?>

<h2 class="fl">Administration</h2>

<?php include_partial('admin/subnav') ?>

<?php include_partial('global/formMessages') ?>

<?php if ($form->hasErrors()): ?>
<div class="error">
    Please fix the errors below.
</div>
<?php endif; ?>

<form action="<?php echo url_for('admin/edit') ?>" method="post">
    <?php if (isset($staff)): ?>
    <input type="hidden" name="id" value="<?php echo $staff->getId() ?>" />
    <?php echo $form['id']->render() ?>
    <?php endif; ?>
    <table cellspacing="0" cellpadding="5" border="0">
        <tr valign="top">
            <th width="120">Name</th>
            <td style="padding-right: 15px;">
                <?php echo $form['name']->render(array('class' => "input")) ?>
                <?php if ($form['name']->hasError()): ?>
                    <div class="form-error"><?php echo $form['name']->getError() ?></div>
                <?php endif; ?>
            </td>
            <th>Level</th>
            <td>
                <?php echo $form['level']->render(array('class' => "select")) ?>
                <?php if ($form['level']->hasError()): ?>
                    <div class="form-error"><?php echo $form['level']->getError() ?></div>
                <?php endif; ?>
            </td>
        </tr>
        <tr valign="top">
            <th width="120">Username</th>
            <td style="padding-right: 15px;">
                <?php echo $form['username']->render(array('class' => "input")) ?>
                <?php echo $form['org_username']->render() ?>
                <?php if ($form['username']->hasError()): ?>
                    <div class="form-error"><?php echo $form['username']->getError() ?></div>
                <?php endif; ?>
            </td>
            <th>Email Address</th>
            <td>
                <?php echo $form['email']->render(array('class' => "input")) ?>
                <?php if ($form['email']->hasError()): ?>
                    <div class="form-error"><?php echo $form['email']->getError() ?></div>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <th>Website Username</th>
            <td>
                <?php echo $form['website_username']->render(array('class' => "input")) ?>
                <?php if ($form['website_username']->hasError()): ?>
                    <div class="form-error"><?php echo $form['website_username']->getError() ?></div>
                <?php endif; ?>
            </td>
        </tr>
        <?php if (!isset($staff)): ?>
        <tr valign="top">
            <th width="120">Password</th>
            <td style="padding-right: 15px;">
                <?php echo $form['password']->render(array('class' => "input", 'autocomplete' => 'off')) ?>
                <?php if ($form['password']->hasError()): ?>
                    <div class="form-error"><?php echo $form['password']->getError() ?></div>
                <?php endif; ?>
            </td>
            <th>Confirm Password</th>
            <td>
                <?php echo $form['confirm_password']->render(array('class' => "input", 'autocomplete' => 'off')) ?>
                <?php if ($form['confirm_password']->hasError()): ?>
                    <div class="form-error"><?php echo $form['confirm_password']->getError() ?></div>
                <?php endif; ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
    <input type="submit" name="save" value="Save" />
    <?php if (isset($staff)): ?>
    <input type="submit" name="delete" value="Delete" onclick="return confirm('Are you sure you want to delete this staff member?');" />
    <?php endif; ?>
</form>
    
<div style="height: 20px;"></div>
    
<?php if (isset($staff)): ?>
<form action="<?php echo url_for('admin/edit') ?>" method="post">
    <input type="hidden" name="id" value="<?php echo $staff->getId() ?>" />
    <h2>Change Password</h2>
    
    <?php if ($sf_params->get('changeSuccess')): ?>
    <div class="success"><?php echo $sf_params->get('changeSuccess') ?></div>
    <?php endif; ?>
    
    <?php if ($sf_params->get('changeError')): ?>
    <div class="error"><?php echo $sf_params->get('changeError') ?></div>
    <?php endif; ?>
    
    <table cellspacing="0" cellpadding="5" border="0">
        <tr valign="top">
            <th width="120">New Password</th>
            <td style="padding-right: 15px;">
                <input type="password" name="new_password" value="" class="input" autocomplete="off" />
            </td>
            <th>Confirm Password</th>
            <td>
                <input type="password" name="confirm_password" value="" class="input" autocomplete="off" />
            </td>
        </tr>
    </table>
    <input type="submit" name="change_password" value="Change Password" />
</form>
<?php endif; ?>