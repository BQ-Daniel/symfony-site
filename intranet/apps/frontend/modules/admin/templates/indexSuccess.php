<?php slot('title', 'Administration') ?>

<h2 class="fl">Administration</h2>

<?php include_partial('admin/subnav') ?>

<?php include_partial('global/formMessages') ?>
    <div style="width: 800px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="800">
            <tr>
                <th width="100"><?php echo sortable_link('Name', url_for('admin/index'), 'name') ?></th>
                <th width="80"><?php echo sortable_link('Username', url_for('users/index'), 'username') ?></th>
                <th width="250"><?php echo sortable_link('Email', url_for('admin/index'), 'email') ?></th>
                <th width="60"><?php echo sortable_link('Level', url_for('admin/index'), 'level') ?></th>
                <th><?php echo sortable_link('Last Login', url_for('admin/index'), 'last_login') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('admin/edit?id='.$result->getId()) ?>"><?php echo $result->getName() ?></a></td>
                <td><a href="<?php echo url_for('admin/edit?id='.$result->getId()) ?>"><?php echo $result->getUsername() ?></a></td>
                <td><?php echo $result->getEmail() ?></td>
                <td><?php echo $levels[$result->getLevel()] ?></td>
                <td><?php echo ($result->getLastLogin() != '' ? date('D jS M y', strtotime($result->getLastLogin())) : 'Never') ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="5">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 800px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
