<?php slot('title', 'Catalogue access limit') ?>
<form action="<?php echo url_for('admin/general') ?>" method="post">
 
    <input type="hidden" name="id" value="" />
   
    <table cellspacing="0" cellpadding="5" border="0">
        <tr valign="top">
            <td style="padding-right: 15px;" colspan="2">
                <span style="font-size: 12px; margin-left:8px; color:green"><?php echo (isset($msg))?$msg:""; ?></span>
            </td>
        </tr>
        <tr valign="top">
            <th width="120">Email addresses:</th>
            <td style="padding-right: 15px;">
               <?php
               $emailsString = '';
               if(isset($emails)) {
                   $emailsString = $emails;
               }
               ?>
                <input type="text" name="email" value="<?php echo $emailsString; ?>" class="input" style="width:312px" autocomplete="off" placeholder="username@example.com"/>
                <br>
                <span style="font-size: 9px">You can enter more than one email by putting comma in between.</span>
            </td>
        </tr>
          <tr valign="top">
            <th>Threshold value:</th>
            <td>
                <?php
               $thresholdValue = 0;
               if(isset($threshold)) {
                   $thresholdString = $threshold;
               }
               ?>
                    <input type="text" name="threshold" value="<?php echo $thresholdString; ?>" style="width:312px" class="input" autocomplete="off" placeholder=""/>
               
            </td>
        </tr>
    </table>
    <input type="submit" name="save" value="Save" style="cursor: pointer; margin-left:10px" />
</form>

<div style="height: 20px"></div>

<h2>Import Data</h2>

<div class="info-table">
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td colspan="2">
                    <span style="font-size: 12px; color:green"><?php echo (isset($exportMsg)) ? $exportMsg : ""; ?></span>
                </td>
            </tr>
            <tr>
                <td>Import DEBTORS.CSV into database</td>
                <td>
                    <form action="<?php echo url_for('admin/general') ?>" method="post">
                        <input type="submit" name="import_debtors" value="Import Debtors" style="cursor: pointer;" />
                    </form>
                </td>
            </tr>
            <tr>
                <td>Import PARTS.CSV into database</td>
                <td>
                    <form action="<?php echo url_for('admin/general') ?>" method="post">
                        <input type="submit" name="import_parts" value="Import Parts" style="cursor: pointer;" />
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</div>