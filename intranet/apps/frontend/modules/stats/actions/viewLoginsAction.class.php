<?php

/**
 * view logins
 * 
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class viewLoginsAction extends sfAction {

    public function execute($request) {
        $id = $request->getParameter('id');
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'login_time'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        $parameters = $this->getUser()->getParameter('search_parameters');
        $this->getUser()->setParameter('search_parameters', $parameters);
        $values = unserialize($parameters);
        
        $this->user = USERSTable::getInstance()->find($id);
        
        $q = Doctrine_Query::create()->from('USER_LOGINS ul')->where('ul.user_id = ?', $request->getParameter('id'));
        // Where clause
        $fromDate = $values['from_date'];
        if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
            $q->addWhere('login_time >= ?', $fromDate['year'] . '-' . $fromDate['month'] . '-' . $fromDate['day']);
        }
        $toDate = $values['to_date'];
        if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
            $q->addWhere("login_time <= DATE_ADD('" . $toDate['year'] . '-' . $toDate['month'] . '-' . $toDate['day'] . "', INTERVAL 1 DAY)");
        }
        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);

        $this->pager = new sfDoctrinePager('USER_LOGINS', sfConfig::get('app_results_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($page);
        $this->pager->init();
        
        $this->totalResults = $this->pager->getNbResults();
    }

}