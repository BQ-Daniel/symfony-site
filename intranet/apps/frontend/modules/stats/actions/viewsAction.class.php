<?php

/**
 * sessions
 *
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class viewsAction extends sfAction {

    public function execute($request)
    {
        $this->form = $form = new SearchViewsForm();
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'total_views'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction

        $parameters = $request->getParameter($this->form->getName());

        if ($request->getParameter('page', null) || $request->getParameter('sort', null) || $request->getParameter('return', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }

            $request->setParameter('submit', true);
            if ($request->getParameter('return', null)) {
                $keys = array('sort', 'page', 'dir');
                foreach ($keys as $key) {
                    $$key = $parameters[$key];
                    unset($parameters[$key]);
                }
            }
        }


        if ($request->getParameter('submit') || $request->getParameter('page', null))
        {
            $this->form->bind($parameters);

            $values = $parameters;
            $q = Doctrine_Query::create()->select('*, COUNT(cvg.web_category_id) as total_views')->from('CategoryViewGroup cvg')->
                    innerJoin('cvg.CategoryView cv')->leftJoin('cv.USER_LOGINS ul')->
                    leftJoin('ul.USERS u')->where('cvg.web_category_type_id = ?', $values['category_type_id'])->groupBy('cvg.web_category_id');

            // Where clause
            $fromDate = $values['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $q->addWhere('cv.created_at >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $toDate = $values['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $q->addWhere("cv.created_at <= DATE_ADD('".$toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']."', INTERVAL 1 DAY)");
            }

            if (!empty($values['username'])) {
                $q->addWhere('UPPER(u.username) = ?', strtoupper($values['username']));
            }

            // Add order by
            $q->addOrderBy($sort.' '.$dir);

            $this->pager = new sfDoctrinePager('CategoryViewGroup', 1000);
            $this->pager->setQuery($q);
            $this->pager->setPage($page);
            $this->pager->init();

            // Set in session to store on next page
            $parameters['page'] = $page;
            $parameters['sort'] = $sort;
            $parameters['dir'] = $dir;
            $this->getUser()->setParameter('search_parameters', serialize($parameters));

            $this->totalResults = $this->pager->getNbResults();

            $this->averageTime = null;
            if ($this->totalResults == 0) {
                $this->getUser()->setFlash('error', 'No results found');
            }
            else {
                // Work out avg session time
                $sql = "SELECT FLOOR(AVG(UNIX_TIMESTAMP(LOGIN_END) - UNIX_TIMESTAMP(LOGIN_TIME))) as avg FROM USER_LOGINS ul ".
                    "INNER JOIN USERS u ON u.ID = ul.USER_ID ".
                    "WHERE LOGIN_TIME >= '".$fromDate[2]."-".$fromDate[1]."-".$fromDate[0]."' AND ".
                    "LOGIN_TIME <= DATE_ADD('".$toDate[2]."-".$toDate[1]."-".$toDate[0]."', INTERVAL 1 DAY) $usernameSql ";

                    $q = Doctrine_Query::create()->select('FLOOR(AVG(UNIX_TIMESTAMP(LOGIN_END) - UNIX_TIMESTAMP(LOGIN_TIME))) as avg')->
                            from('USER_LOGINS ul')->innerJoin('ul.USERS u')->groupBy('ul.ID');

                    // Where clause
                    $fromDate = $values['from_date'];
                    if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                        $q->addWhere('login_time >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
                    }
                    $toDate = $values['to_date'];
                    if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                        $q->addWhere("login_time <= DATE_ADD('".$toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']."', INTERVAL 1 DAY)");
                    }
                    if (!empty($values['username'])) {
                        $q->addWhere('UPPER(username) = ?', strtoupper($values['username']));
                    }

                    $result = $q->fetchOne();
                    //$this->averageTime = ($result->getAvg() / 60 > 0 ? floor($result->getAvg() / 60).' mins' : floor($result->getAvg()).' secs' );
            }
        }
    }

}
