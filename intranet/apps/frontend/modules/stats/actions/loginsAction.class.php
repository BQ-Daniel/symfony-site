<?php

/**
 * logins
 * 
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class loginsAction extends sfAction {

    public function execute($request)
    {
        $this->form = $form = new StatsForm();
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'login_time'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        if (empty($sort)) {
            $sort = 'login_time';
            $dir = 'desc';
        }
        
        $parameters = $request->getParameter($this->form->getName());

        if ($request->getParameter('page', null)  || $request->getParameter('sort', null) || $request->getParameter('return', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }
            $request->setParameter('submit', true);
            if ($request->getParameter('return', null)) {
                $keys = array('sort', 'page', 'dir');
                foreach ($keys as $key) {
                    $$key = $parameters[$key];
                    unset($parameters[$key]);
                }
            }
        }
        $this->form->bind($parameters);
        
        if ($request->getParameter('submit') && $this->form->isValid() || $request->getParameter('page', null) || $request->getParameter('return', null))
        {   
            $values = $parameters;
            $q = Doctrine_Query::create()->select('*, COUNT(ul.USER_ID) total_logins')->from('USER_LOGINS ul')->leftJoin('ul.USERS u');
            
            // Where clause
            $fromDate = $values['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $q->addWhere('login_time >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $toDate = $values['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $q->addWhere("login_time <= DATE_ADD('".$toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']."', INTERVAL 1 DAY)");
            }
            
            if (!empty($values['username']))
            {
                $q->addWhere('username = ?', $values['username']);
                $q->addGroupBy('DATE(ul.LOGIN_TIME)');
            }
            else {
                $q->addWhere("username != ''");
                $q->addGroupBy('ul.USER_ID');
            }
            
            // Add order by            
            $q->addOrderBy($sort.' '.$dir);

            $this->pager = new sfDoctrinePager('USER_LOGINS', sfConfig::get('app_results_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($page);
            $this->pager->init();
            
            // Set in session to store on next page
            $parameters['page'] = $page;
            $parameters['sort'] = $sort;
            $parameters['dir'] = $dir;
            
            $this->sort = $sort;
            $this->dir = $dir;
            $this->getUser()->setParameter('search_parameters', serialize($parameters));
            
            $this->totalResults = $this->pager->getNbResults();
            
            if ($this->totalResults == 0) {
                $this->getUser()->setFlash('error', 'No results found');
            }
            
        }
    }

}