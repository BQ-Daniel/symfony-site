<?php

/**
 * orders
 * 
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class ordersAction extends sfAction {

    public function execute($request)
    {
        $this->form = $form = new OrderStatsForm();
        
        $parameters = $request->getParameter($this->form->getName());
        
        if (isset($_GET['fromDate'])) {
            list($yr, $mth, $d) = explode('-', $_GET['fromDate']);
            $parameters['from_date'] = array(
                'day' => $d,
                'month' => $mth,
                'year' => $yr,
            );
        }
        if (isset($_GET['toDate'])) {
            list($yr, $mth, $d) = explode('-', $_GET['toDate']);
            $parameters['to_date'] = array(
                'day' => $d,
                'month' => $mth,
                'year' => $yr,
            );
        }
        $this->form->bind($parameters);
        
        if ($form->isValid() && $request->getParameter('submit'))
        {   
            if (isset($_GET['month'])) {
                $parameters['date']['month'] = $_GET['month'];
                $parameters['date']['year'] = $_GET['year'];
            }
            
            if ($parameters['date']['month']) 
            {
                if (!$parameters['date']['year']) {
                    $parameters['date']['year'] = date('Y');
                }
                
                $parameters['from_date'] = array(
                    'day' => '01',
                    'month' => $parameters['date']['month'],
                    'year' => $parameters['date']['year'],
                );
                // Get last date of month
                $day = date('t', strtotime(date('Y').'-'.$parameters['date']['month'].'-01'));
                $parameters['to_date'] = array(
                    'day' => $day,
                    'month' => $parameters['date']['month'],
                    'year' => $parameters['date']['year'],
                );
            
            
                $this->filteredDate = new DateTime($parameters['date']['year'].'-'.$parameters['date']['month'].'-01 00:00:00');
                $month = $this->filteredDate->format('m');
                $year = $this->filteredDate->format('Y');

                $prevMonthDate = new DateTime($this->filteredDate->format('Y-m-d'));
                $prevMonthDate->modify("-1 month");
                $prevMonth = array(
                    'month' => $prevMonthDate->format('m'),
                    'year' => $prevMonthDate->format('Y'),
                    'submit' => true
                );
                $this->prevMonthUrl = http_build_query($prevMonth);

                $nextMonthDate = new DateTime($this->filteredDate->format('Y-m-d'));
                $nextMonthDate->modify("+1 month");
                $nextMonth = array(
                    'month' => $nextMonthDate->format('m'),
                    'year' => $nextMonthDate->format('Y'),
                    'submit' => true
                );
                $this->nextMonthUrl = http_build_query($nextMonth);
            }
            $this->fromDate = null;
            $fromDate = $parameters['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $this->fromDate = $fromDate = new DateTime($fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $this->toDate = null;
            $toDate = $parameters['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $this->toDate = $toDate = new DateTime($toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']);
            }
            
            if (!$this->toDate && !$this->fromDate) {
                return sfView::SUCCESS;
            }   
            
            // Get total orders
            $this->totalResults = ORDERSTable::getCountOrdersMonthYear($this->fromDate, $this->toDate);
            
            if ($this->totalResults == 0) {
                return sfView::SUCCESS;
            }
            
            $this->avgCost = ORDERSTable::getAverageOrderCostPerCustomer($this->fromDate, $this->toDate);
            $this->avgOrders = ORDERSTable::getAverageOrdersPerCustomer($this->fromDate, $this->toDate);
            
            $query = Doctrine_Query::create()->from("ORDERS o");
            
            $dql = array();
            if ($this->fromDate) {
                $dql[] = "DATE(o2.DATE_ADDED) >= '".$this->fromDate->format('Y-m-d')."'";
                $query->andWhere('DATE(o.DATE_ADDED) >= ?', $this->fromDate->format('Y-m-d'));
            }
            if ($this->toDate) {
                $dql[] = "DATE(o2.DATE_ADDED) <= '".$this->toDate->format('Y-m-d')."'";
                $query->andWhere("DATE(o.DATE_ADDED) <= ?", $this->toDate->format('Y-m-d'));
            }
           
            $query->select("o.id, u.ID, u.USERNAME, COUNT(o.id) totalOrders, AVG(REPLACE(o.COST, ',', '')) avgValue")
                    ->addSelect('(SELECT COUNT(*) as count FROM ORDER_DETAILS od INNER JOIN od.ORDERS o2 WHERE o2.USER_ID = o.USER_ID AND '.implode(' AND ', $dql).') totalLines')
                    ->innerJoin('o.USERS u')
                    ->groupBy('o.USER_ID');
            
            if (isset($_GET['sort'])) {
                $query->orderBy($_GET['sort'].' '.$_GET['dir']);
            }
            else {
                $query->orderBy('totalOrders DESC');
            }

            $this->results = $query->fetchArray();
                   
            // Loop through and add avgLines
            foreach ($this->results as $key => $result) {
                $this->results[$key]['avgLines'] = number_format($result['totalLines'] / $result['totalOrders'], 0);
            }
            
            if ($_GET['sort'] == "totalLines") {
                usort($this->results, function($a, $b) {
                    return $a['avgLines'] - $b['avgLines'];
                });
                if ($_GET['dir'] == "desc") {
                    usort($this->results, function($a, $b) {
                        return $b['avgLines'] - $a['avgLines'];
                    });
                }
            }

        }
    }
    

    private function mul_sort_asc($a,$b)
    {
        if($a['avgLines'] > $b['avgLines']) {
            return 1; 
        }
        if($a['avgLines'] < $b['avgLines']) {
            return -1; 
        }

        if ($a['avgLines'] == $b['avgLines']) 
            return 0;
    }
    
    private function mul_sort_desc($a,$b)
    {
        if($a['avgLines'] > $b['avgLines']) {
            return -1; 
        }
        if($a['avgLines'] < $b['avgLines']) {
            return 1; 
        }
        if($a['avgLines'] == $b['avgLines'])
            return 0;
    }

}