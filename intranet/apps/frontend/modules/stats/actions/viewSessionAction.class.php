<?php

/**
 * view session
 * 
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class viewSessionAction extends sfAction {

    public function execute($request) {
        $id = $request->getParameter('id');
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'pv.created_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        

        $q = Doctrine_Query::create()->from('PartView pv')
                ->select('*')
                ->innerJoin('pv.WebPart wp')
                ->where('pv.user_logins_id = ?', $request->getParameter('id'));
        
        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);

        $this->pager = new sfDoctrinePager('web_part', 50);
        $this->pager->setQuery($q);
        $this->pager->setPage($page);
        $this->pager->init();
        
        $this->totalResults = $this->pager->getNbResults();
    }

}