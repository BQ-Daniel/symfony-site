<?php slot('title', 'Statistics') ?>

<h2 class="fl">Logins</h2>

<?php include_partial('stats/subnav') ?>


<div id="search-form" class="filter" style="width: 645px;">
    <form action="<?php echo url_for('stats/logins') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="645">
            <tr>
                <th width="60">Username</th>
                <td><?php echo $form['username']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th>From Date</th>
                <td width="237"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="60" style="padding-left: 20px">To Date</th>
                <td width="237"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <?php if ($form['username']->getValue() == ''): ?>
                <th width="150"><?php echo sortable_link('Username', url_for('stats/logins'), 'username') ?></th>
                <?php endif; ?>
                <th><?php echo sortable_link('Logins', url_for('stats/logins'), 'total_logins') ?></th>
                <?php if ($form['username']->getValue() == ''): ?>
                <th><?php echo sortable_link('Last Login', url_for('stats/logins'), 'login_time') ?></th>
                <?php else: ?>
                <th><?php echo sortable_link('Date', url_for('stats/logins'), 'login_time') ?></th>
                <?php endif; ?>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <?php if ($form['username']->getValue() == ''): ?>
                <td><a href="<?php echo url_for('users/edit?id='.$result->getUSERS()->getId()) ?>"><?php echo $result->getUSERS()->getUsername() ?></a></td>
                <?php endif; ?>
                <td><a href="<?php echo url_for('stats/viewLogins?id='.$result->getUSERS()->getId()) ?>" title="View Logins"><?php echo $result->getTotalLogins() ?></a></td>
                <?php if ($form['username']->getValue() == ''): ?>
                <td><?php echo date('D jS M y, g:ia', strtotime($result->getLoginTime())) ?></td>
                <?php else: ?>
                <td><?php echo date('D jS M y', strtotime($result->getLoginTime())) ?></td>
                <?php endif; ?>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
</div>
<div id="order-details">

</div>

<?php endif; ?>