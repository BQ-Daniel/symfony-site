<?php slot('title', 'Statistics') ?>

<h2 class="fl">Orders</h2>

<?php include_partial('stats/subnav') ?>


<div id="search-form" class="filter" style="width: 645px;">
    <form action="<?php echo url_for('stats/orders') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="645">
            <tr>
                <th colspan="4" style="vertical-align: middle">Filter Month / Year &nbsp;&nbsp; <?php echo $form['date']->render() ?></th>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th>From Date</th>
                <td width="237"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="60" style="padding-left: 20px">To Date</th>
                <td width="237"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults) && $totalResults > 0): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<?php endif; ?>

<?php if (isset($_REQUEST['submit'])): ?>
<?php
if (isset($filteredDate)):
?>
<h2>
    <a href="<?php echo url_for('stats/orders') ?>?<?php echo $prevMonthUrl ?>">&lt;</a>
    Results for <?php echo $filteredDate->format('F Y') ?>
    <a href="<?php echo url_for('stats/orders') ?>?<?php echo $nextMonthUrl ?>">&gt;</a>
</h2>
<?php endif; ?>

    <?php if ($totalResults > 0):?>
    <div style="height:15px;"></div>
    <div class="info-table" style="width: 600px">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="200">Total Unique Orders</th>
                <td><?php echo $totalResults ?></td>
            </tr>
            <tr>
                <th>Average orders per customer</th>
                <td><?php echo $avgOrders ?></td>
            </tr>
            <tr>
                <th>Average order value per customer</th>
                <td>$<?php echo number_format($avgCost, 2) ?></td>
            </tr>
        </table>
    </div>
    <div style="height:15px;"></div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="500">
            <tr>
                <?php
                $url['submit'] = true;
                if ($fromDate) {
                    $url['fromDate'] = $fromDate->format('Y-n-j');
                }
                if ($toDate) {
                    $url['toDate'] = $toDate->format('Y-n-j');
                }
                if (isset($filteredDate)) {
                    $url['month'] = $filteredDate->format('m');
                    $url['year'] = $filteredDate->format("Y");
                }
                $sortUrl = http_build_query($url);
                ?>
                <th width="100"><?php echo sortable_link('Account', url_for('stats/orders'), 'username', '&'.$sortUrl) ?></th>
                <th><?php echo sortable_link('Total Orders', url_for('stats/orders'), 'totalOrders', '&'.$sortUrl) ?></th>
                <th><?php echo sortable_link('Avg Lines', url_for('stats/orders'), 'totalLines', '&'.$sortUrl) ?></th>
                <th><?php echo sortable_link('Avg Value', url_for('stats/orders'), 'avgValue', '&'.$sortUrl) ?></th>
            </tr>
            <?php foreach ($results as $result): ?>
            <tr>
                <td><?php echo $result['USERS']['username'] ?></td>
                <td><?php echo $result['totalOrders'] ?></td>
                <td><?php echo $result['avgLines'] ?></td>
                <td>$<?php echo number_format($result['avgValue'], 2) ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php else: ?>
    <p>No results returned</p>

    <?php endif; ?>

<?php endif; ?>