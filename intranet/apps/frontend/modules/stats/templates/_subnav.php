<ul class="sub-nav">
    <li><?php echo link_to('Logins', 'stats/logins', ($sf_params->get('action') == "logins" ? 'class="active"' : "")) ?></li>
    <li><?php echo link_to('Sessions', 'stats/sessions', ($sf_params->get('action') == "sessions" ? 'class="active"' : "")) ?></li>
    <li><?php echo link_to('Views', 'stats/views', ($sf_params->get('action') == "views" ? 'class="active"' : "")) ?></li>
    <li><?php echo link_to('Orders', 'stats/orders', ($sf_params->get('action') == "orders" ? 'class="active"' : "")) ?></li>
    <!--<li><?php echo link_to('Distributors', 'stats/distributors', ($sf_params->get('action') == "distributors" ? 'class="active"' : "")) ?></li>-->
</ul>
<div class="cb" style="height: 20px;"></div>