<?php slot('title', 'Statistics') ?>

<h2 class="fl">Logins: View</h2>

<?php include_partial('stats/subnav') ?>

<p><a href="<?php echo url_for('stats/logins?return=true') ?>" class="button"><span>Go back</span></a></p>

<div class="cb" style="height: 10px"></div>

<table cellspacing="0" cellpadding="0" border="0" class="data">
    <tr>
        <th width="100">Username</th>
        <td><?php echo $user->getUsername() ?></td>
    </tr>
    <?php
    $fromDate = $values['from_date'];
    if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])): ?>
    <tr>
        <th>From Date</th>
        <td><?php echo date('D jS M y', strtotime($fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day'])) ?></td>
    </tr>
    <?php endif; ?>
    <?php
    $toDate = $values['to_date'];
    if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])): ?>
    <tr>
        <th>To Date</th>
        <td><?php echo date('D jS M y', strtotime($toDate['year'].'-'.$toDate['month'].'-'.$toDate['day'])) ?></td>
    </tr>
    <?php endif; ?>
</table>

<div class="cb" style="height: 10px"></div>

<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'id='.$sf_params->get('id'))); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150">IP</th>
                <th>Login Time</th>
                <th>Duration</th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><?php echo $result->getIp() ?></td>
                <td><?php echo date('D jS M y, g:ia', strtotime($result->getLoginTime())) ?></td>
                <td><?php echo getTimeDifference($result->getLoginTime(), $result->getLoginEnd()) ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
       <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'id='.$sf_params->get('id'))); ?>
    </div>
</div>
