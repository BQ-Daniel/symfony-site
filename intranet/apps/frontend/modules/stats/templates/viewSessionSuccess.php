<?php slot('title', 'Statistics') ?>

<h2 class="fl">Sessions: View Session</h2>

<?php include_partial('stats/subnav') ?>

<p><a href="<?php echo url_for('stats/sessions?return=true') ?>" class="button"><span>Go back</span></a></p>

<div class="cb" style="height: 10px"></div>

<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'id='.$sf_params->get('id'))); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('BQ Number', url_for('stats/viewSession?id='.$sf_params->get('id')), 'wp.bq_number') ?></th>
                <th><?php echo sortable_link('Viewed At', url_for('stats/viewSession?id='.$sf_params->get('id')), 'wp.created_at') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('part/edit?id='.$result->getWebPart()->getId()) ?>"><?php echo $result->getWebPart()->getBqNumber() ?></td>
                <td><?php echo date('D jS M y, g:ia', strtotime($result->getCreatedAt())) ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
       <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'id='.$sf_params->get('id'))); ?>
    </div>
</div>
