<?php slot('title', 'Statistics') ?>

<h2 class="fl">Sessions</h2>

<?php include_partial('stats/subnav') ?>

<div id="search-form" class="filter" style="width: 645px;">
    <form action="<?php echo url_for('stats/sessions') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="645">
            <tr>
                <th width="60">Username</th>
                <td><?php echo $form['username']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th>From Date</th>
                <td width="237"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="60" style="padding-left: 20px">To Date</th>
                <td width="237"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<div id="search-results">
    <?php if (isset($totalResults)): ?>
    <p>Average Session Length: <?php echo $averageTime ?></p>
    <?php endif; ?>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('Username', url_for('stats/sessions'), 'username') ?></th>
                <th><?php echo sortable_link('Session Start', url_for('stats/sessions'), 'login_time') ?></th>
                <th><?php echo sortable_link('Parts Viewed', url_for('stats/sessions'), 'parts_viewed') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('users/edit?id='.$result->getUSERS()->getId()) ?>"><?php echo $result->getUSERS()->getUsername() ?></a></td>
                <td><?php echo date('D jS M y, g:ia', strtotime($result->getLoginTime())) ?></td>
                <td><a href="<?php echo url_for('stats/viewSession?id='.$result->getId()) ?>"><?php echo $result->getPartsViewed() ?></a></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
</div>

<?php endif; ?>