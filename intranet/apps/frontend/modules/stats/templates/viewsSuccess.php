<?php slot('title', 'Statistics') ?>

<h2 class="fl">Views</h2>

<?php include_partial('stats/subnav') ?>


<div id="search-form" class="filter" style="width: 645px;">
    <form action="<?php echo url_for('stats/views') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0" width="645">
            <tr>
                <th width="100">Username</th>
                <td><?php echo $form['username']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th width="100">Category Type</th>
                <td><?php echo $form['category_type_id']->render() ?></td>
            </tr>
            <tr>
                <td height="20"></td>
            </tr>
            <tr>
                <th>From Date</th>
                <td width="237"><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                <th width="60" style="padding-left: 20px">To Date</th>
                <td width="237"><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
            </tr>
        </table>
        <div style="padding: 20px 0 0 0">
            <input type="submit" name="submit" value="Search" />
        </div>
    </form>
</div>

<div style="height: 20px"></div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>
<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th>Category</th>
                <th>Views</th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><?php echo $result->getCategoryString() ?></td>
                <td><?php echo $result->getTotalViews() ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
</div>
<?php endif; ?>