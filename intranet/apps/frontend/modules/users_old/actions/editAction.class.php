<?php

/**
 * edit user
 * 
 * @package    brakequip
 * @subpackage users
 * @author     John Smythe
 */
class editAction extends sfAction {
    
    public function execute($request)
    {
      
        $this->invoiceAddress = $this->deliveryAddrss = new USER_ADDRESS();
        
        // If user id exists, we are editing user
        if ($this->userId = $request->getParameter('id')) {
            $this->user = USERSTable::getInstance()->find($this->userId);
            $this->invoiceAddress = USER_ADDRESSTable::getInstance()->findOneByUserIdAndAddressType($this->userId, 'invoice');
            $this->deliveryAddress = USER_ADDRESSTable::getInstance()->findOneByUserIdAndAddressType($this->userId, 'delivery');
        }
       
        
        $this->form = new EditUserForm();
        // Set defaults
        if ($this->user) {
            $this->form->getWidget('org_username')->setAttribute('value', $this->user->getUsername());
            $this->form->getWidget('org_email')->setAttribute('value', $this->user->getEmail());     
        }
        $this->invoiceAddressForm = new USER_ADDRESSForm($this->invoiceAddress);
        $this->invoiceAddressForm->getWidgetSchema()->setNameFormat('invoice[%s]');
        $this->deliveryAddressForm = new USER_ADDRESSForm($this->deliveryAddress);
        $this->deliveryAddressForm->getWidgetSchema()->setNameFormat('delivery[%s]');
        
        $unwantedFields = array('user_id', 'address_type', 'lat', 'lng');
        foreach ($unwantedFields as $field) {
           unset($this->invoiceAddressForm[$field]); 
           unset($this->deliveryAddressForm[$field]); 
        }
        
        if ($request->getParameter('delete'))
        {
            $this->user->setDeleted(true);
            $this->user->save();
            $this->getUser()->setFlash('success', 'User successfully deleted');
            $this->redirect('users/index');
        }
        
        if ($request->getParameter('save'))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            
            if ($this->form->isValid()) 
            {                
                
                // Save user
                $userValues = $this->form->getValues();
                
                if ($userValues['deleted'] != "1") {
                    $this->invoiceAddressForm->bind($request->getParameter($this->invoiceAddressForm->getName()));
                    $this->deliveryAddressForm->bind($request->getParameter($this->deliveryAddressForm->getName()));
                    if (!$this->invoiceAddressForm->isValid() || !$this->deliveryAddressForm->isValid()) {
                        return sfView::SUCCESS;
                    }
                }
                
                if ($userValues['order_no_req'] == '1') {
                    $userValues['order_no_req'] = 'Y';
                }
                $user = new USERS();
                if ($this->user) {
                    $user = $this->user;
                }
                $user->fromArray($userValues);
                $user->save();
                
                // Save invoice address
                $invoiceAddressValues = $this->invoiceAddressForm->getValues();
                $invoiceAddress = new USER_ADDRESS();
                if ($this->invoiceAddress) {
                    $invoiceAddress = $this->invoiceAddress;
                }
                $invoiceAddressValues['user_id'] = $user->getId();
                $invoiceAddressValues['address_type'] = 'invoice';
                $invoiceAddress->fromArray($invoiceAddressValues);
                $invoiceAddress->setUserId($user->getId());
                $invoiceAddress->save();
                
                
                // Save delivery address
                $deliveryAddressValues = $this->deliveryAddressForm->getValues();
                $deliveryAddress = new USER_ADDRESS();
                if ($this->deliveryAddress) {
                    $deliveryAddress = $this->deliveryAddress;
                }
                $deliveryAddressValues['user_id'] = $user->getId();
                $deliveryAddressValues['address_type'] = 'delivery';
                $deliveryAddress->fromArray($deliveryAddressValues);
                $deliveryAddress->setUserId($user->getId());
                $deliveryAddress->save();
                
                if ($userValues['order_no_req'] == 'Y') {
                    $userValues['order_no_req'] = '1';
                }
                
                $request->setParameter('success', 'User successfully saved');
                
            }
        }
        else 
        {
            // If user exists, bind values to form
            if ($this->user)
            {
                $valueArray = $this->user->toArray();
                $valueArray['order_no_req'] = ($valueArray['order_no_req'] == 'Y' ? '1' : false); 
                $valueArray['order_access'] = ($valueArray['order_access'] == '1' ? '1' : false);
                $valueArray['stop_credit'] = ($valueArray['stop_credit'] == '1' ? '1' : false);
                $valueArray['distributor'] = ($valueArray['distributor'] == '1' ? '1' : false);
                $valueArray['deleted'] = ($valueArray['deleted'] == '1' ? '1' : false);
                
                $this->form->bindInputValuesFromArray($valueArray);
            }
        }
        
    }
}