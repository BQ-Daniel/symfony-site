<?php slot('title', 'Website') ?>

<h2 class="fl">Users</h2>

<?php include_partial('users/subnav') ?>

<?php if ($sf_user->getFlash('success')): ?>
<div class="success">
    <?php echo $sf_user->getFlash('success') ?>
</div>
<?php endif; ?>

<div id="search-form">
    <div class="filter" style="width: 720px;">
        <form action="<?php echo url_for('users/index') ?>" method="post">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <th width="100">Username</th>
                    <td><?php echo $form['username']->render() ?></td>
                    <th width="100" style="padding: 0 0 0 20px">Email</th>
                    <td><?php echo $form['email']->render() ?></td>
                </tr>
                <tr>
                    <td height="20"></td>
                </tr>
                <tr>
                    <th width="100">From Date</th>
                    <td><div class="select-date"><?php echo $form['from_date']->render() ?></div></td>
                    <th width="100" style="padding: 0 0 0 20px">To Date</th>
                    <td><div class="select-date"><?php echo $form['to_date']->render() ?></div></td>
                </tr>
            </table>
            <div style="padding: 20px 0 0 0">
                <input type="submit" name="submit" value="Search" />
            </div>
        </form>
    </div>
    <div class="cb" style="height: 20px;"></div>
</div>

<div class="filter-link" style="margin-bottom: -20px;">
    <a href="" style="display: none;" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Show Filter</a>
    <a href="" onclick="$('#search-form').slideToggle(); $('.filter-link a').toggle(); return false;">Hide Filter</a>
</div>

<div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

<?php if (isset($totalResults)): ?>
<script>
    $('#search-form').hide();
    $('.filter-link a').hide();
    var link = $('.filter-link a');
    var link = link[0];
    $(link).show();
    
</script>


<div id="search-results">
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('Username', url_for('users/index'), 'username') ?></th>
                <th width="250"><?php echo sortable_link('Email', url_for('users/index'), 'email') ?></th>
                <th><?php echo sortable_link('Last Active', url_for('users/index'), 'last_login') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('users/edit?id='.$result->getId()) ?>"><?php echo $result->getUsername() ?></a></td>
                <td><?php echo $result->getEmail() ?></td>
                <td><?php echo ($result->getLastLogin() != '' ? date('D jS M y', strtotime($result->getLastLogin())) : 'Never') ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="4">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager, 'params' => 'sort='.$sort.'&dir='.$dir)); ?>
    </div>
</div>
<?php endif; ?>