<?php slot('title', 'Website') ?>

<h2 class="fl">Users</h2>

<?php include_partial('users/subnav') ?>

<?php if ($sf_params->get('success')): ?>
<div class="success">
    <?php echo $sf_params->get('success') ?>
</div>
<?php endif; ?>

<?php if ($form->hasErrors() || $deliveryAddressForm->hasErrors() || $invoiceAddressForm->hasErrors()): ?>
<div class="error">
    Please fix the errors below.
</div>
<?php endif; ?>

<form action="<?php echo url_for('users/edit') ?>" method="post">
    <?php if (isset($user)): ?>
    <input type="hidden" name="id" value="<?php echo $user->getId() ?>" />
    <?php endif; ?>
    <table cellspacing="0" cellpadding="5" border="0">
        <tr valign="top">
            <th width="120">Username</th>
            <td style="padding-right: 15px;">
                <?php echo $form['username']->render(array('class' => "input")) ?>
                <?php echo $form['org_username']->render() ?>
                <?php if ($form['username']->hasError()): ?>
                    <div class="form-error"><?php echo $form['username']->getError() ?></div>
                <?php endif; ?>
            </td>
            <th>Email Address</th>
            <td>
                <?php echo $form['email']->render(array('class' => "input")) ?>
                <?php echo $form['org_email']->render() ?>
                <?php if ($form['email']->hasError()): ?>
                    <div class="form-error"><?php echo $form['email']->getError() ?></div>
                <?php endif; ?>
            </td>
        </tr>
        <tr valign="top">
            <th>Status</th>
            <td colspan="3">
             <?php echo $form['order_no_req']->render() ?> Require Order Number &nbsp;
             <?php echo $form['order_access']->render() ?> Order Access &nbsp;
             <?php echo $form['stop_credit']->render() ?> Stop Credit &nbsp;
             <?php echo $form['distributor']->render() ?> Distributor &nbsp;
             <?php echo $form['deleted']->render() ?> Deleted </td>
        </tr>
    </table>

    <script>
        $(function() {
            var $tabs = $( "#tabs" ).tabs();
            <?php if (!$deliveryAddressForm->hasErrors() && $invoiceAddressForm->hasErrors()): ?>
            $tabs.tabs('select', 1);
            <?php endif; ?>
        });
    </script>
    
    <div style="height: 30px;"></div>
    
    <div style="width: 685px">
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Delivery Address</a><?php echo ($deliveryAddressForm->hasErrors() ? image_tag('error_icon.png', 'class="error-icon"') : '') ?></li>
                <li><a href="#tabs-2">Invoice Address</a><?php echo ($invoiceAddressForm->hasErrors() ? image_tag('error_icon.png', 'class="error-icon"') : '') ?></li>
            </ul>
            <div id="tabs-1">
                <div id="delivery-form" class="form">
                    <?php $form = $deliveryAddressForm ?>
                    <table cellspacing="0" cellpadding="5" border="0">
                        <tr>
                            <th>Name</th>
                            <td>
                                <?php echo $form['name']->render(array('class' => "input")) ?>
                                <?php if ($form['name']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['name']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Address1</th>
                            <td>
                                <?php echo $form['address1']->render() ?>
                                <?php if ($form['address1']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['address1']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Address2</th>
                            <td>
                                <?php echo $form['address2']->render() ?>
                                <?php if ($form['address2']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['address2']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Suburb</th>
                            <td>
                                <?php echo $form['suburb']->render() ?>
                                <?php if ($form['suburb']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['suburb']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>State</th>
                            <td>
                                <?php echo $form['state']->render() ?>
                                <?php if ($form['state']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['state']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Postcode</th>
                            <td>
                                <?php echo $form['postcode']->render() ?>
                                <?php if ($form['postcode']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['postcode']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>
                                <?php echo $form['phone']->render() ?>
                                <?php if ($form['phone']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['phone']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="tabs-2">
                <div id="invoice-form" class="form">
                    <a href="" onclick="copyDeliveryAddress(); return false;" class="button fr"><span style="color: #fff;">Copy Delivery Address</span></a>
                    <?php $form = $invoiceAddressForm; ?>
                    <table cellspacing="0" cellpadding="5" border="0">
                        <tr>
                            <th>Name</th>
                            <td>
                                <?php echo $form['name']->render(array('class' => "input")) ?>
                                <?php if ($form['name']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['name']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Address1</th>
                            <td>
                                <?php echo $form['address1']->render() ?>
                                <?php if ($form['address1']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['address1']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Address2</th>
                            <td>
                                <?php echo $form['address2']->render() ?>
                                <?php if ($form['address2']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['address2']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Suburb</th>
                            <td>
                                <?php echo $form['suburb']->render() ?>
                                <?php if ($form['suburb']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['suburb']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>State</th>
                            <td>
                                <?php echo $form['state']->render(array('maxlength' => 3)) ?>
                                <?php if ($form['state']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['state']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Postcode</th>
                            <td>
                                <?php echo $form['postcode']->render(array('maxlength' => 6)) ?>
                                <?php if ($form['postcode']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['postcode']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>
                                <?php echo $form['phone']->render(array('maxlength' => 25)) ?>
                                <?php if ($form['phone']->hasError()): ?>
                                    <div class="form-error"><?php echo $form['phone']->getError() ?></div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div style="height: 20px;"></div>
    
    <input type="submit" name="save" value="Save" />
    <?php if ($user): ?>
    <input type="submit" name="delete" value="Delete" />
    <?php endif; ?>
    
</form>


<script>
    function copyDeliveryAddress()
    {
        $invoiceInputs = $('#invoice-form input, #invoice-form textarea');
        $('#delivery-form input, #delivery-form textarea').each(function (index, value) {
            $($invoiceInputs[index]).val($(value).val());
        });
    }
</script>