<?php

/**
 * sessions
 * 
 * @package    brakequip
 * @subpackage stats
 * @author     John Smythe
 */
class snapshotAction extends sfAction {

    public function execute($request)
    {
        $sort = $request->getParameter('sort', 'created_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        $q = Doctrine_Query::create()->from('Snapshot s')->innerJoin('s.SnapshotLog sl')->leftJoin('sl.Staff');
        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);
        
        $this->results = $q->execute();
        $this->totalResults = count($this->results);
    }

}