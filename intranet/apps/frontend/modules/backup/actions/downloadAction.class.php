<?php

/**
 * download
 * 
 * @package    brakequip
 * @subpackage backup
 * @author     John Smythe
 */
class downloadAction extends sfAction {

    public function execute($request)
    {
        $realpath = sfConfig::get('sf_root_dir').'/backups/'.$request->getParameter('file');
        $size = intval(sprintf("%u", filesize($realpath)));

        // Maybe the problem is Apache is trying to compress the output, so:
        @apache_setenv('no-gzip', 1);
        @ini_set('zlib.output_compression', 0);
        // Maybe the client doesn't know what to do with the output so send a bunch of these headers:
        header("Content-type: application/force-download");
        header('Content-Type: application/octet-stream');
        if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE") != false) {
          header("Content-Disposition: attachment; filename=" . urlencode(basename($realpath)) . ';');
        } else {
          header("Content-Disposition: attachment; filename=\"" . basename($realpath) . '";');
        }
        // Set the length so the browser can set the download timers
        header("Content-Length: " . $size);
        // If it's a large file we don't want the script to timeout, so:
        set_time_limit(300);
        // If it's a large file, readfile might not be able to do it in one go, so:
        $chunksize = 1 * (1024 * 1024); // how many bytes per chunk
        if ($size > $chunksize) {
          $handle = fopen($realpath, 'rb');
          $buffer = '';
          while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            echo $buffer;
            ob_flush();
            flush();
          }
          fclose($handle);
        } else {
          readfile($realpath);
        }
        exit;
    }

}