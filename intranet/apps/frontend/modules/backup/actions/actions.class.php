<?php

/**
 * backup actions.
 *
 * @package    brakequip
 * @subpackage backup
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class backupActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {

        $sort = $request->getParameter('sort', 'created_at'); // default sort field
        $dir = $request->getParameter('dir', 'asc'); // default direction
        
        $q = Doctrine_Query::create()->from('Backup');
        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);
        
        $this->results = $q->execute();
        $this->totalResults = count($this->results);
    }

}
