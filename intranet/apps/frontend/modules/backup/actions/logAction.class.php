<?php

/**
 * View update log
 * 
 * @package    brakequip
 * @subpackage website
 * @author     John Smythe
 */
class logAction extends sfAction {

    public function execute($request) 
    {
        $id = false;
        if ($request->getParameter('id', false)) {
            $id = $request->getParameter('id');
        }
        
        $q = Doctrine_Query::create()->from('Backup b')->where('id = ?', $id);
        if (!$id || !$this->backup = $q->fetchOne()) {
            $this->redirect('backup/index');
            exit;
        }
        
        
    }

}