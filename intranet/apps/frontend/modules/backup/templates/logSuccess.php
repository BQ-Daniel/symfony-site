<?php slot('title', 'Backup') ?>

<h2>View Log</h2>

<p><a href="javascript:history.go(-1);" class="button"><span>Go back</span></a></p>

<div class="cb" style="height: 10px;"></div>

Completed At: <b><?php echo date("d/m/y h:i:s A", strtotime($backup->getCreatedAt())) ?></b><br />

<div class="cb" style="height: 10px;"></div>

<div style="width: 800px; height: 500px; overflow: scroll; padding: 5px; border: 1px solid #ccc; line-height: 16px">
    <?php echo nl2br($backup->getLog()) ?>
</div>



