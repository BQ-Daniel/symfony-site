<?php slot('title', 'Backup') ?>

<h2>Backup</h2>

<?php include_partial('global/formMessages') ?>

    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="800">
            <tr>
                <th width="100"><?php echo sortable_link('Date', url_for('backup/index'), 'created_at') ?></th>
                <th width="140"><?php echo sortable_link('Data File', url_for('backup/index'), 'zip') ?></th>
                <th width="140"><?php echo sortable_link('DB File', url_for('backup/index'), 'db') ?></th>
                <th width="100"><?php echo sortable_link('FTP Upoad Status', url_for('backup/index'), 'ftp') ?></th>
                <th>Action</th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($results as $result): ?>
            <tr>
                <td><?php echo date('D jS M y', strtotime($result->getCreatedAt())) ?></td>
                <td><a href="<?php echo url_for('backup/download?file='.$result->getZip()) ?>"><?php echo $result->getZip() ?></a></td>
                <td><a href="<?php echo url_for('backup/download?file='.$result->getDb()) ?>"><?php echo $result->getDb() ?></a></td>
                <td><?php echo $result->getFtp() ?></td>
                <td><a href="<?php echo url_for('backup/log?id='.$result->getId()) ?>">View Log</a></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="5">No backups found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
