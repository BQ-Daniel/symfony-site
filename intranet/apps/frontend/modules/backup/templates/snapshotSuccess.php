<?php slot('title', 'Snapshots') ?>

<h2>List</h2>

<?php include_partial('global/formMessages') ?>

    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('Date', url_for('backup/snapshot'), 'created_at') ?></th>
                <th width="140"><?php echo sortable_link('Data File', url_for('backup/snapshot'), 'file') ?></th>
                <th>Action</th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php $changesLost = array() ?>
                <?php $i=0; foreach ($results as $result): $i++; ?>
            <tr>
                <td><?php echo date('D jS M y H:i:s', strtotime($result->getCreatedAt())) ?></td>
                <td><?php echo $result->getFile() ?></td>
                <td><a href="#" onclick="$(this).parent().parent().next().toggle(); return false;">View Log</a><?php if (count($changesLost) > 0): ?>, <a href="#" onclick="alert('Coming soon...'); return false;">Restore to this point</a><?php endif; ?></td>
            </tr>
            <tr style="display:none">
                <td colspan="3">
                    <?php if (count($changesLost) > 0): ?>
                    <p><strong>Changes that will be lost</strong></p>
                        <?php foreach ($changesLost as $line): ?><?php echo $line ?><?php endforeach; ?>
                    <?php endif; ?>
                    
                    
                    <?php if ($i == 1): ?><p><strong>Recent changes</strong></p><?php endif; ?>
                    
                    <?php $logs = $result->getSnapshotLog(); ?>
                    <?php foreach ($logs as $log): ?>
                    <?php $str = $log->getLog().' by <a href="'.url_for('admin/edit?id='.$log->getStaff()->getId()).'">'.$log->getStaff()->getName().'</a><br />'; ?>
                    <?php $changesLost[] = $str; ?>
                    <?php echo ($i == 1 ? $str : '') ?>
                    <?php endforeach; ?>
                </td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="5">No snapshots found in the last 10 days</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
