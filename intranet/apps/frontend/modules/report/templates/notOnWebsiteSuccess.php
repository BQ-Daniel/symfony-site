<?php slot('title', 'Reports') ?>

<style>
    a.reportLink {
      display:block;border-bottom:1px dotted #ccc;padding:5px 5px 5px 0;  
    }
    a.reportLink:hover {
        background:#f2f2f2;
    }
    .tab {
        padding:20px;
    }
</style>

<h2>Not On Website</h2>

<script>
    var docHeight = $(window).height();
    $(function() {
        var $tabs = $( "#tabs" ).tabs();
    });
</script>

<div id="tabs" style="width:1000px;">
    <ul>
        <li><a href="#tab1">Not In Categories</a></li>
        <li><a href="#tab2">No Illustration</a></li>
        <li><a href="#tab3">Website display turned off</a></li>
        <li><a href="#tab4">Hidden Categories</a></li>
        <li><a href="#tab5">Empty Categories</a></li>
        <li><a href="#tab6">Categories > 1 Part</a></li>
    </ul>
    <div id="tab1" class="tab">
        <h3>Parts not in Categories (<?php echo count($notInCategory) ?>)</h3><br />
        <div id="1" style="height:90%;overflow-y:scroll;line-height:22px;">
            <?php foreach ($notInCategory as $part): ?>
                <a href="<?php echo url_for('part/edit?id=' . $part['id']) ?>" class="reportLink"><?php echo $part['bq_number'] ?></a>
            <?php endforeach; ?>
        </div> 
    </div>
    <div id="tab2" class="tab">
        <h3>Parts with No Illustration (<?php echo count($noIllustration) ?>)</h3><br />
        <div id="2" style="overflow-y:scroll;line-height:22px;">
            <?php foreach ($noIllustration as $part): ?>
                <a href="<?php echo url_for('part/edit?id=' . $part['id']) ?>" class="reportLink"><?php echo $part['bq_number'] ?></a>
            <?php endforeach; ?>
        </div>
    </div>
    <div id="tab3" class="tab">
        <h3>Parts with website display turned off (<?php echo count($websiteDisplay) ?>)</h3><br />
        <div id="3" style="overflow-y:scroll;line-height:22px;">
            <?php foreach ($websiteDisplay as $part): ?>
                <a href="<?php echo url_for('part/edit?id=' . $part['id']) ?>" class="reportLink"><?php echo $part['bq_number'] ?></a>
            <?php endforeach; ?>
        </div>
    </div>
    <div id="tab4" class="tab">
        <h3>Hidden Categories (<?php echo count($hiddenCategories) ?>)</h3><br />
        <div id="4" style="overflow-y:scroll;line-height:22px;">
        <?php foreach ($hiddenCategories as $category): ?>
            <?php $categories = CategoryTable::getParentCategories($category['id']); ?>
            <a href="<?php echo url_for('category/index?id='.$category['id'].'&typeId=0') ?>" target="_blank" class="reportLink">
            <?php foreach ($categories as $result): ?><?php echo $result['name'] ?> > <?php endforeach; ?></a>
        <?php endforeach; ?>
        </div>
    </div>
    <div id="tab5" class="tab">
        <h3>Categories with No Parts (<?php echo count($categoriesNoPart) ?>)</h3><br />
        <div id="5" style="overflow-y:scroll;line-height:22px;">
        <?php foreach ($categoriesNoPart as $category): ?>
            <?php $categories = CategoryTable::getParentCategories($category['id']); ?>
            <a href="<?php echo url_for('category/index?id='.$category['id'].'&typeId=0') ?>" target="_blank" class="reportLink">
            <?php foreach ($categories as $result): ?><?php echo $result['name'] ?> > <?php endforeach; ?></a>
        <?php endforeach; ?>
        </div>
    </div>

    <div id="tab6" class="tab">
        <h3>Category with more than one Part (<?php echo count($categoriesMoreParts) ?>)</h3><br />
        <div id="6" style="overflow-y:scroll;line-height:22px;">
        <?php foreach ($categoriesMoreParts as $category): ?>
            <?php $category = CategoryTable::getInstance()->find($category['category_id']) ?>
            <a href="<?php echo url_for('category/index?id='.$category['id'].'&typeId=0') ?>" target="_blank" class="reportLink">
            <?php echo $category['name'] ?></a>
        <?php endforeach; ?>
        </div>
    </div>

</div>

<div class="cb"></div>

<script>
    $('#1, #2, #3, #4, #5, #6').height(docHeight - 300);
</script>