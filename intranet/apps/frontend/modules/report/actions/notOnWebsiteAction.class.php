<?php

/**
 * 
 * 
 * @package    brakequip
 * @subpackage illustration
 * @author     John Smythe
 */
class notOnWebsiteAction extends sfAction {

    public function execute($request)
    {
        // Get parts no in category
        $this->notInCategory = Doctrine_Query::create()->from('Part')->where('id NOT IN (select part_id from part_to_category)')->fetchArray();
        
        // Get parts that have no illustration
        $this->noIllustration = Doctrine_Query::create()->from('Part')->where("illustration_id is NULL or illustration_id = ''")->fetchArray();
        
        // Get parts where website_display = false
        $this->websiteDisplay = Doctrine_Query::create()->from('Part')->where("website_display = ?", false)->fetchArray();
        
        $this->categoriesNoPart = Doctrine_Query::create()->from('Category c')->where('sibling_category_type_id = 0 AND id NOT IN (select category_id from part_to_category)')->orderBy('sort_order ASC')->fetchArray();
        
        $this->hiddenCategories = Doctrine_Query::create()->from('Category c')->where('hidden = ?', true)->orderBy('sort_order ASC')->fetchArray();
        
        $q = Doctrine_Query::create()->select('count(*) as total, ptc.category_id')->from('PartToCategory ptc')->innerJoin('ptc.Part p')->having('total > 1')->groupBy('ptc.category_id');
        $this->categoriesMoreParts = $q->fetchArray();
    }

}