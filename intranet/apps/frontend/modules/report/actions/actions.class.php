<?php

/**
 * report actions.
 *
 * @package    brakequip
 * @subpackage report
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class reportActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

    }
    
    
    public function executeRemoveDuplicates()
    {
        $q = Doctrine_Query::create()->select('count(*) as total, ptc.category_id')->from('PartToCategory ptc')->having('total > 1')->groupBy('ptc.category_id, ptc.part_id');
        foreach ($q->execute() as $row)
        {
            MarkedForDeleteTable::markForDelete($row->getId(), 'part_to_category');
            $row->delete();
        }
        return sfView::NONE;
    }
}
