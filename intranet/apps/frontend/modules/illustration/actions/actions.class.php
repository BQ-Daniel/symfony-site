<?php

/**
 * illustration actions.
 *
 * @package    brakequip
 * @subpackage illustration
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class illustrationActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'updated_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        $this->num = $request->getParameter('num');
       
        $q = Doctrine_Query::create()->from('Illustration');

        if ($this->num) {
            $q->addWhere("UPPER(illustration_number) = '".strtoupper($this->num)."'");
        }

        // Add order by            
        $q->addOrderBy($sort . ' ' . $dir);

        $this->pager = new sfDoctrinePager('Illustration', sfConfig::get('app_results_per_page'));
        $this->pager->setQuery($q);
        $this->pager->setPage($page);
        $this->pager->init();

        $this->totalResults = $this->pager->getNbResults();
    }

}
