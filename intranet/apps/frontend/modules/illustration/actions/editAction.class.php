<?php

/**
 * edit user
 * 
 * @package    brakequip
 * @subpackage illustration
 * @author     John Smythe
 */
class editAction extends sfAction {

    public function execute($request) {

        $error_types = array(
            1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
            'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
            'The uploaded file was only partially uploaded.',
            'No file was uploaded.',
            6 => 'Missing a temporary folder.',
            'Failed to write file to disk.',
            'A PHP extension stopped the file upload.'
        );

        $this->illustration = false;

        if ($request->getParameter('id')) {
            $this->illustration = IllustrationTable::getInstance()->find($request->getParameter('id'));
            $oeResults = PartTable::getInstance()->findByIllustrationId($request->getParameter('id'));
            $this->totalParts = count($oeResults);
        }

        if ($request->getParameter('upload')) {
            // Make sure illustration number exists
            if (!$request->getParameter('illustration_number')) {
                $request->setParameter('error', 'Please specify an illustration number');
                return sfView::SUCCESS;
            }

            // Check file is uploaded
            if (empty($_FILES['illustration']['name'])) {
                $request->setParameter('error', 'Please upload a file');
                return sfView::SUCCESS;
            }

            // Make sure its a jpeg
            //if (!in_array($_FILES['illustration']['type'], array('image/jpeg', 'image/pjpeg'))) 
            if (in_array($_FILES['illustration']['type'], array('image/jpeg', 'image/pjpeg'))) {
                $imgExt = ".jpg";
            } elseif (in_array($_FILES['illustration']['type'], array('image/svg+xml'))) {
                $imgExt = ".svg";
            } else {
                $request->setParameter('error', 'File has to be a JPEG or SVG');
                return sfView::SUCCESS;
            }

            // Is there an array
            if ($_FILES['illustration']['error'] > 0) {
                $request->setParameter('error', $error_types[$_FILES['illustration']['error']]);
                return sfView::SUCCESS;
            }

            // If file exists, delete and upload
            $illNumber = $request->getParameter('illustration_number');
            $fileName = sfConfig::get('app_illustration_dir') . '/' . $illNumber . $imgExt;
            $thumbFileName = sfConfig::get('app_illustration_dir') . '/' . $illNumber . '_s' . $imgExt;

            if ($this->illustration) {
                if (file_exists($fileName)) {
                    @unlink($fileName);
                    @unlink($thumbFileName);
                }
            }

            if (!move_uploaded_file($_FILES['illustration']['tmp_name'], $fileName)) {
                $request->setParameter('error', 'Problem when moving uploaded file');
                return sfView::SUCCESS;
            }


            sfContext::getInstance()->getConfiguration()->loadHelpers('General');

            // Now to resize and save
            // Create large image
            //$img = new sfImage($fileName, $_FILES['illustration']['type']);
            //$img->thumbnail(sfConfig::get('app_illustration_width'), sfConfig::get('app_illustration_height'), 'scale');
            //$img->saveAs($fileName);

            $img = new SimpleImage();
            $img->load($fileName);
            $img->resizeToWidth(sfConfig::get('app_illustration_width'));
            $img->save($fileName);


            // Create larger image but resized
            //$img = new sfImage($fileName, $_FILES['illustration']['type']);
            //$img->thumbnail(sfConfig::get('app_illustration_thumb_width'), sfConfig::get('app_illustration_thumb_height'), 'scale');
            //$img->saveAs($thumbFileName);
            $img = new SimpleImage();
            $img->load($fileName);
            $img->resizeToWidth(sfConfig::get('app_illustration_thumb_width'));
            $img->save($thumbFileName);

            // If illustration doesn't exist, add to database
            if (!$this->illustration) {
                $this->illustration = new Illustration();
                $this->illustration->setIllustrationNumber($illNumber);

                $this->totalParts = 0;
            }
            $this->illustration->setUpdatedAt(date('Y-m-d H:i:s'));
            $this->illustration->save();

            SnapshotLogTable::add('Uploaded Illustration: ' . $this->illustration->illustration_number, 'illustration', $this->illustration->id);

            $request->setParameter('success', 'Successfully saved illustration');
        }

        // If they wish to delete illustration
        if ($request->getParameter('delete')) {
            IllustrationTable::removeFromSystem($this->illustration->getId());


            $this->getUser()->setFlash('success', 'Successfully removed illustration ' . $illustrationNumber);
            $this->redirect('illustration/index');
        }
    }

}
