<?php slot('title', 'Parts') ?>

<h2 class="fl">Illustrations</h2>

<?php include_partial('illustration/subnav') ?>

<?php include_partial('global/formMessages') ?>

<div class="filter" style="width: 400px;">
    <form action="<?php echo url_for('illustration/index') ?>" method="post">
        <table cellspacing="0" cellpadding="0" border="0">
            <tr>
                <td width="150">Illustration Number</td>
                <td><input type="text" name="num" value="<?php echo $num ?>" class="input" /></td>
                <td><input type="submit" name="search" value="Search" />
            </tr>
        </table>
    </form>
</div>

<div style="height: 30px"></div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
    <div class="list-table">
        <table cellspacing="0" cellpadding="0" border="0" width="600">
            <tr>
                <th width="150"><?php echo sortable_link('Illustration Number', url_for('illustration/index'), 'illustration_number') ?></th>
                <th><?php echo sortable_link('Updated', url_for('illustration/index'), 'updated_at') ?></th>
            </tr>
            <?php if ($totalResults > 0): ?>
                <?php foreach ($pager->getResults() as $result): ?>
            <tr>
                <td><a href="<?php echo url_for('illustration/edit?id='.$result->getId()) ?>"><?php echo $result->getIllustrationNumber() ?></a></td>
                <td><?php echo ($result->getUpdatedAt() != '' ? date('D jS M y', strtotime($result->getUpdatedAt())) : 'Never') ?></td>
            </tr>
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="2">No results found</td>
            </tr>
            <?php endif; ?>
        </table>
    </div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>