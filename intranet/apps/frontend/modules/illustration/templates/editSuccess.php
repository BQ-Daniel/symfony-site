<?php
slot('title', 'Parts');

$path = "";

if ($illustration) {
    $path = "uploads/illustrations/" . $illustration->getIllustrationNumber();
    if (file_exists($path . ".svg")) {
        $path .= ".svg";
    } else {
        $path .= "_s.jpg";
    }
    
    $path = "/" . $path;
}
?>

<h2 class="fl">Illustrations</h2>

<?php include_partial('illustration/subnav') ?>
<?php if ($illustration): ?>
    <h3>Illustration Number: <?php echo $illustration->getIllustrationNumber() ?></h3>
    <?php if ($totalParts > 0): ?>
        <p>This illustration is used in <?php echo $totalParts ?> part number<?php echo ($totalParts > 1 ? 's' : '') ?></p>
    <?php else: ?>
        <p>This illustration is not used in any part numbers</p>
    <?php endif; ?>
<?php endif; ?>
<?php include_partial('global/formMessages') ?>
<div class="cb"></div>
<p><img src="<?php echo $path ?>?<?php echo time() + rand(1, 9999999999) ?>" style="-moz-border-radius: 5px; border: 5px solid #e5e5e5; max-width: 100%; height: auto;" /></p>
<form action="<?php echo url_for('illustration/edit') ?>" method="post" enctype="multipart/form-data">
    <table cellspacing="0" cellpadding="0" border="0">
        <?php if ($illustration): ?>
            <input type="hidden" name="id" value="<?php echo $illustration->getId() ?>" />
            <input type="hidden" name="illustration_number" value="<?php echo $illustration->getIllustrationNumber() ?>" />
        <?php else: ?>
            <tr>
                <td width="150">Illustration Number</td>
                <td><input type="text" name="illustration_number" class="input small" /></td>
            </tr>
        <?php endif; ?>
        <tr>
            <td width="150">Upload New Image</td>
            <td><input type="file" name="illustration" /> <input type="submit" name="upload" value="Upload" /></td>
        </tr>
    </table>

    <?php if ($illustration): ?>
        <div style="height: 20px; margin-bottom: 20px; border-bottom:1px dotted #ccc; width: 600px;"></div>
        <input type="submit" name="delete" value="Delete Illustration" onclick="return confirm('Are you sure you want to delete this illustration?')"/>
    <?php endif; ?>
</form>

