<?php

/**
 * users actions.
 *
 * @package    brakequip
 * @subpackage users
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class usersActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->form = $form = new SearchUsersForm();
        $this->page = $page = $request->getParameter('page', 1);
        $sort = $request->getParameter('sort', 'last_login'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        
        if (empty($sort)) {
            $sort = 'last_login';
            $dir = 'desc';
        }
        
        $parameters = $request->getParameter($this->form->getName());

        if ($request->getParameter('page', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }
        }
        $this->form->bind($parameters);
        
        if ($request->getParameter('submit') && $this->form->isValid() || $request->getParameter('page', null))
        {   
            $values = $this->form->getValues();
            $q = Doctrine_Query::create()->from('USERS u');
            
            // Where clause
            $fromDate = $values['from_date'];
            if (checkdate($fromDate['month'], $fromDate['day'], $fromDate['year'])) {
                $q->addWhere('last_login >= ?', $fromDate['year'].'-'.$fromDate['month'].'-'.$fromDate['day']);
            }
            $toDate = $values['to_date'];
            if (checkdate($toDate['month'], $toDate['day'], $toDate['year'])) {
                $q->addWhere('last_login <= ?', $toDate['year'].'-'.$toDate['month'].'-'.$toDate['day']);
            }
            
            if (!empty($values['username'])) {
                $q->addWhere("UPPER(username) LIKE '%".strtoupper($values['username'])."%'");
            }
            else {
                $q->addWhere('deleted = 0');
            }
            if (!empty($values['email'])) {
                $q->addWhere('email = ?', $values['email']);
            }
            
            // Add order by            
            $q->addOrderBy($sort.' '.$dir);

            $this->pager = new sfDoctrinePager('USERS', sfConfig::get('app_results_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($page);
            $this->pager->init();
            $this->sort = $sort;
            $this->dir = $dir;
            
            // Set in session to store on next page
            $this->getUser()->setParameter('search_parameters', serialize($parameters));
            
            $this->totalResults = $this->pager->getNbResults(); 
        }
    }

}
