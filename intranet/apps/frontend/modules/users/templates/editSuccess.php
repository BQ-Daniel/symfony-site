<?php 

slot('title', 'Website') ?>

<h2 class="fl">Users</h2>

<?php include_partial('users/subnav') ?>

<?php if ($sf_params->get('success')): ?>
<div class="success">
    <?php echo $sf_params->get('success') ?>
</div>
<?php endif; ?>

<?php if ($form->hasErrors() || $deliveryAddressForm->hasErrors() || $invoiceAddressForm->hasErrors()): ?>
<div class="error">
    Please fix the errors below.
</div>
<?php endif; ?>

<form action="<?php echo url_for('users/edit') ?>" method="post">
    
    <?php if (isset($user)): ?>
    <input type="hidden" name="id" value="<?php echo $user->getId() ?>" />
    <?php endif; ?>
    

    
    
    <script>
        $(function() {
            var $tabs = $( "#tabs" ).tabs();
            <?php if (!$deliveryAddressForm->hasErrors() && $invoiceAddressForm->hasErrors()): ?>
            $tabs.tabs('select', 1);
            <?php endif; ?>
        });
    </script>
    
     
     <div class="box-container">
        <table cellspacing="0" cellpadding="5" border="0" class="box-top-container">
               <tr valign="top">
                   <th width="120">Username</th>
                   <td style="padding-right: 15px;">
                       <?php echo $form['username']->render(array('class' => "input")) ?>
                       <?php echo $form['org_username']->render() ?>
                       <?php if ($form['username']->hasError()): ?>
                           <div class="form-error"><?php echo $form['username']->getError() ?></div>
                       <?php endif; ?>
                   </td>
                   <th>Email Address</th>
                   <td>
                       <?php echo $form['email']->render(array('class' => "input")) ?>
                       <?php echo $form['org_email']->render() ?>
                       <?php if ($form['email']->hasError()): ?>
                        <div class="form-error"><?php echo $form['email']->getError() ?></div>
                       <?php endif; ?>
                   </td>
                        
               </tr>
        </table>
       <div style="width: 420px" class="box-left">
           <div id="tabs">
               <ul>
                   <li><a href="#tabs-1">Delivery Address</a><?php echo ($deliveryAddressForm->hasErrors() ? image_tag('error_icon.png', 'class="error-icon"') : '') ?></li>
                   <li><a href="#tabs-2">Invoice Address</a><?php echo ($invoiceAddressForm->hasErrors() ? image_tag('error_icon.png', 'class="error-icon"') : '') ?></li>
               </ul>
               <div id="tabs-1">
                   <div id="delivery-form" class="form">
                       
                       <?php $xform = $deliveryAddressForm ?>
                    
                       <table cellspacing="0" cellpadding="5" border="0">
                           <tr>
                               <th>Name</th>
                               <td>
                                   <?php echo $xform['name']->render(array('class' => "input")) ?>
                                   <?php if ($xform['name']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['name']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Address1</th>
                               <td>
                                   <?php echo $xform['address1']->render() ?>
                                   <?php if ($xform['address1']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['address1']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Address2</th>
                               <td>
                                   <?php echo $xform['address2']->render() ?>
                                   <?php if ($xform['address2']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['address2']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Suburb</th>
                               <td>
                                   <?php echo $xform['suburb']->render() ?>
                                   <?php if ($xform['suburb']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['suburb']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>State</th>
                               <td>
                                   <?php echo $xform['state']->render() ?>
                                   <?php if ($xform['state']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['state']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Postcode</th>
                               <td>
                                   <?php echo $xform['postcode']->render() ?>
                                   <?php if ($xform['postcode']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['postcode']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Phone</th>
                               <td>
                                   <?php echo $xform['phone']->render() ?>
                                   <?php if ($xform['phone']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['phone']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                       </table>
                   </div>
               </div>
               <div id="tabs-2">
                   <div id="invoice-form" class="form">
                      
                       <?php $xform = $invoiceAddressForm; ?>
                       <table cellspacing="0" cellpadding="5" border="0">
                           <tr>
                               <th>Name</th>
                               <td>
                                   <?php echo $xform['name']->render(array('class' => "input")) ?>
                                   <?php if ($xform['name']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['name']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Address1</th>
                               <td>
                                   <?php echo $xform['address1']->render() ?>
                                   <?php if ($xform['address1']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['address1']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Address2</th>
                               <td>
                                   <?php echo $xform['address2']->render() ?>
                                   <?php if ($xform['address2']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['address2']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Suburb</th>
                               <td>
                                   <?php echo $xform['suburb']->render() ?>
                                   <?php if ($xform['suburb']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['suburb']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>State</th>
                               <td>
                                   <?php echo $xform['state']->render(array('maxlength' => 3)) ?>
                                   <?php if ($xform['state']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['state']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Postcode</th>
                               <td>
                                   <?php echo $xform['postcode']->render(array('maxlength' => 6)) ?>
                                   <?php if ($xform['postcode']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['postcode']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                               <th>Phone</th>
                               <td>
                                   <?php echo $xform['phone']->render(array('maxlength' => 25)) ?>
                                   <?php if ($xform['phone']->hasError()): ?>
                                       <div class="form-error"><?php echo $xform['phone']->getError() ?></div>
                                   <?php endif; ?>
                               </td>
                           </tr>
                           <tr>
                            <td></td>
                            <td> <a href="" onclick="copyDeliveryAddress(); return false;" class="button fl"><span style="color: #fff;">Copy Delivery Address</span></a></td>
                           </tr>
                       </table>
                   </div>
               </div>
           </div>
       </div>
       <div class="box-left-margin tabs custom-tab  ui-widget ui-widget-content ui-corner-all fl-right">
           <div class="tab-heading ui-widget-header ui-corner-all">Details</div>
           <div class="tab-content">
            <table cellspacing="0" cellpadding="5" border="0">
                 <tr>
                 <th>Website</th>
                    <td>
                        <?php echo $form['website']->render(array('class' => "input")) ?>
                        <?php echo $form['org_website']->render() ?>
                        <?php if ($form['website']->hasError()): ?>
                         <div class="form-error"><?php echo $form['website']->getError() ?></div>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr valign="top">
                    <th>Catalogue Limit</th>
                     <td>
                        <?php echo $form['threshold']->render(array('class' => "input small-input-box")) ?>
                        <?php echo $form['org_threshold']->render() ?>
                        <?php if ($form['threshold']->hasError()): ?>
                         <div class="form-error"><?php echo $form['threshold']->getError() ?></div>
                        <?php endif; ?>
                    </td>
                         
                </tr>
                <tr valign="top">
                    <th>Price Code</th>
                     <td>
					 <?php if(isset($price_code) && $price_code != ''):?>
                        <?php echo $form['price_code']->render(array('class' => "input small-input-box", 'value' => $price_code)) ?>
						<?php else: ?>
						 <?php echo $form['price_code']->render(array('class' => "input small-input-box")) ?>
						 <?php endif;?>
                        <?php if ($form['price_code']->hasError()): ?>
                         <div class="form-error"><?php echo $form['price_code']->getError() ?></div>
                        <?php endif; ?>
                    </td>
                         
                </tr>
                
               
                
                 <tr valign="top">
                    <td colspan="3" class="input-radio-td">
                     <?php echo $form['order_access']->render(array('disabled'=>$orderAccess)) ?> Order Access &nbsp;
                      </td>
                </tr>
                  <tr valign="top">
                    <td colspan="3" class="input-radio-td">
                    <?php echo $form['distributor']->render() ?> Show on find a manufacturers search page &nbsp;
                    </td>
                </tr>
                 <tr valign="top">
                    <td colspan="3" class="input-radio-td">
                     <?php echo $form['stop_credit']->render() ?> Stop Credit &nbsp;
                       </td>
                </tr>
                <tr valign="top">
                    <td colspan="3" class="input-radio-td">
                     <?php echo $form['order_no_req']->render() ?> Require Order Number &nbsp;
                    </td>
                </tr>
                 <tr valign="top">
                    <td colspan="3" class="input-radio-td">
                     <?php echo $form['deleted']->render() ?> Deleted
                    </td>
                </tr>
            </table>
           </div>
       </div>
       
       <br clear="all" />
       
       <div style="height: 20px;"></div>
     </div> 
    
    <input type="submit" name="save" value="Save" />
    <?php if ($user): ?>
    <input type="submit" name="delete" value="Delete" />
    <?php endif; ?>
    
</form>


<script>
    function copyDeliveryAddress()
    {
        $invoiceInputs = $('#invoice-form input, #invoice-form textarea');
        $('#delivery-form input, #delivery-form textarea').each(function (index, value) {
            $($invoiceInputs[index]).val($(value).val());
        });
    }
	$(function(){
	var uname = 0;
	var price_code =0;
	
		$("#users_price_code").keyup(function(){
			if($(this).val() == 'e' || $(this).val() == 'E')
				{
					price_code = 1;
				}
			else
				{
					price_code = 0;
				}
				if(price_code == 1 || uname == 1)
					{
						$("#users_distributor").attr('disabled', 'disabled');
						$("#users_distributor").removeAttr('checked');
					}
				else
					{
						$("#users_distributor").removeAttr('disabled');
					}
		});
		if($("#users_price_code").val() == 'e' || $("#users_price_code").val() == 'E')
			{
				price_code = 1;
			}
		else
			{
				price_code = 0;
			}	
		$("#users_username").keyup(function(){
			if(isNaN($(this).val()) == true)
				{
					uname = 1;
				}
			else
				{
					uname =0;
				}
			if(price_code == 1 || uname == 1)
					{
						$("#users_distributor").attr('disabled', 'disabled');
						$("#users_distributor").removeAttr('checked');
					}
				else
					{
						$("#users_distributor").removeAttr('disabled');
					}	
		});
		if(isNaN($("#users_username").val()) == true)
			{
				uname = 1;
			}
		else
			{
				uname =0;
			}		
		if(price_code == 1 || uname == 1)
			{
				$("#users_distributor").attr('disabled', 'disabled');
				$("#users_distributor").removeAttr('checked');
			}
		else
			{
				$("#users_distributor").removeAttr('disabled');
			}	
			
		$("#users_username").keyup(function(){
			if(isNaN($(this).val()))
				{
					$.ajax({
						url: 'http://brakequip.com.au/intranet/checkuser.php',
						data: {username: $(this).val().slice(0, -1)},
						type: 'GET',
						dataType: 'JSON',
						success: function(data){
						data = JSON.parse(data);
							if(data.code==1)
								$("#users_price_code").val(data.price_code);
						},
						error: function(error){
							console(error);
						}
					});
				}
		
		});
	});
</script>
