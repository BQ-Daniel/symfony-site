<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="title" content="BrakeQuip Intranet" />
        <title>BrakeQuip Intranet</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" type="text/css" media="screen" href="/css/login.css" />
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="/js/global.js"></script>
    </head>

    <body> 
        <div id="container">
            <div id="inner-container">
                <div id="login-panel">
                    <div class="content">
                        <?php if ($error): ?>
                            <span class="error"><?php echo $error ?></span>
                        <?php else: ?>
                            Please enter your staff username and password below
                        <?php endif; ?>
                        <form action="<?php echo url_for('login/index') ?>" method="post">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <th><?php echo $form['username']->renderLabel() ?></th>
                                    <td><?php echo $form['username']->render() ?> <span class="error"><?php echo $form['username']->getError() ?></span></td>
                                </tr> 
                                <tr>
                                    <th><?php echo $form['password']->renderLabel() ?></th>
                                    <td><?php echo $form['password']->render() ?> <span class="error"><?php echo $form['password']->getError() ?></span> <input type="submit" name="submit" value="Login" class="button" style="margin-left: 15px; margin-top: -3px;" /></td>
                                </tr> 
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>