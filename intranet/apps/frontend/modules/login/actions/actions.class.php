<?php

/**
 * login actions.
 *
 * @package    brakequip
 * @subpackage login
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class loginActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->error = null;
        
        $this->setLayout(false);
        $this->form = new LoginForm();
        
        
        if ($request->isMethod('post'))
        {
          $this->form->bind($request->getParameter('login'));
          if ($this->form->isValid())
          {
            $user = StaffTable::getInstance()->findOneByUsernameAndPassword($this->form->getValue('username'), sha1($this->form->getValue('password')));
            if (!$user) {
                $this->error = "Invalid username and/or password";
                return sfView::SUCCESS;
            }
            $user->setLastLogin(date('Y-m-d h:i:s'));
            $user->save();
            
            $this->getUser()->setAttribute('staffId', $user->getId());
            $this->getUser()->setAttribute('username', $user->getUsername());
            $this->getUser()->setAttribute('websiteUsername', $user->getWebsiteUsername());
            $this->getUser()->setAttribute('level', $user->getLevel());
            $this->getUser()->setAuthenticated(true);      
            $this->getUser()->setFlash('loginSuccess', true);
            $this->redirect('home/index');
          }
        }
    }

}
