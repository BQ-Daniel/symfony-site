<?php slot('title', 'Categories') ?>
    <h2>Delete Category</h2>
    <h3><?php echo $category->getName() ?></h3>
    
    <?php if ($sf_params->get('go')): ?>
    <div style="height:10px;clear:both;"></div>
    <div class="success">Category has successfully been removed</div>
    
    <p><a href="<?php echo url_for('category/index?id='.$category->getParentCategoryId().'&typeId='.$category->getCategoryTypeId()) ?>">Back to Categories</a></p>
    
    <?php else: ?>
    <p>Move categories and products to other categories.</p>
    
    <div style="height: 20px;"></div>

    <div class="list-table">
        <form action="<?php echo url_for('category/deleteAndMove') ?>" method="post">
            <input type="hidden" name="go" value="go" />
            <input type="hidden" name="id" value="<?php echo $category->getId() ?>" />
            <table cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <th style="min-width: 300px;">Category</th>
                    <th style="min-width: 100px;">Move to</th>
                </thead>
                <tfoot>

                </tfoot>
                <tbody>
                    <?php if (count($categories) > 0): ?>
                    <?php foreach ($categories as $sibling): ?>
                    <tr>
                        <td><?php echo $sibling->getName() ?></td>
                        <td>
                            <select name="move[<?php echo $sibling->getId() ?>]" style="min-width: 100px;">
                                <option value="">Delete</option>
                                <?php foreach ($otherCategories as $otherCategory): ?>
                                <option value="<?php echo $otherCategory->getId() ?>"><?php echo $otherCategory->getName() ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="2">There are no categories to move to.</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>

            <?php if (count($categories) > 0): ?>
            <p><a href="<?php echo url_for('category/deleteAndMove') ?>" class="button submit"><span>Delete and Move</span></a></p>
            <?php else: ?>
            <p><a href="<?php echo url_for('category/deleteAndMove') ?>" class="button submit"><span>Delete</span></a></p>
            <?php endif; ?>
        </form>
    </div>
    <?php endif; ?>