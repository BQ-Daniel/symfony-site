<?php
/* 
 * This is an ajax request
 * The following HTML will be insert into the category list
 */
?>
                <li id="category-<?php echo $category->getId() ?>">
                    <input type="hidden" name="sort[]" value="<?php echo $category->getId() ?>" />
                    <div class="li">    
                        <span class="icon" title="Drag to change order"></span>    
                        <div class="content"><span class="category"><a href="<?php echo url_for('category/index?id=' . $category->getId().'&typeId='.$categoryTypeId) ?>"><?php echo $category->getName() ?></a></span></div>
                        <div class="cb"></div>    
                    </div>    
                </li>     