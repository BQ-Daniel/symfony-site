<?php slot('title', 'Categories') ?>

<h2>Move Category</h2>

<p>The category we are moving is: <b id="category-name"><?php echo $category->getName() ?></b></p>

<?php include_component('part', 'breadcrumbs', 
        array('categoryId' => $categoryId,
              'url' => '/category/moveCategory',
              'params' => 'id=' . $category->getId(),
              'links' => true)) ?>



<div style="height: 20px;"></div>
    <form action="<?php echo url_for('category/moveCategory') ?>" method="post">
<input type="hidden" name="id" value="<?php echo $category->getId() ?>" />
<input type="hidden" name="categoryId" value="<?php echo $categoryId ?>" />
<?php if ($categoryId > 0): ?>
<input type="submit" name="move_category" value="Move category under category selected above" class="button" onclick="return confirm('Are you sure you want to move ' + $('#category-name').text() + ' and all it\'s siblings under ' + $('.breadcrumb li:last').text() +'?');"/>
<?php else: ?>  
<input type="submit" name="move_category" value="Move category under Make" class="button" onclick="return confirm('Are you sure you want to move ' + $('#category-name').text() + ' and all it\'s siblings under the parent category type Make?');"/>
<?php endif; ?>    
    </form>
<div style="height: 20px; border-bottom: 1px dotted #999; width: 500px; margin-bottom: 20px;"></div>

<form action="<?php echo url_for('category/moveCategory') ?>" method="get">
<?php if ($categoryType): ?>
<h2 style="font-size: 18px; float: none">Select <?php echo $categoryType->getName() ?></h2>
    <input type="hidden" name="id" value="<?php echo $category->getId() ?>" />
    <select id="category-select" name="categoryId" onchange="this.form.submit()">
        <option value="">Please select</option>
        <?php foreach ($categories as $result): ?>
        <?php if ($result->getId() == $category->getId()) continue; ?>
        <option value="<?php echo $result->getId() ?>"><?php echo $result->getName() ?></option>
        <?php endforeach; ?>
    </select>
<?php endif; ?>
</form>
