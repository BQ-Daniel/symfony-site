<?php if (count($breadcrumbs) > 0): ?>
<ul class="breadcrumb">
    <li><?php echo link_to('Home', 'category/index') ?></li>
    <?php foreach ($breadcrumbs as $breadcrumb): ?>
    <li class="crumb" id="category-<?php echo $breadcrumb['id'] ?>"><?php echo link_to($breadcrumb['name'], 'category/index?id='.$breadcrumb['id'].'&typeId='.$breadcrumb['type']) ?></li>
    <?php endforeach; ?>
</ul>
<div class="cb"></div>
<?php endif; ?>