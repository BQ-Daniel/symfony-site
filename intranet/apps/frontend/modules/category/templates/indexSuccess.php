<?php slot('title', 'Categories') ?>
    
    <?php if ($categoryType): ?>
    
<div id="categories">
    
    <h2><?php echo $categoryType->getName() ?></h2> <a onclick="return openChangeCategoryType(this);" href="" class="<?php echo $sf_params->get('id', 0) ?>" alt="<?php echo $categoryType->getName() ?>" onclick="displayCreateCategory(); return false;">change category type</a> / <a href="" onclick="displayCreateCategory(); return false;">create category</a> | <input id="sortMode" type="radio" name="mode" value="sort" checked /> sort mode, <input id="moveMode" type="radio" name="mode" value="move" /> move mode
    <?php include_component('category', 'breadcrumbs', array('categoryId' => $sf_params->get('id', 0))) ?>
    
    <div style="height: 10px;"></div>

    <form action="" method="post">
        <input type="hidden" name="parent_category_id" value="<?php echo $sf_params->get('id', 0) ?>" />
        <input type="hidden" name="category_type_id" id="category-type-id" value="<?php echo $categoryType->getId() ?>" />
        <input type="hidden" name="org_category_type" id="category-type" value="<?php echo $categoryType->getName() ?>" />
        <input type="hidden" name="sibling_category_type" id="sibling-category-type" value="<?php echo ($siblingCategoryType ? $siblingCategoryType->getName() : '') ?>" />

        <ul id="sortable-categories">
            <?php foreach ($categories as $category): ?>
                <li id="category-<?php echo $category->getId() ?>">
                    <input type="hidden" name="sort[]" value="<?php echo $category->getId() ?>" />
                    <div class="li<?php echo $category->getHidden() ? ' hidden' : '' ?>">    
                        <span class="icon" title="Drag to change order"></span>    
                        <div class="content"><span class="category"><a href="<?php echo url_for('category/index?id=' . $category->getId().'&typeId='.$category->getCategoryType()->getId()) ?>"><?php echo $category->getName() ?></a></span></div>
                        <div class="cb"></div>    
                    </div>    
                </li>    
            <?php endforeach; ?>
        </ul>

    </form>

    <div id="action-icons">
        <?php echo image_tag('icon_sml_add.png', array('class' => 'add-category', 'title' => 'Add category')) ?> &nbsp;
        <?php echo image_tag('icon_sml_edit.png', array('class' => 'edit-category', 'title' => 'Edit category title')) ?> &nbsp; 
        <?php echo image_tag('icon_sml_move.png', array('class' => 'move-category', 'title' => 'Move category')) ?> &nbsp; 
        <?php echo image_tag('icon_sml_delete.png', array('class' => 'delete-category', 'title' => 'Delete category')) ?> &nbsp; 
        <?php echo image_tag('icon_sml_status_hidden.png', array('class' => 'unhide-category', 'title' => 'Unhide category', 'style' => 'display:none')) ?>
        <?php echo image_tag('icon_sml_status.png', array('class' => 'hide-category', 'title' => 'Hide category', 'style' => 'display:none')) ?>
    </div>

    <!-- Templates -->
    <div id="add-category-form">
        <div class="cb"></div>
        <div class="add-category-form">
            <input type="hidden" name="sort[]" value="" />
            <input type="text" name="category" value="Enter category" style="width: 230px;" class="input default" /><br />
            <input type="text" name="category_type" value="Enter category type" style="width: 230px" class="input default" /><br /><br />
            <input type="submit" name="add_category" value="Save" class="sml-button" />
        </div>
    </div>
    
    <div id="dialog-message" title="Delete Category" style="display: none; width: 400px;">
            <p>
                    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                    Are you sure you wish to delete this category?
            </p>
    </div>
</div>
    <?php else: ?>
    <h2>Products</h2>
    <?php include_component('category', 'breadcrumbs', array('categoryId' => $sf_params->get('id', 0))) ?>
    
    <p>Products will be stored under this category.</p>
    
    <?php $products = Doctrine_Query::create()->from('PartToCategory ptc')->innerJoin('ptc.Part p')->where('category_id = ?', $sf_params->get('id', 0))->fetchArray(); ?>
    <?php foreach ($products as $part): ?>
    <a href="<?php echo url_for('part/edit?id='.$part['Part']['id']) ?>"><?php echo $part['Part']['bq_number'] ?></a><br /> 
    <?php endforeach; ?>
    
    <p><b>If you wish you can convert this to a category, enter the category type this will be below</b></p>
    
    <form action="<?php echo url_for('category/addCategoryType') ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $sf_params->get('id') ?>" />
        <input type="hidden" name="typeId" value="<?php echo $sf_params->get('typeId') ?>" />
        <table>
            <tr>
                <td>Category Type:</td>
                <td><input id="category-type-name" type="text" name="category_type_name" value="" class="input" /></td>
                <td><a class="button submit" href=""><span>Save</span></a></td>
            </tr>
        </table>
    </form>
    <script>
    // Attach autocomplete function
    $('#category-type-name').autocomplete({
        source: 'category/categoryTypes',
        minLength: 2 
    });
    </script>
    <?php endif; ?>

        <div id="change-category-type" title="Change Category Type">
            <strong style="font-size:16px; display:block; margin-bottom:5px;" id="current-category-type"></strong>
            Enter new category type:
            <form id="changeCategoryTypeForm" action="<?php echo url_for('category/changeCategoryType') ?>" method="post" onsubmit="submitCategoryTypeChange(); return false;">
                <input type="hidden" name="categoryId" value="<?php echo $sf_params->get('id', 0) ?>" />
                <input type="text" id="category-type-name2" name="category_type" value="" style="width: 230px" class="input default" />
            </form>
        </div>
    
    <script>
        $("#change-category-type").dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
             buttons: {
                "Cancel": function () {
                   $("#change-category-type").dialog('close'); 
                },
                "Change": function() {
                    submitCategoryTypeChange();
                },
            }
        });
        
            function openChangeCategoryType(el) {
               $('#current-category-type').html($(el).attr('alt')); 
               $("#change-category-type").dialog('open');
               return false;
            }
            
            function submitCategoryTypeChange() {
                if ($('#category-type-name2').val() == '') {
                    alert('Please enter a category type');
                    return false;
                }
                $.post('<?php echo url_for('category/changeCategoryType') ?>', $('#changeCategoryTypeForm').serialize(), function (data) {
                    data = $.parseJSON(data);
                    location.href = '/category?id=<?php echo $sf_params->get('id') ?>&typeId=' + data.id;
                });
            }
           
            
     $('#category-type-name2').autocomplete({
        source: 'category/categoryTypes',
        minLength: 2 
    });
    </script>
