<?php

/**
 * update status
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class updateStatusAction extends sfAction {
    
    public function execute($request)
    {
        $categoryId = $request->getParameter('categoryId');
        $hidden = $request->getParameter('hidden');
        
        $values = array('hidden' => $hidden, 'updated_at' => date('Y-m-d H:i:s'));
        Doctrine_Query::create()->update('Category')->set($values)->where('id = ?', $categoryId)->execute();
        
        return sfView::NONE;
    }
}