<?php

/**
 * category actions.
 *
 * @package    brakequip
 * @subpackage category
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class categoryActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $categoryId = $request->getParameter('id', null);
        
        if ($categoryId) {
            $this->parentCategory = CategoryTable::getInstance()->find($categoryId);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => $categoryId));
            $this->categoryType = CategoryTypeTable::getInstance()->find($this->parentCategory->getSiblingCategoryTypeId());
        }
        else {
            // Get first level of category type
            $this->categoryType = CategoryTypeTable::getInstance()->find(1);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => 0));
        }
        
        // Get the next category type down from this one
        // This is for guessing which category type they might want to use when they add a new category
        if ($this->categoryType) {
            $this->siblingCategoryType = CategoryTypeTable::getInstance()->findOneByParentCategoryTypeId($this->categoryType->getId());
        }
        
        
    }

}
