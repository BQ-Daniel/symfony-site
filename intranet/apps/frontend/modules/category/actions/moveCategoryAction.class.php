<?php

/**
 * move category action
 * 
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class moveCategoryAction extends sfAction {
    
    public function execute($request)
    {
        if (!$id = $request->getParameter('id')) {
            $this->redirect('category/index');
            exit;
        }
        $this->category = $category =  CategoryTable::getInstance()->find($id);
        
        $this->categoryId = $categoryId = $request->getParameter('categoryId', 0);        
        if ($categoryId) {
            $this->parentCategory = CategoryTable::getInstance()->find($categoryId);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => $categoryId));
            $this->categoryType = CategoryTypeTable::getInstance()->find($this->parentCategory->getSiblingCategoryTypeId());
        } else {
            // Get first level of category type
            $this->categoryType = CategoryTypeTable::getInstance()->find(1);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => 0));
        }
        
        
        // If they wish to move a category
        if ($request->getParameter('move_category')) {
            
            if ($categoryId == 0) {
                $category->setParentCategoryId(0);
                $category->setCategoryTypeId(1);
                $category->setSiblingCategoryTypeId(2);
                // Get last sort order
                $sortOrder = CategoryTable::getLastSortOrder(0);
                $category->setSortOrder($sortOrder);
                $category->save();
                
                SnapshotLogTable::add('Category: '.$category->name.' moved under Make');
                
                $typeId = 1;
            }
            else {
                if (!$newCategory = CategoryTable::getInstance()->find($categoryId)) {
                    die("New category doesn't exist anymore.");
                }
                $category->setParentCategoryId($newCategory->getId());
                $category->setCategoryTypeId($newCategory->getSiblingCategoryTypeId());
                // Get last sort order
                $sortOrder = CategoryTable::getLastSortOrder($newCategory->getId());
                $category->setSortOrder($sortOrder);
                $category->save();
                
                $typeId = $newCategory->getSiblingCategoryTypeId();
                
                exec('cd .. && symfony bq:updateSingleCategoryParents --id='.$newCategory->id);
                
                SnapshotLogTable::add('Category: '.$category->name.' moved under '.$newCategory->name);
            }
            
            $this->redirect('category/index?id='.$categoryId.'&typeId='.$typeId);
            exit;
            
        }
        
        
    }
}