<?php

/**
 * move category drag
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class moveCategoryDragAction extends sfAction {
    
    public function execute($request)
    {
        $fromId = $request->getParameter('from');
        $toId   = $request->getParameter('to');
        
        // Get categories
        $fromCategory = CategoryTable::getInstance()->find($fromId);
        $toCategory   = CategoryTable::getInstance()->find($toId);
        
        // Set new category
        $fromCategory->setParentCategoryId($toCategory->getId());
        $fromCategory->setCategoryTypeId($toCategory->getSiblingCategoryTypeId());
        $fromCategory->setUpdatedAt(date('Y-m-d H:i:s'));
        $fromCategory->save();
        
        $parents = CategoryTable::getParentCategories($fromCategory->getId());
        $fromCategory->setParents(json_encode($parents));
        $fromCategory->save();
        
        exec('cd .. && symfony bq:updateSingleCategoryParents --id='.$fromCategory->id);
        
        SnapshotLogTable::add('Category: '.$fromCategory->name.' moved under '.$toCategory->name);
        
        return sfView::NONE;
    }
}