<?php

/**
 * change category type
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class changeCategoryTypeAction extends sfAction {
    
    public function execute($request)
    {
        $categoryId = $request->getParameter('categoryId');
        if (!$category = CategoryTable::getInstance()->find($categoryId)) {
            return sfView::NONE;
        }

        // Then get parent category
        if (!$parentCategory = CategoryTable::getInstance()->find($category->getParentCategoryId())) {
            return sfView::NONE;
        }
        
        if ($request->getParameter('category_type'))
        {
            $categoryTypeName = trim($request->getParameter('category_type'));
            $categoryType = Doctrine_Query::create()->from('CategoryType')->where('upper(name) = ?', strtoupper($request->getParameter('category_type')))->fetchOne();
            if (!$categoryType) { // No result, lets add
                $obj = new CategoryType();
                $obj->setParentCategoryTypeId(0);
                $obj->setName($categoryTypeName);
                $obj->save();
                
                SnapshotLogTable::add('Category Type: '.$categoryTypeName.' added');

                $categoryType = $obj;
            }
            
            SnapshotLogTable::add('Sibling Category Type: '.$category->getName().' changed');
            
            $category->setSiblingCategoryTypeId($categoryType->getId());
            $category->save();
            
            // Update siblings
            $q = Doctrine_Query::create()
                    ->update('Category')
                    ->set('category_type_id', "'".$categoryType->getId()."'")
                    ->where('parent_category_id = ?', $category->getId());
            $q->execute();
            
            $result = array('id' => $categoryType->getId());
            print json_encode($result);
        }
        
        return sfView::NONE;
    }
}