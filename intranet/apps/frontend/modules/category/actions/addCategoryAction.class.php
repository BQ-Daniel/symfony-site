<?php

/**
 * add new category action
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class addCategoryAction extends sfAction {
    
    public function execute($request)
    {
        $sort                       = $request->getParameter('sort');
        $id                         = $request->getParameter('parent_category_id', 0);
        $this->category             = $request->getParameter('category');
        $this->categoryTypeName     = trim($request->getParameter('category_type'));
        $this->parentCategoryTypeId = $request->getParameter('category_type_id'); // parent category type id
        
        // Save current order of categories
        $nextSortOrder = array_search('', $sort) + 1;
        
        $q = Doctrine_Query::create()->update('Category')->set('sort_order', 'sort_order + 1')
                ->where('sort_order >= ? AND parent_category_id = ?', array($nextSortOrder, $id))->execute();
        
        
        // Check if category type exists, if it doesn't add it under parent category_type_id
        $this->categoryTypeId = null;
        $this->categoryType = null;
        if (!empty($this->categoryTypeName)) {
            $this->categoryType = Doctrine_Query::create()->from('CategoryType')->where('upper(name) = ?', strtoupper($this->categoryTypeName))->fetchOne();
            if (!$this->categoryType) { // No result, lets add
                $obj = new CategoryType();
                $obj->setParentCategoryTypeId($this->parentCategoryTypeId);
                $obj->setName($this->categoryTypeName);
                $obj->save();
                
                SnapshotLogTable::add('Category Type: '.$this->categoryTypeName.' added', 'category_type', $obj->id);

                $this->categoryType = $obj;
            }
            $this->categoryTypeId = $this->categoryType->getId();
            
        }
        
        // Add new category, slot it under the parent one
        $category = new Category();
        $category->setParentCategoryId($id);
        $category->setCategoryTypeId($this->parentCategoryTypeId);
        $category->setSiblingCategoryTypeId($this->categoryTypeId);
        $category->setName($this->category);
        $category->setSortOrder($nextSortOrder);
        $category->save();
        
        exec('cd .. && symfony bq:updateSingleCategoryParents --id='.$category->id);
        
        SnapshotLogTable::add('Category: '.$this->category.' added', 'category', $category->id);
        
        $this->category = $category;
    }
}