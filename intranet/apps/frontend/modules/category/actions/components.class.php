<?php

class categoryComponents extends sfComponents
{
    public function executeBreadcrumbs()
    {
        $this->breadcrumbs = CategoryTable::getParentCategories($this->categoryId);
    }
}
?>