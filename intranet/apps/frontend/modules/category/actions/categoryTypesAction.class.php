<?php

/**
 * save category name action.
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class categoryTypesAction extends sfAction {
    
    public function execute($request)
    {
        $term = $request->getParameter('term');
        
        if (empty($term)) {
            return sfView::NONE;
        }
        
        $categoryTypes = CategoryTypeTable::getCategoryTypes(array('name' => $term));
        $output = "[";
        foreach ($categoryTypes as $index => $categoryType) {
            if ($index != 0 && $index != count($categoryTypes)) {
                $output .= ",";
            }
            $output .= '"'.$categoryType->getName().'"';
        }
        $output .= "]";
        
        echo $output;
        
        return sfView::NONE;
    }
}