<?php

/**
 * add new category action
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class deleteAndMoveAction extends sfAction {
    
    public function execute($request)
    {
        if (!$this->categoryId = $request->getParameter('id', null)) {
            $this->redirect('category/index');
            exit;
        }
        
        $this->category = CategoryTable::getInstance()->find($this->categoryId);
        $this->categories = CategoryTable::getInstance()->findByParentCategoryId($this->categoryId);
        $this->otherCategories = Doctrine_Query::create()->from('Category c')->
                orderBy('sort_order ASC')->
                where('c.category_type_id = ? AND c.parent_category_id = ? AND c.id != ?', array($this->category->getCategoryTypeId(), $this->category->getParentCategoryId(), $this->categoryId))->execute();
        
        
        $move = $request->getParameter('move');
        if ($request->getParameter('go')) 
        {
            if (count($move) > 0)
            {
                // Loop through selects
                foreach ($move as $siblingCatgoryId => $moveToCategoryId)
                {
                    // If $moveToCategoryId isnt empty, they wish to move category
                    if ($moveToCategoryId)
                    {
                        // Update parts to new category id if any are directly under it
                        //Doctrine_Query::create()->update('Parts')->where('category_id = ?', $siblingCatgoryId)->set('category_id = ?', $moveToCategoryId);
                        $moveToCategory = CategoryTable::getInstance()->find($moveToCategoryId);
                        $query = Doctrine_Query::create()->update('Category')
                                ->set('parent_category_id', $moveToCategoryId)
                                ->set('category_type_id', $moveToCategory->getCategoryTypeId())
                                ->set('sibling_category_type_id', $moveToCategory->getSiblingCategoryTypeId())
                                ->set('updated_at', "'".date('Y-m-d H:i:s')."'")
                                ->where('id = ?', $siblingCatgoryId)->execute();
                        
                        $parents = CategoryTable::getParentCategories($siblingCatgoryId);
                        $category->setParents(json_encode($parents));
                        $category->save();
                    }
                    // Else no category to move to, delete
                    else {
                        CategoryTable::delete($siblingCatgoryId);
                    }
                }
            }
            // Delete category
            CategoryTable::delete($this->category->getId());
        }
        
    }
}