<?php

/**
 * add new category type action
 * This is used when no category type was entered when adding the category
 * 
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class addCategoryTypeAction extends sfAction {
    
    public function execute($request)
    {
        // Check if category type exists
        $categoryTypeName = $request->getParameter('category_type_name');
        
        // if category type name isnt empty.
        //    if category type existss
        
        $categoryType = false;
        if (!empty($categoryTypeName)) {
            $categoryType = Doctrine_Query::create()->from('CategoryType')->where('upper(name) = ?', strtoupper($categoryTypeName))->fetchOne();
        }
        
        if (!empty($categoryTypeName) && !$categoryType) {
            $categoryType = new CategoryType();
            $categoryType->setParentCategoryTypeId($request->getParameter('typeId'));
            $categoryType->setName($categoryTypeName);
            $categoryType->save();
            
            SnapshotLogTable::add('Category Type: '.$categoryTypeName.' added');
        }
        
        // If category type isn't empty, add it to the current category
        if ($categoryType) {
            $category = CategoryTable::getInstance()->find($request->getParameter('id'));
            $category->setSiblingCategoryTypeId($categoryType->getId());
            $category->save();
        }
        
        
        $this->forward('category', 'index');
    }
}