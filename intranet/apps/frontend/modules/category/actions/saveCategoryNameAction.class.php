<?php

/**
 * save category name action.
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class saveCategoryNameAction extends sfAction {
    
    public function execute($request)
    {
        $id = $request->getParameter('id');
        $name = $request->getParameter('name');
        
        if (empty($id) || empty($name)) {
            return sfView::NONE;
        }
        
        $values = array('name' => $name, 'updated_at' => date('Y-m-d H:i:s'));
        Doctrine_Query::create()->update('Category')->set($values)->where('id = ?', $id)->execute();
        
        exec('cd .. && symfony bq:updateSingleCategoryParents --id='.$id);
        
        
        SnapshotLogTable::add('Category name changed to '.$name, $type, $id);
        
        return sfView::NONE;
    }
}