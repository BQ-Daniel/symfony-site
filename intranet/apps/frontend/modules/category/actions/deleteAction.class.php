<?php

/**
 * add new category action
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class deleteAction extends sfAction {
    
    public function execute($request)
    {
        CategoryTable::delete($request->getParameter('id'));
        return sfView::NONE;
    }
}