<?php

/**
 * update sort order
 * (ajax request)
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class updateSortOrderAction extends sfAction {
    
    public function execute($request)
    {
        $sort = $request->getParameter('sort');
        
        foreach ($sort as $key => $id) {
            $i = $key + 1;
            $values = array('sort_order' => $i, 'updated_at' => date('Y-m-d H:i:s'));
            Doctrine_Query::create()->update('Category')->set($values)->where('id = ?', $id)->execute();     
        }  

        return sfView::NONE;
    }
}