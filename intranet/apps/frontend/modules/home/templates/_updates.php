                <div id="updates">
                    <div class="inner">
                        <div id="orders-today">
                            <?php if ($ordersCount == 0): ?>
                            <a href="">No orders today</a>
                            <?php else: ?>
                            <a href="<?php echo url_for('website/orders?today=true') ?>"><?php echo $ordersCount ?> order<?php echo ($ordersCount > 1 ? 's' : '') ?> today</a>
                            <?php endif; ?>
                        </div>
                        <div id="users-online">
                            <a href="<?php echo url_for('website/whosOnline') ?>"><?php echo $usersCount ?> online</a>
                        </div>
                        <div id="update-website">
                            <a href="<?php echo url_for('website/doUpdate') ?>" target="_blank">update website</a>
                        </div>
                    </div>
                </div>