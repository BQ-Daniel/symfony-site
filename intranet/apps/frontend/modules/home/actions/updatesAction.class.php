<?php

/**
 * add new part
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class updatesAction extends sfAction {

    public function execute($request)
    {
        $json = array();
        
        $currentOrderCount = $this->getUser()->getAttribute('ordersToday', false);
        $currentUserCount = $this->getUser()->getAttribute('usersOnline', false);
        
        
        $ordersCount = ORDERSTable::getCountOrdersToday();
        $usersCount = USER_SESSIONSTable::getCountUsersOnline();
        
        if ($currentOrderCount != $ordersCount) {
            $json[] = '"ordersToday": '.$ordersCount;
        }
        if ($currentUserCount != $usersCount) {
            $json[] = '"usersOnline": '.$usersCount;
        }
        
        if (count($json) > 0) {
            print '{'.implode(', ', $json).'}';
        }
        
        $this->getUser()->setAttribute('ordersToday', $ordersCount);
        $this->getUser()->setAttribute('usersOnline', $usersCount);
        return sfView::NONE;
    }

}