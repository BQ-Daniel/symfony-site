<?php

class homeComponents extends sfComponents
{
    public function executeUpdates()
    {
        if ($this->getUser()->getAttribute('level') > 2) {
            return sfView::NONE;
        } 
        $this->ordersCount = ORDERSTable::getCountOrdersToday();
        $this->usersCount = USER_SESSIONSTable::getCountUsersOnline();
    }
}
?>