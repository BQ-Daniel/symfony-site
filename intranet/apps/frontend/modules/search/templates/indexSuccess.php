<?php slot('title', 'Parts') ?>

<h2>Search</h2>

<?php include_partial('global/formMessages') ?>

<?php if ($form->hasErrors()): ?>
<div class="error">
    <?php foreach ($form->getErrors() as $error): ?>
    <?php print_r($error); ?>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<form action="<?php echo url_for('search/results') ?>" method="post">
    <table cellspacing="0" cellpadding="5" border="0">
        <tr>
            <td>OE Number</td>
            <td width="200"><?php echo $form['oe_number']->render() ?></td>
            <td>BQ Number</td>
            <td><?php echo $form['bq_number']->render() ?></td>
        </tr>
        <tr>
            <td>Illustration Number</td>
            <td><?php echo $form['illustration_number']->render() ?></td>
            <td>Cut Hose at</td>
            <td><?php echo $form['cut_hose_at']->render() ?></td>
        </tr>
        <tr>
            <td>PBR Number</td>
            <td><?php echo $form['pbr_number']->render() ?></td>
            <td>OL</td>
            <td><?php echo $form['ol']->render() ?></td>
        </tr>
    </table>

    <div style="height: 20px"></div>

    <div class="fl" style="margin-right: 50px;">
        <table cellspacing="1" cellpadding="8" border="0">
            <thead>
                <tr>
                    <td colspan="3" style="background: #ececec; color: #5f5f5f">Lengths</td>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td style="background: #f4f4f4;">A-C</td>
                    <td style="background: #f4f4f4;">A-D</td>
                    <td style="background: #f4f4f4;">A-E</td>
                </tr>
                <tr>
                    <td><?php echo $form['ac_length']->render() ?></td>
                    <td><?php echo $form['ad_length']->render() ?></td>
                    <td><?php echo $form['ae_length']->render() ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="fl">
        <table cellspacing="1" cellpadding="8" border="0">
            <thead>
                <tr>
                    <td colspan="5" style="background: #ececec; color: #5f5f5f">Fittings</td>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td style="background: #f4f4f4;">A</td>
                    <td style="background: #f4f4f4;">B</td>
                    <td style="background: #f4f4f4;">C</td>
                    <td style="background: #f4f4f4;">D</td>
                    <td style="background: #f4f4f4;">E</td>
                </tr>
                <tr>
                    <td><?php echo $form['a_fitting']->render() ?></td>
                    <td><?php echo $form['b_fitting']->render() ?></td>
                    <td><?php echo $form['c_fitting']->render() ?></td>
                    <td><?php echo $form['d_fitting']->render() ?></td>
                    <td><?php echo $form['e_fitting']->render() ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    
    <div class="cb" style="height: 20px"></div>
    
    Fittings <small>(search all fields)</small><br />
    <?php echo $form['fittings']->render() ?><br /><br />
    

    How to make a hose (contains)<br />
    <?php echo $form['make_hose_instructions']->render() ?><br />
    <br />

    Hose storage information (contains)<br />
    <?php echo $form['hose_storage']->render() ?><br />

    <div style="margin-top: 30px">
        <input type="submit" name="submit_search" value="Search" />
    </div>
</form>