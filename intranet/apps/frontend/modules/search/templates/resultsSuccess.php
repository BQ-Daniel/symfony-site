<?php slot('title', 'Parts') ?>

<h2>Search Results</h2>
<h3><?php echo $totalResults ?> result<?php echo ($totalResults > 1 ? 's' : '') ?> found</h3>

<div style="height: 10px;"></div>

    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
<div class="list-table">
    <table cellspacing="0" cellpadding="0" border="0" width="600">
        <tr>
            <th width="200"><?php echo sortable_link('BQ Number', url_for('search/results?page=1'), 'bq_number') ?></th>
            <th width="200"><?php echo sortable_link('OE Number', url_for('search/results?page=1'), 'oe_number') ?></th>
            <th width="150"><?php echo sortable_link('Illustration Number', url_for('search/results?page=1'), 'illustration_number') ?></th>
        </tr>
        <?php foreach ($pager->getResults() as $result): ?>
        <tr>
            <td><a href="<?php echo url_for('part/edit?id=' . $result->getId()) ?>"><?php echo $result->getBqNumber() ?></a></td>
            <td><?php echo $result->getOeNumber() ?></td>
            <td><?php echo ($result->getIllustration() != '' ? $result->getIllustration()->getIllustrationNumber() : 'None') ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
    <div style="width: 600px;">
        <?php include_partial('global/pager', array('pager' => $pager)); ?>
    </div>
