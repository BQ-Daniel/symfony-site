<?php

/**
 * search results
 * 
 * @package    brakequip
 * @subpackage search
 * @author     John Smythe
 */
class resultsAction extends sfAction {
    
    public function execute($request)
    {
        $this->form = new SearchForm();
        $sort = $request->getParameter('sort', 'updated_at'); // default sort field
        $dir = $request->getParameter('dir', 'desc'); // default direction
        
        $parameters = $request->getParameter($this->form->getName());

        if ($request->getParameter('page', null)) {
            $searchParameters = $this->getUser()->getParameter('search_parameters');
            if ($searchParameters) {
                $parameters = unserialize($searchParameters);
            }
        }
        $this->form->bind($parameters);
        
        if ($request->getParameter('submit_search') && $this->form->isValid() || $request->getParameter('page', null))
        {
            $q = Doctrine_Query::create()->from('Part p')->innerJoin('p.Illustration i');
            $values = $this->form->getValues();
            
            foreach ($values as $key => $value) {
                // Not a field
                if (in_array($key, array('fittings'))) {
                    continue;
                }
                $value = $values[$key] = str_replace('*', '%', $value);
                if (!empty($value)) {
                    $q->addWhere('UPPER('.$key.')' . " LIKE '%".strtoupper($value)."%'");
                }
            }
            
            // If fittings isn't empty, search all fields
            if (!empty($values['fittings'])) {
                $q->orWhere("UPPER(a_fitting) LIKE '%".strtoupper($values['fittings'])."%'");
                $q->orWhere("UPPER(b_fitting) LIKE '%".strtoupper($values['fittings'])."%'");
                $q->orWhere("UPPER(c_fitting) LIKE '%".strtoupper($values['fittings'])."%'");
                $q->orWhere("UPPER(d_fitting) LIKE '%".strtoupper($values['fittings'])."%'");
                $q->orWhere("UPPER(e_fitting) LIKE '%".strtoupper($values['fittings'])."%'");
                
            }
            
            // Add order by            
            $q->addOrderBy($sort.'+0 '.$dir);
            
            // Set in session to store on next page
            $this->getUser()->setParameter('search_parameters', serialize($parameters));

            $this->pager = new sfDoctrinePager('Part', sfConfig::get('app_results_per_page'));
            $this->pager->setQuery($q);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
            
            $this->totalResults = $this->pager->getNbResults();
            
            if ($this->totalResults == 0) {
                $this->getUser()->setFlash('error', 'No results found');
                $this->forward('search', 'index');
            }            
        }
    }
}