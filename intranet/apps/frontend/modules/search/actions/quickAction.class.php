<?php

/**
 * search results
 * 
 * @package    brakequip
 * @subpackage search
 * @author     John Smythe
 */
class quickAction extends sfAction {
    
    public function execute($request)
    {
        $keyword = $request->getParameter('quick_search_keyword');
        
        if ($part = PartTable::getPartByBqNumber($keyword)) {
            $this->redirect('/part/edit?id='.$part->getId());
            exit;
        }
        if ($part = PartTable::getPartByOeNumber($keyword)) {
            $this->redirect('/part/edit?id='.$part->getId());
            exit;
        }

        $this->getUser()->setFlash('error', 'No results found');
        $this->redirect('/search/index');
        
    }
}