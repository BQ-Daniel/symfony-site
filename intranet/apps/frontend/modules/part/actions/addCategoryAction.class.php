<?php

/**
 * part actions.
 *
 * @package    brakequip
 * @subpackage part
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class addCategoryAction extends sfAction {
    
    public function execute($request)
    {
        $id = $request->getParameter('id');
        $categoryId = $request->getParameter('categoryId');
        
        $this->part = $part = PartTable::getInstance()->find($id);
        
        // If they wish to save
        if ($request->getParameter('save_category')) 
        {
            $link = new PartToCategory();
            $link->setPartId($id);
            $link->setCategoryId($categoryId);
            $link->save();
            
            $category = CategoryTable::getInstance()->find($categoryId);
            
            SnapshotLogTable::add('Category: '.$category->name.' added to part: '.$part->bq_number, 'part', $part->id);
            
            $this->getUser()->setFlash('success', 'Successfully added category to part');
            $this->redirect('part/edit?id='.$id.'&categories=true');
            exit;
        }
        
        $this->category = CategoryTable::getInstance()->find($categoryId);
        
        if ($categoryId) {
            $this->parentCategory = CategoryTable::getInstance()->find($categoryId);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => $categoryId));
            $this->categoryType = CategoryTypeTable::getInstance()->find($this->parentCategory->getSiblingCategoryTypeId());
        } else {
            // Get first level of category type
            $this->categoryType = CategoryTypeTable::getInstance()->find(1);
            $this->categories = CategoryTable::getCategories(array('parent_category_id' => 0));
        }
        
        // If no category type, then check if the product already exists 
        if (!$this->categoryType)
        {
            $this->alreadyExistsInCategory = false;
            $q = Doctrine_Query::create()->from('PartToCategory')->where('part_id = '.$id.' AND category_id = '.$categoryId);
            if ($q->fetchOne()) {
                $this->alreadyExistsInCategory = true;
            }
        }
        $this->categoryId = $categoryId;
    }
}
