<?php

class partComponents extends sfComponents
{
    public function executeBreadcrumbs()
    {
        $this->breadcrumbs = CategoryTable::getParentCategories($this->categoryId);
    }
}
?>