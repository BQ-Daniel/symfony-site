<?php

/**
 * part actions.
 *
 * @package    brakequip
 * @subpackage part
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class deleteCategoryAction extends sfAction {
    
    public function execute($request)
    {
        $categoryId = $request->getParameter('categoryId', false);
        $partId = $request->getParameter('partId', false);
        
        if (!$categoryId || !$partId) {
            return sfView::NONE;
        }
        
        $partToCategory = PartToCategoryTable::getInstance()->findOneByPartIdAndCategoryId($partId, $categoryId);
        
        MarkedForDeleteTable::markForDelete($partToCategory->getId(), 'part_to_category');
        $partToCategory->delete();
        
        return sfView::NONE;
    }
}
