<?php

/**
 * edit part
 * 
 * @package    brakequip
 * @subpackage Part
 * @author     John Smythe
 */
class editAction extends sfAction {

    public function execute($request) {
        $this->part = new Part();
        $this->illustration = false;
        $new = true;

        // If admin wants to delete part
        if ($request->getParameter('delete')) {
            PartTable::delete($request->getParameter('id'));
            $request->setParameter('success', 'Part has successfully been deleted');
        }

        $values = $request->getParameter('part');


        if ($partId = $request->getParameter('id')) {
            $this->part = PartTable::getInstance()->find($partId);
            $new = false;
        }
        if (isset($values['id']) && !empty($values['id'])) {
            $this->part = PartTable::getInstance()->find($values['id']);
            $new = false;
        }
        if (!$this->part) {
            $this->part = new Part();
            $new = true;
        }

        // If user is wanting to remove illustration from oe number
        if ($request->getParameter('remove_illustration')) {
            $this->part->setIllustration(null);
            $this->part->save();

            SnapshotLogTable::add('Illustration removed from part only: ' . $this->part->bq_number);

            $this->illustration = false;
            $request->setParameter('success', 'Successfully removed illustration from part');
        }

        // If user is wanting to remove illustration from system
        if ($request->getParameter('remove_illustration_from_system')) {
            $this->part->setIllustration(null);
            $this->part->save();
            $this->illustration = false;
            IllustrationTable::removeFromSystem($values['illustration_id']);
            $request->setParameter('success', 'Successfully removed illustration from System');
        }


        $this->form = new PartForm($this->part);
        if (!$this->part->isNew()) {
            $this->illustration = $this->part->getIllustration();
            $this->form->getWidget('org_oe_number')->setAttribute('value', $this->part->getOeNumber());
            $this->form->getWidget('org_bq_number')->setAttribute('value', $this->part->getBqNumber());
        }

        // FORM DISPLAY
        // Saving product details
        if ($request->getParameter('save')) {
            $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
            if ($this->form->isValid()) {
                $values = $this->form->getValues();

                $this->part = $this->form->getObject();
                $this->part->fromArray($values);

                // If new illustration is being uploaded
                if (!empty($values['illustration_number']) && !empty($values['illustration'])) {
                    $values['illustration_number'] = trim($values['illustration_number']);
                    $file = $values['illustration'];

                    $fileExtension = $file->getExtension();
                    if ($fileExtension == "" || $fileExtension == ".txt")
                        $fileExtension = ".svg";

                    // Save master file (this will be overwritten)
                    $fileName = sfConfig::get('app_illustration_dir') . '/' . $values['illustration_number'] . $fileExtension; //$file->getExtension();
                    $thumbFileName = sfConfig::get('app_illustration_dir') . '/' . $values['illustration_number'] . '_s' . $fileExtension; //$file->getExtension();
                    $file->save($fileName);

                    ##test
                    $prodDir = dirname(__FILE__) . '/../../../../../../products';
                    copy($fileName, $prodDir . '/' . $values['illustration_number'] . $fileExtension);
                    if (file_exists($thumbFileName)) {
                        copy($thumbFileName, $prodDir . '/' . $values['illustration_number'] . '_s' . $fileExtension);
                    }


                    //Create large image
                    //$img = new sfImage($fileName, $_FILES['illustration']['type']);
                    //$img->thumbnail(sfConfig::get('app_illustration_width'), sfConfig::get('app_illustration_height'), 'scale');
                    //$img->saveAs($fileName);

                    $img = new SimpleImage();
                    $img->load($fileName);
                    $img->resizeToWidth(sfConfig::get('app_illustration_width'));
                    $img->save($fileName);


                    // Create larger image but resized
                    //$img = new sfImage($fileName, $_FILES['illustration']['type']);
                    //$img->thumbnail(sfConfig::get('app_illustration_thumb_width'), sfConfig::get('app_illustration_thumb_height'), 'scale');
                    //$img->saveAs($thumbFileName);
                    $img = new SimpleImage();
                    $img->load($fileName);
                    $img->resizeToWidth(sfConfig::get('app_illustration_thumb_width'));
                    $img->save($thumbFileName);

                    // Save illustration
                    if (!$ill = IllustrationTable::getInstance()->findOneByIllustrationNumber($values['illustration_number'])) {
                        $ill = new Illustration();
                        $ill->setCreatedAt(date('Y-m-d H:i:s'));
                    }
                    $ill->setIllustrationNumber($values['illustration_number']);
                    $ill->setUpdatedAt(date('Y-m-d H:i:s'));
                    $ill->save();

                    SnapshotLogTable::add('Uploaded new illustation: ' . $ill->illustration_number, 'illustration', $ill->id);
                    $this->illustration = $ill;

                    $this->part->setIllustrationId($ill->getId());
                    $values['illustration_number'] = null;
                } elseif ($values['illustration_id'] > 0) {
                    $this->part->setIllustrationId($values['illustration_id']);
                    $this->illustration = IllustrationTable::getInstance()->find($values['illustration_id']);
                }
                $this->part->setStaffId($this->getUser()->getStaffId());
                $this->part->save();

                SnapshotLogTable::add('Part ' . ($new ? 'added' : 'modified') . ': ' . $this->part->bq_number, 'part', $this->part->id);

                $request->setParameter('success', 'Part has successfully been saved');
            } else {
                $request->setParameter('error', 'Please fix the errors below before continuing');
            }
        }
    }

}
