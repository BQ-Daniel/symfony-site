<?php

/**
 * part actions.
 *
 * @package    brakequip
 * @subpackage part
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class illustrationSearchAction extends sfAction {
    
    public function execute($request)
    {
        $term = $request->getParameter('term');
        
        if (empty($term)) {
            return sfView::NONE;
        }
        
        if ($illustration = IllustrationTable::getIllustationByNumber($term)) {
            echo '{"id": "' . $illustration->getId() . '", "number": "' . $illustration->getIllustrationNumber(). '"}';
        }

        return sfView::NONE;
  }
}
