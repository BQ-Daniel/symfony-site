<?php

/**
 * add new part
 * 
 * @package    brakequip
 * @subpackage category
 * @author     John Smythe
 */
class addAction extends sfAction {

    public function execute($request) {
        /*
         * EDIT
         * If illustration id exists, load object
         * If oe_number_id exists, load object
         * Add display on website feature
         */


        $this->categoryId = $categoryId = $request->getParameter('categoryId', null);
        $this->illustration = false;
        $this->oe = false;
        $this->bq = false;
        $bqId = $request->getParameter('bqId');

        if (!$bqId) {
            if ($categoryId) {
                $this->parentCategory = CategoryTable::getInstance()->find($categoryId);
                $this->categories = CategoryTable::getCategories(array('parent_category_id' => $categoryId));
                $this->categoryType = CategoryTypeTable::getInstance()->find($this->parentCategory->getSiblingCategoryTypeId());
            } else {
                // Get first level of category type
                $this->categoryType = CategoryTypeTable::getInstance()->find(1);
                $this->categories = CategoryTable::getCategories(array('parent_category_id' => 0));
            }
        }

        // Create form
        $this->form = new PartForm();
        $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));



        $values = $this->form->getValues();

        if ($request->getParameter('change') && $request->getParameter('org_oe_number') != $values['oe_number']) {
            // See if new oe number exists
            if (!OeNumberTable::getInstance()->findOneByNumber($values['oe_number'])) {
                $values['oe_number'] = $request->getParameter('org_oe_number');
                $request->setParameter('error', 'New oe number does not exist.');
            }
        }

        $oeArray = array();

        // If bq number exists, load object
        if (!empty($values['bq_number'])) {
            $this->bq = BqNumberTable::getInstance()->findOneByPart($values['bq_number']);
        }

        // Lets check if oe number exists, if so load into form
        $oeNumber = @$values['oe_number'];

        if ($request->getParameter('bqId')) {
            $this->bq = BqNumberTable::getInstance()->find($request->getParameter('bqId'));
            $this->oe = $this->bq->getOeNumber();
        }

        if (!empty($oeNumber)) {
            $this->oe = OeNumberTable::getInstance()->findOneByNumber($oeNumber);
        }

        if ($this->oe) {
            $oeArray = $this->oe->toArray();
            $oeArray['oe_number'] = $oeArray['number'];
            $this->illustration = $this->oe->getIllustration();
            $this->illustration = ($this->illustration->getId() ? $this->illustration : false);
        }

        // If user is wanting to remove illustration from oe number
        if ($request->getParameter('remove_illustration')) {
            $this->oe->setIllustration(null);
            $this->oe->save();
            $this->illustration = null;
            $values['illustration_id'] = null;
            $request->setParameter('success', 'Successfully removed illustration from OE Number');
        }

        // If user is wanting to remove illustration from system
        if ($request->getParameter('remove_illustration_from_system')) {
            $this->oe->setIllustration(null);
            $this->oe->save();
            $this->illustration = null;
            IllustrationTable::removeFromSystem($values['illustration_id']);
            $values['illustration_id'] = null;
            $request->setParameter('success', 'Successfully removed illustration from System');
        }

        if ($request->getParameter('save')) {
            if ($this->form->isValid()) {
                $oe = new OeNumber();
                if ($this->oe) {
                    $oe = $this->oe;
                }
                $oe->fromArray($values);
                $oe->setNumber($values['oe_number']);
                $oe->setCreatedAt(date('Y-m-d H:i:s'));
                $oe->setUpdatedAt(date('Y-m-d H:i:s'));

                // If new illustration is being uploaded
                if (!empty($values['illustration_number']) && !empty($values['illustration'])) {

                    $file = $values['illustration'];

                    $fileExtension = $file->getExtension();
                    if ($fileExtension == "" || $fileExtension == ".txt")
                        $fileExtension = ".svg";

                    // Save master file (this will be overwritten)
                    $fileName = sfConfig::get('app_illustration_dir') . '/' . $values['illustration_number'] . $fileExtension; //$file->getExtension();
                    $thumbFileName = sfConfig::get('app_illustration_dir') . '/' . $values['illustration_number'] . '_s' . $fileExtension; //$file->getExtension();
                    $file->save($fileName);

                    ##test
                    $prodDir = dirname(__FILE__) . '/../../../../../../products';
                    copy($fileName, $prodDir . '/' . $values['illustration_number'] . $fileExtension);
                    if (file_exists($thumbFileName)) {
                        copy($thumbFileName, $prodDir . '/' . $values['illustration_number'] . '_s' . $fileExtension);
                    }

                    // Create thumbnail
                    $img = new sfImage($fileName, $file->getType());
                    $img->thumbnail(sfConfig::get('app_illustration_thumb_width'), sfConfig::get('app_illustration_thumb_height'), 'scale');
                    $img->saveAs($fileName);
                    // Create larger image but resized
                    $img = new sfImage($fileName, $file->getType());
                    $img->thumbnail(sfConfig::get('app_illustration_width'), sfConfig::get('app_illustration_height'), 'scale');
                    $img->saveAs($thumbFileName);

                    // Save illustration
                    if (!$ill = IllustrationTable::getInstance()->findOneByIllustrationNumber($values['illustration_number'])) {
                        $ill = new Illustration();
                        $ill->setCreatedAt(date('Y-m-d H:i:s'));
                        $ill->setUpdatedAt(date('Y-m-d H:i:s'));
                    }
                    $ill->setIllustrationNumber($values['illustration_number']);
                    $ill->save();
                    $this->illustration = $ill;

                    $oe->setIllustration($ill);
                    $values['illustration_number'] = null;
                }
                // Else there is already one assigned, get id and assign it to oe object
                elseif (!empty($values['illustration_id'])) {
                    $ill = IllustrationTable::getInstance()->find($values['illustration_id']);
                    if ($ill) {
                        $oe->setIllustration($ill);
                    }
                    $this->illustration = $ill;
                }

                $oe->save();

                if (!empty($values['bq_number'])) {
                    if (!$this->bq) {
                        $this->bq = new BqNumber();
                        $this->bq->setCreatedAt(date('Y-m-d H:i:s'));
                        $this->bq->setPart($values['bq_number']);
                    }
                    $this->bq->setOeNumber($oe);
                    $this->bq->save();
                }
                $this->oe = $oe;

                $this->form->bind($values, $request->getFiles($this->form->getName()));
            }
        } else {
            if ($this->oe) {
                $this->form->bindInputValuesFromArray($oeArray, $request->getFiles($this->form->getName()));
            }
        }
    }

}
