<?php slot('title', 'Parts') ?>

<div id="edit-part">
<?php if (!$bq): ?>
<?php include_component('part', 'breadcrumbs', array('categoryId' => $categoryId, 'links' => (!$bq))) ?>
    <div style="height: 10px;"></div>
<?php endif; ?>

<?php if ($sf_params->get('error')): ?>
    <div class="error"><?php echo $sf_params->get('error') ?></div>
<?php endif; ?>

<?php if (@$categoryType): ?>
<h2 style="font-size: 18px; float: none">Select <?php echo $categoryType->getName() ?></h2>
<form action="<?php echo url_for('part/add') ?>" method="get">
    <select id="category-select" name="categoryId">
        <option value="">Please select</option>
        <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
        <?php endforeach; ?>
    </select>
</form>

<div style="height: 25px;"></div>
<?php endif; ?>

<?php if (!@$categoryType): ?>

<form action="<?php echo url_for('part/add') ?>" method="post" enctype="multipart/form-data">
    <?php if ($bq): ?>
    <h2><?php echo $bq->getPart() ?></h2>
    <ul class="nav">
        <li><a id="part-details-link" href="" class="active" onclick="$('#categories').hide(); $('#part-details').show(); $(this).addClass('active'); $('#categories-link').removeClass('active'); return false;">Part Details</a></li>
        <li><a id="categories-link" href="" onclick="$('#part-details').hide(); $('#categories').show(); $(this).addClass('active'); $('#part-details-link').removeClass('active'); return false;">Manage Categories</a></li>
    </ul>
    <div style="border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px; clear:both"></div>
    <input type="hidden" name="part[bq_number]" value="<?php echo $bq->getPart() ?>" />
    <?php endif; ?>

    <div id="part-details">
    <?php echo $form->renderHiddenFields() ?>
    <input type="hidden" name="categoryId" value="<?php echo $categoryId ?>" />
    <table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="100"><?php echo (!$form->getValue('oe_number') ? 'Enter ' : '') ?>OE Number</td>
            <td><input type="hidden" name="org_oe_number" value="<?php echo $form['oe_number']->getValue() ?>" /><?php echo $form['oe_number']->render() ?></td>
            <?php if (!$form->getValue('oe_number')): ?>
            <td><a href="" class="button submit"><span>Next</span></a></td>
            <?php endif; ?>
            <?php if ($bq): ?>
            <td><input type="submit" name="change" value="Load" class="button" /></td>
            <?php endif; ?>
        <?php if ($form->getValue('oe_number') && !$bq): ?>
            <td width="100" style="padding-left: 30px;">BQ Number</td>
            <td><?php echo $form['bq_number']->render() ?></td>
        <?php endif; ?>
        </tr>
    </table>
    
    <?php if ($form->getValue('oe_number')): ?>
    
        <div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

        <div class="fl" style="width: 400px; padding-right: 63px">
            <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td width="100">PBR Number</td>
                    <td><?php echo $form['pbr_number']->render() ?></td>
                </tr>
                <tr>
                    <td width="100">OL</td>
                    <td><?php echo $form['ol']->render() ?></td>
                </tr>
                <tr>
                    <td width="100">Cut Hose at</td>
                    <td><?php echo $form['cut_hose_at']->render() ?></td>
                </tr>
            </table>
            
            <div style="height: 20px;"></div>
            
            <table cellspacing="1" cellpadding="8" border="0">
                <thead>
                    <tr>
                        <td colspan="3" style="background: #ececec; color: #5f5f5f">Lengths</td>
                    </tr>
                </thead>
                <tbody>
                    <tr align="center">
                        <td style="background: #f4f4f4;">A-C</td>
                        <td style="background: #f4f4f4;">A-D</td>
                        <td style="background: #f4f4f4;">A-E</td>
                    </tr>
                    <tr>
                        <td><?php echo $form['ac_length']->render() ?></td>
                        <td><?php echo $form['ad_length']->render() ?></td>
                        <td><?php echo $form['ae_length']->render() ?></td>
                    </tr>
                </tbody>
            </table>
            
            <div style="height: 20px;"></div>
            
            How to make the hose<br />
            <?php echo $form['make_hose_instructions']->render(array('style' => 'width: 400px; height: 65px;'), array()) ?>
            
        </div>
        <div class="fl">
            New illustration required?<br />
            <div style="margin-top: 5px;">
            <div class="fl" style="padding: 6px 0 0 0;"><input type="radio" name="illustration_required" value="1"<?php echo (!$illustration ? ' checked="checked"' : '') ?>/> Yes <input type="radio" name="illustration_required" value="0" <?php echo ($illustration ? ' checked="checked"' : '') ?> /> No</div>
            <div class="fl"><input type="text" id="illustration-search" name="search_number" value="<?php echo ($illustration ? $illustration->getIllustrationNumber() : '') ?>" onkeyup="searchIllustration(this)" class="input medium" style="margin-left: 30px;"/> <?php echo image_tag('ajax-loader.gif', 'id="search-loader" class="hide"') ?> <?php echo image_tag('tick_small.png', 'id="search-success" class="hide"') ?> <?php echo image_tag('cross_small.png', 'id="search-error" class="hide"') ?></div>
            </div>

            
            <div style="height: 15px; clear: both"></div>
            
            <div id="illustration-input" class="illustration-box" style="background: #f4f4f4; padding: 10px; -moz-border-radius: 4px; display: none">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="150">Illustration Number</td>
                        <td><?php echo $form['illustration_number']->render() ?></td>
                    </tr>
                    <tr>
                        <td width="150">Upload Photo</td>
                        <td><?php echo $form['illustration']->render() ?></td>
                    </tr>
                </table>
            </div>
            
            <div id="illustration" style="background: #e5e5e5; -moz-border-radius: 5px; padding: 3px;<?php echo ($illustration ? ' display: block' : ' display: none') ?>">
                <img src="/uploads/illustrations/<?php echo ($illustration ? $illustration->getIllustrationNumber() : '') ?>_s.jpg" />
                <div style="text-align: right; padding: 5px"><input type="submit" name="remove_illustration" value="Remove" /> <input type="submit" name="remove_illustration_from_system" value="Remove from System" /></div> 
            </div>
            
            
            <div style="height: 20px;"></div>
            
            Hose storage information<br />
            <?php echo $form['hose_storage']->render(array('style' => 'width: 400px; height: 45px;')) ?>
            
            <div style="height: 20px;"></div>
            
            <table cellspacing="1" cellpadding="8" border="0">
                <thead>
                    <tr>
                        <td colspan="5" style="background: #ececec; color: #5f5f5f">Fittings</td>
                    </tr>
                </thead>
                <tbody>
                    <tr align="center">
                        <td style="background: #f4f4f4;">A</td>
                        <td style="background: #f4f4f4;">B</td>
                        <td style="background: #f4f4f4;">C</td>
                        <td style="background: #f4f4f4;">D</td>
                        <td style="background: #f4f4f4;">E</td>
                    </tr>
                    <tr>
                        <td><?php echo $form['a_fitting']->render() ?></td>
                        <td><?php echo $form['b_fitting']->render() ?></td>
                        <td><?php echo $form['c_fitting']->render() ?></td>
                        <td><?php echo $form['d_fitting']->render() ?></td>
                        <td><?php echo $form['e_fitting']->render() ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <div style="height: 40px; clear: both;"></div>

        <input type="submit" name="save" value="Save Changes" />
    
    <?php endif; ?>
    </div>
</form>

    <div id="categories" style="display: none">
        <?php if ($bq): ?>
        <a class="button" href="<?php echo url_for('part/addCategory?bqId='.$bq->getId()) ?>"><span>Add Category</span></a>
        <div class="cb" style="height: 20px;"></div>
        <div class="list-table">
            <input type="hidden" name="bqId" value="<?php echo $bq->getId() ?>" />
            <table cellspacing="0" cellpadding="0" border="0">
                <thead>
                    <tr>
                        <th width="20"></th>
                        <th>Category</th>
                    </tr>
                </thead>
            <?php if (count($bq->getCategories()) > 0): ?>
            <?php foreach ($bq->getCategories() as $result): ?>
                <tr>
                    <td><input type="hidden" name="categoryId" value="<?php echo $result->getCategory()->getId() ?>" /><a href="delete-category" class="delete"><?php echo image_tag('icon_sml_delete.png', '') ?></a></td>
                    <td>
                        <?php include_component('part', 'breadcrumbs', array('categoryId' => $result->getCategory()->getId(), 'links' => false)) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td></td>
                    <td>This BQ Number is currently in no categories</td>
                </tr>
            <?php endif; ?>
            </table>
        </div>
        
            <?php if ($sf_params->get('categories')): ?>
            <script>
                $('#part-details').hide();
                $('#categories').show();
                $('#categories-link').addClass('active');
                $('#part-details-link').removeClass('active');
            </script>
            <?php endif; ?>
        <?php endif; ?>
    </div>

<?php endif; ?>
</div>