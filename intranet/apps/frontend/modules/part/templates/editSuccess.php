<?php
slot('title', 'Parts');

$path = "";

if ($illustration) {
    $path = "uploads/illustrations/" . $illustration->getIllustrationNumber();
    if (file_exists($path . ".svg")) {
        $path .= ".svg";
    } else {
        $path .= "_s.jpg";
    }

    $path = "/" . $path;
}
?>

<?php include_partial('global/formMessages') ?>

<?php if ($form->hasErrors()): ?>

<?php endif; ?>

<div id="edit-part">

    <form action="<?php echo url_for('part/edit') ?>" method="post" enctype="multipart/form-data">
        <input type="submit" name="save" value="Save Changes" style="display:none" />
        <?php if ($part->isNew() !== true): ?>
            <input type="hidden" name="id" value="<?php echo $part->getId() ?>" />
            <h2><?php echo $part->getOeNumber() ?><?php echo ($part->getBqNumber() != '' ? ' / ' . $part->getBqNumber() : '') ?></h2>
            <ul class="nav">
                <li><a id="part-details-link" href="" class="active" onclick="$('#categories').hide(); $('#part-details').show(); $(this).addClass('active'); $('#categories-link').removeClass('active'); return false;">Part Details</a></li>
                <li><a id="categories-link" href="" onclick="$('#part-details').hide(); $('#categories').show(); $(this).addClass('active'); $('#part-details-link').removeClass('active'); return false;">Manage Categories</a></li>
            </ul>
            <div style="border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px; clear:both"></div>
        <?php endif; ?>

        <div id="part-details">
            <?php echo $form->renderHiddenFields() ?>
            <table cellspacing="0" cellpadding="0" border="0">
                <tr valign="top">
                    <td width="100">OE Number</td>
                    <td>
                        <?php echo $form['oe_number']->render(array('class' => 'input', 'maxlength' => 100)) ?>
                        <?php if ($form['oe_number']->hasError()): ?>
                            <div class="form-error"><?php echo $form['oe_number']->getError() ?></div>
                        <?php endif; ?>
                    </td>
                    <td width="100" style="padding-left: 30px;">BQ Number</td>
                    <td>
                        <?php echo $form['bq_number']->render(array('class' => 'input', 'maxlength' => 12)) ?>
                        <?php if ($form['bq_number']->hasError()): ?>
                            <div class="form-error"><?php echo $form['bq_number']->getError() ?></div>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>

            <div style="height: 20px; border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px"></div>

            <div class="fl" style="width: 400px; padding-right: 63px">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td width="100">PBR Number</td>
                        <td><?php echo $form['pbr_number']->render(array('class' => 'input medium')) ?></td>
                    </tr>
                    <tr>
                        <td width="100">OL</td>
                        <td><?php echo $form['ol']->render(array('class' => 'input medium')) ?></td>
                    </tr>
                    <tr>
                        <td width="100">Cut Hose at</td>
                        <td><?php echo $form['cut_hose_at']->render(array('class' => 'input medium')) ?></td>
                    </tr>
                </table>

                <div style="height: 20px;"></div>

                <table cellspacing="1" cellpadding="8" border="0">
                    <thead>
                        <tr>
                            <td colspan="3" style="background: #ececec; color: #5f5f5f">Lengths</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr align="center">
                            <td style="background: #f4f4f4;">A-C</td>
                            <td style="background: #f4f4f4;">A-D</td>
                            <td style="background: #f4f4f4;">A-E</td>
                        </tr>
                        <tr>
                            <td><?php echo $form['ac_length']->render(array('class' => 'input small')) ?></td>
                            <td><?php echo $form['ad_length']->render(array('class' => 'input small')) ?></td>
                            <td><?php echo $form['ae_length']->render(array('class' => 'input small')) ?></td>
                        </tr>
                    </tbody>
                </table>

                <div style="height: 20px;"></div>

                How to make the hose<br />
                <?php echo $form['make_hose_instructions']->render(array('style' => 'width: 400px; height: 65px;', 'class' => 'input'), array()) ?>

                <div style="height: 20px;"></div>

                Website Display<br />
                <?php echo $form['website_display']->render() ?>

            </div>
            <div class="fl">
                New illustration required?<br />
                <div style="margin-top: 5px;">
                    <div class="fl" style="padding: 6px 0 0 0;"><input type="radio" name="illustration_required" value="1"<?php echo (!$illustration ? ' checked="checked"' : '') ?>/> Yes <input type="radio" name="illustration_required" value="0" <?php echo ($illustration ? ' checked="checked"' : '') ?> /> No</div>
                    <div class="fl"><input type="text" id="illustration-search" name="search_number" value="<?php echo ($illustration ? $illustration->getIllustrationNumber() : '') ?>" onkeyup="searchIllustration(this)" class="input medium" style="margin-left: 30px;"/> <?php echo image_tag('ajax-loader.gif', 'id="search-loader" class="hide"') ?> <?php echo image_tag('tick_small.png', 'id="search-success" class="hide"') ?> <?php echo image_tag('cross_small.png', 'id="search-error" class="hide"') ?></div>
                </div>


                <div style="height: 15px; clear: both"></div>

                <div id="illustration-input" class="illustration-box" style="background: #f4f4f4; padding: 10px; -moz-border-radius: 4px; display: none">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td width="150">Illustration Number</td>
                            <td><?php echo $form['illustration_number']->render(array('class' => 'input small')) ?></td>
                        </tr>
                        <tr>
                            <td width="150">Upload Photo</td>
                            <td><?php echo $form['illustration']->render() ?></td>
                        </tr>
                    </table>
                </div>

                <div id="illustration" style="background: #e5e5e5; -moz-border-radius: 5px; padding: 3px;<?php echo ($illustration ? ' display: block' : ' display: none') ?>">
                    <img src="<?php echo $path ?>" />
                    <div style="text-align: right; padding: 5px"><input type="submit" name="remove_illustration" value="Remove" onclick="return confirm('Are you sure?');" /> <input type="submit" name="remove_illustration_from_system" value="Remove from System" onclick="return confirm('Are you sure?');" /></div> 
                </div>


                <div style="height: 20px;"></div>

                Hose storage information<br />
                <?php echo $form['hose_storage']->render(array('style' => 'width: 400px; height: 45px;', 'class' => 'input')) ?>

                <div style="height: 20px;"></div>

                <table cellspacing="1" cellpadding="8" border="0">
                    <thead>
                        <tr>
                            <td colspan="5" style="background: #ececec; color: #5f5f5f">Fittings</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr align="center">
                            <td style="background: #f4f4f4;">A</td>
                            <td style="background: #f4f4f4;">B</td>
                            <td style="background: #f4f4f4;">C</td>
                            <td style="background: #f4f4f4;">D</td>
                            <td style="background: #f4f4f4;">E</td>
                        </tr>
                        <tr>
                            <td><?php echo $form['a_fitting']->render(array('class' => 'input medium')) ?></td>
                            <td><?php echo $form['b_fitting']->render(array('class' => 'input medium')) ?></td>
                            <td><?php echo $form['c_fitting']->render(array('class' => 'input medium')) ?></td>
                            <td><?php echo $form['d_fitting']->render(array('class' => 'input medium')) ?></td>
                            <td><?php echo $form['e_fitting']->render(array('class' => 'input medium')) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div style="height: 40px; clear: both;"></div>

            <input type="submit" name="save" value="Save Changes" />
            <?php if ($part->isNew() !== true): ?>
                <input type="submit" name="delete" value="Delete Part" onclick="return confirm('Are you sure you want to permanently delete this part? ');" />
            <?php endif; ?>

        </div>
    </form>

    <div id="categories" style="display: none">
        <?php if ($part->isNew() !== true): ?>
            <a class="button" href="<?php echo url_for('part/addCategory?id=' . $part->getId()) ?>"><span>Add Category</span></a>
            <div class="cb" style="height: 20px;"></div>
            <div class="list-table">
                <input type="hidden" name="partId" value="<?php echo $part->getId() ?>" />
                <table cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <th width="20"></th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <?php if (count($part->getPartToCategory()) > 0): ?>
                        <?php foreach ($part->getPartToCategory() as $result): ?>
                            <tr>
                                <td><input type="hidden" name="categoryId" value="<?php echo $result->getCategoryId() ?>" /><a href="delete-category" class="delete"><?php echo image_tag('icon_sml_delete.png', '') ?></a></td>
                                <td>
                                    <?php include_component('part', 'breadcrumbs', array('categoryId' => $result->getCategoryId(), 'links' => false)) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td></td>
                            <td>This part is currently in no categories</td>
                        </tr>
                    <?php endif; ?>
                </table>
            </div>

            <?php if ($sf_params->get('categories')): ?>
                <script>
                    $('#part-details').hide();
                    $('#categories').show();
                    $('#categories-link').addClass('active');
                    $('#part-details-link').removeClass('active');
                </script>
            <?php endif; ?>
        <?php endif; ?>
    </div>


</div>