<?php slot('title', 'Parts') ?>

<div id="edit-part">
    <h2><?php echo $part->getBqNumber() ?></h2>
    <ul class="nav">
        <li>&nbsp;</li>
        <li><a href="<?php echo url_for('part/edit?id='.$part->getId().'&categories=true') ?>">Back to Part Categories</a></li>
    </ul>
    <div style="border-bottom: 1px dotted #000; width: 1000px; margin-bottom: 20px; clear:both"></div>

<?php include_component('part', 'breadcrumbs', 
        array('categoryId' => $categoryId,
              'url' => '/part/addCategory',
              'params' => 'id=' . $part->getId(),
              'links' => true)) ?>

<form action="<?php echo url_for('part/addCategory') ?>" method="get">
<?php if ($categoryType): ?>
<h2 style="font-size: 18px; float: none">Select <?php echo $categoryType->getName() ?></h2>
    <input type="hidden" name="id" value="<?php echo $part->getId() ?>" />
    <select id="category-select" name="categoryId">
        <option value="">Please select</option>
        <?php foreach ($categories as $category): ?>
        <option value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
        <?php endforeach; ?>
    </select>
<?php endif; ?>

<?php if (!$categoryType): ?>
    <br />
    <?php if ($alreadyExistsInCategory): ?>
    <b>Hey!</b> Part already exists under this category.
    <div class="cb" style="height: 20px"></div>
    <a class="button" href="<?php echo url_for('part/add?id='.$part->getId().'&categories=true') ?>"><span>Back to Part Categories</span></a>
    <?php else: ?>
<input type="hidden" name="id" value="<?php echo $part->getId() ?>" />
<input type="hidden" name="categoryId" value="<?php echo $categoryId ?>" />
<input type="submit" name="save_category" value="Add Category to Part" class="button" />
    <?php endif; ?>
<?php endif; ?>
    
</form>

</div>

