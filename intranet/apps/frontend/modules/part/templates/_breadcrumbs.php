<?php if (count($breadcrumbs) > 0): ?>
<?php
if (!isset($url)) {
    $url = '/part/add';
}
$params = (!isset($params) ? '' : '&' . $params);
?>
<ul class="breadcrumb<?php echo (!$links ? '2' : '') ?>">
    <?php if ($links): ?>
    <li><?php echo link_to('Home', $url . '?' .$params) ?></li>
    <?php endif; ?>
    <?php foreach ($breadcrumbs as $breadcrumb): ?>
        <?php if ($links): ?>
    <li><?php echo link_to($breadcrumb['name'], $url . '?categoryId='.$breadcrumb['id'] . $params) ?></li>
        <?php else: ?>
    <li><?php echo $breadcrumb['name'] ?> &nbsp; &gt; &nbsp;</li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>
<div class="cb"></div>
<?php endif; ?>