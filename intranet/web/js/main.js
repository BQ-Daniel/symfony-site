/**
 * On page load
 */
$(document).ready(function() {
    // Get all anchors with button class
    anchorLinksToSubmit();
});


/**
 * Add onclick events to add submit anchor buttons
 */
function anchorLinksToSubmit()
{
    // Get all anchors with button class
    $('a.button.submit').each(function () {
        // Add onclick event
        $(this).click(function () {
            // Find parent form and submit
            $(this).parents('form:first').submit();
            return false;
        });
    });
}

/**
 * On window resize
 */
$(window).resize(function() {
    $('#content-container').height($(window).height() - $('#header').height());
});