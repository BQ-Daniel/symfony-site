/**
 * On page load
 */
$(document).ready(function() {
    // Get all anchors with button class
    anchorLinksToSubmit();
    // Handle default input on clicks
    handleInputDefaultClick();
    
    if (document.getElementById('launch-website-login-as')) {
        $("#launch-website-login-as").dialog({
            resizable: false,
            modal: true,
            autoOpen: false,
            buttons: {
                "Launch Website": function() {
                    launchOnlineApplications();
                }
            }
        });
    }
    
    $('a[href="launch-online-applications"]').click(function () {
        $("#launch-website-login-as").dialog('open');
        return false;
    });
});

/**
 * On window resize
 */
$(window).resize(function() {
    $('#content-container').height($(window).height() - $('#header').height());
});

checkUpdates();
setInterval(function() {
    checkUpdates();
}, 10000);

/**
 * Replace all default input field text colours to their default input colour
 * Grey to black and clear default message
*/
function handleInputDefaultClick() {
    $('.input.default').each(function() {
        $(this).keypress(function() {
            if ($(this).hasClass('default')) {
                $(this).val('');
                $(this).removeClass('default');
            }
        });
    });
}

/**
 * Add onclick events to add submit anchor buttons
 */
function anchorLinksToSubmit()
{
    // Get all anchors with button class
    $('a.button.submit').each(function () {
        // Add onclick event
        $(this).click(function () {
            // Find parent form and submit
            $(this).parents('form:first').submit();
            return false;
        });
    });
}

function resizeWindowAndCentre(width, height)
{
    var left = parseInt((screen.availWidth/2) - (width/2));
    var top = parseInt((screen.availHeight/2) - (height/2));
        
    window.resizeTo(width, height);
    window.moveTo(left,top);
}

function launchOnlineApplications()
{
    // Check that user exists, and if so add them to the website database
    $.getJSON('/website/launchWebsite', {username: $('#launch-website-login-as input[name="login_as"]').val()}, function(data) {
        if (data.success) {
            $("#launch-website-login-as").dialog('close');
            window.open('http://www.brakequip.com.au/login/' + data.success, 'brakequip');
        }
        else {
            alert(data.error);
        }
    });
}

function checkUpdates()
{
    $.getJSON('/home/updates', function(data) {
        if (data == undefined) {
            return;
        }
        
        if (data.ordersToday != undefined) {
            $('#orders-today a').slideUp('slow', function () {
                if (data.ordersToday == 0) {
                    $(this).text(data.ordersToday + ' No orders today');
                }
                else {
                    $(this).text(data.ordersToday + ' order' + (data.ordersToday > 1 ? 's' : '') + ' today');
                }
                $(this).slideDown('slow');
            });
        }

        if (data.usersOnline != undefined) {
            $('#users-online a').slideUp('slow', function () {
                $(this).text(data.usersOnline + ' online');
                $(this).slideDown('slow');
            });
        }
    });
}

function maxWindow()
{
    window.moveTo(0,0);


    if (document.all)
    {
        top.window.resizeTo(screen.availWidth,screen.availHeight);
    }

    else if (document.layers||document.getElementById)
    {
        if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth)
        {
            top.window.outerHeight = screen.availHeight;
            top.window.outerWidth = screen.availWidth;
        }
    }
}

function launchWebsiteUpdate(reload)
{
    $.get('/website/doUpdate', function () {
        alert('Website update process launched');
        if (reload) {
            location.href = location.href; 
        }
    }); 
}