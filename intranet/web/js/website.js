function viewOrderDetails(id)
{
    $.get('/website/orderDetails', {id: id}, function(data) {
        $('#order-details').html(data);
        $('#search-results').fadeOut('fast', function() {
            $('#order-details').fadeIn('fast'); 
        });
    });
}

function backToResults() {
    $('#order-details').fadeOut('fast', function() {
        $('#search-results').fadeIn('fast'); 
    });
}