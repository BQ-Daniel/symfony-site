function selectCategory(el)
{
    if ($(el).val() != '') { 
        $(el).parents("form").submit();
        return true;
    }
    return false;
}

function illustrationRadioSelection(el)
{
    if ($(el).val() == 1) {
        $('#illustration').hide();
        $('#illustration-input').show();
        $('#illustration-search').hide();
    }
    else {
        $('#illustration-input').hide();
        $('#illustration-search').show();
        if ($('#illustration-search').val() != '') {
            $('#illustration').show();
        }
    }   
}

var searchTimeout = null;
function searchIllustration(el)
{
    $('#search-error').hide();
    $('#search-success').hide();
    $('#search-loader').show();
    
    searchTimeout = setTimeout(function () {
        $.getJSON('/part/illustrationSearch', {term: el.value}, function (data) {
           if (data) {
               $('#illustration img').attr('src', '/uploads/illustrations/' + data.number + '_s.jpg');
               $('#part_illustration_id').val(data.id);
               $('#illustration').show();
               $('#search-success').show();
           }
           else {
               $('#illustration').hide();
               $('#part_illustration_id').val(0);
               $('#search-error').show();
           }
           $('#search-loader').hide();
        });
    }, 500);
}

function deleteCategoryFromPart(el)
{
    var td = $(el).parent();
    var tr = $(el).parent().parent();
    var partId = $(td).parents('.list-table').find('input[name=partId]').val();
    var categoryId = $(td).find('input').val();
    
    $.post('/part/deleteCategory', {categoryId: categoryId, partId: partId}, function() {
        $(tr).remove();
    });
}

$(function() {
    $('#category-select').change(function () {return selectCategory(this);});
    $("input[name='illustration_required']").click(function() {illustrationRadioSelection(this);});
    illustrationRadioSelection($("input[name='illustration_required']:checked"));
    
    $('#categories a.delete').click(function() { deleteCategoryFromPart(this); return false; });
    
});

