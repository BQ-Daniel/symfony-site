
/**
 * Category functions
 */
            
/******************************************************
 * ADD CATRGORY
 * Display add category form
 * Slides down form to add category under list item
 */
function displayAddCategoryForm() 
{
    // get category id from classname
    // the action icons classname is updated when rolled over for tooltip
    var categoryId = $('#action-icons').attr('class'); // get category id from classname
    var parent = $('#category-' + categoryId); // selected li element
    
    var additionalHeight = 60;
    if (categoryId == 0) {
        additionalHeight = 25;
    }
                
    // Slide up any other forms 
    noDisplay = false;
    $('#sortable-categories li .add-category-form').each(function () {
        var parentId = $(this).parent().attr('id');
        if ('category-'+categoryId == parentId) {
            noDisplay = true;
        }
        $(this).parent().animate({
            height: $(parent).find('.li').height() + 6 + 'px'
        }, 200, function() {
            $(this).find('.add-category-form').remove();
        });
    });
                
    // Don't display form if selected element is already shown
    if (noDisplay) {
        return true;
    }
                
    if (parent && parent.queue().length > 0) {
        return false;
    }
                
    // Slide down height of li
    $(parent).append($('#add-category-form').html());
    handleInputDefaultClick();
    var input = $(parent).find('.add-category-form input[name=category_type]');
    $(input).val($('#sibling-category-type').val());
        
    // Attach autocomplete function
    $(input).autocomplete({
        source: '/category/categoryTypes',
        minLength: 2
            
    });
    
    var formDiv = $(parent).find('.add-category-form');
        
    var boxHeight = $(formDiv).height() + additionalHeight;
    $(parent).animate({
        height: boxHeight + 'px'
    }, 200, function() {
        $(formDiv).fadeIn('fast', function () {$(formDiv).find('input[name=category]').focus();});
    });
    
    moveMode();
    sortMode();
    
    return true;
}
            
            
/*************************************************
 * EDIT CATEGORY
 * Display input box for edit category 
 */
function displayEditCategoryForm() {
    var categoryId = $('#action-icons').attr('class');
    var parent = $('#category-' + categoryId);
                
    var category = $('#category-' + categoryId + ' .category');
    var categoryName = $(category).text();
    var categoryLink = $(category).html();
                
    var input = $('<input type="text" name="category_name" value="' + categoryName + '" class="input small" style="width: 170px;" maxlength="50" />');

    // Attach On blur event, display link back
    $(input).blur(function () {
        $(category).html(categoryLink);
    });
                
    // Attach On enter key press event
    $(input).keydown(function(e) {
        if (e.keyCode == 13 && $(input).val() != '') {
            if ($(input).val() == categoryName) {
                $(category).html($(input).val());
                return false;
            }
            $(input).attr('disabled', 'true');
            $(category).append('<span class="saving">Saving...</span>');
            $.get('/category/saveCategoryName', {
                id: categoryId, 
                name: $(input).val()
            }, function(data) {
                $(category).html($(input).val());
            });
        }
    });
    $(category).html(input);
    $(input).focus();
}
    
function displayCreateCategory() 
{
    $('#sortable-categories').prepend('<li id="category-0"></li>');
    $('#action-icons').attr('class', '0');
    displayAddCategoryForm();
    
    moveMode();
    sortMode();

}

function moveMode() 
{
    if ($('input[name=mode]:checked').val() != 'move') {
        return false;
    }
    
    $( "#sortable-categories" ).sortable('destroy');
    $( "#sortable-categories li, .breadcrumb li.crumb" ).droppable(
    {
        hoverClass: "active",
        tolerance: "pointer",
        drop: function( event, ui ) {
            if ($('input[name=mode]:checked').val() != 'move') {
                return false;
            }
            var toCategory = $(this);
            var fromCategory = ui.draggable;

            var toCategoryId = $(toCategory).attr('id').split('-');
            toCategoryId = toCategoryId[1];
            var toCategoryName = $.trim($(toCategory).text());
            var fromCategoryId = $(fromCategory).attr('id').split('-');
            fromCategoryId = fromCategoryId[1];
            var fromCategoryName = $.trim($(fromCategory).text());

            if (confirm("Are you sure you want to move " + fromCategoryName + " and it's siblings under " + toCategoryName + "?")) {
                $.post("/category/moveCategoryDrag", {
                    from: fromCategoryId, 
                    to: toCategoryId
                });
                $(fromCategory).fadeOut('fast');
            }
        }
    });
    $( "#sortable-categories li" ).draggable({
        revert: true
    });
}

function sortMode() {
    if ($('input[name=mode]:checked').val() != 'sort') {
        return false;
    }
    $( "#sortable-categories li" ).draggable('destroy');
    $( "#sortable-categories li" ).droppable('destroy');
    $("#sortable-categories").sortable('destroy');
    $("#sortable-categories").sortable({
        handle: '.icon', 
        update: function(event, ui) { 
            sortUpdate();
        }
    });
}

function sortUpdate() {
    $.post("/category/updateSortOrder", $("#categories form").serialize());
}

$(function()
{               
    moveMode();
    sortMode();

    $('#moveMode').click(function ()
    {
        moveMode();
    
    });
    
    $('#sortMode').click(function ()
    {
        sortMode();
    });

    $("#sortable-categories li").disableSelection();
    
    // Add tooltips to all categories
    $("#sortable-categories li div.content").tooltip({
        tip: '#action-icons',
        position: 'center right',
        offset: [0, 0],
        predelay: 200,
        delay: 100,
        opacity: 0.9,
        onBeforeShow: function() {
            $('#action-icons .hide-category').show();
            $('#action-icons .unhide-category').hide();
            if (this.getTrigger().parent().hasClass('hidden')) {
                $('#action-icons .unhide-category').show();
                $('#action-icons .hide-category').hide();
            }
            $('#action-icons').attr('class', this.getTrigger().parents(1).find('input').val());
        }
    });
    
    // Display add category form click event
    $('#action-icons .add-category').click(function() {
        displayAddCategoryForm();
        $('#action-icons').hide();
    });
    // Display edit category form click event
    $('#action-icons .edit-category').click(function() {
        displayEditCategoryForm();
        $('#action-icons').hide();
    });
    // Move category
    $('#action-icons .move-category').click(function() {
        var categoryId = $('#action-icons').attr('class'); // get category id from classname
        window.location = '/category/moveCategory?id='+categoryId;
    });
    $('#action-icons .hide-category').click(function() {
        var categoryId = $('#action-icons').attr('class'); // get category id from classname
        $('#category-'+categoryId+' div.li').addClass('hidden');
        $.post("/category/updateStatus", {categoryId: categoryId, hidden: 1 });
        $(this).hide();
        $('#action-icons .unhide-category').show();

    });
    $('#action-icons .unhide-category').click(function() {
        var categoryId = $('#action-icons').attr('class'); // get category id from classname
        $('#category-'+categoryId+' div.li').removeClass('hidden');
        $.post("/category/updateStatus", {categoryId: categoryId, hidden: 0 });
        $(this).hide();
        $('#action-icons .hide-category').show();

    });
    // Launch dialog box for deleting category
    $('#action-icons .delete-category').click(function() {
                var categoryId = $('#action-icons').attr('class');
		$("#dialog-message").dialog({
			modal: true,
                        width: 500,
			buttons: {
                                "Move Siblings then Delete": function() {
                                    location.href = '/category/deleteAndMove?id=' + categoryId;
                                },
                                /*
				"Delete and Remove Siblings": function() {
                                    var dialogBox = this;
                                    $.get('/frontend_dev.php/category/delete', {id: categoryId}, function (data) {
                                        $('#category-' + categoryId).remove();
                                        $(dialogBox).dialog('close');
                                    });
				},
                                */
                                "Cancel": function () {
                                    $(this).dialog('close');
                                }	
			}
		});
        $('#action-icons').hide();
    }); 
    
    // Add functionality to the form
    var form = $('#categories form').submit(function ()
    {
    
        var categoryId = $('#action-icons').attr('class'); // get category id from classname
        var parent = $('#category-' + categoryId); // selected li element
        var formDiv = $(parent).find('.add-category-form');
        var additionalHeight = 60;
        if (categoryId == 0) {
            additionalHeight = 25;
        }
    
        var categoryName = $(form).find('input[name=category]');
        var categoryType = $(form).find('input[name=category_type]');
        $(formDiv).find('.error').remove();
        $(parent).height(($(formDiv).height() + additionalHeight) + 'px');
            
        // Make sure both fields arnt blank
        if ($.trim($(categoryName).val()) == '' ||
            $(categoryName).hasClass('default')) {
            $('<span class="error">Category name is a required field</span>').prependTo(formDiv);
            $(parent).height(($(formDiv).height() + additionalHeight) + 'px');
            return false;
        }
            
        $.post("/frontend_dev.php/category/addCategory", $("#categories form").serialize(), function (data) {
            $(parent).animate({
                height: $(parent).find('.li').height() + 6 + 'px'
            }, 200, function() {
                $(this).find('.add-category-form').remove();
                var newCategoryRow = $(data).insertAfter(parent);
                $(newCategoryRow).fadeIn('fast');
                // Add tooltip to new category
                $(newCategoryRow).find('div.content').tooltip({
                    tip: '#action-icons',
                    position: 'center right',
                    offset: [0, 0],
                    predelay: 200,
                    delay: 100,
                    opacity: 0.9,
                    onBeforeShow: function() {
                        $('#action-icons').attr('class', this.getTrigger().parents(1).find('input').val());
                    }
                });
                moveMode();
                sortMode();
            });
        });
            
        return false;
    });
});