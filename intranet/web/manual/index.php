<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BrakeQuip Manual</title>
<link href="style.css" rel="stylesheet" media="screen" type="text/css" /> 
</head>

<body>
<a href="index.php"><img src="http://www.brakequip.com.au/images/bqlogo.jpg" class="logo"></a><br>

<div class="nav">
<?php
	$page = "nav.php";
include($page);
?>
</div>

<div class="content">
<?php
	$page = "home.php";
	if (isset($_GET['p'])) {
    if (file_exists($_GET['p'].".php")) {
	$page = $_GET['p'].".php";
		}
}
include($page);
?>

</div>
</body>
</html>
