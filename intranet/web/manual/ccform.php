<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BrakeQuip Manual</title>
<link href="style.css" rel="stylesheet" media="screen" type="text/css" /> 
<link href="style.css" rel="stylesheet" media="print" type="text/css" /> 

</head>

<body><div class="cccontent">
<div style="width:310px; float:left"><img src="http://www.brakequip.com.au/images/bqlogo.jpg" class="logo"></div><div class="ccheader"><h1>Credit Card payment form</h1><script language="JavaScript" type="text/javascript"> 
<!-- 
var 
month = new Array(); 
month[0]="January"; 
month[1]="February"; 
month[2]="March"; 
month[3]="April"; 
month[4]="May"; 
month[5]="June"; 
month[6]="July"; 
month[7]="August"; 
month[8]="September"; 
month[9]="October"; 
month[10]="November"; 
month[11]="December"; 
var 
day = new Array(); 
day[0]="Sunday"; 
day[1]="Monday"; 
day[2]="Tuesday"; 
day[3]="Wednesday"; 
day[4]="Thursday"; 
day[5]="Friday"; 
day[6]="Saturday"; 
today = new Date(); 
date = today.getDate(); 
day = (day[today.getDay()]); 
month = (month[today.getMonth()]); 
year = (today.getFullYear()); 
suffix = (date==1 || date==21 || date==31) ? "st" : "th" && 
(date==2 || date==22) ? "nd" : "th" && (date==3 || date==23) ? "rd" : "th" 
function print_date() 
{ 
document.write(date + "<sup>" + suffix + "</sup>" + "&nbsp;" + month + "&nbsp;" + year); 
} 
// --> 
</script> 
<script> 
<!-- 
print_date(); 
//--> 
</script></div>

<SCRIPT type = "text/JavaScript">
function nextbox(fldobj, nbox) { 
  if (fldobj.value.length==fldobj.maxLength) {
     fldobj.form.elements[nbox].focus();
  }
}
</script>

<form class="cccontent">
Name:<br />
  <input type="text" class="ccform" size="30px" /><br /><br />
    Card number:<br>
  <input type="text" class="ccform" name="card_no1" maxlength="4" size="1" onkeyup="nextbox(this,'card_no2');" /> 
  <input type="text" class="ccform" name="card_no2" maxlength="4" size="1" onkeyup="nextbox(this,'card_no3');" />
  <input type="text" class="ccform" name="card_no3" maxlength="4" size="1" onkeyup="nextbox(this,'card_no4');" />
  <input type="text" class="ccform" name="card_no4" maxlength="4" size="1" onkeyup="nextbox(this,'card_no5');" /><br>
  <br>
    Expiry:<br>
  <input type="text" class="ccform" name="card_no5" maxlength="2" size="1" onkeyup="nextbox(this,'card_no6');" align="middle"/>
  <input type="text" class="ccform" name="card_no6" maxlength="2" size="1" onkeyup="nextbox(this,'card_no7');" align="middle"/>
  <br>
  <br>
    CVV:<br>
  <input type="text" class="ccform" name="card_no7" maxlength="4" size="1" onkeyup="nextbox(this,'card_no67');" />
  <br />
  <br />
  Payment taken by
  <select name="takenby" id="takenby" class="ccform">
    <option selected="selected">Select</option>
      <option value="1">John</option>
      <option value="2">Daniel</option>
      <option value="3">Chris</option>
      <option value="4">Jo</option>
      <option value="5">Graeme</option>
  </select>

</form>
<br />
<br />
<style type="text/css">
@media print {
    input#printButton {
        display: none;
        }
    }
</style>

<input type="button" id="printButton" onclick="window.print();" value="Print Page" />
</div>