<h2>How to send statements</h2>
1.	Restart your machine completely. Once it's turned back on, make sure NETSCAPE, EPS, Winfax & Microsoft Outlook are all running. It is best not to do anything else in EASE, Outlook, or Internet Explorer whilst running the statements as it can have an effect on the process. <br><br>

2.	Change the area via F7 to area 2 (PREVIOUS MONTH)<br><br>

3.	Go to:<br>
-	Reports (R)<br>
-	Statements (1)<br>
-	FROM DEBTOR: Leave blank<br>
-	TO DEBTOR: Leave blank<br>
-	Print credit balance statements: N<br>
-	Print zero balance statements: N<br>
-	Minimum balance to print: Leave blank<br>
-	Print zero balance trans: N<br>
-	From area: Leave blank<br>
-	To area: Leave blank<br>
-	Print, Fax, Email, all (Select account step): P (print) E (email) F (fax)<br>
-	Print, Fax, Email (Override account step): Leave blank<br>
-	Enter to the next screen<br>
-	“Please load the statement stationary on printer: \\SERVER\REPORT\”: Press enter<br><br>

4.	Once print out statements are complete and have printed, repeat step 3 and change the selection for “Select account step”. This time enter F (fax) and progress the same way. Wait for the faxes to send, there is usually about 10 or so.<br>
<br>

5.	Once all faxes have been sent repeat step 3, changing the selection for “Select account step”. This time enter E (email) and progress the same way. Waiti for the emails to send.<br>
<br>

6.	Once all emails have been sent, create a new folder in the “sent folder” in outlook, naming it for the month of statements that have just been sent, and move all sent statements into that folder. 
<br>
<br>
Statements have now been completed. <u><strong>Be sure to change back to your normal area of EASE via F7</strong>.</u>
