<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="description" content="Brake Hose Manufacturing Systems. Stainless steel and rubber brake hose manufacturing systems for all vehicles.">
        <meta name="keywords" content="brake hose, brake hose Manufacturing, rubber hoses, braided hoses, brake, brakequip, australia">
        <title>Brake Hose Manufacturing Systems, BrakeQuip Australia</title>

        <meta name="language" content="English">
        <meta name="google-site-verification" content="KkT2BTpaOl_J6FYcNyV_OrhiOT_zMb5OWDWO1bYTZig">

        <script type="text/javascript" async="" src="https://ssl.google-analytics.com/ga.js"></script><script language="javascript" type="text/javascript" src="/assets/js/jquery.js?v1541134010"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/global.js?v1541134010"></script>
        <script language="javascript" type="text/javascript" src="/assets/js/jquery-ui.js?v1"></script>

        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/screen.css?0.3" charset="utf-8">
        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/blitzer/jquery-ui.css" charset="utf-8">
<!--        <script src="//load.sumome.com/" data-sumo-site-id="1f87a8df884db6d6a7c2bd0846db6f6b82414d85d16cda67a83daff931d3b854" async="async"></script>-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="container">
            <div id="topNav">
                                                    <h1 style="line-height:12px;;">                    Brake Hose Manufacturing Systems &amp; replacement brake hoses
                                    </h1>
                <ul>
                                                            <li><a href="/" class="home selected"><img src="/assets/images/homeIcon.png"></a></li>
                    <li><a href="/find-manufacturer">find a manufacturer</a></li>
                    <li><a href="/rubber-brake-hose">rubber</a></li>
                    <li><a href="/braided-brake-hose">braided</a></li>
                                        <li><a href="/contact">contact</a></li>
                </ul>
            </div>
            <div id="innerCotainer">
                <div id="mainTop" class="mod">
                    <a href="/"><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia"></a>
                                                            <div class="cb"></div>
                </div>

                <div id="content" class="mod">
                  <div style="position:relative;height:0;padding-bottom:56.28%"><iframe src="https://www.youtube.com/embed/WEd6LX_ugiM?controls=0" style="position:absolute;width:100%;height:100%;left:0" width="100%" height="auto" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                </div>
            </div>
        </div>
        <div id="footer">
            <ul class="mod">
                            <li><a href="/">home</a></li>
                <li><a href="/manufacturers/">become a brake hose manufacturer</a></li>
                <li><a href="/rubber-brake-hose">rubber</a></li>
                <li><a href="/braided-brake-hose">braided</a></li>
                <li><a href="/contact">contact</a></li>
                <li><a href="/manufacturers/faq">faq</a></li>
                <li><a href="/manufacturers/how-it-works">see how it works</a></li>
                <li><a href="/sitemap">sitemap</a></li>
                        </ul>
            <div class="cb"></div>
            Copyright © 2018 BrakeQuip Australia. All rights reserved.<br>
            Specialists in Brake Hose Manufacturing Systems

        </div>

        <script type="text/javascript">

          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-35746447-1']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

        </script>


</body></html>
