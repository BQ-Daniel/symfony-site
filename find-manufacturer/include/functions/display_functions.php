<?php

function display_search($criteria) {
	$address = $criteria['address'];
	$category_id = $criteria['category_id'];
	$max_distance = $criteria['max_distance'];

	echo '<form method="GET"> <div class="form-row">';

		echo '<div class="col-sm-8"><input type="text" id="address" name="address" value="'.$address.'" class="form-control" placeholder="Search Suburb & State" /></div>';

		//display the categories filter
		if($GLOBALS['categories_filter']=='1') {
			$categories = get_categories_list();

			echo '&nbsp;&nbsp;<select id="category_id" name="category_id" class="form-control" onchange="form.submit();">';
			echo '<option value=""></option>';
			for($i=0; $i<count($categories); $i++) {
				if($categories[$i]['id']==$category_id) echo '<option selected value="'.$categories[$i]['id'].'">'.$categories[$i]['name'].'</option>';
				else echo '<option value="'.$categories[$i]['id'].'">'.$categories[$i]['name'].'</option>';
			}
			echo '</select>';
		}

		if($GLOBALS['max_distance_filter']=='1') {
			$distance_tab = array('10'=>'10 '.$GLOBALS['distance_unit'], '50'=>'50 '.$GLOBALS['distance_unit'], '100'=>'100 '.$GLOBALS['distance_unit']);

			echo '<div class="col-sm-2"><select id="max_distance" name="max_distance" class="form-control" onchange="form.submit();">';
			echo '<option value="">Distance</option>';
			foreach($distance_tab as $ind=>$value) {
				if($ind==$max_distance) echo '<option selected value="'.$ind.'">'.$value.'</option>';
				else echo '<option value="'.$ind.'">'.$value.'</option>';
			}
			echo '</select></div>';
		}

		echo '<div class="col-sm-2"><input type="submit" value="Search" style="width:100%; height:38px" class="btn btn-danger" /></div>';

	echo '</div></form>';
}

function get_sidebar_display_1($criteria=array()) {
	$locations = $criteria['locations'];
	$lat = $criteria['lat'];
	$lng = $criteria['lng'];

	for($i=0; $i<count($locations);$i++) {
		$name = $locations[$i]['name'];
		$logo = $locations[$i]['logo'];
		$url = $locations[$i]['url'];
		$address = $locations[$i]['address'];
		$city = $locations[$i]['city'];
		$tel = $locations[$i]['tel'];
		$email = $locations[$i]['email'];
		$category_id = $locations[$i]['category_id'];
		$distance = round($locations[$i]['distance'],0);

		$d .= '<div class="store_locator_sidebar_entry sidebar_entry_btn" data-id="'.$locations[$i]['id'].'" data-lat="'.$locations[$i]['lat'].'" data-lng="'.$locations[$i]['lng'].'">';

			if($logo!='') {
				$d .= '<img src="'.$logo.'" style="padding-right:5px;" align="left"> ';
			}

			if($url=='') $d .= '<b>'.$name.'</b>';
			else $d .= '<b>'.$name.'</b>';

			$d .= ''.$address.'';
			$d .= '<br>'.$city.'';

			if($lat!='' && $lng!='') $d .= ' <span class="store_locator_sidebar_entry_distance">'.$distance.' '.$GLOBALS['distance_unit'].'</span>';

		$d .= '</div>';

		//Marker Info Window
		//$d .= '<div style="display:none;" id="marker_content_'.$locations[$i]['id'].'">'.get_marker_content($locations[$i]).'</div>';
	}

	return $d;
}

function get_sidebar_display_2($criteria=array()) {
	$locations = $criteria['locations'];
	$lat = $criteria['lat'];
	$lng = $criteria['lng'];

	if(count($locations)>0) {

		$d .= '<br><table class="table table-hover">';
		$d .= '<thead><tr><th>Name</th><th>Address</th><th>Tel</th><th style="text-align:right;">Distance</th></tr></thead>';

		for($i=0; $i<count($locations);$i++) {
			$name = $locations[$i]['name'];
			$logo = $locations[$i]['logo'];
			$url = $locations[$i]['url'];
			$address = $locations[$i]['address'];
			$tel = $locations[$i]['tel'];
			$email = $locations[$i]['email'];
			$category_id = $locations[$i]['category_id'];
			$distance = round($locations[$i]['distance']);

			$d .= '<tr class="sidebar_entry_btn" data-id="'.$locations[$i]['id'].'" data-lat="'.$locations[$i]['lat'].'" data-lng="'.$locations[$i]['lng'].'" style="cursor:pointer;">';

				$d .= '<td>';
					if($logo!='') $d .= '<img src="'.$logo.'" style="padding-right:5px;" align="left">';
					$d .= $name;
				$d .= '</td>';

				$d .= '<td>'.$address.'</td>';
				if($tel!='') $d .= '<td>'.$tel.'</td>';
				else $d .= '<td>N/A</td>';

				$d .= '<td style="text-align:right;">';
					if($lat!='' && $lng!='') $d .= ''.$distance.' '.$GLOBALS['distance_unit'].'';
					else $d .= 'N/A';
				$d .= '</td>';

			$d .= '</tr>';
		}

		$d .= '</table>';
	}

	return $d;
}

//Get Marker Content Display
function get_marker_content($criteria=array()) {
	$name = $criteria['name'];
	$logo = $criteria['logo'];
	$url = $criteria['url'];
	$address = $criteria['address'];
	$city = $criteria['city'];
	$tel = $criteria['tel'];
	$email = $criteria['email'];

	$d .= '<div class="store_locator_map_infowindow">';
	$d .= '<div class="col infowindow-address">';

		if($logo!='') {
			//$d .= '<img src="'.$logo.'" style="margin-bottom:5px;" width=80><br>';
		}

		if($url!='') $d .= '<a href="'.$url.'" target="_blank"><b>'.$name.'</b></a>';
		else $d .= '<b>'.$name.'</b>';


		$d .= ''.$address.'';
		$d .= '<br/>'.$city.'';

		if($tel!='') $d .= '<br/>ph: '.$tel.'</div>';
		//if($email!='') $d .= '<br/>'.$email.'';

		//Get Directions
		if($GLOBALS['get_directions_display']=='1') {
			$address = str_replace('<br />', ' ', $address);
			$d .= '<div class="col"><br /><a href="http://maps.google.com/maps?f=d&z=13&daddr='.urlencode($address).'" target="_blank">Directions</a> | ';

		//Streetview
		if($GLOBALS['streetview_display']==1) $d .= '<a href="#" id="displayStreetViewBtn">Street View</a>';

		}

	$d .= '</div></div>';

	return $d;
}

function displayMarkersContent($locations) {
	for($i=0; $i<count($locations);$i++) {
		$markers[$i] = get_marker_content($locations[$i]);
	}
	return $markers;
}

function display_pagination($criteria) {
    $page_number = $criteria['page_number'];
    $nb_display = $criteria['nb_display'];
    $nb_stores = $criteria['nb_stores'];

	$nb_pages = ceil($nb_stores/$nb_display);

	$d .= $nb_stores.' stores';

	if($nb_pages>1) {
		$d .= ' - ';
	    if($page_number==1 && $nb_stores>($page_number*$nb_display)) {
	        $d .= '<a href="#" id="store_locator_next">Next</a>';
	    }
	    else if($page_number>1) {
	        $d .= '<a href="#" id="store_locator_previous">Back</a> - ';
	        $d .= '<b>'.$page_number.' / '.$nb_pages.'</b>';
	        if(($page_number*$nb_display) < $nb_stores) $d .= ' - <a href="#" id="store_locator_next">View more</a>';
	    }
	}

    return $d;
}

?>
