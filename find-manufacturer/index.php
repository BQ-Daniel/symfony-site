<?php
include_once('include/webzone.php');
$address = $_GET['address'];
$category_id = $_GET['category_id'];
$max_distance = $_GET['max_distance'];
?>

<?php
    $pageName = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Find a Brake Hose Manufacturer, Brake Hose Manufacturing Systems, BrakeQuip Australia</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Brake Hose Manufacturers in Australia Brake Hose Manufacturing Systems. Stainless steel and rubber brake hose manufacturing systems for all vehicles.">
    <meta name="keywords" content="brake hose, brake hose Manufacturing, rubber hoses, braided hoses, brake, brakequip, australia">
    <meta name="language" content="English" />
    <meta name="google-site-verification" content="KkT2BTpaOl_J6FYcNyV_OrhiOT_zMb5OWDWO1bYTZig" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./include/css/style.css" />

	<script type='text/javascript'>
	var Store_locator = {
		ajaxurl:"", lat:"", lng:"", current_lat:"", current_lng:"", category_id:"", max_distance:"",
		page_number:1, nb_display: <?php echo $GLOBALS['nb_display']; ?>, map_all_stores: "<?php echo $GLOBALS['map_all_stores']; ?>",
		marker_icon:"<?php echo $GLOBALS['marker_icon']; ?>", marker_icon_current:"<?php echo $GLOBALS['marker_icon_current']; ?>",
		autodetect_location:"<?php echo $GLOBALS['autodetect_location']; ?>",
		streetview_display:<?php echo $GLOBALS['streetview_display']; ?>,
		display_type:""
	};
	</script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.google.com/maps/api/js?key=<?php echo $GLOBALS['google_api_key']; ?>" type="text/javascript"></script>
    <script src="./include/js/script.js" type="text/javascript"></script>

	<script>
	$(document).ready(function() {
		init_locations();
		$('#address').focus();
	})
	</script>

</head>

<body>
  <div id="container" class="">
    <div class="navbg">
      <div id="topNav">
              <?php
              $ignorePages = array('applications');
              $ignoreModules = array('catalogue', 'gallery', 'order', 'pictorial');
              ?>
          <?php if (in_array($page, $ignorePages) || in_array($modules, $ignoreModules)): ?>
          <?php if (isset($_SESSION['userId'])): ?>
          <div style="z-index:-10;position:absolute;margin-left:60px;width:820px;text-align:center;line-height:38px;color:#fff;font-size:18px;"><?php echo $_SESSION['name'] ?></div>
          <?php endif; ?>
          <?php endif; ?>
          <h1 style="line-height:12px;font-size: 15px;"><?php if (isset($_SESSION['userId'])): ?>
              <a href="/applications">Return to Applications</a> &nbsp; | &nbsp; <a href="/logout">Logout</a>
              <?php else: ?>
              Brake Hose Manufacturing Systems &amp; replacement brake hoses
              <?php endif; ?>
          </h1>
          <nav class="navbar navbar-expand-sm navbar-light bg-light">
            <?php
            $ignorePages = array('applications');
            $ignoreModules = array('catalogue', 'gallery', 'order', 'pictorial');
            ?>
            <?php if (!in_array($page, $ignorePages) && !in_array($modules, $ignoreModules)): ?>
            <div class="navbar-collapse collapse show" id="navbarNavAltMarkup" style="">
              <div class="navbar-nav" style="font-size: 15px;margin-top: -7px;">
                <a class="nav-item nav-link" href="/"><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia" class="img-fluid mobLogo"/></a>
                <a class="nav-item nav-link" href="/">Home</a>
                <a class="nav-item nav-link" href="/find-manufacturer">Find a Manufacturer</a>
                <a class="nav-item nav-link" href="/rubber-brake-hose">Rubber</a>
                <a class="nav-item nav-link" href="/braided-brake-hose">Braided</a>
              <?php endif; ?>
                <a class="nav-item nav-link" href="/contact">Contact</a>
              </div>
            </div>
          </nav>
      </div>
    </div>
      <div id="" class="">

          <div id="innerCotainer" class="">
            <div id="mainTop" class="mod row no-gutters">
              <div class="col-md">
                <a href="/"><img src="/assets/images/brakequip-logo.png" alt="Brake Hose Manufacturing Systems, BrakeQuip Australia" title="Brake Hose Manufacturing Systems, BrakeQuip Australia" class="img-fluid mainlogo"></a></div>
              </div>

            <div id="content">
              <h2>Find a Manufacturer</h2>
              <div class="mb-4">
                <?php
                  display_search(array('address'=>$address, 'category_id'=>$category_id, 'max_distance'=>$max_distance));
                ?>
              </div>
              <div id="sl_current_location" style="display:none"></div>

              <div class="row">

              <div id="result-map" class="col-md-12">
                  <div id="sl_map"></div>
              </div>

              <div id="result-list" class="col-md-12">
                <div id="sl_sidebar" class="store_locator_sidebar">Loading...</div>
                <div id="sl_pagination" class="store_locator_pagination"></div>
                <span id="sl_pagination_loading"></span>
              </div>

            </div>
          </div>
          </div>
      </div>

  <div id="footer">
      <ul style="width: 100%; max-width: 750px !important;" class="nav nav-pills nav-fill mod">
          <li class="nav-item"><a href="/">home</a></li>
          <li class="nav-item"><a href="/manufacturers/">become a brake hose manufacturer</a></li>
          <li class="nav-item"><a href="/rubber-brake-hose">rubber</a></li>
          <li class="nav-item"><a href="/braided-brake-hose">braided</a></li>
          <li class="nav-item"><a href="/contact">contact</a></li>
          <li class="nav-item"><a href="/manufacturers/faq">faq</a></li>
          <li class="nav-item"><a href="/manufacturers/how-it-works">see how it works</a></li>
          <li class="nav-item"><a href="/sitemap">sitemap</a></li>
      </ul>
      <div class="cb"></div>
      <span>Specialists in Brake Hose Manufacturing Systems<br><br>&copy; BrakeQuip Australia <?php echo date('Y') ?> </span>


  </div>
  </div>

</body>
</html>
