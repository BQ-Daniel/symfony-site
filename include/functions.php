<?php

function auth($option = null)
{
    // Update user login stats
    if (isset($_SESSION['userId']) && !empty($_SESSION['userId']))
    {
        Doctrine_Query::create()->update('UserLogins')->set('login_end', 'NOW()')->
                where("(NOW() < ADDTIME(login_end, '00:05:00') OR login_end IS NULL)")->
                addWhere('USER_ID = ?', array($_SESSION['userId']))->
                orderBy('id DESC')->limit(1)->execute();
        updateSession();
    }

    // Remove expired sessions
    Doctrine_Query::create()->delete('UserSessions')->where("ADDTIME(last_active, '00:05:00') < NOW()")->execute();

    // check if session id exists in database
    $session = UserSessionsTable::getInstance()->findOneBySession(session_id());
    
    // if session doesn't exist - redirect
    if (!$session || !isset($_SESSION['userId'])) {
        if ($option == "redirect") 
        {
            // If ajax request, echo json code to redirect
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $result = array('timeout' => true);
                echo json_encode($result);
                exit;
            }
            header('Location: /session-timeout');
            exit;
        }
        return false;
    }

    return true;
}

function sessionIdAuth($sessionId) {
    auth();
    // check if session id exists in database
    return UserSessionsTable::getInstance()->findOneBySession($sessionId);
}

function updateSession()
{
    Doctrine_Query::create()->update('UserSessions')->set('last_active', 'NOW()')->where('user_id = ? AND session = ?', array($_SESSION['userId'], session_id()))->execute();
}

function toXml($data, $rootNodeName = 'data', $xml=null) 
{
    // turn off compatibility mode as simple xml throws a wobbly if you don't.
    if (ini_get('zend.ze1_compatibility_mode') == 1) {
        ini_set('zend.ze1_compatibility_mode', 0);
    }

    if ($xml == null) {
        $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
    }

    // loop through the data passed in.
    foreach ($data as $key => $value) {
        // no numeric keys in our xml please!
        if (is_numeric($key)) {
            // make string key...
            $key = "category";
        }

        // replace anything not alpha numeric
        $key = preg_replace('/[^a-z]/i', '', $key);

        // if there is another array found recrusively call this function
        if (is_array($value)) {
            $node = $xml->addChild($key);
            // recrusive call.
            toXml($value, $rootNodeName, $node);
        } else {
            // add single node.
            $value = htmlentities($value);
            $xml->addChild($key, $value);
        }
    }
    // pass back as string. or simple xml object if you want!
    return $xml->asXML();
}
?>
