<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('GoogleGeoLog', 'main');

/**
 * BaseGoogleGeoLog
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_address_id
 * @property text $address
 * @property text $result
 * @property boolean $success
 * @property UserAddress $UserAddress
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class BaseGoogleGeoLog extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('google_geo_log');
        $this->hasColumn('id', 'integer', 8, array(
             'type' => 'integer',
             'fixed' => 0,
             'unsigned' => false,
             'primary' => true,
             'autoincrement' => true,
             'length' => '8',
             ));
        $this->hasColumn('user_address_id', 'integer', 8, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '8',
             ));
        $this->hasColumn('address', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('result', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('success', 'boolean', null, array(
             'type' => 'boolean',
             'default' => 1,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('UserAddress', array(
             'local' => 'user_address_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}