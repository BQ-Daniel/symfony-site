<?php


class PriceCodeTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PriceCode');
    }

    public static function getByCode($code)
    {
        return Doctrine_Query::create()
            ->from('PriceCode')
            ->where('UPPER(PriceCode.price_code) = ?', strtoupper($code))
            ->fetchOne();
    }
}