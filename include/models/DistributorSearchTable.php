<?php

/**
 * DistributorSearchTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class DistributorSearchTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object DistributorSearchTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('DistributorSearch');
    }
}