<?php

/**
 * UsersTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class UsersTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object UsersTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Users');
    }
    
    public static function getUserByUsername($username, $deleted = null)
    {
        $q = Doctrine_Query::create()->from('Users')->where('username = ?', $username);
        if ($deleted != null) {
            $q->addWhere('deleted = ?', $deleted);
        }
        return $q->fetchOne();
    }
    
    public static function getSubUsers($username)
    {
        return Doctrine_Query::create()
            ->from('Users')
            ->where('username LIKE ?', $username . '%')
            ->andWhere('username <> ?', $username)
            ->execute();
    }
}