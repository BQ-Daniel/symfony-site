<?php

class customTemplateRenderer extends sfTemplateRenderer {

    public function evaluate(sfTemplateStorage $template, array $parameters = array()) {
        if ($template instanceof sfTemplateStorageFile) {
            extract($parameters);
            ob_start();
            
            /**
             * Add action files
             */
            
            
            // If is an ajax request, include actions/ajax
            if ($isAjaxRequest)
            {
                // Check for a load file to include, this is always included 
                if (file_exists(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/ajax/'.$page.'.php')) {
                    require_once(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/ajax/'.$page.'.php');
                    exit;
                }
            }
            // Check for a load file to include, this is always included 
            if (file_exists(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/load.php')) {
                require_once(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/load.php');
            }

            // Check if action exists, if so include it
            if ($page != 'load' && file_exists(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/' . $page . '.php')) {
                require_once(realpath(dirname(__FILE__).'/../../').'/templates/' . $modules . '/actions/' . $page . '.php');
            }
            
            require $template;

            return ob_get_clean();
        } elseif ($template instanceof sfTemplateStorageString) {
            extract($parameters);
            ob_start();
            eval('; ?>' . $template . '<?php ;');

            return ob_get_clean();
        }

        return false;
    }

}

?>
