<?php

class OrderParts {

    public $fileLocation = null;
    public $partNumber = null;
    public $description = null;
    public $price = 0;
    public $priceCodeP = 0;
    public $weight = 0;
    public $stock = 0;
    public $cat = null;
    public $fp = null;
    public $priceGroup = null;

    public function __construct($fileLocation = null) {
        if ($fileLocation) {
            $this->setFile($fileLocation);

            $this->open();
        }
    }

    public function open() {
        $this->fp = fopen($this->getFile(), "r");
    }

    public function readLine() {
        $fget = fgets($this->fp);

        $explode = explode(',', $fget);
        $csv = '';
        foreach ($explode as $line) {
            $line = trim($line);
            $csv .= $this->checkCSVField($line) . ',';
        }

        $data = $this->getCSVValues(substr($csv, 0, -1));

        // Make sure all fields are there
        if (count($data) >= 5) {
            $obj = new OrderParts();
            $unwanted_array = array('ø'=>'o');
	    $str = strtr($data[1], $unwanted_array);
            $obj->setPartNumber($data[0]);
            $obj->setDescription($str);
            $price = number_format((float)$data[2], 2, '.', '');
            $obj->setPrice($price);
            $obj->setWeight($data[3]);
            $obj->setStock($data[4]);
            $priceCode = number_format((float)$data[5], 2, '.', '');
            $obj->setPriceCodeP($priceCode);
            $obj->setPriceGroup($data[6]);

            return $obj;
        }

        return false;
    }

    public function checkCSVField(&$item) {
        $item = str_replace('""', '"', $item);
        return $item;
    }

    function getCSVValues($string, $separator=",") {
        $elements = explode($separator, $string);
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes % 2 == 1) {
                for ($j = $i + 1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') % 2 == 1) { // Look for an odd-number of quotes
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j - $i + 1, implode($separator, array_slice($elements, $i, $j - $i + 1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr = & $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
        }
        return $elements;
    }

    public function close() {
        fclose($this->fp);
    }

    public function import() {
        // Select all order parts from database
        $parts = OrderProductsTable::getInstance()->findByActive(1);

        // Store all file contents in array
        while ($fget = fgets($this->fp)) {
            $explode = explode(',', $fget);
            $csv = null;
            foreach ($explode as $line) {
                $line = trim($line);
                $csv .= $this->checkCSVField($line) . ',';
            }
            $data = $this->getCSVValues(substr($csv, 0, -1));
            $data[0] = trim($data[0]);
            $file[$data[0]] = true;
        }
        $this->close();

        // If products, dont continue on
        if (count($file) == 0) {
            return false;
        }

        foreach ($parts as $row) {
            $row->part_number = trim($row->part_number);
            if (!isset($file[$row->part_number])) {
                $row->active = 0;
                $row->save();
                echo "DELETING " . $row->part_number . "<br />";
            }
        }
        unset($file);
        unset($data);


        $this->open();
        // Ignore the first 2 lines
        $data = fgetcsv($this->fp, 1000, ",");
        $data = fgetcsv($this->fp, 1000, ",");

        while ($obj = $this->readLine()) {
            $partNumber = trim($obj->getPartNumber());
            // Check if part number exists
            $result = OrderProductsTable::getInstance()->findOneByPartNumber($partNumber);

            preg_match("/[A-Z]*/", $partNumber, $matches);
            $sortAplha = $matches[0];

            $partNumber2 = $partNumber;
            if (eregi("-", $partNumber)) {
                $pos = strpos($partNumber, '-');
                $partNumber2 = substr($partNumber, 0, $pos);
            }

            preg_match_all("{(\d+)}", $partNumber2, $matches2);


            $sortKey = implode("", $matches2[0]);

            // Product exists, update
            if ($result)
            {
                echo "Update: " . $obj->getPartNumber() . ", Cat: " . $obj->getCat() . ", Aplha: $sortAplha, Key: $sortKey<br />\n";
                $result->description = $obj->getDescription();
                $result->price = $obj->getPrice();
                $result->weight = $obj->getWeight();
                $result->stock = $obj->getStock();
                $result->cat = $obj->getCat();
                $result->price_code_p = $obj->getPriceCodeP();
                $result->sort_alpha = $sortAplha;
                $result->sort_key = $sortKey;
                $result->active = 1;
                $result->price_group = $obj->getPriceGroup();
                $result->save();
            }
            // Insert new product
            else {
                echo "Insert: " . $obj->getPartNumber() . ", Cat: " . $obj->getCat() . "<br />\n";

                $result = new OrderProducts();
                $result->part_number = $partNumber;
                $result->description = $obj->getDescription();
                $result->price = $obj->getPrice();
                $result->weight = $obj->getWeight();
                $result->stock = $obj->getStock();
                $result->cat = $obj->getCat();
                $result->price_code_p = $obj->getPriceCodeP();
                $result->sort_alpha = $sortAplha;
                $result->sort_key = $sortKey;
                $result->active = 1;
                $result->price_group = $obj->getPriceGroup();
                $result->save();
            }
        }

        // Close file pointer
        $this->close();
    }

    public function setFile($fileLocation) {
        $this->fileLocation = $fileLocation;
    }

    public function getFile() {
        return $this->fileLocation;
    }

    public function setPartNumber($partNumber) {
        $this->partNumber = $partNumber;
    }

    public function getPartNumber() {
        return trim($this->partNumber);
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDescription() {
        return trim($this->description);
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice() {
        return number_format($this->price, 2, '.', '');
    }

    public function setPriceCodeP($priceCodeP) {
        $this->priceCodeP = $priceCodeP;
    }

    public function getPriceCodeP() {
        return $this->priceCodeP;
    }

    public function setWeight($weight) {
        $this->weight = $weight;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function setStock($stock) {
        $this->stock = $stock;
    }

    public function getStock() {
        return (int) $this->stock;
    }

    public function setCat($cat) {
        // If cat matches
        $this->cat = $cat;
    }

    public function getCat() {
        preg_match("/[A-Z]*/", $this->getPartNumber(), $matches);
        // print_r($matches);
        $cat = $matches[0];

        if (empty($cat)) {
            $cat = 'BQ';
        } else {

            switch ($cat) {
                case 'BQB':
                case 'BQJAWS':
                case 'BQJAW':
                case 'BQRHC':
                case 'PV':
                case 'W':
                case 'LL':
                case 'BQE':
                case 'BQV':
                case 'BQGAUGE':

                case (substr($cat, 0, 2) == 'LL'):
                    $cat = 'BQ';
                    break;

                case (substr($cat, 0, 1) == 'T'):
                case (substr($cat, 0, 2) == 'CN'):
                case (substr($cat, 0, 2) == 'BT'):
                case 'TUBE':
                    $cat = 'TUBE';
                break;

                case (substr($cat, 0, 5) == 'HFIFX'):
                    $cat = 'HFIF';
                break;

                case (substr($cat, 0, 5) == 'HFMFX'):
                    $cat = 'HFMF';
                break;

                case (substr($cat, 0, 5) == 'PSHFB'):
                case (substr($cat, 0, 6) == 'PSHFMM'):
                case (substr($cat, 0, 4) == 'PSHF'):
                case (substr($cat, 0, 2) == 'PC'):
                    $cat = 'PS';
                break;

                case (substr($cat, 0, 2) == 'SS'):
                    $cat = 'SS';
                    break;

                case (substr($cat, 0, 2) == 'LP'):
                    $cat = 'LP';
                    break;
            }
        }

        return $cat;
    }


    public function getPriceGroup()
    {
        return $this->priceGroup;
    }

    public function setPriceGroup($priceGroup)
    {
        $this->priceGroup = $priceGroup;
    }
}

?>
