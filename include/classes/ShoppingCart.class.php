<?php

class ShoppingCart 
{
    
    public $cart = array();
    
    
    public function __construct() 
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = array();
        }
        
        $this->cart = &$_SESSION['cart'];
    }
    
    public function addItem($partDetails = array())
    {
        $this->cart[$partDetails['id']] = $partDetails;
    }
    
    public function getItems()
    {
        // Create array with index as part number
        $tmp = array();
        foreach ($this->cart as $item) {
            $tmp[$item['id']] = $item['part'];
        }
        // Sort array by index which is part number ASC
        natsort($tmp);

        // Create new array with sorted keys
        $items = array();
        foreach ($tmp as $id => $name) {
            $items[$id] = $this->cart[$id];
        }

        return $items;
    }
    
    public function getQty($id) {
        if (isset($this->cart[$id])) {
            return $this->cart[$id]['qty'];
        }
        return false;
    }
    
    public function deleteItem($id) {
        unset($this->cart[$id]);
    }
    
    public function getPrice($id) {
        $price = $this->cart[$id]['price'];
        return number_format(str_replace(',', '', $price) * $this->cart[$id]['qty'], 2);
    }
    
    /**
     * Returns total weight for all items in the shopping cart.
     *
     * @return float
     */
    public function getTotalWeight() 
    {
        $totalWeight = 0;

        foreach ($this->cart as $item) {
            $totalWeight += $item['qty'] * $item['weight'];
        }

        return $totalWeight;
    }
    
    /**
     * Returns total price for all items in the shopping cart.
     *
     * @return float
     */
    public function getTotalPrice() 
    {
        $totalPrice = 0;

        foreach ($this->cart as $item) {
            $totalPrice += $item['qty'] * str_replace(',', '', $item['price']);
        }

        return number_format($totalPrice, 2);
    }
    
    public function reset() {
        unset($_SESSION['cart']);
        unset($_SESSION['order']);
    }
    
}


?>