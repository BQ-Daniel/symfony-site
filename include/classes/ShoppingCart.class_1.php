<?php

/* * ***************************************************************************
 * Shopping Cart Storage System
 * -------------------------------
 * This shopping cart system will store the cart in the browser session.
 *
 * Call the following function to begin using the cart and to have access to it.
 * Usuage: 
 *   $obj  = new ShoppingCart();    // Create shopping cart object
 *   $cart = $obj->getStoredCart(); // Retrieve current cart stored in the session
 *
 * The cart can store the following information
 * - The Items
 * - Each Item
 *   - Unique ID
 *   - Description
 *   - Quantity
 *   - Price
 *   - Weight
 * - Order ID
 * - Shipping Address
 *
 * **************************************************************************** */

class ShoppingCart {

    private
    $cart = null,
    $items = array(),
    $shippingAddress = null,
    $orderId = null;

    public function __construct() {

    }

    public function getStoredCart() {
        // If shopping cart is not in the session, store it
        if (!isset($_SESSION['shopping_cart'])) {
            $_SESSION['shopping_cart'] = new ShoppingCart();
        }

        $this->cart = $_SESSION['shopping_cart'];
        return $this->cart;
    }

    /**
     * Returns item object to adding a new item to the cart
     *
     * @param  integer unique identifer for this item of this class (primary key for database object)
     * @return array
     */
    public function newItem() {
        return new ShoppingCartItem();
    }

    /**
     * Returns item (sfShoppingCartItem instance) from the shopping cart or null if not found.
     *
     * @param  integer unique identifer for this item of this class (primary key for database object)
     * @return array
     */
    public function getItem($id) {
        $ind = $this->getItemIndice($id);
        return ($ind !== null ? $this->items[$ind] : null);
    }

    /**
     * Returns indice of the given item in the $this->items array.
     *
     * @return integer
     */
    public function getItemIndice($id) {
        $ind = null;

        foreach ($this->items as $key => $item) {
            if ($item->getId() == $id) {
                $ind = $key;
                break;
            }
        }
        return $ind;
    }

    /**
     * Return items from shopping cart
     *
     * @return array
     */
    public function getItems() {
        // Create array with index as part number
        $tmp = array();
        foreach ($this->items as $item) {
            $tmp[$item->getId()] = $item->getName();
        }
        // Sort array by index which is part number ASC
        natsort($tmp);

        // Create new array with sorted keys
        $items = array();
        foreach ($tmp as $id => $name) {
            $items[] = $this->getItem($id);
        }

        return $items;
    }

    /**
     * Adds an item to the shopping cart.
     *
     * @param class of item
     */
    public function addItem($item) {
        // Check if there is already an existing item
        $existingItem = $this->getItem($item->getId());

        // If item exists, update quantity
        if ($existingItem) {
            $existingItem->setQuantity($item->getQuantity());
        }
        // else add to items
        else {
            $this->items[$item->getName()] = $item;
        }
    }

    /**
     * Deletes item from the shopping cart.
     *
     * @param  integer unique identifier for this item of this class (primary key for database object for example)
     */
    public function deleteItem($id) {
        foreach ($this->items as $i => $v) {
            if ($this->items[$i]->getId() == $id) {
                unset($this->items[$i]);
            }
        }
    }

    /**
     * Returns total weight for all items in the shopping cart.
     *
     * @return float
     */
    public function getTotalWeight() {
        $totalWeight = 0;

        foreach ($this->getItems() as $item) {
            $totalWeight += $item->getQuantity() * $item->getWeight();
        }

        return $totalWeight;
    }

    /**
     * Returns total price for all items in the shopping cart.
     *
     * @return float
     */
    public function getTotalPrice() {
        $totalPrice = 0;

        foreach ($this->getItems() as $item) {
            $totalPrice += $item->getQuantity() * str_replace(",", "", $item->getPrice());
        }

        return $totalPrice;
    }

    /**
     * Returns the number of items in the shopping cart.
     *
     * @param  boolean count the actualy number of items instead of the number of items in the array
     * @return integer
     */
    public function getNbItems($countQuantities = false) {
        if ($countQuantities) {
            $count = 0;
            foreach ($this->getItems() as $item) {
                $count += $item->getQuantity();
            }
            return $count;
        }

        return count($this->getItems());
    }

    /**
     * Is shopping cart empty?
     *
     * @return boolean
     */
    public function isEmpty() {
        return ($this->getNbItems() == 0 ? true : false);
    }

    /**
     * Remove all items from the shopping cart.
     */
    public function clear() {
        $this->items = array();
    }

    /**
     * Clears items from cart, and resets it
     */
    public function resetCart() {
        // Clear items
        $this->clear();

        $this->setShippingAddress(null);
        $this->setOrderId(null);
    }

    /**
     * Returns item id
     *
     * @return integer
     */
    public function getOrderId() {
        return $this->orderId;
    }

    /**
     * Sets order id
     *
     * @param integer
     */
    public function setOrderId($orderId) {
        $this->orderId = $orderId;
    }

    /**
     * Get shipping address
     *
     * @return string
     */
    public function getShippingAddress() {
        return $this->shippingAddress;
    }

    /**
     * Sets shipping address
     *
     * @param integer
     */
    public function setShippingAddress($shippingAddress) {
        $this->shippingAddress = $shippingAddress;
    }

}

/* * ***************************************************************************
 * Shopping Cart Items
 * -------------------------------
 * An object to store the items in ShoppingCart->items
 *
 * **************************************************************************** */

class ShoppingCartItem {

    private
    $id = 0,
    $name = null,
    $desc = null,
    $quantity = 0,
    $price = 0,
    $totalPrice = 0,
    $weight = 0;

    /**
     * Returns item id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Sets item id
     *
     * @param integer
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * Returns item name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets item id
     *
     * @param string
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Returns item description
     *
     * @return string
     */
    public function getDescription() {
        return $this->desc;
    }

    /**
     * Sets item description
     *
     * @param integer
     */
    public function setDescription($desc) {
        $this->desc = $desc;
    }

    /**
     * Returns quanity
     *
     * @return integer
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * Sets quanity
     *
     * @param integer
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    /**
     * Returns price
     *
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Sets price
     *
     * @param float
     */
    public function setPrice($price) {
        $this->price = $price;
    }

    /**
     * Returns total price
     *
     * @return float
     */
    public function getTotalPrice() {
        return $this->totalPrice;
    }

    /**
     * Sets total price
     *
     * @param float
     */
    public function setTotalPrice($totalPrice) {
        $this->totalPrice = $totalPrice;
    }

    /**
     * Returns weight
     *
     * @return float
     */
    public function getWeight() {
        return $this->weight;
    }

    /**
     * Sets weight
     *
     * @param float
     */
    public function setWeight($weight) {
        $this->weight = $weight;
    }

}

?>