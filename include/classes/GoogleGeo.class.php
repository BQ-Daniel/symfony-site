<?php

class GoogleGeo
{
    public $result = null;
    
    public function __construct()
    {
        $this->geocodeUrl = "http://maps.googleapis.com/maps/api/geocode/xml";
    }
    
    /**
     * Get geo codes for address
     * 
     * @param string $address
     * @return boolean|SimpleXMLElement
     */
    public function getGeocode($address)
    {
        $request = $this->baseUrl . "&q=" . urlencode($address).'&format=xml&sensor=false';
        
        $params = array(
            'address' => $address,
            'sensor' => 'false'
        );
        
        
        $xml = simplexml_load_file($this->geocodeUrl.'?'. http_build_query($params));
        if (!$xml) {
            return false;
        }
        
        $this->result = $xml;
        
        $status = $xml->status;
        if ($status != "OK")
        {
            return array(
                'result' => false,
                'xml' => $xml,
            );
        }
        
        return array(
            'result' => true,
            'status' => $status,
            'xml' => $xml,
        );
    }
    
    /**
     * Return LNG / LAT of location
     * 
     * @return boolean|array
     */
    public function getGeometry()
    {
        if (is_null($this->result)) {
            return false;
        }
        
        $result = $this->result;
        $location = $result->result->geometry->location;
        if (!$location) {
            return false;
        }
        
        return array(
            'lng' => $location->lng,
            'lat' => $location->lat,
        );
    }
    
}