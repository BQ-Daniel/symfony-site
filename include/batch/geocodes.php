<?php
require_once(__DIR__ . '/../load.php');

require_once(__DIR__ . '/../classes/GoogleGeo.class.php');

$googleGeo = new GoogleGeo();

// Get all users that don't have geo code and are not deleted
$q = Doctrine_Query::create()->from('UserAddress ua')
        ->innerJoin('ua.Users u')
        ->where('u.deleted = 0')
        ->andWhere('u.distributor = 1')
        ->andWhere('ua.address_type = ?', 'delivery')
        ->andWhere('ua.lat IS NULL OR (ua.lat = ? AND ua.lng = ?)', array('0.000000', '0.000000'));

$results = $q->execute();

$errors = 0;

foreach ($results as $result)
{
    sleep(2);
    
    $row = $result->toArray();

    if ($row['address_type'] != 'delivery') {
        continue;
    }

    $address = ucwords((!empty($row['address1']) ? $row['address1'].' ' : '').
            (!empty($row['address2']) ? $row['address2'].' ' : '').
            $row['suburb'].' '.$row['state'].' '.$row['postcode']);
    
    // Get geo result
    $geo = $googleGeo->getGeocode($address);
    
    // Setup log
    $log = new GoogleGeoLog();
    $log->user_address_id = $row['id'];
    $log->address = $address;
    $log->result = json_encode($geo['xml']);
    $log->success = false; // Default to false
    
    // Set default as 0, as this means it has been processed at least
    $result->lng = 0;
    $result->lat = 0;
    
    
    
    // If failed, log and continue on
    if ($geo['result'])
    {
        if ($geometry = $googleGeo->getGeometry()) {
            $result->lng = $geometry['lng'];
            $result->lat = $geometry['lat'];
            $log->success = true;
        }
    }
    
    if (!$log->success) {
        $errors++;
    }

    $log->save(); 
    $result->save();
}
if ($errors > 0) {
    mail('daniel@dmwebdesign.com.au', 'BrakeQuip GEO Error', 'There were '.$errors.' errors', 'From: Brakequip Error <noreply@brakequip.com.au>');
    print "There were $errors errors\n";
}
echo "DONE";
