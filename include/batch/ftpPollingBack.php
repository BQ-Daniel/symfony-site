<?php
$ftpStatus = true;
function saveLog($msg){
    $fp = fopen('data2.txt', 'a+');
    fwrite($fp, $msg);
    fclose($fp);
}

function uploadWebFile($conn_id, $dir, $entry)
{
    saveLog('Uploaded Started '.$entry.", Time : ".date("d-M-Y H:i:s")."\r\n");
    
    if (!ftp_put($conn_id, $entry, $dir.$entry, FTP_ASCII)) {
        $msg = $php_errormsg;

     	saveLog('Uploaded Failed '.$entry.", Time : ".date("d-M-Y H:i:s")."\r\n");

        mail('daniel@brakequip.com.au, orders@brakequip.com.au', 'Failed to upload ' . $entry, '',  'From: BrakeQuip Orders <noreply@brakequip.com.au>');
	//mail('kuldeepgill.impinge@gmail.com', 'Failed to upload '.FTP_HOST . $entry, $msg,  'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        // mail('john@smytheweb.com', 'Failed to upload ' . $entry, '',  'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        return false;
    }
    
    $size = ftp_size($conn_id, $entry);
    $size2 = ftp_size($conn_id, "ARCHIVE/".$entry);

    saveLog('Uploaded Finished '.$entry.", Size: ".$size.", Time : ".date("d-M-Y H:i:s")."\r\n");

    $fp = fopen('data.txt', 'a+');
    fwrite($fp, 'Uploaded '.$entry.", Size: ".$size.", Time : ".date("d-M-Y H:i:s")."\r\n");
    fclose($fp);

    // Check if file exists on server
    if ($size == -1 && $size2 == -1) {
        mail('daniel@brakequip.com.au', 'Check that ' . $entry.' was uploaded successfully. '.$size, '', 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
       // mail('kuldeepgill.impinge@gmail.com', 'Check that ' . $entry.' was uploaded successfully. ('.$size.') ('.$size2.')', '', 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
    }

    // Archive
    copy($dir . $entry, $dir . 'archive/' . $entry);
    // Remove file once completed
    unlink($dir . $entry);

    return true;
}

set_time_limit(0);

require_once('../load.php');
require_once('../classes/OrderParts.php');

$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS);
mysql_select_db(DB_NAME, $conn);

saveLog("Connection Openned, Time : ".date("d-M-Y H:i:s")."\r\n");
// Connect to firewall
$conn_id = ftp_connect(FTP_HOST);

if ( !$conn_id ) {
    saveLog("Connection Failed , Time : ".date("d-M-Y H:i:s")."\r\n");
    die;
}

// Open a session to an external ftp site
$login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

// Check open
if ((!$conn_id) || (!$login_result)) {
    saveLog("Login Failed , Time : ".date("d-M-Y H:i:s")."\r\n");
    ftp_close($conn_id);
    die;
}

// turn on passive mode transfers
ftp_pasv($conn_id, true);

// Upload any order files to the FTP server
$dir = $ordersDir = "../../orders/";
$d = dir($dir);
$orderFiles = array();

while (false !== ($entry = $d->read()))
{
    if ($entry != "." && $entry != "..") {
        // Upload order file
        if (substr($entry, -4) == ORDER_EXTENSION)
        {
           $ftpStatus  = uploadWebFile($conn_id, $ordersDir, $entry);
            if (!$ftpStatus) {
            	saveLog("Upload Failed - WHILE, Time : ".date("d-M-Y H:i:s")."\r\n");
                break;
                //continue;
            }
        }
    }
}
$d->close();

if (!$ftpStatus) {
    saveLog("Exiting - Upload Failed , Time : ".date("d-M-Y H:i:s")."\r\n");
    ftp_close($conn_id);
    die;
}

//sleep(6);

// Get a list of orders that have been submitted, we want to check if they have been processed or not
$orders = array();
$q = mysql_query("SELECT ORDER_NO, EASE_ORDER_NO FROM ORDERS WHERE STATUS = 'S'") or die(mysql_error());
$notProcessed = array();
while ($result = mysql_fetch_object($q))
{
    // If file is still waiting to be processed, then ignore
    if (ftp_size($conn_id, $result->EASE_ORDER_NO . ".WEB") != '-1') {
        continue;
    }
	$already_proccesed = mysql_num_rows(mysql_query("SELECT * FROM proccesed_files where filename='".$result->EASE_ORDER_NO."' and status='proccessed'"));
	if($already_proccesed>0){
		//mail("kuldeepgill.impinge@gmail.com", "Order".$result->EASE_ORDER_NO." already proccessed", "It has been already proccessed.", 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
	}
    mysql_query("INSERT INTO proccesed_files (filename,time,status) VALUES ('".$result->EASE_ORDER_NO."','".date('Y-m-d h:i:s')."','submitted')");
    // Check if file exists, returns -1 if it doesn't
    if (ftp_size($conn_id, "ARCHIVE/" . $result->EASE_ORDER_NO . ".WEB") != '-1') {
        // Update order to be processed
        mysql_query("UPDATE ORDERS SET STATUS = 'P' WHERE ORDER_NO = '" . $result->ORDER_NO . "'");
        mysql_query("INSERT INTO proccesed_files (filename,time,status) VALUES ('".$result->EASE_ORDER_NO."','".date('Y-m-d h:i:s')."','proccessed')");
    }
    // Couldn't find archive version, lets record this
    else {

       $notProcessed[$result->EASE_ORDER_NO . ".WEB"] = true;
    }
}

if ( !$conn_id ) {
    saveLog("Connection Already Closed, Time : ".date("d-M-Y H:i:s")."\r\n");
    die;
}
// Poll directory for PARTS file and Processed order numbers
$contents = ftp_nlist($conn_id, ".");
if ( !empty($contents) ) {
 foreach ($contents as $fileName)
 {
    $file = substr($fileName, 2);
    // If STATUS stop credit file,
    if (strtoupper($file) == "STATUS.TAB")
    {

        if (ftp_get($conn_id, "../tmp/" . $file, $file, FTP_ASCII) ) {
        // Read file
	        if (($handle = fopen("../tmp/" . $file, "r")) !== FALSE) {
	            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	                $account = trim($data[0]);
	                //$account1 = split("          ", $account);
	                $account = str_replace(' ', '', $data[0]);
	                $sc =  substr($account, -1);
	                $username=substr(preg_replace('/\s+/', '', $account), 0, -1);
	                $stopCredit = (trim($sc) == 'Y' ? 1 : 0);
			//X990          N
	                //X990
	                // Update user table
	                mysql_query("UPDATE USERS SET STOP_CREDIT = '" . $stopCredit . "', UPDATED_AT = NOW() WHERE USERNAME = '". $username ."'");

	            }
	            fclose($handle);
	        }
	        // Remove file from server once we are finished
	        unlink("../tmp/" . $file);
        } else {
        	saveLog("Exiting -1- download failed ".$file.", Time : ".date("d-M-Y H:i:s")."\r\n");
        	ftp_close($conn_id);
    		die;
        }
        continue;
    }


    // If web file, record
    if (substr($file, -4) == ORDER_EXTENSION) {
        // If it's in the not processed yet array,
        // then removed it as EASE hasn't processed it yet
        if (isset($notProcessed[$file])) {
            unset($notProcessed[$file]);
        }
        continue;
    }

    // If not parts file, ignore
    if (strtoupper($file) != strtoupper(PARTS_FILE)) {
        continue;
    }

    $filename = md5(time() + rand(1, 9999999999999));
    if ( ftp_get($conn_id, "../tmp/" . $filename, $file, FTP_ASCII) ) {
    	//saveLog("File downloaded".$filename .", Time : ".date("d-M-Y H:i:s")."\r\n");
    }  else {
    	saveLog("Exiting -2- download failed ".$filename .", Time : ".date("d-M-Y H:i:s")."\r\n");
    	ftp_close($conn_id);
    	die;
    }

    if (!file_exists("../tmp/" . $filename)) {
        continue;
    }

    $products = new OrderParts("../tmp/" . $filename);
    $products->import();
    // Remove from server
    unlink("../tmp/" . $filename);
    // Remove from FTP server
    //ftp_delete($conn_id, $file);
 }
}

unset($orders);

// If there are orders in the $notProcessed array
if (count($notProcessed) > 0)
{
    sleep(30);
    // Check archive folder one more time
    foreach ($notProcessed as $file => $value) {
        if (ftp_size($conn_id, "ARCHIVE/" . $file) != '-1') {
            unset($notProcessed[$file]);
        }
    }

    // Still exist, then resubmit
    if (count($notProcessed) > 0)
    {
        mail("daniel@brakequip.com.au", "Order".$notProcessed > 1 ? 's' : ''." not processed [RESUBMITTED]", print_r($notProcessed, true)."It has been resubmitted.", 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        //mail("kuldeepgill.impinge@gmail.com", "Order".$notProcessed > 1 ? 's' : ''." not processed [RESUBMITTED]", print_r($notProcessed, true)."It has been resubmitted.", 'From: BrakeQuip Orders <noreply@brakequip.com.au>');
        foreach ($notProcessed as $file => $value) {
            copy(realpath($ordersDir.'archive/'.$file), $ordersDir.$file);
            if (!uploadWebFile($conn_id, $ordersDir, $file)) {
              break;
            }
        }
    }

}

saveLog("Connection Close, Time : ".date("d-M-Y H:i:s")."\r\n");
// Close FTP Connection
ftp_close($conn_id);
?>
