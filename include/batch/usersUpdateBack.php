<?php
set_time_limit(0);

require_once('../load.php');

// Connect to firewall
$conn_id = ftp_connect(FTP_HOST);

// Open a session to an external ftp site
$login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD);

// Check open
if ((!$conn_id) || (!$login_result)) {
    echo "Connection failed";
    die;
}

// turn on passive mode transfers
ftp_pasv($conn_id, true);

$usernames = array();

// Poll directory for PARTS file and Processed order numbers
$contents = ftp_nlist($conn_id, "");
foreach ($contents as $file) {

    // If not parts file, ignore
    if (strtoupper($file) != strtoupper('DEBTORS.CSV') && strtoupper($file) != strtoupper('DEBTORS2.CSV')) {
        continue;
    }

    $filename = md5(time() + rand(1, 9999999999999));
    ftp_get($conn_id, $filename, $file, FTP_BINARY);

    if (!file_exists($filename)) {
        echo "NO FILE: ".$filename;
        continue;
    }

    // Open file
    $handle = fopen($filename, "r");

    // Ignore first line
    fgetcsv($handle, 1000, ",");

    // Get first line, as this holds the headers
    $data = fgetcsv($handle, 1000, ",");
    defineFieldNames($data);

    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
    {
        /* USERNAMES */
        $username = trim(addslashes($data[AC_CODE]));
        $email = trim(addslashes($data[EMAIL_ADDRESS]));
        
        $usernames[] = $username;

        if (empty($username) || empty($email)) {
            if (empty($email)) {
                $emailMissing[] = $username;
            } else {
                continue;
            }
        }

        // Check if username exists
        $user = UsersTable::getInstance()->findOneBy('username', $username);
        // add 
        if (!$user) {
            $user = new Users();
            $user->username = $username;
            $user->email = $email;
            $user->created_at = date('Y-m-d H:i:s');
            $user->distributor = true;
if($data[PRICE_CODE]=='E'){
$user->distributor = 0;
}
            $user->price_code = $data[PRICE_CODE];
            $user->order_access = true;
            $user->save();
           
            $added[] = $username;
            $userId = $user->id;
        }
        // update
        else {
            $userId = $user->id;
            $user->email = $email;
            $user->price_code = $data[PRICE_CODE];
$user->distributor = 1;
if($data[PRICE_CODE]=='E'){
$user->distributor = 0;
} 
           $user->deleted = 0;
            $user->save();
            $updated[] = $username;
        }

        if (empty($userId)) {
            continue;
        }



        /* ADDRESSES */

        $states = array('VIC', 'NSW', 'QLD', 'SA', 'NT', 'WA', 'TAS', 'NZ', 'ACT');

        // Setup variables for DELIVERY ADDRESS
        $values['name'] = addslashes(trim($data[NAME]));
        $values['address1'] = addslashes(trim($data[ADDRESS1]));
        $values['address2'] = addslashes(trim($data[ADDRESS2]));
        $values['suburb'] = addslashes(trim($data[SUBURB]));
        $values['state'] = strtoupper(trim(substr($values['suburb'], -3)));
        $values['postcode'] = addslashes(trim($data[POSTCODE]));
        $values['phone'] = addslashes(trim($data[PHONE_NO1]));

        if (!in_array($values['state'], $states)) {
            $values['state'] = '';
        } else {
            $values['suburb'] = trim(rtrim(trim($values['suburb']),trim($values['state'])));
        }
        
        $address = Doctrine_Query::create()->from("UserAddress")->
                where("user_id = ? AND address_type = ?", array($userId, 'delivery'))->fetchOne();
        // Check if delivery address exists
        //$address = UserAddressTable::getInstance()->findOneByUserIdAndAddressType(array($userId, 'delivery'));
        
        if (!$address) {
            $address = new UserAddress();
            $address->user_id = $userId;
        }
        $address->name = $values['name'];
        $address->address1 = $values['address1'];
        $address->address2 = $values['address2'];
        $address->suburb = $values['suburb'];
        $address->state = $values['state'];
        $address->postcode = $values['postcode'];
        $address->phone = $values['phone'];
        $address->address_type = 'delivery';

        // If address is modified, then set lat / lng to null so google updates it
        if ($address->isModified()) {
            mail('daniel@brakequip.com.au', 'isModified', print_r($address->getModified(), true), 'BrakeQuip Error <noreply@brakequip.com.au>');
            $address->updated_at = date('Y-m-d H:i:s');
            $address->lng = null;
            $address->lat = null;
        }
        $address->save();


        // Setup variables for INVOICE ADDRESS
        $values['name'] = addslashes(trim($data[INVOICE_NAME]));
        $values['address1'] = addslashes(trim($data[INVOICE_ADDRESS1]));
        $values['address2'] = addslashes(trim($data[INVOICE_ADDRESS2]));
        $values['suburb'] = addslashes(trim($data[INVOICE_SUBURB]));
        $values['state'] = strtoupper(trim(substr($values['suburb'], -3)));
        $values['postcode'] = addslashes(trim($data[POSTCODE]));
        $values['phone'] = addslashes(trim($data[PHONE_NO1]));

        if (!in_array($values['state'], $states)) {
            $values['state'] = null;
        } else {
            $values['suburb'] = trim(str_replace($values['state'], '', $values['suburb']));
        }

        // Check if delivery address exists
        $address = Doctrine_Query::create()->from("UserAddress")->
                where("user_id = ? AND address_type = ?", array($userId, 'invoice'))->fetchOne();

        if (!$address) {
            $address = new UserAddress();
            $address->user_id = $userId;
        }
        $address->name = $values['name'];
        $address->address1 = $values['address1'];
        $address->address2 = $values['address2'];
        $address->suburb = $values['suburb'];
        $address->state = $values['state'];
        $address->postcode = $values['postcode'];
        $address->phone = $values['phone'];
        $address->address_type = 'invoice';

        if ($address->isModified()) {
            $address->updated_at = date('Y-m-d H:i:s');
            $address->lng = null;
            $address->lat = null;
        }
        $address->save();
    }
   
    // Remove from server
    
    
    //unlink($filename);
}

// Need to handle 936a, 936b, 936c
  
  if(count($usernames)>0){
    $usernames = array_merge($usernames, $excludeFromStats);

    $deleteIds = array();
    $q = Doctrine_Query::create()->from('Users')->whereNotIn('username', $usernames)->andWhere('deleted = 0');
    if ($results = $q->fetchArray()) 
    {
    	$myfile1 = fopen("usernames.txt", "a");
	fwrite($myfile1, implode(',',$usernames));
	fwrite($myfile1, "\n\n\n\n");
	fclose($myfile1); 
	
	$myfile2 = fopen("exclude.txt", "a");
	fwrite($myfile2, implode(',',$excludeFromStats));
	fwrite($myfile2, "\n\n\n\n");
	fclose($myfile2);
	
        foreach ($results as $result)
        {
            $username = preg_replace("/[^\d]/", "", $result['username']);
            if (!in_array($username, $usernames)) {
                $deleteIds[] = $result['id'];
            }
        }
    }
   
    if (count($deleteIds) > 0) {
        /*$count = Doctrine_Query::create()->update("Users")
                ->set('deleted', 1)
                ->set('distributor', 0)
                ->set('PRICE_CODE', 'X')
                ->set('updated_at', "'".date('Y-m-d H:i:s')."'")
                ->whereIn('id', $deleteIds)
                ->execute(); */
        
        foreach($deleteIds as $delete_id){
            $user = UsersTable::getInstance()->findOneBy('id', $delete_id);
            $userId = $delete_id;

            $user->price_code = 'X';
	    $user->distributor = 0;
            $user->deleted = 1;
            $user->save();
        }
                $txt = implode(',',$deleteIds);
          	$myfile = fopen("debtors.txt", "a");
		fwrite($myfile, $txt);
		fwrite($myfile, "\n\n\n\n");
		fclose($myfile);    
    }
   }

// Close FTP Connection
ftp_close($conn_id);

include 'geocodes.php';

/**
 * Defines each field name with the value of the key in the array
 *
 * @param array
 */
function defineFieldNames($data) {
    foreach ($data as $key => $value) {
        // Replace -'s with _
        $value = str_replace('-', '', $value);
        $value = str_replace(' ', '_', $value);
        $value = str_replace('/', '', $value);
        define($value, $key);
    }
}

?>