<?php
$ftpStatus = true;
function saveLog($msg){
    $fp = fopen('data2.txt', 'a+');
    fwrite($fp, $msg);
    fclose($fp);
}

set_time_limit(0);

require_once(__DIR__ . '/../load.php');
require_once(__DIR__ . '/../classes/OrderParts.php');
//
$conn = mysql_connect(DB_HOST, DB_USER, DB_PASS);
mysql_select_db(DB_NAME, $conn);
//
saveLog("Connection Openned, Time : ".date("d-M-Y H:i:s")."\r\n");

// Upload any order files to the FTP server
$ordersDir = "../../orders/";
$d         = dir($ordersDir);

while (false !== ($entry = $d->read()))
{
    if ($entry == "." || $entry == "..") {
        continue;
    }

    // If not parts file, ignore
    if (strtoupper($entry) != strtoupper('PARTS.csv')) {
        continue;
    }

    $products = new OrderParts($ordersDir . $entry);
    $products->import();
}
$d->close();
saveLog("ftpPolling ended : ".date("d-M-Y H:i:s")."\r\n");
?>