<?php

// Database details
define('DB_HOST', 'localhost');
define('DB_USER', 'brakeq_app');
define('DB_PASS', 'placeholder');
define('DB_NAME', 'brakeq_copy');

// ENCRYPT PASSWORD FOR USER PASSWORDS
define('ENCRYPT_PASS', 'placeholder');

// Website url
define('WEBSITE_URL', 'http://www.brakequip.com.au/');

// Directory of where product images are stored
define('PRODUCTS_IMAGE_DIR', 'products/');

// How long doe the sessions go for? (in minutes)
define('SESSION_EXPIRY', '600');

// FTP Settings
define('FTP_HOST',     'placeholder');
define('FTP_USERNAME', 'placeholder');
define('FTP_PASSWORD', 'placeholder');

// Polling Settings
define('ORDER_EXTENSION', '.WEB');
define('PARTS_FILE', 'PARTS.csv');

// Google
define("GOOGLE_MAPS_KEY", "ABQIAAAAEGGQCiftYuYiwkrmSk0o33TRRxXHu0SwMafewrfereEH0kvSkqvByCQ9BcxRv7ftTK0pJa8ApcYQEcoF3GADs1A");
define("GOOGLE_MAPS_HOST", "maps.google.com");

// Major Category descriptions
$majorCat = array('BQ'		=> 'HOSE, ACCESSORIES ETC...',
                  'HF'		=> 'FITTINGS',
				  'HFB'		=> 'BANJO',
				  'HFC'     => 'CENTRE PEICE',
				  'HFIF'    => 'IMPERIAL FEMALE',
				  'HFIFX'   => 'IMPERIAL FEMALE EXTERNAL THREAD',
				  'HFIM'    => 'IMPERIAL MALE',
				  'HFMF'    => 'METRIC FEMALE',
				  'HFMFX'   => 'METRIC FEMALE EXTERNAL THREAD',
				  'HFMM'    => 'METRIC MALE',
				  'SS'      => 'STAINLESS STEEL');

// Brake positions
$positions = array(1 => 'R/H Front', 2 => 'L/H Front', 3 => 'R/H Rear', 4 => 'L/H Rear');

// Exclude from stats
$excludeFromStats = array('DANIEL', 'JOHN', 'SMYTHE', 'CHRIS', 'GRAEME', 'margas', 'Fred', 'BQDON', 'BQGARRET', '733', '774', '961', '1014', 'ADEN', 'IMPINGE' );
define('EXLUDE_FROM_STATS', serialize($excludeFromStats));

global $positions;


include('doctrine.config.php');

// Load Doctrine

require_once(dirname(__FILE__).'/doctrine/lib/Doctrine.php');
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine', 'modelsAutoload'));

// Load the Doctrine connection
Doctrine_Manager::connection('mysql://brakeq_website:placeholder@localhost/brakeq_catalogue', 'old_website');
Doctrine_Manager::connection('mysql://' . DB_USER . ':' . DB_PASS. '@' . DB_HOST . '/' . DB_NAME, 'main');

// Set the model loading to conservative/lazy loading
Doctrine_Manager::getInstance()->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE);

// Load the models for the autoloader
Doctrine_Core::loadModels(MODELS_PATH);

?>