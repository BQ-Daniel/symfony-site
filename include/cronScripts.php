<?php

$scripts = array(
    'minute'  => array('batch/ftpPolling.php'),
    'hourly'  => array(),
    'daily'   => array(),
    '2daily'  => array(
        'batch/usersUpdate.php',
        'batch/statusPolling.php',
    ),
  	'weekly'  => array(),
  	'monthly' => array(),
);

// Daily
if (date("h:ia") == "12:05am") {
    foreach ($scripts['daily'] as $script) {
        exec('php ' . $script);
    }
}

// Twice Daily
if (date("h:ia") == "12:05am" || date("h:ia") == "12:05pm") {
    foreach ($scripts['2daily'] as $script) {
        exec('php ' . $script);
    }
}

// Every minute
foreach ($scripts['minute'] as $script) {
    exec('php ' . $script);
}

// Every 5 minutes
/*
if (substr(date("i"), 1) == "5" || substr(date("i"), 1) == "0") {
    foreach ($scripts['minute'] as $script) {
        exec('php ' . $script);
    }
}
 *
 */


// Hourly
if (date("i") == "00") {
    foreach ($scripts['hourly'] as $script) {
        exec('php ' . $script);
    }
}


// Weekly, every sunday
if (date("w") == "0") {
    foreach ($scripts['weekly'] as $script) {
        exec('php ' . $script);
    }
}

// Monthly - first of every month
if (date("j") == "1") {
    foreach ($scripts['monthly'] as $script) {
        exec('php ' . $script);
    }
}
?>
