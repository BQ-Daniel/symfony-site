<?php

define('_DIR_INCLUDE', dirname(__FILE__.'../'));

define('DATA_FIXTURES_PATH', _DIR_INCLUDE . '/data/fixtures');
define('MODELS_PATH', _DIR_INCLUDE. '/models');
define('MIGRATIONS_PATH', _DIR_INCLUDE . '/migrations');
define('SQL_PATH', _DIR_INCLUDE . '/data/sql');
define('YAML_SCHEMA_PATH', _DIR_INCLUDE . '/');

$doctrineConfig = array(
    'data_fixtures_path' => DATA_FIXTURES_PATH,
    'models_path' => MODELS_PATH,
    'migrations_path' => MIGRATIONS_PATH,
    'sql_path' => SQL_PATH,
    'yaml_schema_path' => YAML_SCHEMA_PATH,
    'generate_models_options' => array(
        'pearStyle' => false,
        'generateTableClasses' => true,
        'baseClassPrefix' => 'Base',
        'baseClassesDirectory' => 'base',
    )
);
?>