<?php
die("Website is currently down. Please check back later.");
session_start();

include(realpath(dirname(__FILE__)).'/config.php');
include(realpath(dirname(__FILE__)).'/functions.php');

define('_DIR_ROOT', realpath(dirname(__FILE__).'/../'));
define('_DIR_CLASSES', realpath(dirname(__FILE__)).'/classes/');


/* AJAX check  */
$isAjaxRequest = false;
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
  $isAjaxRequest = true;
}

$_GET['req'] = (isset($_GET['req']) ? $_GET['req'] : null);
$request = explode("/", $_GET['req']);

$dir = $modules = null;

if (count($request) > 1) {
    $tmp = $request;
    unset($tmp[(count($tmp)-1)]);
    $dir = $modules = implode('/', $tmp);
    $dir = $dir . '/';
}
$templateName = $request[count($request)-1];
$templateName = (empty($templateName) ? 'index' : $templateName);

$extendDir = str_repeat('../', count($tmp));

require_once 'classes/sfTemplateAutoloader.php';
sfTemplateAutoloader::register();
$loader = new sfTemplateLoaderFilesystem(dirname(__FILE__).'/../templates/' . $dir . '%name%.php');

require_once 'classes/customTemplateRenderer.php';

$engine = new sfTemplateEngine($loader, array(
  'php' => new customTemplateRenderer($engine),
));
?>