// when the DOM is ready...
$(document).ready(function ()
{
    if ($('#orderProductsContainer').length > 0) {
        var $container = $('.scrollContainer');
        if ($container.length > 0) {
            var $panels = $('.panel');

            // if false, we'll float all the panels left and fix the width 
            // of the container
            var horizontal = true;

            // float the panels left if we're going horizontal
            if (horizontal) {
              $panels.css({
                'float' : 'left',
                'position' : 'relative' // IE fix to ensure overflow is hidden
              });

              // calculate a new width for the container (so it holds all panels)
              $container.css('width', $panels[0].offsetWidth * $panels.length);
            }

            // collect the scroll object, at the same time apply the hidden overflow
            // to remove the default scrollbars that will appear
            var $scroll = $('.scroll');

            // handle nav selection
            function selectNav() {
              $(this)
                .parents('ul:first')
                  .find('a')
                    .removeClass('selected')
                  .end()
                .end()
                .addClass('selected');
            }

            $('.sliderNav').find('a').click(selectNav);

            // go find the navigation link that has this target and select the nav
            function trigger(data) {
              var el = $('#slider .sliderNav').find('a[href$="' + data.id + '"]').get(0);
              $('#'+data.id).scrollTop(0);
              selectNav.call(el);
            }

            if (window.location.hash) {
              trigger({id : window.location.hash.substr(1)});
            } else {
              $('ul.sliderNav a:first').click();
            }

            // offset is used to move to *exactly* the right place, since I'm using
            // padding on my example, I need to subtract the amount of padding to
            // the offset.  Try removing this to get a good idea of the effect
            var offset = parseInt((horizontal ? 
              $container.css('paddingTop') : 
              $container.css('paddingLeft')) 
              || 0) * -1;


            var scrollOptions = {
              target: $scroll, // the element that has the overflow

              // can be a selector which will be relative to the target
              items: $panels,

              navigation: '.sliderNav a',

              // allow the scroll effect to run both directions
              axis: 'xy',

              onAfter: trigger, // our final callback

              offset: offset,

              // duration of the sliding effect
              duration: 500,

              // easing - can be used with the easing plugin: 
              // http://gsgd.co.uk/sandbox/jquery/easing/
              easing: 'swing'
            };

            // apply serialScroll to the slider - we chose this plugin because it 
            // supports// the indexed next and previous scroll along with hooking 
            // in to our navigation.
            $('#slider').serialScroll(scrollOptions);

            // now apply localScroll to hook any other arbitrary links to trigger 
            // the effect
            $.localScroll(scrollOptions);

            // finally, if the URL has a hash, move the slider in to position, 
            // setting the duration to 1 because I don't want it to scroll in the
            // very first page load.  We don't always need this, but it ensures
            // the positioning is absolutely spot on when the pages loads.
            scrollOptions.duration = 1;
            $.localScroll.hash(scrollOptions);
        }
        
        $('.loadProducts').each(function ()
        {
            var cat = this.id;
            $.get('/order/load-products', {category: cat}, function (data) {
               $('#'+cat).html(data); 
                $('.qty', $('#'+cat)).change(updateCart);   

                $('.qty', $('#'+cat)).keydown(function (event) {
                    var el = event.currentTarget;
                    var enterKey = 13;
                    var tabKey   = 9;
                    var parentDiv = $(el).parent().parent().parent();
                    if (event.keyCode == enterKey || event.keyCode == tabKey) {
                        return setFocusOnNextQty(parentDiv, el);
                    }
                });
            });
        });
        setCheckAuth();
    }
    
    // Add functions to qty inputs
    $('.qty').change(updateCart);   
    
    $('.qty').keydown(function (event) {
        var el = event.currentTarget;
        var enterKey = 13;
        var tabKey   = 9;
        var parentDiv = $(el).parent().parent().parent();
        if (event.keyCode == enterKey || event.keyCode == tabKey) {
            return setFocusOnNextQty(parentDiv, el);
        }
    });
    
    // Add delete functions to delete buttons on cart
    $('#liveList .delete').click(function () {
        var el = $(this).parent().parent().attr('id');
        var split = el.split('-');
        var id = split[1];
        deleteFromCart(id);
        return false;
    });
    
    // Shipping method
    
    $('#shipping').change(function () {
        shippingMethod();
    });
    shippingMethod();
    
    $('#orderForm').submit(submitOrder);
});

function shippingMethod() {
    if ($('#shipping').val() == "Other") {
        $('#shippingOther').show();
    }
    else {
        $('#shippingOther').hide();
    }
}

function updateCart(event)
{    
    var el = event.currentTarget;
    var part_num = $(el).attr('data-value'); 
    var id = $(el).attr('id').split("-"); 
    var id = id[1]; 
    var enterKey = 13;
    var tabKey   = 9;
    var qty      = el.value;
    if (!qty.match(/^([0-9])$/)) {
       
        $(el).attr('value',Math.ceil(qty));
        qty = Math.ceil(qty);
       
    }
    var parentDiv = $(el).parent().parent().parent();
  
  	//impinge: check if its ordered in 5 metre intervals - removed for now.

	if(part_num == "HFIF01B"  || part_num == "HFIM10B" ){
	
   		
		if(confirm("You have selected the brass version of this part, is this correct?")) {
		     
		}
		else {
			var existing = $('#qty-'+id).val();
			if(existing.length == 0) {
			$('#qty-'+id).val('');
			} else {
			$.get('/order/get-items', function (data) {
				var html = $(data).filter("#cart-"+id).html();
				var quantity = $(html).filter('li.col3').html();
                var final = quantity.replace('x ','');
                $('#qty-'+id).val(final);
            });
			}
			return false;
		}
	}
    if (qty != '' && qty > 0)
    {   
        getJSON('/order/update-cart', {action: 'add', id: id, qty: qty}, function (data) 
        {
            if (data.e != undefined) {
                $('#qty-'+id).focus();
                alert(data.e);
                return false;
            }
            
            
            // Update totals
            $('#totalWeight').html(data.totalWeight+'kg');
            $('#totalPrice').html('$'+data.totalPrice);
            
            // No error request for new items list
            $.get('/order/get-items', function (data) {
                data = $(data+'<div style="clear:both;"></div>');
                $('.delete', data).click(function () {
                    var split = $(this).parent().parent().attr('id').split('-');
                    deleteFromCart(split[1]);
                    return false;
                });
                $('#liveList').html(data); 
                $('#liveList').scrollTop(2000);
            });
            
        });
    }
	
	
    if (event.keyCode == enterKey || event.keyCode == tabKey) {
        return setFocusOnNextQty(parentDiv, el);
    }

}

function deleteFromCart(id) 
{
    setCheckAuth();
    $('#cart-' + id).remove();
    $('#qty-' + id).val('');
    
    getJSON('/order/update-cart', {
        action: 'delete',
        id: id
    }, function (data) {
            // Update totals
            $('#totalWeight').html(data.totalWeight+'kg');
            $('#totalPrice').html('$'+data.totalPrice);
    });
}


/**
 * Return an index from a form element
*/
function getIndex(element)
{
    for (var i=0;i<document.forms[0].elements.length;i++)
        if (element == document.forms[0].elements[i])
            return i;
    return -1;
}

/**
 * Set focus on next qty field
*/
function setFocusOnNextQty(scrollDiv, input) 
{
    var qtys = $('input', scrollDiv);

    for (var i=0;i<qtys.length;i++) {
        if (input == qtys[i]) {
            i = i + 1;
            if (qtys[i] != null) {
                qtys[i].focus();
                break;
            }
            else {
                qtys[(i-2)].focus();
                qtys[(i-1)].focus();
            }
        }
    }
    return false;
}

/**
 * Submit order
*/
function submitOrder()
{
    setCheckAuth();
    var message = '';
    if (($('#shipping').val() == '')
        || ($('#shipping').val() == 'Other' && ($('#shippingOther').val().replace(/^\s+|\s+$/g, '') == '' || $('#shippingOther').hasClass('default'))))
    {
        message = "- Shipping Method\n";
    }

    // Check ordered by
    if ($('#orderedBy').val() == '') {
        message += "- Ordered by\n";
    }
    // Check order number
    if ($('#orderNo').val() == '' && $('#orderNo').hasClass('required')) {
        message += "- Order number\n";
    }


    if (message != '')
    {
        alert("Please make sure the following fields are not blank: \n" + message);
        return false;
    }

    // Check to make sure they have products added
    if ($('#liveList ul').length == 0)
    {
        alert('Please add some products to your cart before you continue.');
        return false;
    }

    return true;
}

function updateAddress(el) {
    getJSON('/order/update-address', {address: el.value}, function () { });
}