var timeoutCheck = null;

function setCheckAuth() {
    timeoutCheck = setTimeout(function () {
        $.getJSON('/check-auth');
        setCheckAuth();
    }, 30000);
}
setCheckAuth();

function resizeFlashContainer() {
    $('#body').css('height', '1024px');
}