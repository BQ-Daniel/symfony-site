// define custom book settings here
flippingBook.settings.bookWidth = 726;
flippingBook.settings.bookHeight = 484;
flippingBook.settings.pageBackgroundColor = 0xcbcbcb;
flippingBook.settings.backgroundColor = 0xFFFFFF;
flippingBook.settings.zoomUIColor = 0xcbcbcb;
flippingBook.settings.smoothPages = true;	
flippingBook.settings.useCustomCursors = false;
flippingBook.settings.dropShadowEnabled = true,
flippingBook.settings.allowPagesUnload = false;
flippingBook.settings.zoomImageWidth = 943;
flippingBook.settings.zoomImageHeight = 1335;
flippingBook.settings.downloadURL = "";
flippingBook.settings.flipSound = "sounds/02.mp3";
flippingBook.settings.flipCornerStyle = "first page only";

// default settings can be found in the flippingbook.js file
flippingBook.create();
