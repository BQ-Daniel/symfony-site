$(document).ready(function ()
{
    $('.default').focus(function() {
        $(this).val('');
        $(this).removeClass('default');
        $(this).unbind('focus');
    });

    $('#manufacturerLoginForm').submit(manufacturerLogin);

    // Store cookie value onchange if class name has storeCookie
    $('.storeCookie').each(function () {
       $(this).change(function () {
           createCookie(this.name, this.value, 1);
       });

       // If cookie exists, prepopulate the field
       cookieValue = readCookie(this.name);
       if (cookieValue) {
           this.value = cookieValue;
           $(this).removeClass('default');
       }

    });
});

function manufacturerLogin()
{
    if (this.username.value == '' || this.username.value == 'Enter username') {
        return false;
    }

    $.getJSON(this.action, {username: this.username.value}, function (data) {
        $('#loginResult').attr('class', '');
        if (data.s != undefined) {
            $('#loginResult').addClass('alert alert-success');
            $('#loginResult').text(data.s);
        }
        else {
            $('#loginResult').addClass('alert alert-danger');
            $('#loginResult').text(data.e);
        }
        $('#loginResult').fadeIn('fast');
    });
    return false;
}

function createCookie(name,value,days) {
    var date = new Date();
    if (days) {
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else {
        var expires = date.toGMTString();
    }
    value = value.replace(/[\r\n\|\n]/g, '~!');
    document.cookie = name+"="+value+";"+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length).replace('~!', "\n");
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

function getJSON(href, params, func)
{
    $.getJSON(href, params, function (data)
    {
        if (data == undefined) {
            return false;
        }
        // Check for redirect
        if (data.timeout != undefined)
        {
            if (data.message != undefined) {
                alert(data.message);
            }
            location.href = '/session-timeout';
            return false;
        }

        // global error
        if (data.gError != undefined) {
            alert(data.gError);
            return false;
        }

        // If all good, run function
        func(data);
    });
}
