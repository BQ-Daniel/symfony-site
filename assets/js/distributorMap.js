var map;
var geocoder;
var locationCount = 0;


// Create a base icon for all of our markers that specifies the
// shadow, icon dimensions, etc.
var baseIcon = new GIcon(G_DEFAULT_ICON);
baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
baseIcon.iconSize = new GSize(25, 25);
baseIcon.shadowSize = new GSize(27, 24);
baseIcon.infoWindowAnchor = new GPoint(9, 2);

var bounds = null;


function urlencode(str) {
    return escape(str).replace(/\+/g,'%2B').replace(/%20/g, '+').replace(/\*/g, '%2A').replace(/\//g, '%2F').replace(/@/g, '%40');
}

function loadMap() {
    if (GBrowserIsCompatible()) {
        geocoder = new GClientGeocoder();
        geocoder.setBaseCountryCode('au');
        map = new GMap2(document.getElementById('map'));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(133.7751360, -25.2743980), 0);
    }
}

function searchLocations() {
    locationCount = 0;
    $('#totalResults').text('0');
    var address = document.getElementById('addressInput').value;
    $('#search-load').show();
    geocoder.getLatLng(address, function(latlng) {
        if (!latlng) {
            alert('Location: ' + address + ' not found');
            $('#search-load').hide();
        } else {
            
            searchLocationsNear(latlng);
            setTimeout(function() { 

            }, 500);
        }
    });
}

var returnValue = true;
function searchLocationsNear(center) {
    var radius = document.getElementById('radiusSelect').value;
    var fromRadius = document.getElementById('fromRadius').value;
    var toRadius = document.getElementById('toRadius').value;
    
    var searchUrl = '/geo-search-xml?address='+document.getElementById('addressInput').value+'&lat=' + center.lat() + '&lng=' + center.lng();
    if ($.trim(fromRadius) != '' || $.trim(toRadius)) {
        if ($.trim(fromRadius) != '') {
            searchUrl += '&fromRadius=' + fromRadius;
        }
        if ($.trim(toRadius) != '') {
            searchUrl += '&toRadius=' + toRadius;
            radius = toRadius;
        }
    }
    else {
        searchUrl += '&radius=' + radius;
    }

    GDownloadUrl(searchUrl, function(data) {
        var xml = GXml.parse(data);
        var markers = xml.documentElement.getElementsByTagName('marker');
        map.clearOverlays();

        var sidebar = document.getElementById('sidebar');
        sidebar.innerHTML = '';
        if (markers.length == 0) {
            alert('No distributors found near your location.\n\nPlease try a larger search radius.');
            $('#totalResults').text('0');
            document.getElementById('fromRadius').value = '';
            document.getElementById('toRadius').value = '';
            return false;
        }

        $('#totalResults').text(markers.length);
        $('#locationValue').text($('#addressInput').val());
        
        
        // Record search 
        recordSearch($('#addressInput').val(), radius, markers.length);

        bounds = new GLatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            var id = markers[i].getAttribute('id');
            var name = markers[i].getAttribute('name');
            var address = [];
            address['full'] = markers[i].getAttribute('address');
            address['address1'] = markers[i].getAttribute('address1');
            address['address2'] = markers[i].getAttribute('address2');
            address['suburb'] = markers[i].getAttribute('suburb');
            address['state'] = markers[i].getAttribute('state');
            address['postcode'] = markers[i].getAttribute('postcode');
            var phone = markers[i].getAttribute('phone');

            var distance = parseFloat(markers[i].getAttribute('distance'));
            var point = new GLatLng(parseFloat(markers[i].getAttribute('lat')),
                parseFloat(markers[i].getAttribute('lng')));

            var marker = createMarker(point, id, name, address, phone, i);
            map.addOverlay(marker);
            var sidebarEntry = createSidebarEntry(marker, id, name, address, phone, distance);
            sidebar.appendChild(sidebarEntry);
            bounds.extend(point);
        }

        if ($('#totalResults').text() > 0) {
            $('#search').fadeOut('slow', function() {
                $('#results').css('position', 'static');
                $('#results').css('marginLeft', '0')
            });
            $('#map').append('<div class="cb"></div>');


        }
                var point = new GLatLng(parseFloat(center.lat()),
                    parseFloat(center.lng()));
                    var blueIcon = baseIcon;
                    blueIcon.image = "http://www.brakequip.com.au/assets/images/home.png";
                    blueIcon.iconSize = new GSize(32, 32);
                    markerOptions = { icon:blueIcon };
                var marker = new GMarker(point, markerOptions);
                
                map.addOverlay(marker);
                    bounds.extend(point);
                    map.setCenter(bounds.getCenter(), map.getBoundsZoomLevel(bounds));
        $('#search-load').hide();
        
                
    });
    
    return returnValue;
}

function createMarker(point, id, name, address, phone, index) {
    
    index++;
    
    var numIcon = new GIcon(baseIcon);
    numIcon.image = "/mapIcon?n=" + index;

    // Set up our GMarkerOptions object
    markerOptions = {
        icon:numIcon
    };
  
    var marker = new GMarker(point, markerOptions);
    var html = '<div style="width: 200px"><strong>' + name + '</strong><br /><div class="address">' + address['address1'] + '<br />';
    if (address['address2'] != '') {
        html += address['address2']+ '<br />';
    }
    html +=  address['suburb'] + ' ' + address['state'] + ' ' + address['postcode'];
    html += '<br />';
    if (phone != '')
    {
        html += '<strong>Phone:</strong> ' + phone + '<br />';
    }
    html += '<br /><a href="http://maps.google.com.au/maps?saddr= ' + urlencode(document.getElementById('addressInput').value) + '&daddr=' + urlencode(address['full']) + '" target="_blank">Get Directions</a>';
    GEvent.addListener(marker, 'click', function() {
        recordDistributorClick(id);
        marker.openInfoWindowHtml(html);
		map.setCenter(bounds.getCenter(), 12);
    });
    return marker;
}

function createSidebarEntry(marker, id, name, address, phone, distance) {
    locationCount++;
    var div = document.createElement('div');
    $(div).addClass('location');

    var icon = document.createElement('div');
    $(icon).addClass('icon');
    $(icon).text(locationCount);
    div.appendChild(icon);

    var info = document.createElement('div');
    $(info).addClass('info');

    var html = '<strong>' + name + '</strong><br /><span class="km">' + distance.toFixed(1) + 'km</span><div class="address">' + address['address1'] + '<br />';
    if (address['address2'] != '') {
        html += address['address2']+ '<br />';
    }
    html +=  address['suburb'] + ' ' + address['state'] + ' ' + address['postcode'];
    if (phone != '') {
        html += '<br /><strong>Phone:</strong> ' + phone;
    }
    info.innerHTML = html;
    div.appendChild(info);

    GEvent.addDomListener(div, 'click', function() {
        GEvent.trigger(marker, 'click');
    });

    return div;
}

function showSearchPanel() {
    $('#results').css('marginLeft', '-10000px').css('position', 'absolute');
    $('#search').fadeIn('slow');
    map.clearOverlays();
    $('#sidebar').html('');
}

$(document).ready(function() {
    loadMap();
    $('a[href=search-again]').each( function() {
        $(this).click(function() {showSearchPanel();return false;});
    });
    $('#distributor-search').submit(function () {searchLocations();return false;});
});

function recordSearch(location, radius, total) {
    $.post("distributorRecord", {type: 'search', location: location, radius: radius, total: total} );
}

function recordDistributorClick(id) {
    $.post("distributorRecord", {type: 'click', id: id} );
}